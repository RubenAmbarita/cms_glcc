<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class PermintaanPelayanan_Model extends Model
{

  public function update_status($data)
  {
      $save = DB::table('tb_permintaan_pelayanan')
          ->where('id_permintaan_pelayanan', $data['id_permintaan_pelayanan'])
          ->update([
            'id_realisasi' => '1',
            'updated_at' => date('Y-m-d H:i:s')
          ]);

      if($save){
        return true;
      }else {
        // code...
        return false;
      }
  }

  public function get_pelayanan_tunggu()
  {
      $sql = "SELECT t1.id_permintaan_pelayanan,
                    t1.id_gereja,
                     t1.jenis_pelayanan,
                     t1.nama_jemaat,
                     t1.nama_pelayan,
                     t1.tanggal_permintaan,
                     t1.tanggal_realisasi,
                     t1.id_provinsi,
                     t1.id_kota,
                     t1.id_kecamatan,
                     t1.id_kelurahan
                FROM tb_permintaan_pelayanan t1 WHERE t1.id_realisasi = '2'
            ORDER BY t1.id_permintaan_pelayanan DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function get_pelayanan_terlaksana()
  {
      $sql = "SELECT t1.id_permintaan_pelayanan,
                     t1.jenis_pelayanan,
                     t1.id_gereja,
                     t1.nama_jemaat,
                     t1.nama_pelayan,
                     t1.tanggal_permintaan,
                     t1.tanggal_realisasi,
                     t1.id_provinsi,
                     t1.id_kota,
                     t1.id_kecamatan,
                     t1.id_kelurahan
                FROM tb_permintaan_pelayanan t1 WHERE t1.id_realisasi = '1'
            ORDER BY t1.id_permintaan_pelayanan DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function get_detail($id_permintaan_pelayanan,$id_gereja)
  {
      $sql = "SELECT t1.id_permintaan_pelayanan,
                    t1.id_gereja,
                    t1.id_provinsi,
                    t1.id_kota,
                    t1.id_kecamatan,
                    t1.id_kelurahan,
                    (SELECT t2.nama_gereja from tb_gereja t2 WHERE t2.id_gereja = t1.id_gereja AND t1.id_permintaan_pelayanan = '".$id_permintaan_pelayanan."') AS nama_gereja,
                    (SELECT t3.pro_name from municipality_provinces t3 WHERE t3.pro_id = t1.id_provinsi AND t1.id_permintaan_pelayanan = '".$id_permintaan_pelayanan."') AS nama_provinsi,
                    (SELECT t4.reg_name from municipality_regencies t4 WHERE t4.reg_id = t1.id_kota AND t1.id_permintaan_pelayanan = '".$id_permintaan_pelayanan."') AS nama_kota,
                    (SELECT t5.dis_name from municipality_districts t5 WHERE t5.dis_id = t1.id_kecamatan AND t1.id_permintaan_pelayanan = '".$id_permintaan_pelayanan."') AS nama_kecamatan,
                    (SELECT t6.sub_name from municipality_subdistricts t6 WHERE t6.sub_id = t1.id_kelurahan AND t1.id_permintaan_pelayanan = '".$id_permintaan_pelayanan."') AS nama_kelurahan,
                    t1.jenis_pelayanan,
                    t1.nama_jemaat,
                    t1.no_handphone,
                    t1.alamat,
                    t1.nama_pelayan,
                    t1.tanggal_realisasi,
                    t1.tanggal_permintaan,
                    t1.kode_pos
                FROM tb_permintaan_pelayanan t1
               WHERE t1.id_permintaan_pelayanan = '". $id_permintaan_pelayanan ."'";

      $data = DB::select($sql);
      return $data[0];
  }

  public function delete_pelayanan($id_permintaan_pelayanan)
  {
      $delete = DB::table('tb_permintaan_pelayanan')->where('id_permintaan_pelayanan', '=', $id_permintaan_pelayanan)->delete();

      return $delete;
  }

  public function save_pelayanan($data)
  {

    $save = DB::table('tb_permintaan_pelayanan')
        ->where('id_permintaan_pelayanan', $data['id_permintaan_pelayanan'])
        ->update([
          'id_gereja' => $data['id_gereja'],
          'jenis_pelayanan' => $data['jenis_pelayanan'],
          'nama_jemaat' => $data['nama_jemaat'],
          'nama_pelayan' => $data['nama_pelayan'],
          'no_handphone' => $data['no_handphone'],
          'alamat' => $data['alamat'],
          'id_provinsi' => $data['id_provinsi'],
          'id_kota' => $data['id_kota'],
          'id_kecamatan' => $data['id_kecamatan'],
          'id_kelurahan' => $data['id_kelurahan'],
          'kode_pos' => $data['kode_pos'],
          'tanggal_permintaan' => $data['tanggal1'],
          'tanggal_realisasi' => $data['tanggal2'],
          'updated_at' => date('Y-m-d H:i:s')
        ]);

        if($save){
          return true;
        }else {
          // code...
          return false;
        }

  }

}
