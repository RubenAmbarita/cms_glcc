<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class MasterPelayanan_Model extends Model
{
  public function insert($data)
  {
    if(empty($data['id_master_pelayanan']))
    {
      $save = DB::table('tb_master_pelayanan')->insert([
        'id_gereja' => $data['id_gereja'],
        'nama_pelayanan' => $data['nama_pelayanan'],
        'divisi_penanggung_jawab' => $data['divisi_penanggung_jawab'],
        'nama_gambar' => $data['nama_gambar'],
        'nama_dokumen' => $data['nama_dokumen'],
        'deskripsi' => $data['deskripsi'],
        'created_at' => date('Y-m-d H:i:s')
      ]);
    }else{
      $save = DB::table('tb_master_pelayanan')
          ->where('id_master_pelayanan', $data['id_master_pelayanan'])
          ->update([
            'id_gereja' => $data['id_gereja'],
            'nama_pelayanan' => $data['nama_pelayanan'],
            'divisi_penanggung_jawab' => $data['divisi_penanggung_jawab'],
            'nama_gambar' => $data['nama_gambar'],
            'nama_dokumen' => $data['nama_dokumen'],
            'deskripsi' => $data['deskripsi'],
            'updated_at' => date('Y-m-d H:i:s')
          ]);
    }

      if($save){
        return true;
      }else {
        // code...
        return false;
      }
  }

  public function get_gereja(){
    $sql = "SELECT t1.id_gereja, t1.nama_gereja from tb_gereja t1";
    $data = DB::select($sql);
    return $data;
  }

  public function get_pelayanan()
  {
      $sql = "SELECT t2.nama_gereja,
                     t1.nama_pelayanan,
                     t1.id_master_pelayanan,
                     t1.divisi_penanggung_jawab
                FROM tb_master_pelayanan t1 INNER JOIN tb_gereja t2 ON t1.id_gereja = t2.id_gereja
            ORDER BY t1.id_master_pelayanan DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function get_detail($id_master_pelayanan)
  {
      $sql = "SELECT *
                FROM tb_master_pelayanan t1
               WHERE t1.id_master_pelayanan = '". $id_master_pelayanan ."'";

      $data = DB::select($sql);
      return $data[0];
  }

  public function delete_pelayanan($id_master_pelayanan)
  {
      $delete = DB::table('tb_master_pelayanan')->where('id_master_pelayanan', '=', $id_master_pelayanan)->delete();

      return $delete;
  }

}
