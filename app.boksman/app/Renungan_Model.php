<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class Renungan_Model extends Model
{
    public function insert($data)
    {
      if(empty($data['id_renungan']))
      {
        $save = DB::table('tb_renungan')->insert([
          'tema_renungan' => $data['tema_renungan'],
          'judul_renungan' => $data['judul_renungan'],
          'bacaan_alkitab1' => $data['alkitab_1'],
          'bacaan_alkitab2' => $data['alkitab_2'],
          'tanggal_renungan' => $data['tanggal_renungan'],
          'penulis_renungan' => $data['penulis_renungan'],
          'editor_renungan' => $data['editor'],
          'gambar_name' => $data['gambar'],
          'renungan' => $data['renungan'],
          'created_at' => date('Y-m-d H:i:s')
        ]);
      }else{
        $save = DB::table('tb_renungan')
            ->where('id_renungan', $data['id_renungan'])
            ->update([
              'tema_renungan' => $data['tema_renungan'],
              'judul_renungan' => $data['judul_renungan'],
              'bacaan_alkitab1' => $data['alkitab_1'],
              'bacaan_alkitab2' => $data['alkitab_2'],
              'tanggal_renungan' => $data['tanggal_renungan'],
              'penulis_renungan' => $data['penulis_renungan'],
              'editor_renungan' => $data['editor'],
              'gambar_name' => $data['gambar'],
              'renungan' => $data['renungan']
            ]);
      }

        if($save){
          return true;
        }else {
          // code...
          return false;
        }
    }

    public function get_renungan()
    {
        $sql = "SELECT t1.id_renungan,
                       t1.tema_renungan,
                       t1.judul_renungan,
                       t1.bacaan_alkitab1,
                       t1.bacaan_alkitab2,
                       t1.tanggal_renungan,
                       t1.penulis_renungan,
                       t1.editor_renungan,
                       t1.gambar_name,
                       t1.renungan
                  FROM tb_renungan t1
              ORDER BY t1.id_renungan DESC";

        $data = DB::select($sql);
        return $data;
    }

    public function get_detail($id_renungan)
    {
        $sql = "SELECT *
                  FROM tb_renungan t1
                 WHERE t1.id_renungan = '". $id_renungan ."'";

        $data = DB::select($sql);
        return $data[0];
    }

    public function delete_renungan($id_renungan)
    {
        $delete = DB::table('tb_renungan')->where('id_renungan', '=', $id_renungan)->delete();

        return $delete;
    }

}
