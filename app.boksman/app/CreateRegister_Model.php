<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Authenticatable
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function insert($data)
    {
        $save_cust = DB::table('tb_customer')->insert([
            'company' => $data['usr_companyname'],
            'address' => $data['usr_companyaddress'],
            'city' => $data['usr_companycity'],
            'state' => $data['usr_companystate'],
            'country' => $data['usr_companycountry'],
            'zip_code' => $data['usr_companyzipcode'],
            'contact' => $data['usr_companycontactname'],
            'username' => 'administrator',   //$data['usr_username'],
            'password' => $data['usr_password'],
            'phone' => $data['usr_companymobilephone'],
            'telp' => $data['usr_companyphoneoffice'],
            'email' => $data['usr_email'],
            'entry_date' => date('Y-m-d H:i:s'),
            'status_customer' => 1
        ]);
        
        if($save_cust){
            $id_customer = $this->get_customerid($data['usr_companyname']);
            $save = DB::table('users')->insert([
                'name' => $data['usr_companycontactname'],
                'username' => $data['usr_username'],
                'password' => bcrypt($data['usr_password']),
                //'remember_token' => 'onRA2634AG3qMNzbOvggLsOoivIZNKx9gmNXDy9soTujJaiYJJAHff4ljwNP',
                'email' => $data['usr_email'],
                'branch' => 'CGK',
                'dbranch' => 'CGK00',
                'id_customer' => $id_customer->id_customer,
                'privileges' => 'usrad',
                'activestatus' => 'atsac',
                'created_by' => 'administrator',
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $save_file = DB::table('tb_customerfile')->insert([
                'id_customer' => $id_customer->id_customer,
                'company' => $data['usr_companyname'],
                'title' => 'SIUP',
                'file_name' => $data['file_siup'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 'administrator'
            ]);
            
        }else{
            $save = 'error';
        }

        if($save){
          return true;
        }
        return false;
    }

    public function get_customerid($p_param)
    {
    	$data = DB::table('tb_customer')
            ->select('id_customer')
            ->where([
                ['company', '=', $p_param]
            ])
            ->first();

        return $data;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
