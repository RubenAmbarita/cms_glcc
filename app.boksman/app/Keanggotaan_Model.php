<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use DB;

class Keanggotaan_Model extends Model
{
	protected $fillable = [
        'name', 'email'
    ];

    public function get_anggota($p_param)
    {
    	$data = DB::table('users')
            ->select('*')
            ->where([
                ['user_type', '=', '2'],
                ['deleted_at', '=', NULL]
            ])
            ->orderby('created_at', 'desc')
            ->get();

        return $data;
    }

    public function insert($data)
    {
      if(empty($data['id_user']))
      {
        $save = DB::table('users')->insert([
          'name' => $data['name'],
          'jenis_kelamin' => $data['jenis_kelamin'],
          'tempat_lahir' => $data['tempat_lahir'],
          'tanggal_lahir' => $data['tanggal_lahir'],
          'adress' => $data['adress'],
          'provinsi' => $data['provinsi'],
          'kokab' => $data['kokab'],
          'kecamatan' => $data['kecamatan'],
          'kelurahan' => $data['kelurahan'],
          'kode_pos' => $data['kode_pos'],
          'pekerjaan' => $data['pekerjaan'],
          'no_telp' => $data['no_telp'],
          'email' => $data['email'],
          'gol_darah' => $data['gol_darah'],
          'status_nikah' => $data['status_nikah'],
          'jumlah_anak' => $data['jumlah_anak'],
          'lokasi_gereja' => $data['lokasi_gereja'],
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => 'administrator'
        ]);
      }else{
        $save = DB::table('users')
            ->where('id', $data['id_user'])
            ->update([
              'jenis_kelamin' => $data['jenis_kelamin'],
              'tempat_lahir' => $data['tempat_lahir'],
              'tanggal_lahir' => $data['tanggal_lahir'],
              'adress' => $data['adress'],
              'provinsi' => $data['provinsi'],
              'kokab' => $data['kokab'],
              'kecamatan' => $data['kecamatan'],
              'kelurahan' => $data['kelurahan'],
              'kode_pos' => $data['kode_pos'],
              'pekerjaan' => $data['pekerjaan'],
              'gol_darah' => $data['gol_darah'],
              'status_nikah' => $data['status_nikah'],
              'jumlah_anak' => $data['jumlah_anak'],
              'lokasi_gereja' => $data['lokasi_gereja'],
              'updated_at' => date('Y-m-d H:i:s'),
              'updated_by' => 'administrator'
            ]);
      }

        if($save){
          return true;
        }else {
          // code...
          return false;
        }
    }

    public function delete_anggota($data)
    {
        $delete = DB::table('users')
        ->where('id', $data['id'])
        ->update([
            'deleted_by' => 'administrator',
            'deleted_at' => date('Y-m-d H:i:s'),
            ]);
  
            if($delete){
                return true;
              }else {
                // code...
                return false;
              }
    }

    public function update_status($data)
    {
        $save = DB::table('users')
            ->where('id', $data['id'])
            ->update([
              'status' => '1',
              'updated_at' => date('Y-m-d H:i:s'),
              'updated_by' => 'administrator',
            ]);
  
        if($save){
          return true;
        }else {
          // code...
          return false;
        }
    }

    public function get_detail($id)
    {
        $sql = "SELECT *
                  FROM users t1
                 WHERE t1.id = '". $id ."'";
  
        $data = DB::select($sql);
        return $data[0];
    }

    public function get_gereja(){
      $sql = "SELECT t1.id_gereja, t1.nama_gereja from tb_gereja t1";
      $data = DB::select($sql);
      return $data;
    }
}
