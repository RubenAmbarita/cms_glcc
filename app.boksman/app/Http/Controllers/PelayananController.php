<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\PermintaanPelayanan_Model;
use App\Dashboard_Model;
use DB;

class PelayananController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new PermintaanPelayanan_Model();
      $list_tunggu = $data->get_pelayanan_tunggu();
      $list_terlaksana = $data->get_pelayanan_terlaksana();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Pelayanan.Pelayanan', [
                        'custname'  => $custname,
                        'list_tunggu' => $list_tunggu,
                        'list_terlaksana' => $list_terlaksana,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'pelayanan'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new PermintaanPelayanan_Model();
      $detail = $data->get_detail($request->id_permintaan_pelayanan,$request->id_gereja);

      return view('Pelayanan.PelayananDetail', [
                      'custname'  => $custname,
                      'detail' => $detail,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'pelayanan'
                  ]);
    }

    public function update_status(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new PermintaanPelayanan_Model();
      $return = $data->update_status($request);
      return response()->json($return);
    }

    public function delete(Request $request)
    {
        $del = new PermintaanPelayanan_Model();
        $return = $del->delete_pelayanan($request->id_permintaan_pelayanan);

        return response()->json($return);
    }

    public function save_pelayanan(Request $request){
      $save = new PermintaanPelayanan_Model();
      $tanggal_permintaan = date_create($request->tanggal_permintaan);
      $request['tanggal1'] = date_format($tanggal_permintaan, 'Y-m-d H:i:s');
      $tanggal_realisasi = date_create($request->tanggal_realisasi);
      $request['tanggal2'] = date_format($tanggal_realisasi, 'Y-m-d H:i:s');
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->save_pelayanan($request);
      return response()->json($return);
    }
}
