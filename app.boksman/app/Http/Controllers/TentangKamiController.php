<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\TentangKami_Model;
use App\Dashboard_Model;
use DB;

class TentangKamiController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new TentangKami_Model();
      $tentangKami = $data->get_tentangKami();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('TentangKami.index', [
                        'custname'  => $custname,
                        'tentangKami' => $tentangKami,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'tentangKamiList'
                    ]);
    }

    public function detail()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Pesan.DetailPesan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'pesan'
                  ]);
    }


    public function addmessage()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Pesan.AddPesan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'pesan'
                  ]);
    }
}
