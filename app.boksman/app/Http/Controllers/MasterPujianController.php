<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Masterdata_Model;
use App\Dashboard_Model;
use DB;

class MasterPujianController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Masterdata_Model();
      $list_pujian = $data->get_pujian();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Masterdata.Pujian', [
                        'custname'  => $custname,
                        'list_pujian' => $list_pujian,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'ListPujian'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Masterdata_Model();
      $detail = $data->get_detail($request->id_kidung_pujian);
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Masterdata.DetailPujian', [
                      'custname'  => $custname,
                      'detail' => $detail,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'ListPujian'
                  ]);
    }

    public function add()
    {
      // code...
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Masterdata.AddPujian', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'ListPujian'
                  ]);
    }

    public function save(Request $request)
    {
      $save = new Masterdata_Model();
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

    public function delete_pujian(Request $request)
    {
        $del = new Masterdata_Model();
        $return = $del->delete_pujian($request->id_pujian);

        return response()->json($return);
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function upload(){
      if(isset($_FILES['file']['name'])) {

          $file = $_FILES['file']['tmp_name'];
          $sourceProperties = getimagesize($file);
          $fileNewName = time();
          $folderPath = env('IMAGE_DIRECTORY').'pujian/';
          $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
          $imageType = $sourceProperties[2];

          switch ($imageType) {
              case IMAGETYPE_PNG:
                  $imageResourceId = imagecreatefrompng($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagepng($targetLayer,$folderPath. "pujian_". $fileNewName. "_thump.". $ext);
                  $return = "pujian_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_GIF:
                  $imageResourceId = imagecreatefromgif($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagegif($targetLayer,$folderPath. "pujian_". $fileNewName. "_thump.". $ext);
                  $return = "pujian_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_JPEG:
                  $imageResourceId = imagecreatefromjpeg($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagejpeg($targetLayer,$folderPath. "pujian_". $fileNewName. "_thump.". $ext);
                  $return = "pujian_". $fileNewName. "_thump.". $ext;
                  break;

              default:
                  //echo "Invalid Image type.";
                  $return = false;
                  exit;
                  break;
          }

          //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
          //echo "Image Resize Successfully.";
          //return response()->json(['response' => $return]);
          return response()->json($return);
      }
    }
}
