<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
//use Illuminate\Http\RedirectResponse;
//use App\Order_Model;
use DB;

class RegistersController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
      $province = $this->get_province();
      //print_r($province);die;

      return view('CreateRegister', [
          'province'  => $province
      ]);
    }

    public function get_province()
    {
        $data = DB::table('municipality_provinces')
            ->select(DB::raw('pro_id, pro_name'))
            ->get();

        return $data;
    }

    public function get_regency(Request $request)
    {
        $data = DB::table('municipality_regencies')
            ->select(DB::raw('reg_id,reg_name'))
            ->where([
                ['reg_province_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

    function UploadFileNPWP(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'npwp/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "NPWP_". $fileNewName. "_thump.". $ext);
                    $return = "NPWP_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "NPWP_". $fileNewName. "_thump.". $ext);
                    $return = "NPWP_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "NPWP_". $fileNewName. "_thump.". $ext);
                    $return = "NPWP_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    function UploadFileSIUP(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'siup/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:

                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "SIUP_". $fileNewName. "_thump.". $ext);
                    $return = "SIUP_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "SIUP_". $fileNewName. "_thump.". $ext);
                    $return = "SIUP_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "SIUP_". $fileNewName. "_thump.". $ext);
                    $return = "SIUP_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    $return = false;
                    exit;
                    break;
            }
            return response()->json($return);
        }
    }

    function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function check_usertrucker(Request $request)
    {
        $sql = "SELECT IFNULL(COUNT(*), 0) +
                       IFNULL((SELECT COUNT(*) FROM users WHERE username = '".$request->p_param."' AND user_type='usrtrc'), 0)
                        AS v_count
                  FROM tb_trucker
                 WHERE username = '".$request->p_param."'";

        $data = DB::select($sql);
        return response()->json($data[0]->v_count);
    }

    public function get_truckerid($p_param)
    {
        $data = DB::table('tb_trucker')
            ->select('id_trucker')
            ->where([
                ['company_name', '=', $p_param]
            ])
            ->first();

        return $data;
    }

    public function save(Request $request)
    {
        $return = $this->insert($request);

        return response()->json($return);
    }

    public function get_npwpsiup(Request $request)
    {
        $sql = "SELECT IFNULL(COUNT(*), 0) AS v_count
                  FROM tb_trucker
                 WHERE (siup = '".$request->siup."' OR npwp = '".$request->npwp."')";

        $data = DB::select($sql);
        return response()->json($data[0]->v_count);;
    }

    public function insert($data)
    {
         $save_cust_id = DB::table('tb_trucker')->insertGetId([
            'company_name' => $data['company_name'],
            'address_1' => $data['address_1'],
            'address_2' => $data['address_2'],
            'city' => $data['city'],
            'type' => $data['type'],
            'state' => $data['state'],
            'country' => $data['country'],
            'zip_code' => $data['zip_code'],
            'phone_1' => $data['phone_1'],
            'phone_2' => $data['phone_2'],
            'name_1' => $data['name_1'],
            'name_2' => $data['name_2'],
            'telp_1' => $data['telp_1'],
            'telp_2' => $data['telp_2'],
            'email_1' => $data['email_1'],
            'email_2' => $data['email_2'],
            'email_kantor' => $data['email_kantor'],
            'telp_kantor' => $data['telp_kantor'],
            'siup' => $data['siup'],
            'npwp' => $data['npwp'],
            'image_siup' => $data['file_siup'],
            'image_npwp' => $data['file_npwp'],
            'entry_date' => $data['entry_date'],
            'username' => $data['username'],
            'password' => $data['password']
         ]);

         if($save_cust_id){
             // $id_trucker = $this->get_truckerid($data['company_name']);
             $save = DB::table('users')->insert([
                 'name' => $data['name_1'],
                 'username' => $data['username'],
                 'password' => bcrypt($data['password']),
                 //'remember_token' => 'onRA2634AG3qMNzbOvggLsOoivIZNKx9gmNXDy9soTujJaiYJJAHff4ljwNP',
                 'email' => $data['email_kantor'],
                 'branch' => 'CGK',
                 'dbranch' => 'CGK00',
                 'id_customer' => $save_cust_id,
                 'privileges' => 'usrad',
		             'user_type' => 'ustruc',
                 'user_type' => 'usrtrc',
                 'activestatus' => 'atsac',
                 'created_by' => 'administrator',
                 'created_at' => date('Y-m-d H:i:s')
             ]);

         }else{
             $save = 'error';
         }

         if($save){
           return true;
         }
         return false;
    }


     public function get_lat_long($address, $region)
     {
         $address = str_replace(" ", "+", $address);
         $region = str_replace(" ", "+", $region);
         $result = 0;

         $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&region=".urlencode($region));
         $data = json_decode($json, TRUE);
         //$latlong = $data['results'][0]['geometry']['location']['lat'] .','.$data['results'][0]['geometry']['location']['lng'];
         if ($data['status'] == 'OK') {
            $latlong = $data['results'][0]['geometry']['location']['lat'] .','.$data['results'][0]['geometry']['location']['lng'];
            $result = $latlong;
         }

         return $result;
     }

     public function get_lat_longajax(Request $request)
     {
         $address = str_replace(" ", "+", $request->usr_companyaddress);
         $region = str_replace(" ", "+", $request->usr_companycity);
         $result = 0;

         $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&region=".urlencode($region));
         $data = json_decode($json, TRUE);

         if ($data['status'] == 'OK') {
            $latlong = $data['results'][0]['geometry']['location']['lat'] .','.$data['results'][0]['geometry']['location']['lng'];
            $result = $latlong;
         }

         return $result;
     }

}
