<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\MasterPelayanan_Model;
use App\Dashboard_Model;
use DB;

class MasterPelayananController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new MasterPelayanan_Model();
      $list = $data->get_pelayanan();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Masterdata.Pelayanan', [
                        'custname'  => $custname,
                        'list' => $list,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'ListPelayanan'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new MasterPelayanan_Model();
      $detail = $data->get_detail($request->id_master_pelayanan);
      $gereja = $data->get_gereja();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Masterdata.DetailPelayanan', [
                      'custname'  => $custname,
                      'detail' => $detail,
                      'gereja' => $gereja,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'ListPelayanan'
                  ]);
    }

    public function add()
    {
      // code...
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new MasterPelayanan_Model();
      $lokasi = $data->get_gereja();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Masterdata.AddPelayanan', [
                      'custname'  => $custname,
                      'lokasi' => $lokasi,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'ListPelayanan'
                  ]);
    }

    public function save(Request $request)
    {
      $save = new MasterPelayanan_Model();
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

    public function delete_pelayanan(Request $request)
    {
        $del = new MasterPelayanan_Model();
        $return = $del->delete_pelayanan($request->id_master_pelayanan);

        return response()->json($return);
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function upload(){
      if(isset($_FILES['file']['name'])) {

          $file = $_FILES['file']['tmp_name'];
          $sourceProperties = getimagesize($file);
          $fileNewName = time();
          $folderPath = env('IMAGE_DIRECTORY').'pelayanan/';
          $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
          $imageType = $sourceProperties[2];

          switch ($imageType) {
              case IMAGETYPE_PNG:
                  $imageResourceId = imagecreatefrompng($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagepng($targetLayer,$folderPath. "pelayanan_". $fileNewName. "_thump.". $ext);
                  $return = "pelayanan_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_GIF:
                  $imageResourceId = imagecreatefromgif($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagegif($targetLayer,$folderPath. "pelayanan_". $fileNewName. "_thump.". $ext);
                  $return = "pelayanan_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_JPEG:
                  $imageResourceId = imagecreatefromjpeg($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagejpeg($targetLayer,$folderPath. "pelayanan_". $fileNewName. "_thump.". $ext);
                  $return = "pelayanan_". $fileNewName. "_thump.". $ext;
                  break;

              default:
                  //echo "Invalid Image type.";
                  $return = false;
                  exit;
                  break;
          }

          //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
          //echo "Image Resize Successfully.";
          //return response()->json(['response' => $return]);
          return response()->json($return);
      }
    }
}
