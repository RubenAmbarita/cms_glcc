<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\LokasiGereja_Model;
use App\Dashboard_Model;
use DB;

class LokasiGerejaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $getModel = new LokasiGereja_Model();
        $id_user = session()->get('id_user');
        $lokasi = $getModel->get_lokasigereja($id_user);
        

        return view('LokasiGereja.index', [
                                                'lokasi' => $lokasi,
                                                'menu_active' => 'LokasiList'
                                             ]);
    }

    public function create()
    {
        $custid = session()->get('id_user');
        $data = new LokasiGereja_Model();
        $provinsi = $this->get_state();

        $data = new Dashboard_Model();

         return view('LokasiGereja.create', [
                        'menu_active' => 'UserCreate',
                        'provinsi'  => $provinsi
                    ]);
    }

    public function save(Request $request)
    {
        $save = new LokasiGereja_Model();
        $request['username'] = session()->get('username');
        $request['custid'] = session()->get('custid');
        $request['branch'] = session()->get('branch');
        $request['dbranch'] = session()->get('dbranch');

        $return = $save->insert($request);
        
        return response()->json(['response' => $return]); 
    }

    public function edit(Request $request)
    {

        $custid = session()->get('custid');
        $row = new LokasiGereja_Model();
        $provinsi = $this->get_state();
        $data = $row->get_useredit($request->custid);

        return view('LokasiGereja.edit', [
                    'data' => $data,
                    'menu_active' => 'UserEdit',
                    'provinsi'  => $provinsi
                  ]);
    }

    public function updates(Request $request)
    {
        $save = new LokasiGereja_Model();
        $request['username'] = session()->get('username');
        $request['custid'] = session()->get('custid');
        $request['branch'] = session()->get('branch');
        $request['dbranch'] = session()->get('dbranch');

        $return = $save->updates($request);
        
        return response()->json(['response' => $return]); 
    }

    public function deleteds(Request $request)
    {
        $delete = new LokasiGereja_Model();
        $data = $delete->deleteds($request->username, session()->get('username'));
        
        return response()->json(['response' => $data]); 
    }

    public function destroy()
    {
        return view('LokasiGereja.destroy', compact('LokasiGereja'));
    }

    public function get_state()
    {
        $data = DB::table('municipality_provinces')
            ->select(DB::raw('pro_id, pro_name'))
            ->get();

        return $data;
    }

    public function get_city(Request $request)
    {
        $data = DB::table('municipality_regencies')
            ->select(DB::raw('reg_id, reg_name'))
            ->where([
                ['reg_province_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

    public function get_district(Request $request)
    {
        $data = DB::table('municipality_districts')
            ->select(DB::raw('dis_id, dis_name'))
            ->where([
                ['dis_regency_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

    public function get_subdistrict(Request $request)
    {
        $data = DB::table('municipality_subdistricts')
            ->select(DB::raw('sub_id, sub_name'))
            ->where([
                ['sub_district_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

}