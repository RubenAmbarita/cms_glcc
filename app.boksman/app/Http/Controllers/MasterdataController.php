<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Masterdata_Model;
use App\Dashboard_Model;
use DB;

class MasterdataController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {

    }

    public function ListTrans()
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');

        $data = new Masterdata_Model();
        $trans_list = $data->get_transportation($custid);
        $LoopVar = 0;

        $data = new Dashboard_Model();
        $notification = $data->get_notification($custid);
        $notifcount = count($notification);

        return view('Masterdata.listtrans', [
                        'custname'  => $custname,
                        'trans_list' => $trans_list,
                        'LoopVar' => $LoopVar,
                        'notification' => $notification,
                        'notifcount' => $notifcount,
                        'menu_active' => 'ListTrans'
                    ]);
    }

    public function AddTrans()
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');
        $data = new Masterdata_Model();
        $listInsurance = $data->get_Insurance();
        $listBrand = $data->get_Brand();

        $data = new Dashboard_Model();
        $notification = $data->get_notification($custid);
        $notifcount = count($notification);

        return view('Masterdata.addtrans', [
                        'custname'  => $custname,
                        'notification' => $notification,
                        'notifcount' => $notifcount,
                        'listInsurance' =>$listInsurance,
                        'listBrand' => $listBrand,
                        'menu_active' => 'ListTrans'
                    ]);
    }

    public function get_InsuranceAPI(Request $request)
    {
        //$date = date_create($request->p_pickdate);
        //$pickdate = date_format($date, 'Y-m-d H:i:s');
        $resultAPI['Policy'] = '19060011220122001';
        $resultAPI['Premi'] = '1000000';
        $resultAPI['Admin'] = '100000';

        return response()->json($resultAPI);
    }

    public function EditTrans(Request $request)
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');
        $data = new Masterdata_Model();
        $trans = $data->get_edittrans($custid, $request->id_truck_fleet);
        $listBrand = $data->get_Brand();
        $listInsurance = $data->get_Insurance();

        $data = new Dashboard_Model();
        $notification = $data->get_notification($custid);
        $notifcount = count($notification);

        return view('Masterdata.edittrans', [
                        'custname'  => $custname,
                        'trans' => $trans,
                        'notification' => $notification,
                        'notifcount' => $notifcount,
                        'listBrand' => $listBrand,
                        'listInsurance' =>$listInsurance,
                        'menu_active' => 'ListTrans'
                    ]);
    }

    public function UploadFileSTNK(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'stnk/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "stnk_". $fileNewName. "_thump.". $ext);
                    $return = "stnk_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "stnk_". $fileNewName. "_thump.". $ext);
                    $return = "stnk_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "stnk_". $fileNewName. "_thump.". $ext);
                    $return = "stnk_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function uploadFileKirChasis(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'kir_chasis/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "kir_". $fileNewName. "_thump.". $ext);
                    $return = "kir_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "kir_". $fileNewName. "_thump.". $ext);
                    $return = "kir_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "kir_". $fileNewName. "_thump.". $ext);
                    $return = "kir_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function UploadFileKIR(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'kir_head/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "kir_". $fileNewName. "_thump.". $ext);
                    $return = "kir_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "kir_". $fileNewName. "_thump.". $ext);
                    $return = "kir_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "kir_". $fileNewName. "_thump.". $ext);
                    $return = "kir_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function UploadFileTRUK(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'truk/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "truk_". $fileNewName. "_thump.". $ext);
                    $return = "truk_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "truk_". $fileNewName. "_thump.". $ext);
                    $return = "truk_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "truk_". $fileNewName. "_thump.". $ext);
                    $return = "truk_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function UploadFileASURANSI(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'asuransi/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "asuransi_". $fileNewName. "_thump.". $ext);
                    $return = "asuransi_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "asuransi_". $fileNewName. "_thump.". $ext);
                    $return = "asuransi_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "asuransi_". $fileNewName. "_thump.". $ext);
                    $return = "asuransi_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function ListDriver()
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');

        $data = new Masterdata_Model();
        $driver_list = $data->get_driver($custid);
        $LoopVar = 0;

        $data = new Dashboard_Model();
        $notification = $data->get_notification($custid);
        $notifcount = count($notification);

        return view('Masterdata.listdriver', [
                        'custname'  => $custname,
                        'driver_list' => $driver_list,
                        'LoopVar' => $LoopVar,
                        'notification' => $notification,
                        'notifcount' => $notifcount,
                        'menu_active' => 'ListDriver'
                    ]);
    }

    public function AddDriver()
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');

        $data = new Dashboard_Model();
        $notification = $data->get_notification($custid);
        $notifcount = count($notification);

        return view('Masterdata.adddriver', [
                        'custname'  => $custname,
                        'notification' => $notification,
                        'notifcount' => $notifcount,
                        'menu_active' => 'ListDriver'
                    ]);
    }

    public function EditDriver(Request $request)
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');

        $data = new Dashboard_Model();
        $notification = $data->get_notification($custid);
        $notifcount = count($notification);

        $master = new Masterdata_Model();
        $driver = $master->get_editdriver($custid, $request->id_driver);
        $driver->password = $this->decrypt($driver->password);
        //print_r($driver);die;

        return view('Masterdata.editdriver', [
                        'custname'  => $custname,
                        'notification' => $notification,
                        'notifcount' => $notifcount,
                        'menu_active' => 'ListDriver',
                        'driver' => $driver
                    ]);
    }

    public function check_policenumber(Request $request)
    {
        $getModel = new Masterdata_Model();
        $data = $getModel->check_policenumber($request->Police_Number);

        return response()->json($data);
    }

    public function saveTrans(Request $request)
    {
        $sysdate = date('Y-m-d H:i:s');

        $tanggal_declare = date_create($request->tanggal_declare);
        $request['tanggal_declare_format'] = date_format($tanggal_declare, 'Y-m-d H:i:s');

        $tanggal_declare2 = date_create($request->tanggal_declare2);
        $request['tanggal_declare2_format'] = date_format($tanggal_declare2, 'Y-m-d H:i:s');

        $save = new Masterdata_Model();
        $request['username'] = session()->get('username');
        $request['id_trucker'] = session()->get('custid');
        $request['sysdate'] = $sysdate;

        $return = $save->insert_trans($request);

        return response()->json($return);
    }

    public function nonactive_trans(Request $request)
    {
        $del = new Masterdata_Model();
        $return = $del->nonactive_trans($request->id_truck_fleet, session()->get('username'));

        return response()->json($return);
    }

    public function active_trans(Request $request)
    {
        $del = new Masterdata_Model();
        $return = $del->active_trans($request->id_truck_fleet, session()->get('username'));

        return response()->json($return);
    }

    public function saveDriver(Request $request)
    {
        $sysdate = date('Y-m-d H:i:s');
        $tanggal_declare = date_create($request->tanggal_declare);
        $request['tanggal_declare_format'] = date_format($tanggal_declare, 'Y-m-d H:i:s');

        $save = new Masterdata_Model();
        $request['username'] = session()->get('username');
        $request['id_trucker'] = session()->get('custid');
        $request['sysdate'] = $sysdate;
        $request['kata_sandi'] = $this->encrypt($request->kata_sandi);

        $return = $save->insert_driver($request);

        return response()->json($return);
    }

    public function delete_driver(Request $request)
    {
        $del = new Masterdata_Model();
        $return = $del->delete_driver($request->id_driver);

        return response()->json($return);
    }

    public function check_ktpsimtelpuser(Request $request)
    {
        $getModel = new Masterdata_Model();
        $data = $getModel->check_ktpsimtelpuser($request->nomor_ktp, $request->nomor_sim, $request->nomor_telp, $request->nama_pengguna);

        return response()->json($data);
    }

    public function UploadFileKTP(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'ktp/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "ktp_". $fileNewName. "_thump.". $ext);
                    $return = "ktp_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "ktp_". $fileNewName. "_thump.". $ext);
                    $return = "ktp_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "ktp_". $fileNewName. "_thump.". $ext);
                    $return = "ktp_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function UploadFileSIM(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'sim/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "sim_". $fileNewName. "_thump.". $ext);
                    $return = "sim_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "sim_". $fileNewName. "_thump.". $ext);
                    $return = "sim_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "sim_". $fileNewName. "_thump.". $ext);
                    $return = "sim_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

   public function UploadFileSKCK(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'skck/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "skck_". $fileNewName. "_thump.". $ext);
                    $return = "skck_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "skck_". $fileNewName. "_thump.". $ext);
                    $return = "skck_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "skck_". $fileNewName. "_thump.". $ext);
                    $return = "skck_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function UploadFileFoto(Request $request)
    {
        if(isset($_FILES['file']['name'])) {

            $file = $_FILES['file']['tmp_name'];
            $sourceProperties = getimagesize($file);
            $fileNewName = time();
            $folderPath = env('IMAGE_DIRECTORY').'foto/';
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $imageType = $sourceProperties[2];

            switch ($imageType) {
                case IMAGETYPE_PNG:
                    $imageResourceId = imagecreatefrompng($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagepng($targetLayer,$folderPath. "foto_". $fileNewName. "_thump.". $ext);
                    $return = "foto_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_GIF:
                    $imageResourceId = imagecreatefromgif($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagegif($targetLayer,$folderPath. "foto_". $fileNewName. "_thump.". $ext);
                    $return = "foto_". $fileNewName. "_thump.". $ext;
                    break;

                case IMAGETYPE_JPEG:
                    $imageResourceId = imagecreatefromjpeg($file);
                    $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                    imagejpeg($targetLayer,$folderPath. "foto_". $fileNewName. "_thump.". $ext);
                    $return = "foto_". $fileNewName. "_thump.". $ext;
                    break;

                default:
                    //echo "Invalid Image type.";
                    $return = false;
                    exit;
                    break;
            }

            //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
            //echo "Image Resize Successfully.";
            //return response()->json(['response' => $return]);
            return response()->json($return);
        }
    }

    public function encrypt($sData, $sKey='keico')
    {
        $sResult = '';
        for($i=0;$i<strlen($sData);$i++){
            $sChar    = substr($sData, $i, 1);
            $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
            $sChar    = chr(ord($sChar) + ord($sKeyChar));
            $sResult .= $sChar;
        }
        return $this->encode_base64($sResult);
    }

    public function decrypt($sData, $sKey='keico')
    {
        $sResult = '';
        $sData   = $this->decode_base64($sData);
        for($i=0;$i<strlen($sData);$i++){
            $sChar    = substr($sData, $i, 1);
            $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
            $sChar    = chr(ord($sChar) - ord($sKeyChar));
            $sResult .= $sChar;
        }
        return $sResult;
    }

    public function encode_base64($sData)
    {
        $sBase64 = base64_encode($sData);
        return strtr($sBase64, '+/', '-_');
    }

    public function decode_base64($sData)
    {
        $sBase64 = strtr($sData, '-_', '+/');
        return base64_decode($sBase64);
    }

}
