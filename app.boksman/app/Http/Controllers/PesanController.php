<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Pesan_Model;
use App\Dashboard_Model;
use DB;

class PesanController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Pesan_Model();
      $list = $data->get_pesan();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Pesan.Pesan', [
                        'custname'  => $custname,
                        'list' => $list,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'pesan'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Pesan_Model();
      $detail = $data->get_detail($request->id_message);
      $lokasi = $data->get_gereja();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Pesan.DetailPesan', [
                      'custname'  => $custname,
                      'detail' => $detail,
                      'lokasi' => $lokasi,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'pesan'
                  ]);
    }


    public function addmessage()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Pesan_Model();
      $lokasi = $data->get_gereja();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Pesan.AddPesan', [
                      'custname'  => $custname,
                      'lokasi' => $lokasi,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'pesan'
                  ]);
    }

    public function save(Request $request)
    {
      $save = new Pesan_Model();
      $tanggal_berakhir = date_create($request->tanggal_berakhir);
      $request['tanggal_berakhir'] = date_format($tanggal_berakhir, 'Y-m-d H:i:s');
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

    public function delete_pesan(Request $request)
    {
        $del = new Pesan_Model();
        $return = $del->delete_pesan($request->id_message);

        return response()->json($return);
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function upload(){
      if(isset($_FILES['file']['name'])) {

          $file = $_FILES['file']['tmp_name'];
          $sourceProperties = getimagesize($file);
          $fileNewName = time();
          $folderPath = env('IMAGE_DIRECTORY').'pesan/';
          $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
          $imageType = $sourceProperties[2];

          switch ($imageType) {
              case IMAGETYPE_PNG:
                  $imageResourceId = imagecreatefrompng($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagepng($targetLayer,$folderPath. "pesan_". $fileNewName. "_thump.". $ext);
                  $return = "pesan_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_GIF:
                  $imageResourceId = imagecreatefromgif($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagegif($targetLayer,$folderPath. "pesan_". $fileNewName. "_thump.". $ext);
                  $return = "pesan_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_JPEG:
                  $imageResourceId = imagecreatefromjpeg($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagejpeg($targetLayer,$folderPath. "pesan_". $fileNewName. "_thump.". $ext);
                  $return = "pesan_". $fileNewName. "_thump.". $ext;
                  break;

              default:
                  //echo "Invalid Image type.";
                  $return = false;
                  exit;
                  break;
          }

          //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
          //echo "Image Resize Successfully.";
          //return response()->json(['response' => $return]);
          return response()->json($return);
      }
    }
}
