<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Renungan_Model;
use App\Dashboard_Model;
use DB;

class RenunganController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Renungan_Model();
      $list_renungan = $data->get_renungan();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Renungan.Renungan', [
                        'custname'  => $custname,
                        'list_renungan' => $list_renungan,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'renungan'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Renungan_Model();
      $detail = $data->get_detail($request->id_renungan);
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Renungan.DetailRenungan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'renungan',
                      'detail' => $detail
                  ]);
    }


    public function addrenungan()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Renungan.AddRenungan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'renungan'
                  ]);
    }

    public function save(Request $request)
    {
      $save = new Renungan_Model();
      $tanggal_renungan = date_create($request->tanggal_renungan);
      $request['tanggal_renungan'] = date_format($tanggal_renungan, 'Y-m-d H:i:s');
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

    public function delete_renungan(Request $request)
    {
        $del = new Renungan_Model();
        $return = $del->delete_renungan($request->id_renungan);

        return response()->json($return);
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function upload(){
      if(isset($_FILES['file']['name'])) {

          $file = $_FILES['file']['tmp_name'];
          $sourceProperties = getimagesize($file);
          $fileNewName = time();
          $folderPath = env('IMAGE_DIRECTORY').'renungan/';
          $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
          $imageType = $sourceProperties[2];

          switch ($imageType) {
              case IMAGETYPE_PNG:
                  $imageResourceId = imagecreatefrompng($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagepng($targetLayer,$folderPath. "renungan_". $fileNewName. "_thump.". $ext);
                  $return = "renungan_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_GIF:
                  $imageResourceId = imagecreatefromgif($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagegif($targetLayer,$folderPath. "renungan_". $fileNewName. "_thump.". $ext);
                  $return = "renungan_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_JPEG:
                  $imageResourceId = imagecreatefromjpeg($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagejpeg($targetLayer,$folderPath. "renungan_". $fileNewName. "_thump.". $ext);
                  $return = "renungan_". $fileNewName. "_thump.". $ext;
                  break;

              default:
                  //echo "Invalid Image type.";
                  $return = false;
                  exit;
                  break;
          }

          //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
          //echo "Image Resize Successfully.";
          //return response()->json(['response' => $return]);
          return response()->json($return);
      }
    }
}
