<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Glc_Model;
use App\Dashboard_Model;
use DB;

class GlcController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Glc_Model();
      $list = $data->get_glc();
      $list_terlaksana = $data->get_glc_terlaksana();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Glc.Glc', [
                        'custname'  => $custname,
                        'list' => $list,
                        'list_terlaksana' => $list_terlaksana,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'glc'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Glc_Model();
      $detail = $data->get_detail($request->id_glc);
      $lokasi = $data->get_gereja();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Glc.DetailGlc', [
                      'custname'  => $custname,
                      'detail' => $detail,
                      'lokasi' => $lokasi,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'glc'
                  ]);
    }

    public function addglc()
    {
      // code...
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Glc_Model();
      $lokasi = $data->get_gereja();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Glc.AddGlc', [
                      'custname'  => $custname,
                      'lokasi' => $lokasi,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'glc'
                  ]);
    }

    public function daftarpeserta(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Glc_Model();
      $daftar_peserta = $data->daftar_peserta($request->id_glc);
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Glc.DaftarPeserta', [
                      'custname'  => $custname,
                      'daftar_peserta' => $daftar_peserta,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'glc'
                  ]);
    }

    public function update_status(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Glc_Model();
      $return = $data->update_status($request);
      return response()->json($return);
    }

    public function save(Request $request)
    {
      $save = new Glc_Model();
      $tanggal_waktu_glc = date_create($request->tanggal_waktu_glc);
      $request['tanggal_waktu_glc'] = date_format($tanggal_waktu_glc, 'Y-m-d H:i:s');
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

    public function delete_glc(Request $request)
    {
        $del = new Glc_Model();
        $return = $del->delete_glc($request->id_glc);

        return response()->json($return);
    }

    public function imageResize($imageResourceId,$width,$height)
    {
        //$targetWidth =400;
        //$targetHeight =400;

        $targetWidth = 500;
        $diff = $width / $targetWidth;
        $targetHeight = $height / $diff;

        $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
        imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

        return $targetLayer;
    }

    public function upload(){
      if(isset($_FILES['file']['name'])) {

          $file = $_FILES['file']['tmp_name'];
          $sourceProperties = getimagesize($file);
          $fileNewName = time();
          $folderPath = env('IMAGE_DIRECTORY').'glc/';
          $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
          $imageType = $sourceProperties[2];

          switch ($imageType) {
              case IMAGETYPE_PNG:
                  $imageResourceId = imagecreatefrompng($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagepng($targetLayer,$folderPath. "glc_". $fileNewName. "_thump.". $ext);
                  $return = "glc_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_GIF:
                  $imageResourceId = imagecreatefromgif($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagegif($targetLayer,$folderPath. "glc_". $fileNewName. "_thump.". $ext);
                  $return = "glc_". $fileNewName. "_thump.". $ext;
                  break;

              case IMAGETYPE_JPEG:
                  $imageResourceId = imagecreatefromjpeg($file);
                  $targetLayer = $this->imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagejpeg($targetLayer,$folderPath. "glc_". $fileNewName. "_thump.". $ext);
                  $return = "glc_". $fileNewName. "_thump.". $ext;
                  break;

              default:
                  //echo "Invalid Image type.";
                  $return = false;
                  exit;
                  break;
          }

          //move_uploaded_file($file, $folderPath. $fileNewName. ".". $ext);
          //echo "Image Resize Successfully.";
          //return response()->json(['response' => $return]);
          return response()->json($return);
      }
    }
}
