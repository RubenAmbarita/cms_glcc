<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Reg_Model;


class RegController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        return view('CreateRegister');
    }

    public function save(Request $request)
    {
        $save = new Dashboard_Model();
        $return = $save->insert($request);
        
        return response()->json(['response' => $return]);
    }

    public function UploadFile(Request $request)
    {
        if (isset($_FILES['file']['name'])) {
            if (0 < $_FILES['file']['error']) {
                $file = 'Error during file upload' . $_FILES['file']['error'];
            } else {
                if (file_exists('uploads/' . $_FILES['file']['name'])) {
                    $file = 'File already exists : uploads/' . $_FILES['file']['name'];
                } else {
                    move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_FILES['file']['name']);
                    $file = 'File successfully uploaded : uploads/' . $_FILES['file']['name'];
                }
            }
        } else {
            $file = 'Please choose a file';
        }        
     }

}
