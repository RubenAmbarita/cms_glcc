<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

use App\Dashboard_Model;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custid = session()->get('custid');

        $data = new Dashboard_Model();


        if($custid == 0){
            return view('CreateRegister');
        }
        else{
            return view('dashboard', [
                'menu_active' => 'dashboard'
             ]);
        }
    }


    public function get_menu()
    {
        $data = new Dashboard_Model();
        $Menu = $data->get_menu();

        return view('dashboard', [
                                    'Menu' => $Menu
                                 ]);
        //return view('dashboard');
    }

    public function save(Request $request)
    {
        $save = new Dashboard_Model();
        $return = $save->insert($request);

        return response()->json(['response' => $return]);
    }

    public function UploadFile(Request $request)
    {
        if (isset($_FILES['file']['name'])) {
            if (0 < $_FILES['file']['error']) {
                $file = 'Error during file upload' . $_FILES['file']['error'];
            } else {
                if (file_exists('uploads/' . $_FILES['file']['name'])) {
                    $file = 'File already exists : uploads/' . $_FILES['file']['name'];
                } else {
                    move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/' . $_FILES['file']['name']);
                    $file = 'File successfully uploaded : uploads/' . $_FILES['file']['name'];
                }
            }
        } else {
            $file = 'Please choose a file';
        }
     }

}
