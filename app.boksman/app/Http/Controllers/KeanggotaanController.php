<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Keanggotaan_Model;
use App\Dashboard_Model;
use DB;

class KeanggotaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $getModel = new Keanggotaan_Model();
        $custid = session()->get('custid');
        $custname = session()->get('custname');
        $id_user = session()->get('id_user');
        $provinsi = $this->get_state();
        $anggota = $getModel->get_anggota($id_user);
        

        return view('Keanggotaan.index', [
                                                'anggota' => $anggota,
                                                'menu_active' => 'AnggotaList',
                                                'provinsi'  => $provinsi,
                                                'custname'  => $custname,
                                             ]);
    }

    public function save(Request $request)
    {
      $save = new Keanggotaan_Model();
      $request['username'] = session()->get('username');
      $request['custid'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Keanggotaan_Model();
      $provinsi = $this->get_state();
      $lokasi_gereja = $data->get_gereja();
      $detail = $data->get_detail($request->id);

      return view('Keanggotaan.detail', [
                      'custname'  => $custname,
                      'detail' => $detail,
                      'provinsi'  => $provinsi,
                      'lokasi_gereja' => $lokasi_gereja,
                      'menu_active' => 'AnggotaList'
                  ]);
    }
    

    public function update_status(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Keanggotaan_Model();
      $return = $data->update_status($request);
      return response()->json($return);
    }

    public function delete(Request $request)
    {
        $custid = session()->get('custid');
        $custname = session()->get('custname');
        $data = new Keanggotaan_Model();
        $return = $data->delete_anggota($request);
        return response()->json($return);
      }

    public function destroy()
    {
        return view('Keanggotaan.destroy', compact('Keanggotaan'));
    }

    public function get_state()
    {
        $data = DB::table('municipality_provinces')
            ->select(DB::raw('pro_id, pro_name'))
            ->get();

        return $data;
    }

    public function get_city(Request $request)
    {
        $data = DB::table('municipality_regencies')
            ->select(DB::raw('reg_id, reg_name'))
            ->where([
                ['reg_province_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

    public function get_district(Request $request)
    {
        $data = DB::table('municipality_districts')
            ->select(DB::raw('dis_id, dis_name'))
            ->where([
                ['dis_regency_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

    public function get_subdistrict(Request $request)
    {
        $data = DB::table('municipality_subdistricts')
            ->select(DB::raw('sub_id, sub_name'))
            ->where([
                ['sub_district_id', $request->p_param]
            ])
            ->get();

        return $data;
    }

}