<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Warta_Model;
use App\Dashboard_Model;
use DB;

class WartaController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Warta_Model();
      $warta = $data->get_warta();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);

        return view('Warta.index', [
                        'custname'  => $custname,
                        'warta' => $warta,
                        // 'notification' => $notification,
                        // 'notifcount' => $notifcount,
                        'menu_active' => 'wartaList'
                    ]);
    }

    public function detail(Request $request)
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Renungan_Model();
      $detail = $data->get_detail($request->id_renungan);
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Renungan.DetailRenungan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'renungan',
                      'detail' => $detail
                  ]);
    }


    public function addrenungan()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Renungan.AddRenungan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'renungan'
                  ]);
    }

    public function save(Request $request)
    {
      $save = new Renungan_Model();
      $tanggal_renungan = date_create($request->tanggal_renungan);
      $request['tanggal_renungan'] = date_format($tanggal_renungan, 'Y-m-d H:i:s');
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $return = $save->insert($request);

      return response()->json($return);
    }

}
