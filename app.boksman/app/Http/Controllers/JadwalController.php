<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use DateTime;
use DatePeriod;
use DateInterval;
use App\Jadwal_Model;
use App\Dashboard_Model;
use DB;

class JadwalController extends Controller
{

    public function __construct()
    {
        //Commit Order
        $this->middleware('auth');
    }

    public function index()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      $getModel = new Jadwal_Model();
      $jadwal = $getModel->get_jadwal($custid);
      

        return view('Jadwal.index', [
                        'custname'  => $custname,
                        'jadwal' => $jadwal,
                        'menu_active' => 'jadwalList'
                    ]);
    }

    public function detail()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Renungan.DetailRenungan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'renungan'
                  ]);
    }


    public function addrenungan()
    {
      $custid = session()->get('custid');
      $custname = session()->get('custname');
      $data = new Dashboard_Model();
      // $notification = $data->get_notification($custid);
      // $notifcount = count($notification);
      return view('Renungan.AddRenungan', [
                      'custname'  => $custname,
                      // 'notification' => $notification,
                      // 'notifcount' => $notifcount,
                      'menu_active' => 'renungan'
                  ]);
    }

    public function save(Request $request)
    {
      $sysdate = date('Y-m-d H:i:s');

      $save = new RenunganModel();
      $request['username'] = session()->get('username');
      $request['id_trucker'] = session()->get('custid');
      $request['sysdate'] = $sysdate;
      $return = $save->insert($request);

      return response()->json($return);
    }

}
