<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use DB;

class Jadwal_Model extends Model
{
	protected $fillable = [
        'name', 'email'
    ];

    public function get_jadwal($p_param)
    {
    	$data = DB::table('tb_jadwal')
            ->select('*')
            ->where([
                ['created_by', '=', $p_param]
            ])
            ->orderby('created_at', 'desc')
            ->get();

        return $data;
    }

    public function get_useredit($p_param)
    {
        $data = DB::table('tb_gereja')
            ->select('*')
            ->where([
                ['id_user', '=', $p_param]
            ])
            ->first();

        return $data;
    }

    public function insert($data)
    {
        $save = DB::table('tb_gereja')->insert([
            'nama_gereja' => $data['nama_gereja'],
            'id_user' => $data->custid,
            'alamat' => $data['alamat'],
            'kelurahan' => $data['kelurahan'],
            'kecamatan' => $data['kecamatan'],
            'kokab' => $data['kokab'],
            'provinsi' => $data['provinsi'],
            'negara' => $data['negara'],
            'kode_pos' => $data['kode_pos'],
            'PIC' => $data['PIC'],
            'no_telp' => $data['no_telp'],
            'no_telp_2' => $data['no_telp_2'],
            'foto_gereja' => $data['foto_gereja'],
            'created_by' => 'administrator',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        if($save){
          return true;
        }
        return false;
    }


    public function updates($data)
    {
        $update = DB::table('tb_gereja')
            ->where('id_user', $data->custid)
            ->update([
            'nama_gereja' => $data['nama_gereja'],
            'alamat' => $data['alamat'],
            'kelurahan' => $data['kelurahan'],
            'kecamatan' => $data['kecamatan'],
            'kokab' => $data['kokab'],
            'provinsi' => $data['provinsi'],
            'kode_pos' => $data['kode_pos'],
            'PIC' => $data['PIC'],
            'no_telp' => $data['no_telp'],
            'no_telp_2' => $data['no_telp_2'],
            'foto_gereja' => $data['foto_gereja'],
            'updated_by' => 'administrator',
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        if($update){
          return $data->custid;
        }
        return false;
    }

    public function deleteds($p_param, $username)
    {
        $update = DB::table('users')
            ->where('username', $p_param)
            ->update([
            'activestatus' => 'atsna',
            'updated_by' => $username,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);

        if($update){
          return '';
        }
          return $username;
    }
}
