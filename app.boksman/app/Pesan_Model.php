<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class Pesan_Model extends Model
{

  public function insert($data)
  {
    if(empty($data['id_message']))
    {
      $save = DB::table('tb_message')->insert([
        'id_loc_church' => $data['id_gereja'],
        'theme_message' => $data['tema_pesan'],
        'title_message' => $data['judul_pesan'],
        'message' => $data['pesan'],
        'responsible_message' => $data['penanggung_jawab'],
        'division_message' => $data['divisi'],
        'end_date_message' => $data['tanggal_berakhir'],
        'picture_upload' => $data['gambar'],
        'created_at' => date('Y-m-d H:i:s')
      ]);
    }else{
      $save = DB::table('tb_message')
          ->where('id_message', $data['id_message'])
          ->update([
            'id_loc_church' => $data['id_gereja'],
            'theme_message' => $data['tema_pesan'],
            'title_message' => $data['judul_pesan'],
            'message' => $data['pesan'],
            'responsible_message' => $data['penanggung_jawab'],
            'division_message' => $data['divisi'],
            'end_date_message' => $data['tanggal_berakhir'],
            'picture_upload' => $data['gambar'],
            'updated_at' => date('Y-m-d H:i:s')
          ]);
    }

      if($save){
        return true;
      }else {
        // code...
        return false;
      }
  }

  public function get_gereja(){
    $sql = "SELECT t1.id_gereja, t1.nama_gereja from tb_gereja t1";
    $data = DB::select($sql);
    return $data;
  }

  public function get_pesan()
  {
      $sql = "SELECT t1.theme_message,
                     t1.title_message,
                     t1.responsible_message,
                     t1.division_message,
                     t1.end_date_message,
                     t1.id_message
                FROM tb_message t1
            ORDER BY t1.id_message DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function get_detail($id_message)
  {
      $sql = "SELECT *
                FROM tb_message t1
               WHERE t1.id_message = '". $id_message ."'";

      $data = DB::select($sql);
      return $data[0];
  }

  public function delete_pesan($id_message)
  {
      $delete = DB::table('tb_message')->where('id_message', '=', $id_message)->delete();

      return $delete;
  }

}
