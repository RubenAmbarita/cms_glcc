<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class Masterdata_Model extends Model
{
  public function insert($data)
  {
    if(empty($data['id_pujian']))
    {
      $save = DB::table('tb_kidung_pujian')->insert([
        'pujian' => $data['pujian'],
        'judul' => $data['judul'],
        'penulis' => $data['penulis'],
        'editor' => $data['editor'],
        'tautan_video' => $data['tautan_video'],
        'gambar_name' => $data['gambar_name'],
        'lirik_pujian' => $data['lirik_pujian'],
        'created_at' => date('Y-m-d H:i:s')
      ]);
    }else{
      $save = DB::table('tb_kidung_pujian')
          ->where('id_kidung_pujian', $data['id_pujian'])
          ->update([
            'pujian' => $data['pujian'],
            'judul' => $data['judul'],
            'penulis' => $data['penulis'],
            'editor' => $data['editor'],
            'tautan_video' => $data['tautan_video'],
            'gambar_name' => $data['gambar_name'],
            'lirik_pujian' => $data['lirik_pujian'],
            'updated_at' => date('Y-m-d H:i:s')
          ]);
    }

      if($save){
        return true;
      }else {
        // code...
        return false;
      }
  }

  public function get_pujian()
  {
      $sql = "SELECT t1.id_kidung_pujian,
                     t1.pujian,
                     t1.judul,
                     t1.penulis,
                     t1.editor,
                     t1.tautan_video,
                     t1.gambar_name,
                     t1.lirik_pujian
                FROM tb_kidung_pujian t1
            ORDER BY t1.id_kidung_pujian DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function get_detail($id_pujian)
  {
      $sql = "SELECT *
                FROM tb_kidung_pujian t1
               WHERE t1.id_kidung_pujian = '". $id_pujian ."'";

      $data = DB::select($sql);
      return $data[0];
  }

  public function delete_pujian($id_pujian)
  {
      $delete = DB::table('tb_kidung_pujian')->where('id_kidung_pujian', '=', $id_pujian)->delete();

      return $delete;
  }

}
