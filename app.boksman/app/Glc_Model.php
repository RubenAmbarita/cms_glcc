<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class Glc_Model extends Model
{
  public function insert($data)
  {
    if(empty($data['id_glc']))
    {
      $save = DB::table('tb_glc')->insert([
        'id_gereja' => $data['id_gereja'],
        'id_status' => '2',
        'tema_glc' => $data['tema_glc'],
        'judul_glc' => $data['judul_glc'],
        'nama_glc' => $data['nama_glc'],
        'nama_pemimpin' => $data['nama_pemimpin'],
        'alamat_glc' => $data['alamat_glc'],
        'deskripsi_glc' => $data['deskripsi_glc'],
        'gambar_glc' => $data['gambar_glc'],
        'artikel_glc' => $data['artikel_glc'],
        'link_video_glc' => $data['link_video_glc'],
        'tanggal_waktu_glc' => $data['tanggal_waktu_glc'],
        'created_at' => date('Y-m-d H:i:s')
      ]);
    }else{
      $save = DB::table('tb_glc')
          ->where('id_glc', $data['id_glc'])
          ->update([
            'id_gereja' => $data['id_gereja'],
            'tema_glc' => $data['tema_glc'],
            'judul_glc' => $data['judul_glc'],
            'nama_pemimpin' => $data['nama_pemimpin'],
            'alamat_glc' => $data['alamat_glc'],
            'deskripsi_glc' => $data['deskripsi_glc'],
            'gambar_glc' => $data['gambar_glc'],
            'artikel_glc' => $data['artikel_glc'],
            'link_video_glc' => $data['link_video_glc'],
            'tanggal_waktu_glc' => $data['tanggal_waktu_glc'],
            'updated_at' => date('Y-m-d H:i:s')
          ]);
    }

      if($save){
        return true;
      }else {
        // code...
        return false;
      }
  }

  public function get_gereja(){
    $sql = "SELECT t1.id_gereja, t1.nama_gereja from tb_gereja t1";
    $data = DB::select($sql);
    return $data;
  }

  public function get_glc()
  {
      $sql = "SELECT t2.nama_gereja,
                    t1.id_glc,
                     t1.nama_glc,
                     t1.nama_pemimpin,
                     t1.tanggal_waktu_glc
                FROM tb_glc t1 INNER JOIN tb_gereja t2 ON t1.id_gereja = t2.id_gereja
                WHERE t1.id_status='2'
            ORDER BY t1.id_glc DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function get_glc_terlaksana()
  {
      $sql = "SELECT t2.nama_gereja,
                      t1.id_glc,
                     t1.nama_glc,
                     t1.nama_pemimpin,
                     t1.tanggal_waktu_glc
                FROM tb_glc t1 INNER JOIN tb_gereja t2 ON t1.id_gereja = t2.id_gereja
                WHERE t1.id_status='1'
            ORDER BY t1.id_glc DESC";

      $data = DB::select($sql);
      return $data;
  }

  public function daftar_peserta($id_glc){
      $sql = "SELECT * FROM tb_anggota_glc t1 WHERE t1.id_glc = '".$id_glc."'";

      $data = DB::select($sql);
      return $data;
  }

  public function get_detail($id_glc)
  {
      $sql = "SELECT *
                FROM tb_glc t1
               WHERE t1.id_glc = '". $id_glc ."'";

      $data = DB::select($sql);
      return $data[0];
  }

  public function delete_pelayanan($id_master_pelayanan)
  {
      $delete = DB::table('tb_master_pelayanan')->where('id_master_pelayanan', '=', $id_master_pelayanan)->delete();

      return $delete;
  }

  public function update_status($data)
  {
      $save = DB::table('tb_glc')
          ->where('id_glc', $data['id_glc'])
          ->update([
            'id_status' => '1',
            'updated_at' => date('Y-m-d H:i:s')
          ]);

      if($save){
        return true;
      }else {
        // code...
        return false;
      }
  }

}
