<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
//use Illuminate\Database\Query\Builder;
use DB;

class Warta_Model extends Model
{
    public function insert($data)
    {
      if(empty($data['id_renungan']))
      {
        $save = DB::table('tb_renungan')->insert([
          'tema_renungan' => $data['tema_renungan'],
          'judul_renungan' => $data['judul_renungan'],
          'bacaan_alkitab1' => $data['alkitab_1'],
          'bacaan_alkitab2' => $data['alkitab_2'],
          'tanggal_renungan' => $data['tanggal_renungan'],
          'penulis_renungan' => $data['penulis_renungan'],
          'editor_renungan' => $data['editor'],
          'gambar_name' => $data['gambar'],
          'renungan' => $data['renungan'],
          'created_at' => date('Y-m-d H:i:s')
        ]);
      }else{
        $save = DB::table('tb_renungan')
            ->where('id_renungan', $data['id_renungan'])
            ->update([
              'tema_renungan' => $data['tema_renungan'],
              'judul_renungan' => $data['judul_renungan'],
              'bacaan_alkitab1' => $data['alkitab_1'],
              'bacaan_alkitab2' => $data['alkitab_2'],
              'tanggal_renungan' => $data['tanggal_renungan'],
              'penulis_renungan' => $data['penulis_renungan'],
              'editor_renungan' => $data['editor'],
              'gambar_name' => $data['gambar'],
              'renungan' => $data['renungan']
            ]);
      }

        if($save){
          return true;
        }else {
          // code...
          return false;
        }
    }

    public function get_warta()
    {
        $sql = "SELECT t1.id,
                       t1.lokasi,
                       t1.periode_mulai,
                       t1.periode_selesai,
                       t1.tema,
                       t1.tgl_publikasi,
                       t1.file_warta
                  FROM tb_warta t1
              ORDER BY t1.tgl_publikasi DESC";

        $data = DB::select($sql);
        return $data;
    }

    public function get_detail($id_renungan)
    {
        $sql = "SELECT *
                  FROM tb_renungan t1
                 WHERE t1.id_renungan = '". $id_renungan ."'";

        $data = DB::select($sql);
        return $data[0];
    }
}
