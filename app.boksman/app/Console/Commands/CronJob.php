<?php
namespace App\Console\Commands;
use Illuminate\Console\Command;
use DB;

class CronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronJob:cronjob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule Match Successfully.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    /*
            \DB::table('members')
            ->insert([
                        'name' => 'Edy Supriyanto Wiyono',
                        'email' => 'edysw@gmail.com',
                    ]);
        $itemorder = DB::table('tb_item_order')
        ->select(DB::raw('*'))
        ->where([
                    ['id_customer', $custid],
                    ['id_publish', ''],
                    ['status_active', 0]
                ])
        ->get();
        t1.id_customer = ".$custid." AND
                                  t1.id_order = ".$id_order." ");

    */
    public function handle()
    {
        $custid = session()->get('custid');
        $itemorder = DB::table('v_ordermatchonschedule')
                      ->select(DB::raw('*'))
                      ->get();

        $itemcount = count($itemorder);
        $bagibagi = 0;
        for ($i = 1; $i <= $itemcount; $i++){
            $id_public = DB::select("select f_matchonschedule_publish(".$itemorder[$bagibagi]->id_item_order.") as data from dual");
            $id_warehouse = DB::select("select f_matchonschedule_warehouse(".$itemorder[$bagibagi]->id_item_order.") as data from dual");
            $id_depo = DB::select("select id_depo
                                       from tb_depo t1
                                      where ROUND(f_calc_distance(SUBSTRING_INDEX(t1.coordinate,',',1), SUBSTRING_INDEX(t1.coordinate,',',-1), SUBSTRING_INDEX((SELECT t2.coordinate FROM tb_gudang t2 where t2.id_gudang = ".$id_warehouse[0]->data.") ,',',1), SUBSTRING_INDEX((SELECT t2.coordinate FROM tb_gudang t2 where t2.id_gudang = ".$id_warehouse[0]->data."),',',-1)), 2)
                                           = 
                                        (SELECT MIN(ROUND(f_calc_distance(SUBSTRING_INDEX(t1.coordinate,',',1),
                                    SUBSTRING_INDEX(t1.coordinate,',',-1), SUBSTRING_INDEX((SELECT t2.coordinate FROM tb_gudang t2 where t2.id_gudang = ".$id_warehouse[0]->data.") ,',',1), SUBSTRING_INDEX((SELECT t2.coordinate FROM tb_gudang t2 where t2.id_gudang = ".$id_warehouse[0]->data."),',',-1)), 2)) FROM tb_depo t1) ");

            $update_order = DB::table('tb_item_order')
                      ->where('id_item_order', $itemorder[$bagibagi]->id_item_order)
                      ->update(['id_publish' => $id_public[0]->data,
                                'status_active' => 1,
                                'match_on' => date('Y-m-d H:i:s')]);

            $update_public = DB::table('tb_publish_truck')
                      ->where('id_publish', $id_public[0]->data)
                      ->update(['status' => 1,
                                'id_depo' => $id_depo[0]->id_depo]);

            $sendpositoin = DB::select("SELECT t1.id_order,
                                t1.gk_order,
                                t1.id_publish,
                                (SELECT t2.token
                                   FROM tb_driver t2
                                  WHERE EXISTS (SELECT *
                                                  FROM tb_publish_truck t3
                                                 WHERE t2.id_driver = t3.id_driver
                                                   AND t1.id_publish = t3.id_publish)) AS token_driver,
                                (SELECT t2.address_gudang
                                   FROM tb_gudang t2
                                  WHERE EXISTS (SELECT *
                                                  FROM tb_publish_truck t3
                                                  WHERE t2.id_gudang = t3.id_gudang
                                                    AND t1.id_publish = t3.id_publish)) AS address_gudang
                            FROM tb_item_order t1
                           WHERE t1.id_order = ".$itemorder[$bagibagi]->id_item_order." ");

            $sendcount = count($sendpositoin);
            $sendpost = 0;
            $i1 = 0;
            for ($i = 1; $i1 <= $sendcount; $i1++){
                $return = $this->send_driverposition($sendpositoin[0]->token_driver, $sendpositoin[0]->address_gudang, $sendpositoin[0]->id_publish);
            }
                        
        $bagibagi = ($bagibagi+1);
        }
        //id_publish`='$match_publish[id_publish]', `status_active`='1', `match_on`='$now' 

        //print_r($itemorder);
        $this->info('User Name Change Successfully!');
    }

    public function send_driverposition($token, $warehouse, $idpublish)
    {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $res = array();

        echo $user1 = $token;

        $payload = array();
        $payload['team'] = 'India';
        $payload['score'] = '5.6.1';
        $res['data']['title'] = "Order Baru";
        $res['data']['is_background'] = FALSE;
        $res['data']['message'] = "Tujuan : ".$warehouse;
        $res['data']['image'] = "http://api.androidhive.info/images/minion.jpg";
        $res['data']['payload'] = $payload;
        $res['data']['module'] = "order";
        $res['data']['id_publish'] = $idpublish;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');
       /* $fields = array(
            'to' => 'foogjR61DSo:APA91bE-M2ne0_4NVfG_NxPcX943CrthtTjCqS_Mphf2T9x2DNoI5Fxs7YuxveLac8AqvQ8zj3HF0lF5bMncVLnPEbjuX_Nx1zIWsjyJ6JEZNVK4nxVMKS37jhKNxvrEfd8Jdq3c56u7',
            'to' => $user1,
            'notification' => array('title' => "sad", 'body' => '2'),
            'data' => array('data' => $res)
        );*/

        $fields = array(
            'to' => $user1,
            'data' => $res,
        );
 
        $headers = array(
            'Authorization:key=AIzaSyDRQB95yoPKli0qJSt9IpYpkiwnmtaz6uI' ,
            'Content-Type:application/json'
        );
        
        $ch = curl_init();
 
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    
        $result = curl_exec($ch);
       
        curl_close($ch);

        //print_r($fields['data']);
    }
}