<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'mpm_header'                  => 'Shipper Login',
    'company_name'                => 'Company Name',
    'address'                     => 'Address',
    'full_name'                   => 'Full name',
    'email'                       => 'Enter Email',
    'username'                    => 'Username',
    'password'                    => 'Password',
    'retype_password'             => 'Retype password',
    'remember_me'                 => 'Remember Me',
    'register'                    => 'Register',
    'register_a_new_membership'   => 'Register a new membership',
    'i_forgot_my_password'        => 'I forgot my password',
    'i_already_have_a_membership' => 'I already have a membership',
    'sign_in'                     => 'Sign In',
    'log_out'                     => 'Log Out',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Sign in to start your session',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Send Password Reset Link',
];
