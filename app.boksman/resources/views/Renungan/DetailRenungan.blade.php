@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.css')}}">
<link rel="stylesheet" href="{{url('css/bootstrap-fileinput.css')}}">
<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/renungan/index">Renungan</a></li>
        <li class="active">Detail</li>
      </ol>
      <h1 style="font-family:Poppins"> Detail Renungan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Armada Publication</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <form action="{{ 'Order/matchtab1' }}" id="formtab1" method="post">
              {!! csrf_field() !!}
              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Tema Renungan<b style="color:red;"> *</b></label>
                  <input type="hidden" id="id_renungan" value="{{$detail->id_renungan}}" name="">
                  <input type="text" id="tema_renungan" value="{{$detail->tema_renungan}}" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Judul Renungan<b style="color:red;"> *</b></label>
                  <input type="text" id="judul_renungan" value="{{$detail->judul_renungan}}" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Bacaan Alkitab I<b style="color:red;"> *</b></label>
                  <input type="text" id="alkitab_1" name="perusahaan" value="{{$detail->bacaan_alkitab1}}" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Bacaan Alkitab II</label>
                  <input type="text" id="alkitab_2" name="perusahaan" value="{{$detail->bacaan_alkitab2}}" class="form-control">
                </div>
              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Tanggal Renungan<b style="color:red;"> *</b></label>
                  <input type="text" autocomplete="off" id="tanggal_renungan" name="perusahaan" value="{{$detail->tanggal_renungan}}" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Penulis Renungan<b style="color:red;"> *</b></label>
                  <input type="text" id="penulis_renungan" name="perusahaan" value="{{$detail->penulis_renungan}}" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Editor</label>
                  <input type="text" id="editor" name="perusahaan" value="{{$detail->editor_renungan}}" class="form-control">
                </div>

                <table border="0">
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Gambar</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="{{'../images/GCCC/renungan/'.$detail->gambar_name}}" alt="" />
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Pilih Foto </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Ubah </span>
                              <input type="file" id="gambar" name="gambar"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Hapus </a>
                            </div>
                          </div>
                        </div>
                    </td>
                </table>


              </div>

              <!-- /.box-header -->
              <div class="box-body pad">

              </div>

            <!-- /.box-header -->
            <div class="box-body pad">
              <label for="">Renungan <b style="color:red;"> *</b></label>
              <form>
                <textarea id="renungan" class="textarea" placeholder="Ketik ..." value=""
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$detail->renungan}}</textarea>
              </form>
            </div>
            <div class="box-body pad">
              <button style="float:right" class="btn btn-info" id="submit" type="submit" name="button"><i class="fa fa-check"></i> Simpan Perubahan</button>
            </div>
          </div>

              <div class="form-group has-feedback {{ $errors->has('add_publish_tab1') ? 'has-error' : '' }}">
                @if ($errors->has('add_publish_tab1'))
                  <span class="help-block">
                    <strong>{{ $errors->first('add_publish_tab1') }}</strong>
                  </span>
                @endif
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!--<script src="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.js')}}"></script>-->
<!--<script src="{{url('js/order.js')}}"></script>-->
<!-- CK Editor -->
<script src="{{url('adminlte.assets/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{url('js/bootstrap-fileinput.js')}}"></script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})

$( function()
{
  $( "#tanggal_renungan" ).datepicker({
      minDate: 0,
      dateFormat: "d-m-Y"
  });

});


  function showCancelMessage()
  {
      swal({
          title: "Anda Yakin?",
          text: "Data Akan Dihapus !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "{{ url('/') }}/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == 0 || data == 1)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data Successfully Deleted" ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Done ",
                          cancelButtonText: "No ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "{{ url('/') }}/Published/nomatched/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data Failed Saved" ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Close ",
                            cancelButtonText: "No ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }
</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  function UploadGambar()
  {
    var file_data = $('#gambar').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "{{ url('/') }}/renungan/upload",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        async: false,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Gambar Gagal!');
                return false;
            }
            else {
              res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload KTP Failed!');
        //     return false;
        // }
    }); /* AJAX END */
    return res;
  }

  $("#submit").click(function(){
    var tema_renungan = $("#tema_renungan").val();
    var judul_renungan = $("#judul_renungan").val();
    var alkitab_1 = $("#alkitab_1").val();
    var alkitab_2 = $("#alkitab_2").val();
    var tanggal_renungan = $("#tanggal_renungan").val();
    var penulis_renungan = $("#penulis_renungan").val();
    var editor = $("#editor").val();
    var gambar = $("#gambar").val();
    var renungan = $("#renungan").val();
    var id_renungan = $("#id_renungan").val();

    if(tema_renungan=='' || judul_renungan=='' || alkitab_1=='' || tanggal_renungan=='' || penulis_renungan=='' || renungan=='')
    {
      alert('Wajib disi yang Bertanda Bintang (*).');
      return false;
    }

    if($('#gambar').val()){
      gambar = UploadGambar();
    }else{
      gambar = '';
    }

    $.ajax({
        type: 'POST',
        url: "{{ url('/') }}/renungan/save",
        datatype: 'JSON',
        data: {id_renungan:id_renungan,tema_renungan:tema_renungan, judul_renungan:judul_renungan, alkitab_1:alkitab_1, alkitab_2:alkitab_2, tanggal_renungan:tanggal_renungan, penulis_renungan:penulis_renungan, editor:editor, gambar:gambar, renungan:renungan},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Successfully !",
                    text: "Data Successfully Saved" ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Done ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "{{ url('/') }}/renungan/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Failed",
                    text: "Data Failed to Save." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Close ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });

</script>
@stop
