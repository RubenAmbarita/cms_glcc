@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.css')}}">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="{{ url('/') }}/dashboard"> Home</a></li>
        <li><a href="{{ url('/') }}/renungan/index"> Renungan</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Renungan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">

            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                <div class="row">
                  <form action="" method="post">
                    {{ csrf_field() }}
                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('bulan') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('tahun') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                </form>
                <form >
                  <div class="col-md-2">
                  <a href="{{action('RenunganController@addrenungan')}}" style="margin-left: 310px;" class="btn btn-success"><i class="fa fa-plus-square"></i> Unggah Renungan</a>
                  </div>
                </form>
                </div>

                <thead>
                <tr>
                <tr>
                  <th style="text-align:center;">Tema Renungan</th>
                  <th style="text-align:center;">Judul Renungan</th>
                  <th style="text-align:center;">Bacaan Alkitab I</th>
                  <th style="text-align:center;">Bacaan Alkitab II</th>
                  <th style="text-align:center;">Tanggal Renungan</th>
                  <th style="text-align:center;"></th>
                  <th style="text-align:center;"></th>

                </tr>
                </tr>
                </thead>
                <tbody>
                  @foreach ($list_renungan as $row)
                  <tr>
                      <td >{{$row->tema_renungan}}</td>
                      <td >{{$row->judul_renungan}}</td>
                      <td >{{$row->bacaan_alkitab1}}</td>
                      <td >{{$row->bacaan_alkitab2}}</td>
                      <td >{{$row->tanggal_renungan}}</td>
                      <td style="text-align:center;">
                        <form action="{{ 'detail' }}" method="post">
                          {!! csrf_field() !!}
                          <input type="hidden" name="id_renungan" value="{{ $row->id_renungan }}">
                          <button class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                        </form>
                      </td>
                      <td style="text-align:center;">
                        <button class="btn btn-danger" id="delete" type="button" onclick="showCancelMessage({{ $row->id_renungan }})" name="button"><i class="fa fa-close"></i> Hapus</button>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th style="text-align:center;">Tema Renungan</th>
                    <th style="text-align:center;">Judul Renungan</th>
                    <th style="text-align:center;">Bacaan Alkitab I</th>
                    <th style="text-align:center;">Bacaan Alkitab II</th>
                    <th style="text-align:center;">Tanggal Renungan</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!--<script src="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.js')}}"></script>-->
<!--<script src="{{url('js/order.js')}}"></script>-->

<script>
  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true,
                    ordering : true
                });
  });

  $( function()
  {
    $( "#tanggal_declare" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });

  });


  function showCancelMessage(id_renungan)
  {
      swal({
          title: "Anda Yakin?",
          text: "Data Akan Dihapus !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "{{ url('/') }}/renungan/delete_renungan",
                  datatype: 'JSON',
                  data: {id_renungan:id_renungan},
                  success: function(data)
                  {
                    if(data == 0 || data == 1)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data Berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Ya ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "{{ url('/') }}/renungan/index";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Gagal",
                            text: "Data Gagal Disimpan" ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "No ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }
</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  $("#submit").click(function(){
    var tema_renungan = $("#tema_renungan").val();
    var judul_renungan = $("#judul_renungan").val();
    var alkitab_1 = $("#alkitab_1").val();
    var alkitab_2 = $("#alkitab_2").val();
    var tanggal_renungan = $("#tanggal_renungan").val();
    var penulis_renungan = $("#penulis_renungan").val();
    var editor = $("#editor").val();
    var gambar = $("#gambar").val();
    var renungan = $("#renungan").val();

    if(tema_renungan=='' || judul_renungan=='' || alkitab_1=='' || tanggal_renungan=='' || penulis_renungan=='' || renungan=='')
    {
      alert('Wajib disi yang Bertanda Bintang (*).');
      return false;
    }

    $.ajax({
        type: 'POST',
        url: "{{ url('/') }}/renungan/save",
        datatype: 'JSON',
        data: {tema_renungan:tema_renungan, judul_renungan:judul_renungan, alkitab_1:alkitab_1, alkitab_2:alkitab_2, tanggal_renungan:tanggal_renungan, penulis_renungan:penulis_renungan, editor:editor, gambar:gambar, renungan:renungan},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Successfully !",
                    text: "Data Successfully Saved" ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Done ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "{{ url('/') }}/renungan/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Failed",
                    text: "Data Failed to Save." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Close ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });

</script>
@stop
