@extends('layouts.applog')
<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

    <br/><br/></br></br><br/><br/><br/>
    <div class="row">&nbsp;</row>
    <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <!--<div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'dashboard')) }}">{{ trans('auth.mpm_header') }}</a>
        </div>-->
        <!-- /.login-logo -->
        <!--<div class="login-box-body" style="background-color:#ffffff; padding: 20px 23px 19px; border:4px solid #ffffff; box-shadow: 4px 4px 4px 4px #888888;">-->
        <!-- <div class="text-center">
                <img style="width:260px;" src="{{url('images/login-logo.png')}}">
        </div> -->

        <div class="login-box-body"  style="background-color:#FFFFFF; padding: 20px 23px 19px; border:2px solid #ffffff; box-shadow: 1px 1px 1px 1px #888888;border-radius: 10px;">
        <div class="text-center">
               <!-- <h1 style="font-family:Poppins;color:#3c8dbc"><b>GCCC</b></h1> -->
                <img style="width:250px;" src="{{url('images/login-logo.jpg')}}">
        </div>
        <!--<div class="text-center">
        <img src="{{url('images/logo.jpg')}}">
        </div><br />
            <p class="login-box-msg">{{ trans('auth.login_message') }}</p>
        -->
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}" >
                    <input type="text" id="username" name="username" class="form-control" value="{{ old('username') }}"
                           placeholder="{{ trans('auth.username') }}" style="font-family:Poppins;border-top:0;border-right:0;border-left:0">
                    <span class="glyphicon glyphicon-user form-control-feedback" style="color:#3c8dbc"></span>
                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('auth.password') }}" style="border-top:0;border-right:0;border-left:0">
                    <span class="glyphicon glyphicon-lock form-control-feedback" style="color:#3c8dbc"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label style="display:none;">
                                <input type="checkbox" name="remember"> {{ trans('auth.remember_me') }}
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                <input type="hidden" id="id_register" name="id_register" value="No">
                <div class="col-xs-12">
                <button type="submit" class="btn btn-warning btn-block btn-flat" style="border-radius: 15px;background-color:#3c8dbc;" style="font-family:poppins;">{{ trans('auth.sign_in') }}</button>
              </div>
            </div>
            </form>
            <!-- <a href="{{action('RegistersController@index')}}" class="btn btn-warning btn-block btn-flat" style="border-radius: 15px;background-color:#3c8dbc;" style="font-family:poppins;">{{ trans('auth.register') }}</a> -->
            <div style="display:none;" class="auth-links">
                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
                   class="text-center"
                >{{ trans('auth.i_forgot_my_password') }}</a>
                <br>
                @if (config('adminlte.register_url', 'registers'))
                    <a href="{{ 'Registers' }}"
                       class="text-center"
                    >{{ trans('auth.register_a_new_membership') }}</a>
                @endif
            </div>
        </div>
      </div>
      <div class="col-md-4"></div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 3 -->
    <script src="{{ asset('adminlte.assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminlte.assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('adminlte.assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    @yield('js')
