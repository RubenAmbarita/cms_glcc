@extends('layouts.applog')
<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
<div style="height:9%;"></div>
<!--<body class="hold-transition login-page">-->
    <div class="login-box">
        <!--<div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'dashboard')) }}">{{ trans('auth.mpm_header') }}</a>
        </div>-->
        <!-- /.login-logo -->
        <div class="login-box-body" style="background-color:#ffffff; padding: 20px 23px 19px; border:4px solid #ffffff; box-shadow: 4px 4px 4px 4px #888888;">
        <div class="text-center">
        <img src="{{url('images/logo.jpg')}}">
        </div><br />
            <p class="login-box-msg">{{ trans('auth.login_message') }}</p>
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}" >
                    <input type="text" id="username" name="username" class="form-control" value="{{ old('username') }}"
                           placeholder="{{ trans('auth.username') }}">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('auth.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label style="display:none;">
                                <input type="checkbox" name="remember"> {{ trans('auth.remember_me') }}
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <input type="hidden" id="id_register" name="id_register" value="No">
                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('auth.sign_in') }}</button>
            </form>
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {!! csrf_field() !!}
                    <input type="hidden" id="username" name="username" value="user">
                    <input type="hidden" id="password" name="password" value="123">
                    <input type="hidden" id="id_register" name="id_register" value="Yes">
                    <button type="submit" class="btn btn-primary btn-block btn-danger">
                        Register
                    </button>
            </form>
            <div class="auth-links">
                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}"
                   class="text-center"
                >{{ trans('auth.i_forgot_my_password') }}</a>
                <br>
                @if (config('adminlte.register_url', 'registers'))
                    <a href="{{ 'Registers' }}"
                       class="text-center"
                    >{{ trans('auth.register_a_new_membership') }}</a>
                @endif
            </div>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->


    <!-- jQuery 3 -->
    <script src="{{ asset('adminlte.assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('adminlte.assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('adminlte.assets/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    @yield('js')
