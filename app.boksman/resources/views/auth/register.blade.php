@extends('layouts.appreg')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Register</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Create</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details of Shipper Account</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                {!! csrf_field() !!}
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="usr_companyname">Company Name</label>
                    <input type="text" id="usr_companyname" name="usr_companyname" class="form-control" value="" placeholder="{{ trans('auth.full_name') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companyaddress">Company Address</label>
                    <textarea id="usr_companyaddress" name="usr_companyaddress" placeholder="Company Address " class="form-control"></textarea>
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companycity">City</label>
                  <input type="text" id="usr_companycity" name="usr_companycity" class="form-control" value="" placeholder="City">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companystate">State</label>
                  <input type="text" id="usr_companystate" name="usr_companystate" class="form-control" value="" placeholder="State">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companycountry">Country</label>
                  <input type="text" id="usr_companycountry" name="usr_companycountry" class="form-control" value="" placeholder="Country">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companyzipcode">Zip Code</label>
                  <input type="text" id="usr_companyzipcode" name="usr_companycountry" class="form-control" value="" placeholder="Zip Code">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companycontactname">Contact Name</label>
                  <input type="text" id="usr_companycontactname" name="usr_companycontactname" class="form-control" value="" placeholder="Contact Name">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
              <div class="form-group has-feedback">
                  <label for="usr_companymobilephone">Phone Number</label>
                  <table style = "width:100%;">
                    <tr>
                        <td>
                            <input type="text" id="usr_companymobilephone" name="usr_companymobilephone" class="form-control" value="" placeholder="Mobile Phone">
                        </td>
                        <td style = "width:10px;"></td>
                        <td>
                            <input type="text" id="usr_companyphoneoffice" name="usr_companyphoneoffice" class="form-control" value="" placeholder="Phone Office">
                        </td>
                    </tr>
                  </table>
                </div>
                <div style="height:80px;" class="form-group has-feedback">
                  <label for="usr_email">Email address</label>
                    <input type="email" id="usr_email" name="usr_email" class="form-control" value=""
                           placeholder="{{ trans('auth.email') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_username">Username</label>
                    <input type="text" id="usr_username" name="usr_username" class="form-control" value="" placeholder="{{ trans('auth.username') }}">
                </div>

                <div class="form-group has-feedback">
                  <label for="usr_password">Password</label>
                    <input type="password" id="usr_password" name="usr_password" class="form-control" placeholder="{{ trans('auth.password') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_password_conf">Password Confirmation</label>
                    <input type="password" id="usr_password_conf" name="usr_password_conf" class="form-control" placeholder="{{ trans('auth.retype_password') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="file_siup">Upload File (SIUP)</label>
                    <input type="file" id="file" name="file" />
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer right">
                <button type="button" id="submit" class="btn btn-primary">Submit</button>
                <button type="button" class="btn btn-danger" onclick="cancel_process()">Cancel</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': 'Qo38ZKw56Hqa3HqLNxMbcxQdqTnOYI8tCDRmagFZMjVMcP1HhFUUxLDp9Gmd'
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });


  function cancel_process()
  {
     $("#a_logout").click();
     //window.history.back();
  }

  function uploadFile()
  {
    var file_data = $('#file').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
        //url: 'upload.php', // point to server-side PHP script
        url: "{{ url('/') }}/register/UploadFile",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            $('#msg').html(response); // display success response from the PHP script
        },
        error: function (response) {
            $('#msg').html('ruben'); // display error response from the PHP script
        }
    });
  }


//$("#example-form").submit(function(){
  $("#submit").click(function(){
     // DECLARE OBJECT
     /*
     usr_companyname, usr_companyaddress, usr_companycity, usr_companystate, usr_companycountry
     usr_companyzipcode, usr_companymobilephone, usr_companyphoneoffice, usr_fullname,
     usr_email, usr_username, usr_password, usr_password_conf, file_siup
     */

    var usr_companyname = $("#usr_companyname").val();
    var usr_companyaddress = $("#usr_companyaddress").val();
    var usr_companycity = $("#usr_companycity").val();
    var usr_companystate = $("#usr_companystate").val();
    var usr_companycountry = $("#usr_companycountry").val();
    var usr_companyzipcode = $("#usr_companyzipcode").val();
    var usr_companymobilephone = $("#usr_companymobilephone").val();
    var usr_companyphoneoffice = $("#usr_companyphoneoffice").val();
    var usr_companycontactname = $("#usr_companycontactname").val();
    var usr_email = $("#usr_email").val();
    var usr_username = $("#usr_username").val();
    var usr_password = $("#usr_password").val();
    var usr_password_conf = $("#usr_password_conf").val();
    var file_siup = $("#file").val();

    if(usr_companyname=='' || usr_companyaddress=='' || usr_companycity=='' || usr_companystate=='' ||
       usr_companycountry=='' || usr_companymobilephone=='' || usr_companyphoneoffice=='' ||
       usr_companycontactname=='' || usr_email=='' || usr_username=='' || usr_password=='' || usr_password_conf==''){
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }

    if(usr_password != usr_password_conf){
        alert('Password no match.');
        $('#usr_password_conf').focus();
        return false;
    }
      $.ajax({
          type:'POST',
          url: "{{ url('/') }}/register/save",
          datatype: 'JSON',
          data: {
                    usr_companyname:usr_companyname, usr_companyaddress:usr_companyaddress,
                    usr_companycity:usr_companycity, usr_companystate:usr_companystate,
                    usr_companycountry:usr_companycountry, usr_companyzipcode:usr_companyzipcode,
                    usr_companymobilephone:usr_companymobilephone, usr_companyphoneoffice:usr_companyphoneoffice,
                    usr_companycontactname:usr_companycontactname, usr_email:usr_email, usr_username:usr_username,
                    usr_password:usr_password, usr_password_conf:usr_password_conf, file_siup:file_siup
          },
          success: function(data) {
              if(data.response){
                alert('Create data successfully');
                uploadFile();
                $("#a_logout").click();
                //window.location.href = "{{ url('/') }}";
              }else{
                //printErrorMsg(data.error);
                alert('Create data failed');
              }
          }
      });
    });
</script>
@endsection
