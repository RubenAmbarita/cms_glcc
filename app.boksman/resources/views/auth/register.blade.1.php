@extends('layouts.applog')
<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">

<body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-box-body" style="background-color:#ffffff; padding: 20px 23px 19px; border:4px solid #ffffff; box-shadow: 4px 4px 4px 4px #888888;">
        <div class="text-center">
        <img src="{{url('images/logo.jpg')}}">
        </div><br />
            <p class="login-box-msg">{{ trans('auth.register_message') }}</p>
            <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback">
                    <input type="text" id="company" name="company" class="form-control" value="{{ old('company') }}" placeholder="{{ trans('auth.company_name') }} *">
                    <span class="fa fa-user-secret form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                <textarea id="address" name="address" placeholder="Company Address *" class="form-control"></textarea>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="city" name="city" class="form-control" value="" placeholder="City *">
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="state" name="state" class="form-control" value="" placeholder="State *">
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="country" name="country" class="form-control" value="" placeholder="Country *">
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="Zipcode">
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Contact Name *">
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="mobile" name="mobile" class="form-control" value="" placeholder="Mobile Phone *">
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="phone" name="phone" class="form-control" value="" placeholder="Phone *">
                </div>
                
                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="{{ trans('auth.email') }} *">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="text" id="username" name="username" class="form-control" value="{{ old('username') }}"
                           placeholder="{{ trans('auth.username') }} *">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('auth.password') }} *">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password_confirmation" class="form-control"
                           placeholder="{{ trans('auth.retype_password') }} *">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <button type="button" class="btn btn-primary btn-block btn-flat" onclick="save_process()" >{{ trans('auth.register') }}</button>
            </form>
            <div class="auth-links">
                <a href="{{ url(config('adminlte.login_url', 'login')) }}"
                   class="text-center">{{ trans('auth.i_already_have_a_membership') }}</a>
            </div>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->

<!-- jQuery 3 -->
<script src="{{url('adminlte.assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('adminlte.assets/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>

<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte.assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('adminlte.assets/plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  function save_process()
  {
     // DECLARE OBJECT
     //company, address, city, state, country, zipcode, name, mobile, phone, email, username, password, password_confirmation
     var company = $("#company").val();
     var address = $("#address").val();
     var city = $("#city").val();
     var state = $("#state").val();
     var country = $("#country").val();
     var zipcode = $("#zipcode").val();
     var name = $("#name").val();
     var mobile = $("#mobile").val();
     var phone = $("#phone").val();
     var email = $("#email").val();
     var username = $("#username").val();
     var password = $("#password").val();
     var password_confirmation = $("#password_confirmation").val();

     if(company == '' || address == '' || city == '' || state == '' || 
        country == '' || name == '' || mobile == '' || 
        phone == '' || email == '' || username == '' || password == '' || password_confirmation == ''){
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
     }

      $.ajax({
          type:'POST',
          url: "{{ url('/') }}/Register/save",
          datatype: 'JSON',
          data: {
                 company:company, address:address, city:city, state:state, country:country,
                 zipcode:zipcode, name:name, mobile:mobile, phone:phone, email:email,
                 username:username, password:password, password_confirmation:password_confirmation
                },
          success: function(data) {
              if(data.response){
                alert('Create data successfully');
                window.location = "{{ url('/') }}";
              }else{
                //printErrorMsg(data.error);
                alert('Create data failed');
              }
          }
      });
  }

</script>
</body>
</html>