@extends('layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/Warta">Warta</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Warta</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">

            <!-- /.box-header -->
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                  <a href="#menunggu" aria-controls="menunggu" role="tab" data-toggle="tab">
                    Daftar Tunggu
                  </a>
                </li>
                <li role="presentation">
                  <a href="#aktif" aria-controls="aktif" role="tab" data-toggle="tab">
                    Daftar Terlaksana
                  </a>
                </li>
              </ul><br />

              <!-- Tab panes -->
              <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="menunggu">
              <div class="box-body">
                <table id="tblmenunggu" class="table table-bordered table-striped dataTable display nowrap" style="width:100%;">
                <div class="row">
                  <form action="" method="post">
                    {{ csrf_field() }}
                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('lokasi') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="lokasi" name="lokasi" required >
                      <option value="NOL">Pilih Lokasi Gereja</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('bulan') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('tahun') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                </form>
                <form action="{{'addmessage'}}" method="get">
                  <div>
                  <button style="margin-left: 90px;" id="add" class="btn btn-success" type="submit" name="button"><i class="fa fa-plus-square"></i> Unggah Warta</button>
                  </div>
                </form>
                </div>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Periode Mulai</th>
                    <th style="text-align:center;">Periode Berakhir</th>
                    <th style="text-align:center;">Tema Warta</th>
                    <th style="text-align:center;">Tanggal Publikasi</th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($warta as $row)
                  <tr>
                        <td>{{ $row->lokasi }}</td>
                        <td>{{ $row->periode_mulai }}</td>
                        <td>{{ $row->periode_selesai }}</td>
                        <td>{{ $row->tema }}</td>
                        <td>{{ $row->tgl_publikasi }}</td>
                        <td style="text-align:center;">
                          <form action="{{ 'viewCreditNote' }}" method="post">
                            {!! csrf_field() !!}
                              <button class="btn btn-info" type="submit"><i class="fa fa-file-text-o"></i> Detail</button>
                              <button class="btn btn-warning" type="submit"><i class="fa fa-check"></i> Terlaksana</button>
                          </form>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Periode Mulai</th>
                    <th style="text-align:center;">Periode Berakhir</th>
                    <th style="text-align:center;">Tema Warta</th>
                    <th style="text-align:center;">Tanggal Publikasi</th>
                    <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- tab panel 1 -->

          <div role="tabpanel" class="tab-pane" id="aktif">
              <div class="box-body">
                <table id="tblaktif" class="table table-bordered table-striped dataTable display nowrap" cellspacing="0" width="100%">
                <div class="row">
                  <form action="" method="post">
                    {{ csrf_field() }}
                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('lokasi') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="lokasi" name="lokasi" required >
                      <option value="NOL">Pilih Lokasi Gereja</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('bulan') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback {{ $errors->has('tahun') ? 'has-error' : '' }}">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                </form>
                </div>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Periode Mulai</th>
                    <th style="text-align:center;">Periode Berakhir</th>
                    <th style="text-align:center;">Tema Warta</th>
                    <th style="text-align:center;">Tanggal Publikasi</th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($warta as $row)
                  <tr>
                        <td>{{ $row->lokasi }}</td>
                        <td>{{ $row->periode_mulai }}</td>
                        <td>{{ $row->periode_selesai }}</td>
                        <td>{{ $row->tema }}</td>
                        <td>{{ $row->tgl_publikasi }}</td>
                        <td style="text-align:center;">
                          <form action="{{ 'viewCreditNote' }}" method="post">
                            {!! csrf_field() !!}
                              <button class="btn btn-info" type="submit"><i class="fa fa-file-text-o"></i> Detail</button>
                              <button class="btn btn-warning" type="submit"><i class="fa fa-check"></i> Terlaksana</button>
                          </form>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Periode Mulai</th>
                    <th style="text-align:center;">Periode Berakhir</th>
                    <th style="text-align:center;">Tema Warta</th>
                    <th style="text-align:center;">Tanggal Publikasi</th>
                    <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- TAB HALAMAN KEDUA -->
          
          </div>
          <!-- tab-content -->

        </div>
        <!-- tab panel -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('javascript')
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function triger_tab()
  {
    $('[href="#aktif"]').tab('show');
  }

  $('.nav-tabs a:last').click(function(){
          $(this).tab('show');
          $('#tblaktif').DataTable().draw();
  });

  $(document).ready(function() {
      $('#tblmenunggu').DataTable({
              scrollX : true,
              scrollCollapse : true
      });


      $('#tblaktif').DataTable({
              scrollX : true,
              scrollCollapse : true
      });
  } );

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  function Accept_Order()
  {
    var gk_order = $("#gk_order").val();
    var id_customer = $("#id_customer").val();

      swal({
          title: "Terima Order ini?",
          text: "Klik tombol Ya untuk melanjutkan proses!" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "{{ url('/') }}/Published/Accept_Order",
                  datatype: 'JSON',
                  data: {gk_order:gk_order, id_customer:id_customer},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil diterima." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "{{ url('/') }}/Published/AcceptShipperOrder/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal diterima." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

@stop
