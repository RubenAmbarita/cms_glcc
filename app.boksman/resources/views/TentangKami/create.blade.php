@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.css')}}">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/renungan/index">Renungan</a></li>
        <li class="active">Add</li>
      </ol>
      <h1 style="font-family:Poppins"> Formulir Penambahan Renungan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Armada Publication</h3>
            </div> -->
            <!-- /.box-header -->

            <form enctype="multipart/form-data" action="" method="post">
              {!! csrf_field() !!}
              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Tema Renungan<b style="color:red;"> *</b></label>
                  <input type="hidden" id="id_renungan" name="" value="">
                  <input id="tema_renungan" type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Judul Renungan<b style="color:red;"> *</b></label>
                  <input id="judul_renungan" type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Bacaan Alkitab I<b style="color:red;"> *</b></label>
                  <input id="alkitab_1" type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Bacaan Alkitab II</label>
                  <input id="alkitab_2" type="text" name="perusahaan" value="" class="form-control">
                </div>
              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Tanggal Renungan<b style="color:red;"> *</b></label>
                  <input id="tanggal_renungan" autocomplete="off" type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Penulis Renungan<b style="color:red;"> *</b></label>
                  <input id="penulis_renungan" type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Editor</label>
                  <input id="editor" type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group">
      						<b>Upload Gambar</b><br/>
      						<input id="gambar" type="file" name="file">
      					</div>
              </div>

              <!-- /.box-header -->
              <div class="box-body pad">

              </div>

            <!-- /.box-header -->
            <div class="box-body pad">
              <label for="">Renungan<b style="color:red;"> *</b></label>
              <form>
                <textarea class="textarea" id="renungan" placeholder="Ketik ..."
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <button style="float:right" id="submit" class="btn btn-info" name="button"><i class="fa fa-check"></i> Simpan Renungan</button>
            </div>

          </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!--<script src="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.js')}}"></script>-->
<!--<script src="{{url('js/order.js')}}"></script>-->
<!-- CK Editor -->
<script src="{{url('adminlte.assets/bower_components/ckeditor/ckeditor.js')}}"></script>
<script src="{{url('js/bootstrap-fileinput.js')}}"></script>

<script>
$(function () {
  $('.textarea').wysihtml5()
})

  $( function()
  {
    $( "#tanggal_renungan" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });

  });

</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  $("#submit").click(function(){
    var tema_renungan = $("#tema_renungan").val();
    var judul_renungan = $("#judul_renungan").val();
    var alkitab_1 = $("#alkitab_1").val();
    var alkitab_2 = $("#alkitab_2").val();
    var tanggal_renungan = $("#tanggal_renungan").val();
    var penulis_renungan = $("#penulis_renungan").val();
    var editor = $("#editor").val();
    var gambar = $("#gambar").val();
    var renungan = $("#renungan").val();
    var id_renungan = $("#id_renungan").val();

    if(tema_renungan=='' || judul_renungan=='' || alkitab_1=='' || tanggal_renungan=='' || penulis_renungan=='' || renungan=='')
    {
      alert('Wajib disi yang Bertanda Bintang (*).');
      return false;
    }

    $.ajax({
        type: 'POST',
        url: "{{ url('/') }}/renungan/save",
        datatype: 'JSON',
        data: {id_renungan:id_renungan,tema_renungan:tema_renungan, judul_renungan:judul_renungan, alkitab_1:alkitab_1, alkitab_2:alkitab_2, tanggal_renungan:tanggal_renungan, penulis_renungan:penulis_renungan, editor:editor, gambar:gambar, renungan:renungan},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Successfully !",
                    text: "Data Successfully Saved" ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Done ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "{{ url('/') }}/renungan/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Failed",
                    text: "Data Failed to Save." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Close ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });

</script>
@stop
