@extends('layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/TentangKami">Tentang Kami</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins"> Tentang Kami </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
          <div class="box-header" align="right">
            <a href="{{action('LokasiGerejaController@create')}}" class="btn btn-success"> <i class="fa fa-plus-square"></i> Tambah Tentang Kami </a>
          </div>
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Judul</th>
                    <th style="text-align:center;">Penulis</th>
                    <th style="text-align:center;">Editor</th>
                    <th style="text-align:center; width:100px;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($tentangKami as $row)
                    <tr>
                        <td>{{ $row->judul }}</td>
                        <td>{{ $row->penulis }}</td>
                        <td>{{ $row->editor}}</td>
                        <td style="text-align:center; width:100px;">
                          <form action="{{ 'viewhistory' }}" method="post">
                            {!! csrf_field() !!}
                              <button class="btn btn-success " type="submit"><i class="fa fa-file-text-o"></i> Detail</button>
                              <button class="btn btn-danger" type="submit"><i class="fa fa-close"></i> Hapus</button>
                          </form>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Judul</th>
                      <th style="text-align:center;">Penulis</th>
                      <th style="text-align:center;">Editor</th>
                      <th style="text-align:center; width:100px;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('javascript')
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_publish)
  {
      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "{{ url('/') }}/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "{{ url('/') }}/Published/nomatched/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

@stop
