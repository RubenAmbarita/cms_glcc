@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.css')}}">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
@stop

@section('content')
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div style="font-family:Poppins;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{url('/Keanggotaan')}}">Keanggotaan</a></li>
        <li class="active">Detail</li>
      </ol>
      <h1 style="font-family:Poppins;" >
       Form Keanggotaan
        <!-- <small>New</small> -->
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url(config('adminlte.register_url', 'Keanggotaan/detail')) }}" method="post">
                {!! csrf_field() !!}
              <div class="box-body col-md-6">

                <div class="form-group has-feedback">
                  <label for="name">Nama <b style="color:red;"> *</b></label>
                    <input type="hidden" id="id_user" value="{{$detail->id}}"> 
                    <input type="text" id="name" name="name" value="{{$detail->name}}" class="form-control" required>
                </div>

                <div class="form-group has-feedback">
                  <label for="jenis_kelamin">Jenis Kelamin </label>
                  <select class="form-control select2" id="jenis_kelamin" name="jenis_kelamin" style="width: 100%;">
                  <option value="">Pilih Jenis Kelamin</option>
                  <option>Laki-laki</option>
                  <option>Perempuan</option>
                  </select>
                  @if ($errors->has('jenis_kelamin'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('jenis_kelamin') }}</strong>
                              </span>
                  @endif
               </div>

                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="tempat_lahir">Tempat Lahir </label>
                    <input type="text" id="tempat_lahir" name="tempat_lahir" autocomplete="off" value="{{$detail->tempat_lahir}}" class="form-control" >
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="tanggal_lahir">Tanggal Lahir </label>
                    <input type="text" id="tanggal_lahir" name="tanggal_lahir" autocomplete="off" value="{{$detail->tanggal_lahir}}" class="form-control" >
                </div>
                </div>
                </div>

                <div class="form-group has-feedback">
                  <label for="adress">Alamat<b style="color:red;"> *</b></label>
                    <textarea type="text" id="adress" name="adress" value="" class="form-control" required>{{$detail->adress}}</textarea>
                </div>
                
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group has-feedback {{ $errors->has('provinsi') ? 'has-error' : '' }}">
                          <label for="provinsi">Provinsi<b style="color:red;"> *</b></label>
                          <!-- default negara Indonesia -->
                          <input type="hidden" id="negara" name="negara" class="form-control" value="Indonesia" placeholder="Negara">
                          <select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;" required >
                          <option value="">Pilih Provinsi</option>
                          @foreach ($provinsi as $row)
                                <option value="{{$row->pro_id}}" {{ (old("provinsi") == $row->pro_id ? "selected":"") }}>{{ $row->pro_name }}</option>
                              @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('provinsi') }}</strong>
                                  </span>
                            @endif
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback {{ $errors->has('kokab') ? 'has-error' : '' }}">
                          <label for="kokab">Kota/Kabupaten<b style="color:red;"> *</b></label>
                          <select class="form-control select2" id="kokab" name="kokab" style="width: 100%;" required >
                          <option value="">Pilih Kota/Kabupaten</option>
                          </select>
                          @if ($errors->has('kokab'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('kokab') }}</strong>
                                </span>
                          @endif
                  </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                <label for="kecamatan">Kecamatan <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" required>
                  <option value="">Pilih Kecamatan</option>
                  </select>
                  @if ($errors->has('kecamatan'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('kecamatan') }}</strong>
                              </span>
                  @endif
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="kelurahan">Kelurahan <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required>
                  <option value="">Pilih Kelurahan</option>
                  </select>
                  @if ($errors->has('kelurahan'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('kelurahan') }}</strong>
                              </span>
                  @endif
                </div>
                </div>
                </div>

                <div class="form-group has-feedback">
                  <label for="kode_pos">Kode Pos </label>
                    <Input type="text" id="kode_pos" name="kode_pos" value="{{$detail->kode_pos}}"class="form-control" >
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="pekerjaan">Pekerjaan </label>
                    <input type="text" id="pekerjaan" name="pekerjaan" value="{{$detail->pekerjaan}}"class="form-control" >
                </div>

                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="no_telp"># Handphone </label>
                    <input type="tel" id="no_telp" name="no_telp" value="{{$detail->no_telp}}"class="form-control" disabled>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="email">Email </label>
                    <input type="email" id="email" name="email" value="{{$detail->email}}"class="form-control" disabled>
                </div>
                </div>
                </div>

                <div class="form-group has-feedback">
                  <label for="gol_darah">Golongan Darah</label>
                  <select class="form-control select2" id="gol_darah" name="gol_darah" style="width: 100%;">
                  <option value="">Pilih Golongan Darah</option>
                  <option>A</option>
                  <option>AB</option>
                  <option>B</option>
                  <option>0</option>
                  </select>
                  @if ($errors->has('gol_darah'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('gol_darah') }}</strong>
                              </span>
                  @endif
                  <!-- <input type="text" id="gol_darah" name="gol_darah" value="{{$detail->gol_darah}}" class="form-control" > -->
                </div>

                <div class="form-group has-feedback">
                  <label for="status_nikah">Status Pernikahan</label>
                  <select class="form-control select2" id="status_nikah" name="status_nikah" style="width: 100%;">
                  <option value="">Pilih Status Pernikahan</option>
                  <option>Belum Menikah</option>
                  <option>Telah Menikah</option>
                  </select>
                  @if ($errors->has('status_nikah'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('status_nikah') }}</strong>
                              </span>
                  @endif
                  <!-- <input type="text" id="status_nikah" name="status_nikah" value="{{$detail->status_nikah}}" class="form-control" > -->
                </div>

                <div class="form-group has-feedback">
                <br/>
                  <label for="pendidikan_akhir">Pendidikan Terakhir</label>
                  <select class="form-control select2" id="pendidikan_akhir" name="pendidikan_akhir" style="width: 100%;">
                  <option value="">Pilih Pendidikan Terakhir</option>
                  <option>SD/SMP/SMA</option>
                  <option>Diploma</option>
                  <option>S1</option>
                  <option>S2</option>
                  <option>S3</option>
                  <option>Pendidikan Lainnya</option>
                  </select>
                  @if ($errors->has('pendidikan_akhir'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('pendidikan_akhir') }}</strong>
                              </span>
                  @endif
                  <!-- <input type="text" id="pendidikan_akhir" name="pendidikan_akhir" value="{{$detail->pendidikan_akhir}}" class="form-control" > -->
                </div>

                <div class="form-group has-feedback">
                  <label for="jumlahanak">Jumlah Anak</label>
                  <input type="text" id="jumlahanak" name="jumlahanak" value="{{$detail->jumlah_anak}}" class="form-control" required>
                </div>
              
                <div class="form-group has-feedback">
                  <label for="lokasi_gereja">Lokasi Gereja<b style="color:red;"> *</b></label>
                  <select class="form-control select2" id="lokasi_gereja" name="lokasi_gereja" style="width: 100%;"  >
                      <option value="">Pilih Lokasi Gereja</option>
                      @foreach ($lokasi_gereja as $row)
                      <option value="{{$row->id_gereja}}" {{ (old("lokasi_gereja") == $row->id_gereja ? "selected":"") }}>{{ $row->nama_gereja }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('lokasi_gereja'))
                      <span class="help-block">
                        <strong>{{ $errors->first('lokasi_gereja') }}</strong>
                      </span>
                   @endif
                </div>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="box-footer" align="right">
        <button style="float:right" class="btn btn-info" id="submit" name="button"><i class="fa fa-check"></i> Simpan Perubahan</button>
        <!-- <button type="button" class="btn btn-danger" onclick="cancel_process()"><i class="fa fa-times"></i> Cancel</button> -->
      </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });

  $( function()
  {
    $( "#tanggal_lahir" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });

  });

  function cancel_process()
  {
     window.history.back();
  }

  function showVerificationMessage(id)
  {
      swal({
          title: "Verifikasi Data Anggota?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Verifikasi",
          closeOnConfirm: true,
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "{{ url('/') }}/Keanggotaan/update_status",
                  datatype: 'JSON',
                  data: {id:id},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Sukses!",
                          text: "Data berhasil diverifikasi." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "OK ",
                          closeOnConfirm: true,
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "{{ url('/') }}/Keanggotaan";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Oops!",
                            text: "Data gagal diverifikasi." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            closeOnConfirm: true,
                          }, function (isConfirm) {
                              if (isConfirm) {

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();
  $("#submit").click(function(){
    var id_user = $("#id_user").val();
    var name = $("#name").val();
    var jenis_kelamin = $("#jenis_kelamin").val();
    var tempat_lahir = $("#tempat_lahir").val();
    var tanggal_lahir = $("#tanggal_lahir").val();
    var adress = $("#adress").val();
    var provinsi = $("#provinsi").val();
    var kokab = $("#kokab").val();
    var kecamatan = $("#kecamatan").val();
    var kelurahan = $("#kelurahan").val();
    var kode_pos = $("#kode_pos").val();
    var pekerjaan = $("#pekerjaan").val();
    var gol_darah = $("#gol_darah").val();
    var status_nikah = $("#status_nikah").val();
    var jumlahanak = $("#jumlahanak").val();
    var lokasi_gereja = $("#lokasi_gereja").val();

    if(name=='' || adress=='' || provinsi=='' || kokab=='' || kecamatan=='' || kelurahan==''|| lokasi_gereja=='')
    {
      swal('Wajib diisi yang kolom bertanda bintang (*).');
      return false;
    }

    $.ajax({
        type: 'POST',
        url: "{{ url('/') }}/Keanggotaan/save",
        datatype: 'JSON',
        data: {id_user:id_user, name:name, jenis_kelamin:jenis_kelamin, tempat_lahir:tempat_lahir, tanggal_lahir:tanggal_lahir, adress:adress, provinsi:provinsi, 
              kokab:kokab, kecamatan:kecamatan, kelurahan:kelurahan, kode_pos:kode_pos, pekerjaan:pekerjaan, 
              gol_darah:gol_darah, status_nikah:status_nikah, jumlahanak:jumlahanak, lokasi_gereja:lokasi_gereja},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Sukses !",
                    text: "Data Berhasil disimpan." ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "{{ url('/') }}/Keanggotaan";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Gagal",
                    text: "Data Gagal disimpan." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Tutup ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });

  $("select[name='provinsi']").change(function(){
    var p_param = $(this).val();

    $.ajax({
        type: 'POST',
        url: "{{ url('/') }}/Keanggotaan/get_city",
        datatype: 'JSON',
        data: {p_param:p_param},
        success: function(data) {
          $("#kokab option").remove();
          $("#kokab option").val();

          $( "#kokab" ).append(
              $('<option></option>').val("").html("Pilih Kota/Kabupaten")
          );

          var i = 0;
          $.each(data, function()
          {
            $( "#kokab" ).append(
                $('<option></option>').val(data[i].reg_id).html(data[i].reg_name)
            );
            i++;
          });
        }
    });
    });


      $("select[name='kokab']").change(function(){
      var p_param = $(this).val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Keanggotaan/get_district",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#kecamatan option").remove();
            $("#kecamatan option").val();

            $( "#kecamatan" ).append(
                $('<option></option>').val("").html("Pilih Kecamatan")
            );

            var i = 0;
            $.each(data, function()
            {
              $( "#kecamatan" ).append(
                  $('<option></option>').val(data[i].dis_id).html(data[i].dis_name)
              );
              i++;
            });
          }
      });
    });

    $("select[name='kecamatan']").change(function(){
      var p_param = $(this).val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Keanggotaan/get_subdistrict",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#kelurahan option").remove();
            $("#kelurahan option").val();

            $( "#kelurahan" ).append(
                $('<option></option>').val("").html("Pilih Kelurahan")
            );

            var i = 0;
            $.each(data, function()
            {
              $( "#kelurahan" ).append(
                  $('<option></option>').val(data[i].sub_id).html(data[i].sub_name)
              );
              i++;
            });
          }
      });
    });


</script>
@endsection
