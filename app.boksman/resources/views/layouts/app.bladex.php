<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name='csrf-token' content="{{csrf_token()}}" />
    <title>
      @yield('title_prefix', config('adminlte.title_prefix', ''))
      @yield('title', config('adminlte.title', 'Shipper || bagibagi.Com'))
      @yield('title_postfix', config('adminlte.title_postfix', ''))
    </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/x-icon">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/Ionicons/css/ionicons.min.css')}}">

  <!-- DataTables -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

  <!-- Select2 -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/select2/dist/css/select2.min.css')}}">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('adminlte.assets/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{url('adminlte.assets/dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{url('adminlte.assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>MPM</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <!--<img src="{{url('images/bagibagi.png')}}" class="user-image" alt="User Image">-->
        <b>bagi</b>bagi.com
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{url('images/user-icon.png')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
              &nbsp;<i class="fa fa-fw fa-power-off"></i>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{url('images/user-icon.png')}}" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->name }}
                  <small>&nbsp;</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  &nbsp;
                  <!--<a href="#" class="btn btn-default btn-flat"></a>-->
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat"
                      onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Logout
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>

              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('images/user-icon.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class=" @if(url('/jurusan') == request()->url() or url('/prodi') == request()->url() ) active @else '' @endif  treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Program Studi</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{URL::to('jurusan')}}"><i class="fa fa-circle-o"></i> Jurusan </a></li>
                <li><a href="{{URL::to('prodi')}}"><i class="fa fa-circle-o"></i> Program Studi </a></li>
               
              </ul>
            </li>
            <li class="@if(url('/kurikulum') == request()->url() or url('/kurikulum/matakuliah') == request()->url() ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Kurikulum</span> <i class="fa fa-angle-left pull-right"></i>
               
              </a>
              <ul class="treeview-menu">
                <li><a href="{{URL::to('kurikulum')}}"><i class="fa fa-circle-o"></i> Data Kurikulum </a></li>
                <li><a href="{{URL::to('kurikulum/matakuliah')}}"><i class="fa fa-circle-o"></i> Matakuliah </a></li>
              </ul>
            </li>
            <li class="@if(url('/semester') == request()->url() or url('/semester/semesterprodi') == request()->url() ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Semester</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{URL::to('semester')}}}"><i class="fa fa-circle-o"></i> Setting Semester</a></li>
                <li><a href="{{{URL::to('semester/semesterprodi')}}}"><i class="fa fa-circle-o"></i> Semester Prodi</a></li>
              </ul>
            </li>
            <li class="@if(url('/dosen') == request()->url()) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Dosen</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{URL::to('dosen')}}}"><i class="fa fa-circle-o"></i> Data Dosen</a></li>
                
                
              </ul>
            </li>
            <li class="@if(url('/mahasiswa') == request()->url()  ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Mahasiswa</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{URL::to('mahasiswa')}}}"><i class="fa fa-circle-o"></i> Data Mahasiswa</a></li>  
              </ul>
            </li>
            <li class="@if(url('/kelas/register/kelaspeserta') == request()->url() ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Rencana Studi</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{route('kelas.klspeserta.proses')}}}"><i class="fa fa-circle-o"></i> Register Kelas Peserta</a></li>
                
              </ul>
            </li>
            <li class="@if(url('/kelas/mhsregister') == request()->url() or url('/kelas/register') == request()->url() or url('/kelas') == request()->url() ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Kelas</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{route('kelas.mhsregister')}}}"><i class="fa fa-circle-o"></i> Mahasiswa Register</a></li>
                <li><a href="{{{URL::to('kelas/register')}}}"><i class="fa fa-circle-o"></i> Register Kelas</a></li>
                <li><a href="{{{URL::to('kelas')}}}"><i class="fa fa-circle-o"></i> Dosen Kelas</a></li> 
                               
                <!-- <li><a href="{{{URL::to('kelas/9/prodi')}}}"><i class="fa fa-circle-o"></i> Kelas Untuk Prodi tertentu</a></li> -->
              
              </ul>
            </li>
            <li class="@if(url('/nilai/jenis') == request()->url() or url('/kelas/input/nilai') == request()->url() ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-file-text"></i>
                <span>Hasil Studi</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{URL::to('nilai/jenis')}}}"><i class="fa fa-circle-o"></i> Jenis Nilai</a></li>
                <li><a href="{{{route('kelas.input.nilai')}}}"><i class="fa fa-circle-o"></i> Input Nilai</a></li>               
              
              </ul>
            </li>
            <li class="@if(url('/accountmahasiswa') == request()->url() or url('/accountdosen') == request()->url() ) active @else '' @endif treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>User Management</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{{URL::to('accountmahasiswa')}}}"><i class="fa fa-circle-o"></i> Akun Mahasiswa</a></li>
                <li><a href="{{{URL::to('accountdosen')}}}"><i class="fa fa-circle-o"></i> Akun Dosen</a></li> 
                       
              
              </ul>
            </li>   
          </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  @yield('content')
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2017 <a href="https://adminlte.io">bagbagi.Com || MPMLog</a>.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{url('adminlte.assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{url('adminlte.assets/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- DataTables -->
<script src="{{url('adminlte.assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('adminlte.assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

<!-- Select2 -->
<script src="{{url('adminlte.assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<!-- Bootstrap 3.3.7 -->
<script src="{{url('adminlte.assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{url('adminlte.assets/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{url('adminlte.assets/bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{url('adminlte.assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{url('adminlte.assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{url('adminlte.assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('adminlte.assets/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{url('adminlte.assets/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('adminlte.assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{url('adminlte.assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('adminlte.assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('adminlte.assets/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes)
<script src="{{url('adminlte.assets/dist/js/pages/dashboard.js')}}"></script> -->
<!-- AdminLTE for demo purposes -->
<script src="{{url('adminlte.assets/dist/js/demo.js')}}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  $("select[name='wh_province']").change(function(){
      var province = $(this).val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Warehouse/get_datacity",
          datatype: 'JSON',
          data: {province:province},
          success: function(data) {
            $("#wh_city option").remove();
            var i = 0;
            $.each(data, function()
            {
              $( "#wh_city" ).append(
                  $('<option></option>').val(data[i].kabupaten).html(data[i].kabupaten)
              );
              i++;
            });
          }
      });
  });

$(function(){

   $('#btn_delete').click(function(){
      var wareid = ("#warehouse_id").val();
      alert('Test Button');
    });
    
});


</script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable(
    {
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  });
</script>
</body>
</html>