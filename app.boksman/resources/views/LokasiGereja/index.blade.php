@extends('layouts.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div style="font-family:Poppins;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/LokasiGereja">Lokasi Gereja</a></li>
        <li class="active">List</li>

      </ol>
      <h1 style="font-family:Poppins;">
      Lokasi Gereja
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
          <div class="box-header" align="right">
            <a href="{{action('LokasiGerejaController@create')}}" class="btn btn-success"> <i class="fa fa-plus-square"></i> Tambah Lokasi Gereja </a>
          </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th style="text-align:center;">Nama Lokasi</th>
                  <th style="text-align:center;">Provinsi</th>
                  <th style="text-align:center;">Kota/Kabupaten</th>
                  <th style="text-align:center;">PIC</th>
                  <th style="text-align:center;"># Handphone</th>
                  <th style="text-align:center; width:80px;"></th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($lokasi as $row)
                  <tr>
                      <td>{{ $row->nama_gereja}}</td>
                      <td>{{ $row->provinsi}}</td>
                      <td>{{ $row->kokab}}</td>
                      <td>{{ $row->PIC}}</td>
                      <td>{{ $row->no_telp}}</td>
                      <td style="text-align:center;">
                        <table>
                          <tr>
                            <td>
                              <form action="{{ 'LokasiGereja/edit' }}" method="post">
                                 {!! csrf_field() !!}
                                <button class="btn btn-primary" type="submit"><i class="fa fa-edit"></i> Edit</button>
                              </form>
                            </td>
                          </tr>
                        </table>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                <th style="text-align:center;">Nama Lokasi</th>
                  <th style="text-align:center;">Provinsi</th>
                  <th style="text-align:center;">Kota/Kabupaten</th>
                  <th style="text-align:center;">PIC</th>
                  <th style="text-align:center;"># Handphone</th>
                  <th style="text-align:center; width:80px;"></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function delete_data($username)
  {
      var deleteconfirm = confirm("Delete Data?");
      if (deleteconfirm == true) {
          $.ajax({
              type: 'POST',
              url: "{{ url('/') }}/LokasiGereja/deleteds",
              datatype: 'JSON',
              data: {username:$username},
              success: function(data) {
                if(data.respon){
                  // alert('Data failed deleted');
                  sweetAlert("Oops...","Deleted data failed.", "error");
                  return false;
                }else{
                  // alert('Data Successfully deleted');
                  sweetAlert("Done!","Deleted data successfully.", "success");
                  window.location.reload();
                }
              }
          });
      } else {

          return false;
      }

  }
</script>
@stop
