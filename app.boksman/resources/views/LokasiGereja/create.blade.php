@extends('layouts.app')

@section('content')
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div style="font-family:Poppins;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{url('/LokasiGereja')}}">Lokasi Gereja</a></li>
        <li class="active">Tambah</li>
      </ol>
      <h1 style="font-family:Poppins;" >
        Lokasi Gereja
        <!-- <small>New</small> -->
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url(config('adminlte.register_url', 'LokasiGereja/save')) }}" method="post">
                {!! csrf_field() !!}
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="nama_gereja">Nama Gereja <b style="color:red;">*</b></label>
                    <input type="text" id="nama_gereja" name="nama_gereja" class="form-control" value="" placeholder="Nama Gereja">
                </div>
                <div class="form-group has-feedback">
                  <label for="alamat">Alamat <b style="color:red;">*</b></label>
                    <textarea type="text" id="alamat" name="alamat" class="form-control" value="" placeholder="eg. Jalan Rambutan No. 11"></textarea>
                </div>
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group has-feedback {{ $errors->has('provinsi') ? 'has-error' : '' }}">
                          <label for="provinsi">Provinsi<b style="color:red;"> *</b></label>
                          <!-- default negara Indonesia -->
                          <input type="hidden" id="negara" name="negara" class="form-control" value="Indonesia" placeholder="Negara">
                          <select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;" required >
                          <option value="">Pilih Provinsi</option>
                          @foreach ($provinsi as $row)
                                <option value="{{$row->pro_id}}" {{ (old("provinsi") == $row->pro_id ? "selected":"") }}>{{ $row->pro_name }}</option>
                              @endforeach
                            </select>
                            @if ($errors->has('provinsi'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('provinsi') }}</strong>
                                  </span>
                            @endif
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback {{ $errors->has('kokab') ? 'has-error' : '' }}">
                          <label for="kokab">Kota/Kabupaten<b style="color:red;"> *</b></label>
                          <select class="form-control select2" id="kokab" name="kokab" style="width: 100%;" required >
                          <option value="">Pilih Kota/Kabupaten</option>
                          </select>
                          @if ($errors->has('kokab'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('kokab') }}</strong>
                                </span>
                          @endif
                  </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                <label for="kecamatan">Kecamatan <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" required>
                  <option value="">Pilih Kecamatan</option>
                  </select>
                  @if ($errors->has('kecamatan'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('kecamatan') }}</strong>
                              </span>
                  @endif
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="kelurahan">Kelurahan <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required>
                  <option value="">Pilih Kelurahan</option>
                  </select>
                  @if ($errors->has('kelurahan'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('kelurahan') }}</strong>
                              </span>
                  @endif
                </div>
                </div>
                </div>
                <div class="form-group has-feedback">
                  <label for="kode_pos">Kode Pos</label>
                    <Input type="text" id="kode_pos" name="kode_pos" class="form-control" value="" placeholder="eg. 12760">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="PIC">Penanggung Jawab <b style="color:red;">*</b></label>
                    <input type="text" id="PIC" name="PIC" class="form-control" value="" placeholder="Nama Penanggung Jawab">
                </div>

                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="no_telp"># Handphone <b style="color:red;">*</b></label>
                    <input type="tel" id="no_telp" name="no_telp" class="form-control" placeholder="eg. 0812123456789">
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="no_telp_2"># Telephone <b style="color:red;">*</b></label>
                    <input type="tel" id="no_telp_2" name="no_telp_2" class="form-control" placeholder="eg. 0212123456789">
                    <br/>
                </div>
                </div>
                </div>
                <div class="form-group has-feedback">
                  <label for="foto_gereja">Upload Foto Gereja</label>
                  <input type="file" id="foto_gereja"  name="foto_gereja" value="">
                </div>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="box-footer" align="right">
        <button type="button" id="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        <!-- <button type="button" class="btn btn-danger" onclick="cancel_process()"><i class="fa fa-times"></i> Cancel</button> -->
      </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });


  function cancel_process()
  {
     window.history.back();
  }

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#submit").click(function(){
     // DECLARE OBJECT
     //usr_fullname, usr_email, usr_privileges, usr_username, usr_password, usr_password_conf
     var nama_gereja = $("#nama_gereja").val();
     var alamat = $("#alamat").val();
     var kelurahan = $("#kelurahan").val();
     var kecamatan = $("#kecamatan").val();
     var kokab = $("#kokab").val();
     var provinsi = $("#provinsi").val();
     var negara = $("#negara").val();
     var kode_pos = $("#kode_pos").val();
     var PIC = $("#PIC").val();
     var no_telp = $("#no_telp").val();
     var no_telp_2 = $("#no_telp_2").val();
     var str_foto_gereja = $("#foto_gereja").val();
     var foto_gereja_replace = str_foto_gereja.replace(/C:|fakepath./g,'');
     var foto_gereja = foto_gereja_replace.replace(/\\/g,'');

     if(nama_gereja=='' || alamat=='' || kelurahan=='' || kecamatan=='' || kokab=='' || provinsi=='' || PIC=='' || no_telp=='' || no_telp_2==''){
        sweetAlert("Oops...","Please fill in all the required fields marked with asterisks (*.", "error");
        // alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
     }
      $.ajax({
          type:'POST',
          url: "{{ url('/') }}/LokasiGereja/save",
          datatype: 'JSON',
          data: {nama_gereja:nama_gereja, alamat:alamat, kelurahan:kelurahan, kecamatan:kecamatan, kokab:kokab, provinsi:provinsi, PIC:PIC, no_telp:no_telp, no_telp_2:no_telp_2},
          success: function(data) {
              if(data.response){
                sweetAlert("Done!","Create data successfully.", "success");
                // alert('Create data successfully');
                window.location = "{{ url('/') }}/LokasiGereja";
              }else{
                sweetAlert("Oops...","Create data failed.", "error");
                // alert('Create data failed');
              }
          }
      });
  });

  $("select[name='provinsi']").change(function(){
    var p_param = $(this).val();

    $.ajax({
        type: 'POST',
        url: "{{ url('/') }}/LokasiGereja/get_city",
        datatype: 'JSON',
        data: {p_param:p_param},
        success: function(data) {
          $("#kokab option").remove();
          $("#kokab option").val();

          $( "#kokab" ).append(
              $('<option></option>').val("").html("Pilih Kota/Kabupaten")
          );

          var i = 0;
          $.each(data, function()
          {
            $( "#kokab" ).append(
                $('<option></option>').val(data[i].reg_id).html(data[i].reg_name)
            );
            i++;
          });
        }
    });
    });


      $("select[name='kokab']").change(function(){
      var p_param = $(this).val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/LokasiGereja/get_district",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#kecamatan option").remove();
            $("#kecamatan option").val();

            $( "#kecamatan" ).append(
                $('<option></option>').val("").html("Pilih Kecamatan")
            );

            var i = 0;
            $.each(data, function()
            {
              $( "#kecamatan" ).append(
                  $('<option></option>').val(data[i].dis_id).html(data[i].dis_name)
              );
              i++;
            });
          }
      });
    });

    $("select[name='kecamatan']").change(function(){
      var p_param = $(this).val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/LokasiGereja/get_subdistrict",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#kelurahan option").remove();
            $("#kelurahan option").val();

            $( "#kelurahan" ).append(
                $('<option></option>').val("").html("Pilih Kelurahan")
            );

            var i = 0;
            $.each(data, function()
            {
              $( "#kelurahan" ).append(
                  $('<option></option>').val(data[i].sub_id).html(data[i].sub_name)
              );
              i++;
            });
          }
      });
    });

</script>
@endsection
