@extends('layouts.appreg')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Register Account
        <small>Entry</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Register</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Create</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Details of Shipper Account</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                {!! csrf_field() !!}
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="usr_companyname">Company Name</label>
                    <input type="text" id="usr_companyname" name="usr_companyname" class="form-control" value="" placeholder="{{ trans('auth.full_name') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companyaddress">Company Address</label>
                    <textarea id="geocomplete" name="usr_companyaddress" placeholder="Company Address " class="form-control"></textarea>
                </div>
                <div id="id_mapcanvas" class="map_canvas" style="width:100%; display:none;"></div>
                <div class="form-group has-feedback">
                  <label for="usr_companycity">City</label>
                  <input type="text" id="usr_companycity" name="usr_companycity" class="form-control" value="" placeholder="City">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companystate">State</label>
                  <input type="text" id="usr_companystate" name="usr_companystate" class="form-control" value="" placeholder="State">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companycountry">Country</label>
                  <input type="text" id="usr_companycountry" name="usr_companycountry" class="form-control" value="" placeholder="Country">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companyzipcode">Zip Code</label>
                  <input type="text" id="usr_companyzipcode" name="usr_companycountry" class="form-control" value="" placeholder="Zip Code">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_companycontactname">Contact Name</label>
                  <input type="text" id="usr_companycontactname" name="usr_companycontactname" class="form-control" value="" placeholder="Contact Name">
                </div> 
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
              <div class="form-group has-feedback">
                  <label for="usr_companymobilephone">Phone Number</label>
                  <table style = "width:100%;">
                    <tr>
                        <td>
                            <input type="text" id="usr_companymobilephone" name="usr_companymobilephone" class="form-control" value="" placeholder="Mobile Phone">
                        </td>
                        <td style = "width:10px;"></td>
                        <td>
                            <input type="text" id="usr_companyphoneoffice" name="usr_companyphoneoffice" class="form-control" value="" placeholder="Phone Office">
                        </td>
                    </tr>
                  </table>
                </div>
                <div style="height:80px;" class="form-group has-feedback">
                  <label for="usr_email">Email address</label>
                    <input type="email" id="usr_email" name="usr_email" class="form-control" value=""
                           placeholder="{{ trans('auth.email') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_username">Username</label>
                    <input type="text" id="usr_username" name="usr_username" class="form-control" value="" placeholder="{{ trans('auth.username') }}">
                </div>

                <div class="form-group has-feedback">
                  <label for="usr_password">Password</label>
                    <input type="password" id="usr_password" name="usr_password" class="form-control" placeholder="{{ trans('auth.password') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_password_conf">Password Confirmation</label>
                    <input type="password" id="usr_password_conf" name="usr_password_conf" class="form-control" placeholder="{{ trans('auth.retype_password') }}">
                </div>
                <div class="form-group has-feedback">
                  <label for="file_siup">Upload File (SIUP)</label>
                    <input type="file" id="file" name="file" />
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer right">
                <button type="button" id="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" onclick="cancel_process()">Cancel</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('css')
<style>
#geocomplete { width: 500px}
.map_canvas { 
  width: 600px; 
  height: 400px; 
  margin: 10px 20px 10px 0;
}
</style>
@stop

@section('javascript')
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC0Fa_0maHwtWlLKKzZc6lH2szmSrsuHDg"></script>
<script src="{{url('js/jquery.geocomplete.min.js')}}"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });


  function cancel_process()
  {
     $("#a_logout").click();
     //window.history.back();
  }
  
  function uploadFile()
  {
    var file_data = $('#file').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
        //url: 'upload.php', // point to server-side PHP script 
        url: "{{ url('/') }}/Registers/UploadFile",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            $('#msg').html(response); // display success response from the PHP script
        },
        error: function (response) {
            $('#msg').html(response); // display error response from the PHP script
        }
    });
  }


//$("#example-form").submit(function(){
  $("#submit").click(function(){
     // DECLARE OBJECT
     /*
     usr_companyname, usr_companyaddress, usr_companycity, usr_companystate, usr_companycountry
     usr_companyzipcode, usr_companymobilephone, usr_companyphoneoffice, usr_fullname,
     usr_email, usr_username, usr_password, usr_password_conf, file_siup
     */
    //uploadFile();
    //return false;
    var usr_companyname = $("#usr_companyname").val();
    var usr_companyaddress = $("#geocomplete").val();
    var usr_companycity = $("#usr_companycity").val();
    var usr_companystate = $("#usr_companystate").val();
    var usr_companycountry = $("#usr_companycountry").val();
    var usr_companyzipcode = $("#usr_companyzipcode").val();
    var usr_companymobilephone = $("#usr_companymobilephone").val();
    var usr_companyphoneoffice = $("#usr_companyphoneoffice").val();
    var usr_companycontactname = $("#usr_companycontactname").val();
    var usr_email = $("#usr_email").val();
    var usr_username = $("#usr_username").val();
    var usr_password = $("#usr_password").val();
    var usr_password_conf = $("#usr_password_conf").val();
    var str = $("#file").val();
    var file_siup_replace = str.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    if(usr_companyname=='' || usr_companyaddress=='' || usr_companycity=='' || usr_companystate=='' || 
       usr_companycountry=='' || usr_companymobilephone=='' || usr_companyphoneoffice=='' || 
       usr_companycontactname=='' || usr_email=='' || usr_username=='' || usr_password=='' || usr_password_conf==''){
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }

    if(usr_password != usr_password_conf){
        alert('Password no match.');
        $('#usr_password_conf').focus();
        return false;
    }
    $.ajax({
      type: 'POST',
      url: "{{ url('/') }}/Registers/get_lat_longajax",
      datatype: 'JSON',
      data: {usr_companyaddress:usr_companyaddress, usr_companycity:usr_companycity},
      success: function(data) {
        //alert(data);return false;
        if(data=='null'){
           alert('Kordinat tidak ditemukan, pastikan alamat gudang benar.');
           $("#usr_companyaddress").focus();
           return false;
        }
        else{
          $.ajax({
            type:'POST',
            url: "{{ url('/') }}/Registers/save",
            datatype: 'JSON',
            data: {
                      usr_companyname:usr_companyname, usr_companyaddress:usr_companyaddress,
                      usr_companycity:usr_companycity, usr_companystate:usr_companystate,
                      usr_companycountry:usr_companycountry, usr_companyzipcode:usr_companyzipcode,
                      usr_companymobilephone:usr_companymobilephone, usr_companyphoneoffice:usr_companyphoneoffice,
                      usr_companycontactname:usr_companycontactname, usr_email:usr_email, usr_username:usr_username,
                      usr_password:usr_password, usr_password_conf:usr_password_conf, file_siup:file_siup
            },
            success: function(data) {
                if(data.response){
                  alert('Create data successfully');
                  uploadFile();
                  $("#a_logout").click();
                  //window.location.href = "{{ url('/') }}";
                }else{
                  //printErrorMsg(data.error);
                  alert('Create data failed');
                }
            }
          });
        }
      }
    });
  });

  $("#geocomplete").focus(function(){
      $("#id_mapcanvas").show("slow");
  });
    
/*
  $("#geocomplete").blur(function(){
      $("#id_mapcanvas").hide("slow");
  });
*/
  $(function(){
        
        var options = {
          map: ".map_canvas"
        };
        
       // $("#geocomplete").val('indonesia').trigger("geocode");

        $("#geocomplete").geocomplete(options)
          .bind("geocode:result", function(event, result){
            //$.log("Result: " + result.formatted_address);
          })
          .bind("geocode:error", function(event, status){
            //$.log("ERROR: " + status);
          })
          .bind("geocode:multiple", function(event, results){
            //$.log("Multiple: " + results.length + " results found");
          });
        
        /*
        $("#find").click(function(){
          $("#geocomplete").trigger("geocode");
        });
        
        $("#examples a").click(function(){
          $("#geocomplete").val($(this).text()).trigger("geocode");
          return false;
        });
        */
      });

    $( "#geocomplete" ).keypress(function( event ) {
       if ( event.which == 13 ) {
          $("#geocomplete").val($('#geocomplete').val()).trigger("geocode");
          //$("#geocomplete").focus();
       }
    });
  
</script>
@endsection