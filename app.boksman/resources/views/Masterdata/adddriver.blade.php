@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{url('css/bootstrap-fileinput.css')}}">
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/Masterdata/ListDriver">Drivers </a></li>
        <li class="active">Add Driver</li>
      </ol>
      <h1 style="font-family:Poppins"> Add Driver </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Add Driver</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                {!! csrf_field() !!}
              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="company_name">Company Name</label>
                  <input type="text" class="form-control" id="company_name" name="company_name" value="{{$custname}}" placeholder="Nama Perusahaan" disabled>
                  <input type="hidden" class="form-control" id="id_driver" name="id_driver" value="" placeholder="Nama Perusahaan" readonly>
                </div>
                <div class="form-group">
                  <label for="nama_driver">Driver Name<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nama_driver" name="nama_driver" placeholder="Driver Name" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_ktp">KTP #<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nomor_ktp" name="nomor_ktp" placeholder="KTP #" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_sim">SIM #<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nomor_sim" name="nomor_sim" placeholder="SIM #" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_telp">Telephone #<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nomor_telp" name="nomor_telp" placeholder="Telephone #" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="email">Email<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nama_pengguna">Username<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nama_pengguna" name="nama_pengguna" placeholder="Username" onfocus="this.select();" autocomplete="off" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="tanggal_declare">SIM Expired Date<b style="color:red;"> *</b></label>
                  <div class="input-group">
                    <input id="tanggal_declare" autocomplete="off" type="text" name="tanggal_declare" value="" placeholder="Tanggal (dd/mm/yyyy)" class="form-control" >
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="kata_sandi">Password<b style="color:red;"> *</b></label>
                  <input type="password" class="form-control" id="kata_sandi" name="kata_sandi" placeholder="Password" onfocus="this.select();" autocomplete="off" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="ulangkata_sandi">Re-Password<b style="color:red;"> *</b></label>
                  <input type="password" class="form-control" id="ulangkata_sandi" name="ulangkata_sandi" placeholder="Re-Password" onfocus="this.select();" onmouseup="return false;" required>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="alamat">Address</label>
                  <textarea style="height:100px;" class="form-control" id="alamat" name="alamat" placeholder="Address" required></textarea>
                </div>
                <table border="0">
                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload KTP ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_ktp" name="image_ktp"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload SIM<b style="color:red;"> *</b> ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_sim" name="image_sim"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload SKCK ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_skck" name="image_skck"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Photo ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_foto" name="image_foto"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
                <div class="form-group">
                  <label>
                    <input type="checkbox" id="cek" value="1" name="check[]" onchange="checkbox();" class="flat-red">
                    By this I, as representative of this company, declare that all information given here is true.
                  </label>
                </div>
                <table>
                  <tr>
                  <td>
                  </td>
                <td style="width:350px;"></td>
                 <td>
                   <div class="form-group" style="text-align:right;">
                     <button type="button" id="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                   </div>
                </td>
                </tr>
                </table>

              </div>

              <!-- /.box-body -->
              <div class="box-footer" style="text-align:left;">
                &nbsp;
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('css')
<style>
#geocomplete { width: 500px}
.map_canvas {
  width: 600px;
  height: 400px;
  margin: 10px 20px 10px 0;
}
</style>
@stop

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('js/bootstrap-fileinput.js')}}"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask("9999");
  });

  $(function()
    {
      $("#tanggal_declare" ).datepicker({
          minDate: 0,
          dateFormat: "d-m-Y"
      });
  });

  function cancel_process()
  {
    window.location = "{{ url('/') }}/Masterdata/ListDriver/";
  }

  $("#submit").click(function()
  {
      // nama_driver, nomor_ktp, nomor_sim, nomor_telp, email, nama_pengguna, kata_sandi, ulangkata_sandi, image_ktp, image_sim, image_skck, image_foto
      var nama_driver = $("#nama_driver").val();
      var nomor_ktp = $("#nomor_ktp").val();
      var nomor_sim = $("#nomor_sim").val();
      var nomor_telp = $("#nomor_telp").val();
      var email = $("#email").val();
      var nama_pengguna = $("#nama_pengguna").val();
      var kata_sandi = $("#kata_sandi").val();
      var ulangkata_sandi = $("#ulangkata_sandi").val();
      var alamat = $("alamat").val();
      var image_ktp = $("#image_ktp").val();
      var image_sim = $("#image_sim").val();
      var image_skck = $("#image_skck").val();
      var image_foto = $("#image_foto").val();
      var tanggal_declare = $('#tanggal_declare').val();

      var chks = document.getElementsByName('check[]');
      var hasChecked = false;

      for (var i = 0; i < chks.length; i++)
      {
      if (chks[i].checked)
      {
      hasChecked = true;
      break;
      }else {
      alert("Please select term & condition.");
      return false;
        }
      }

      if(nama_driver=='' || nomor_ktp=='' || nomor_sim=='' || nomor_telp=='' || email=='' ||
         nama_pengguna=='' || kata_sandi=='' || ulangkata_sandi=='' || image_sim=='' || tanggal_declare=='')
      {
        if(nama_driver=='')
        {alert('Column Driver Name Cant Empty!'); $("#nama_driver").focus(); return false;}

        if(nomor_ktp=='')
        {alert('Column KTP Number Cant Empty!'); $("#nomor_ktp").focus(); return false;}

        if(nomor_sim=='')
        {alert('Column SIM Number Cant Empty!'); $("#nomor_sim").focus(); return false;}

        if(nomor_telp=='')
        {alert('Column Telp Number Cant Empty!'); $("#nomor_telp").focus(); return false;}

        if(email=='')
        {alert('Column Email Cant Empty!'); $("#email").focus(); return false;}

        if(nama_pengguna=='')
        {alert('Column Username Cant Empty!'); $("#nama_pengguna").focus(); return false;}

        if(kata_sandi=='')
        {alert('Column Password Cant Empty!'); $("#kata_sandi").focus(); return false;}

        if(ulangkata_sandi=='')
        {alert('Column Re-Password Cant Empty!'); $("#ulangkata_sandi").focus(); return false;}

        if(image_sim=='')
        {alert('Column SIM Image Cant Empty!'); $("#image_sim").focus(); return false;}

        if(tanggal_declare=='')
        {alert('Column Expired SIM Date Cant Empty!'); $("#image_sim").focus(); return false;}

      }


      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Masterdata/check_ktpsimtelpuser",
          datatype: 'JSON',
          data: {nomor_ktp:nomor_ktp, nomor_sim:nomor_sim, nomor_telp:nomor_telp, nama_pengguna:nama_pengguna},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Data Driver Already Exist!');
              $("#nomor_ktp").focus();
              return false;
            }
            else{
              if($('#image_ktp').val()){
                response_image_ktp = UploadFileKTP();
              }else{
                response_image_ktp = '';
              }

              if($('#image_sim').val()){
                response_image_sim = uploadFileSIM();
              }else{
                response_image_sim = '';
              }

              if($('#image_skck').val()){
                response_image_skck = uploadFileSKCK();
              }else{
                response_image_skck = '';
              }

              if($('#image_foto').val()){
                response_image_foto = uploadFileFoto();
              }else{
                response_image_foto = '';
              }

              Save_Data(response_image_ktp,response_image_sim,response_image_skck,response_image_foto);
            }
          }
      });
  });

  function UploadFileKTP()
  {
    var file_data = $('#image_ktp').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "{{ url('/') }}/Masterdata/UploadFileKTP",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload KTP Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload KTP Failed!');
        //     return false;
        // }
    }); /* AJAX END */
  }
  function uploadFileSIM()
  {
    var file_data = $('#image_sim').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "{{ url('/') }}/Masterdata/UploadFileSIM",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload SIM Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload SIM Failed!');
        //     return false;
        // }
    });
  }

  function uploadFileSKCK()
  {
    var file_data = $('#image_skck').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "{{ url('/') }}/Masterdata/UploadFileSKCK",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload SKCK Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload SKCK Failed!');
        //     return false;
        // }
    });
  }

  function uploadFileFoto()
  {
    var file_data = $('#image_foto').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "{{ url('/') }}/Masterdata/UploadFileFoto",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Foto Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload Foto Failed!');
        //     return false;
        // }
    });
  }

  function Save_Data(response_KTP, response_SIM, response_SKCK, response_Foto)
  {
    var gtime = new Date();
    var cek = $('#cek').val();
    var image_ktp = $('#image_ktp').val();
    var file_ktp_replace = image_ktp.replace(/C:|fakepath./g,'');
    var file_ktp = file_ktp_replace.replace(/\\/g,'');
    var ext_ktp = file_ktp.split('.').pop();
    var image_ktpname = "KTP_" + gtime.getTime() + "_thump." + ext_ktp;
    if(image_ktp == ''){
      var image_ktpname = '';
    }else{
        var image_ktpname = "KTP_" + gtime.getTime() + "_thump." + ext_ktp;
    }

    var image_sim = $('#image_sim').val();
    var file_sim_replace = image_sim.replace(/C:|fakepath./g,'');
    var file_sim = file_sim_replace.replace(/\\/g,'');
    var ext_sim = file_sim.split('.').pop();
    var image_simname = "SIM_" + gtime.getTime() + "_thump." + ext_sim;
    if(image_sim == ''){
      var image_simname = '';
    }else{
        var image_simname = "KTP_" + gtime.getTime() + "_thump." + ext_sim;
    }

    var image_skck = $('#image_skck').val();
    var file_skck_replace = image_skck.replace(/C:|fakepath./g,'');
    var file_skck = file_skck_replace.replace(/\\/g,'');
    var ext_skck = file_skck.split('.').pop();
    var image_skckname = "SIM_" + gtime.getTime() + "_thump." + ext_skck;
    if(image_skck == ''){
      var image_skckname = '';
    }else{
        var image_skckname = "KTP_" + gtime.getTime() + "_thump." + ext_skck;
    }

    var image_foto = $('#image_foto').val();
    var file_foto_replace = image_foto.replace(/C:|fakepath./g,'');
    var file_foto = file_foto_replace.replace(/\\/g,'');
    var ext_foto = file_foto.split('.').pop();
    var image_fotoname = "SIM_" + gtime.getTime() + "_thump." + ext_foto;
    if(image_foto == ''){
      var image_fotoname = '';
    }else{
        var image_fotoname = "KTP_" + gtime.getTime() + "_thump." + ext_foto;
    }

      var nama_driver = $("#nama_driver").val();
      var nomor_ktp = $("#nomor_ktp").val();
      var nomor_sim = $("#nomor_sim").val();
      var nomor_telp = $("#nomor_telp").val();
      var email = $("#email").val();
      var nama_pengguna = $("#nama_pengguna").val();
      var kata_sandi = $("#kata_sandi").val();
      var ulangkata_sandi = $("#ulangkata_sandi").val();
      var alamat = $("#alamat").val();
      var id_driver = $("#id_driver").val();
      var tanggal_declare = $('#tanggal_declare').val();

      $.ajax({
        type:'POST',
        url: "{{ url('/') }}/Masterdata/saveDriver",
        datatype: 'JSON',
        data: {cek:cek, nama_driver:nama_driver, nomor_ktp:nomor_ktp, nomor_sim:nomor_sim, nomor_telp:nomor_telp, tanggal_declare:tanggal_declare,
               email:email, nama_pengguna:nama_pengguna, kata_sandi:kata_sandi, ulangkata_sandi:ulangkata_sandi, alamat:alamat,
               image_ktp:image_ktpname, image_sim:image_simname, image_skck:image_skckname, image_foto:image_fotoname, id_driver:id_driver},
        success: function(data) {
          if(data==true){
            swal({
                      title: "",
                      text: "Data Saved Successfully" ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Finish ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                          window.location.href = "{{ url('/') }}/Masterdata/ListDriver";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
            });
          }
          else
          {
            swal({
                      title: "",
                      text: "Data Failed to Save" ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Close ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
          }
        }
      });

  }

  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Username trucker tidak tersedia atau sudah digunakan,  silakan cari username lain!');
                $("#username").focus();
                return false;
              }
          }
      });
  }

</script>
@endsection
