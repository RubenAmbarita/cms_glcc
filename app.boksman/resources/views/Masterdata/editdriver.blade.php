
@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{url('css/bootstrap-fileinput.css')}}">
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/Masterdata/ListDriver">Drivers</a></li>
        <li class="active">Edit</li>
      </ol>
      <h1> Edit Driver </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Edit Driver</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                {!! csrf_field() !!}
              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="company_name">Company Name</label>
                  <input type="text" class="form-control" id="company_name" name="company_name" value="{{$custname}}" placeholder="Nama Perusahaan" disabled>
                  <input type="hidden" class="form-control" id="id_driver" name="id_driver" value="" placeholder="Nama Perusahaan" readonly>
                </div>
                <div class="form-group">
                  <label for="nama_driver">Driver Name <b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nama_driver" name="nama_driver" placeholder="Driver Name" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_ktp">KTP # <b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nomor_ktp" name="nomor_ktp" placeholder="KTP Number" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_sim">SIM #<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nomor_sim" name="nomor_sim" placeholder="SIM Number" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_telp">Telp # <b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nomor_telp" name="nomor_telp" placeholder="Telephone Number" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="email">Email <b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nama_pengguna">Username <b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nama_pengguna" name="nama_pengguna" placeholder="Username" onfocus="this.select();" autocomplete="off" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="tanggal_declare">SIM Expired Date<b style="color:red;"> *</b></label>
                  <div class="input-group">
                    <input id="tanggal_declare" autocomplete="off" type="text" name="tanggal_declare" value="" placeholder="Tanggal (dd/mm/yyyy)" class="form-control" >
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="kata_sandi">Password <b style="color:red;"> *</b></label>
                  <input type="password" class="form-control" id="kata_sandi" name="kata_sandi" placeholder="Password" onfocus="this.select();" autocomplete="off" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="ulangkata_sandi">Re-Password <b style="color:red;"> *</b></label>
                  <input type="password" class="form-control" id="ulangkata_sandi" name="ulangkata_sandi" placeholder="Re-Password" onfocus="this.select();" onmouseup="return false;" required>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="alamat">Address</label>
                  <textarea style="height:100px;" class="form-control" id="alamat" name="alamat" placeholder="Address" required></textarea>
                </div>
                <table border="0">
                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload KTP ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="{{url('assets/driver')}}/{{$driver->image_ktp}}" alt="" />
                            <input type="hidden" id="image_ktpname" name="image_ktpname" value="{{$driver->image_ktp}}"> </span>
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_ktp" name="image_ktp"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload SIM ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                          <img src="{{url('assets/driver')}}/{{$driver->image_sim}}" alt="" />
                          <input type="hidden" id="image_simname" name="image_simname" value="{{$driver->image_sim}}"> </span>
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_sim" name="image_sim" > </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload SKCK ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                          <img src="{{url('assets/driver')}}/{{$driver->image_skck}}" alt="" />
                          <input type="hidden" id="image_skckname" name="image_skckname" value="{{$driver->image_skck}}"> </span>
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_skck" name="image_skck" > </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Photo ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                          <img src="{{url('assets/driver')}}/{{$driver->image_driver}}" alt="" />
                          <input type="hidden" id="image_fotoname" name="image_fotoname" value="{{$driver->image_driver}}"> </span>
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_foto" name="image_foto" > </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
                <div class="form-group">
                  <label>
                    <input type="checkbox" id="cek" value="1" name="check[]" onchange="checkbox();" class="flat-red">
                    By this I, as representative of this company, declare that all information given here is true.
                  </label>
                </div>
                <div class="form-group" style="text-align:right;"><br /><br />
                  <!-- <button type="button" class="btn btn-success" onclick="cancel_process()"><i class="fa fa-backward"></i> Back</button> -->
                  <button type="button" id="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer" style="text-align:left;">
                &nbsp;
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('css')
<style>
#geocomplete { width: 500px}
.map_canvas {
  width: 600px;
  height: 400px;
  margin: 10px 20px 10px 0;
}
</style>
@stop

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('js/bootstrap-fileinput.js')}}"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask("9999");

    $("#id_driver").val("{{$driver->id_driver}}");
    $("#nama_driver").val("{{$driver->name}}");
    $("#nomor_ktp").val("{{$driver->no_ktp}}");
    $("#nomor_sim").val("{{$driver->no_sim}}");
    $("#nomor_telp").val("{{$driver->no_telp}}");
    $("#email").val("{{$driver->email}}");
    $("#nama_pengguna").val("{{$driver->username}}");
    $("#alamat").val("{{$driver->address}}");
    $("#kata_sandi").val("{{$driver->password}}");
    $("#ulangkata_sandi").val("{{$driver->password}}");
    $("#tanggal_declare").val("{{$driver->expired_sim_date}}")
    //$("#image_ktp").accept("C:\fakepath");
    //$("#image_sim").accept("assets/*");
    //$("#image_skck").accept("assets/*");
    //$("#image_foto").accept("assets/*");
  });

  $(function()
    {
      $("#tanggal_declare" ).datepicker({
          minDate: 0,
          dateFormat: "d-m-Y"
      });
  });

  function cancel_process()
  {
    window.location = "{{ url('/') }}/Masterdata/ListDriver/";
  }

  $("#submit").click(function()
  {
      // nama_driver, nomor_ktp, nomor_sim, nomor_telp, email, nama_pengguna, kata_sandi, ulangkata_sandi, image_ktp, image_sim, image_skck, image_foto
      var nama_driver = $("#nama_driver").val();
      var nomor_ktp = $("#nomor_ktp").val();
      var tanggal_declare = $('#tanggal_declare').val();
      var nomor_sim = $("#nomor_sim").val();
      var nomor_telp = $("#nomor_telp").val();
      var email = $("#email").val();
      var nama_pengguna = $("#nama_pengguna").val();
      var kata_sandi = $("#kata_sandi").val();
      var ulangkata_sandi = $("#ulangkata_sandi").val();
      var alamat = $("alamat").val();
      var image_ktp = $("#image_ktp").val();
      var image_sim = $("#image_sim").val();
      var image_skck = $("#image_skck").val();
      var image_foto = $("#image_foto").val();
      var type_form = $("#type_form").val();

      var chks = document.getElementsByName('check[]');
      var hasChecked = false;

      for (var i = 0; i < chks.length; i++)
      {
      if (chks[i].checked)
      {
      hasChecked = true;
      break;
      }else {
      alert("Please select term & condition.");
      return false;
        }
      }

      if(nama_driver || nomor_ktp=='' || nomor_sim=='' || nomor_telp=='' || email=='' ||
         nama_pengguna=='' || kata_sandi=='' || ulangkata_sandi=='' || image_ktp=='' ||
         image_sim=='' || image_skck=='' || image_foto=='')
      {
        if(nama_driver=='')
        {alert('Column Driver Name Cant Empty!'); $("#nama_driver").focus(); return false;}

        if(nomor_ktp=='')
        {alert('Column KTP Number Cant Empty!'); $("#nomor_ktp").focus(); return false;}

        if(nomor_sim=='')
        {alert('Column SIM Number Cant Empty!'); $("#nomor_sim").focus(); return false;}

        if(nomor_telp=='')
        {alert('Column Telp Number Cant Empty!'); $("#nomor_telp").focus(); return false;}

        if(email=='')
        {alert('Column Email Cant Empty!'); $("#email").focus(); return false;}

        if(nama_pengguna=='')
        {alert('Column Username Cant Empty!'); $("#nama_pengguna").focus(); return false;}

        if(kata_sandi=='')
        {alert('Column Password Cant Empty!'); $("#kata_sandi").focus(); return false;}

        if(ulangkata_sandi=='')
        {alert('Column Re-Password Cant Empty!'); $("#ulangkata_sandi").focus(); return false;}

        /*
        if(image_ktp=='')
        {alert('Kolom Image KTP tidak boleh kosong!'); $("#image_ktp").focus(); return false;}

        if(image_sim=='')
        {alert('Kolom Image SIM tidak boleh kosong!'); $("#image_sim").focus(); return false;}

        if(image_skck=='')
        {alert('Kolom Image SKCK tidak boleh kosong!'); $("#image_skck").focus(); return false;}

        if(image_foto=='')
        {alert('Kolom Image Foto tidak boleh kosong!'); $("#image_foto").focus(); return false;}
        */
      }

      var file_data = $('#image_ktp').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Masterdata/check_ktpsimtelpuser",
          datatype: 'JSON',
          data: {nomor_ktp:nomor_ktp, nomor_sim:nomor_sim, nomor_telp:nomor_telp, nama_pengguna:nama_pengguna, type_form:type_form},
          success: function(data)
          {
            data = 0;
            if(data > 0)
            {
              alert('Data Driver Already Exist!');
              $("#nomor_ktp").focus();
              return false;
            }
            else{
              if(file_data==undefined)
              {
                uploadFileSIM($("#image_ktpname").val());
              }
              else
              {
                    $.ajax({
                      url: "{{ url('/') }}/Masterdata/UploadFileKTP",
                      dataType: 'text', // what to expect back from the PHP script
                      cache: false,
                      contentType: false,
                      processData: false,
                      data:form_data,
                      type: 'post',
                      success: function (response) {
                          if(response==false)
                          {
                              alert('Upload KTP Failed!');
                              return false;
                          }
                          else {
                              uploadFileSIM(response);
                          }
                      },
                      error: function (response) {
                          alert('Upload KTP Failed!');
                          return false;
                      }
                  }); /* AJAX END */
              }

            }
          }
      });
  });

  function uploadFileSIM(response_KTP)
  {
    var file_data = $('#image_sim').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    if(file_data==undefined)
    {
      uploadFileSKCK(response_KTP, $("#image_simname").val());
    }
    else
    {
        $.ajax({
          url: "{{ url('/') }}/Masterdata/UploadFileSIM",
          dataType: 'text', // what to expect back from the PHP script
          cache: false,
          contentType: false,
          processData: false,
          data:form_data,
          type: 'post',
          success: function (response) {
              if(response==false)
              {
                  alert('Upload SIM Failed!');
                  return false;
              }
              else {
                  uploadFileSKCK(response_KTP, response);
              }
          },
          error: function (response) {
              alert('Upload SIM Failed!');
              return false;
          }
        });
    }
  }

  function uploadFileSKCK(response_KTP, response_SIM)
  {
    var file_data = $('#image_skck').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    if(file_data==undefined)
    {
      uploadFileFoto(response_KTP, response_SIM, $("#image_skckname").val());
    }
    else
    {
        $.ajax({
          url: "{{ url('/') }}/Masterdata/UploadFileSKCK",
          dataType: 'text', // what to expect back from the PHP script
          cache: false,
          contentType: false,
          processData: false,
          data:form_data,
          type: 'post',
          success: function (response) {
              if(response==false)
              {
                  alert('Upload SKCK Failed!');
                  return false;
              }
              else {
                  uploadFileFoto(response_KTP, response_SIM, response);
              }
          },
          error: function (response) {
              alert('Upload SKCK Failed!');
              return false;
          }
      });
    }

  }

  function uploadFileFoto(response_KTP, response_SIM, response_SKCK)
  {
    var file_data = $('#image_foto').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    if(file_data==undefined)
    {
      Save_Data(response_KTP, response_SIM, response_SKCK, $("#image_fotoname").val());
    }
    else
    {
        $.ajax({
          url: "{{ url('/') }}/Masterdata/UploadFileFoto",
          dataType: 'text', // what to expect back from the PHP script
          cache: false,
          contentType: false,
          processData: false,
          data:form_data,
          type: 'post',
          success: function (response) {
              if(response==false)
              {
                  alert('Upload Foto Failed!');
                  return false;
              }
              else {
                  Save_Data(response_KTP, response_SIM, response_SKCK, response);
                  //alert(response_STNK + ' : ' + response_KIR + ' : ' + response_TRUK + ' : ' + response);
              }
          },
          error: function (response) {
              alert('Upload Foto Failed!');
              return false;
          }
        });
    }
  }

  function Save_Data(response_KTP, response_SIM, response_SKCK, response_Foto)
  {
      var image_ktp = response_KTP.replace(/"/g, '');
      var image_sim = response_SIM.replace(/"/g, '');
      var image_skck = response_SKCK.replace(/"/g, '');
      var image_foto = response_Foto.replace(/"/g, '');

      var cek = $('#cek').val();
      var nama_driver = $("#nama_driver").val();
      var nomor_ktp = $("#nomor_ktp").val();
      var nomor_sim = $("#nomor_sim").val();
      var nomor_telp = $("#nomor_telp").val();
      var email = $("#email").val();
      var nama_pengguna = $("#nama_pengguna").val();
      var kata_sandi = $("#kata_sandi").val();
      var ulangkata_sandi = $("#ulangkata_sandi").val();
      var alamat = $("#alamat").val();
      var id_driver = $("#id_driver").val();
      var tanggal_declare = $('#tanggal_declare').val();

      $.ajax({
        type:'POST',
        url: "{{ url('/') }}/Masterdata/saveDriver",
        datatype: 'JSON',
        data: {cek:cek, nama_driver:nama_driver, nomor_ktp:nomor_ktp, nomor_sim:nomor_sim, nomor_telp:nomor_telp, tanggal_declare:tanggal_declare,
               email:email, nama_pengguna:nama_pengguna, kata_sandi:kata_sandi, ulangkata_sandi:ulangkata_sandi, alamat:alamat,
               image_ktp:image_ktp, image_sim:image_sim, image_skck:image_skck, image_foto:image_foto, id_driver:id_driver},
        success: function(data) {
          if(data==true){
            swal({
                      title: "",
                      text: "Data Saved" ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Done ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                          window.location.href = "{{ url('/') }}/Masterdata/ListDriver";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
            });
          }
          else
          {
            swal({
                      title: "",
                      text: "Data Failed to Save" ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Close ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
          }
        }
      });

  }








  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Username trucker tidak tersedia atau sudah digunakan,  silakan cari username lain!');
                $("#username").focus();
                return false;
              }
          }
      });
  }


  // function save_datatrucker(res_npwp, res_siup)
  // {
  //   var res_npwp = res_npwp.replace(/"/g, '');
  //   var res_siup = res_siup.replace(/"/g, '');
  //   var gtime = new Date();
  //   var company_name = $("#company_name").val();
  //   var address_1 = $("#address_1").val();
  //   var address_2 = $("#address_2").val();
  //   var zip_code = $("#zip_code").val();
  //   var city = $("#city").val();
  //   var state = $("#state").val();
  //   var country = $("#country").val();
  //   var email_kantor = $("#email_kantor").val();
  //   var telp_kantor = $("#telp_kantor").val();
  //   var npwp = $("#npwp").val();
  //   var str_npwp = $("#image_npwp").val();
  //   var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
  //   var file_npwp = file_npwp_replace.replace(/\\/g,'');
  //
  //   //console.log(file_npwp.split('.').pop());
  //   var ext_npwp = file_npwp.split('.').pop();
  //   var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;
  //
  //   var name_1 = $("#name_1").val();
  //   var phone_1 = $("#phone_1").val();
  //   var telp_1 = $("#telp_1").val();
  //   var email_1 = $("#email_1").val();
  //   var name_2 = $("#name_2").val();
  //   var phone_2 = $("#phone_2").val();
  //   var telp_2 = $("#telp_2").val();
  //   var email_2 = $("#email_2").val();
  //   var username = $("#username").val();
  //   var password = $("#password").val();
  //   var siup = $("#siup").val();
  //   var str_siup = $("#image_siup").val();
  //   var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
  //   var file_siup = file_siup_replace.replace(/\\/g,'');
  //
  //   //console.log(file_npwp.split('.').pop());
  //   var ext_siup = file_siup.split('.').pop();
  //   var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;
  //
  //   if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' ||
  //      state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' ||
  //      name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' ||
  //      phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
  //   {
  //       alert('Please fill in all the required fields marked with asterisks (*).');
  //       return false;
  //   }
  //   else{
  //       check_usertrucker();
  //
  //       $.ajax({
  //           type:'POST',
  //           url: "{{ url('/') }}/Registers/save",
  //           datatype: 'JSON',
  //           data: {
  //                         company_name:company_name, address_1:address_1, address_2:address_2,
  //                         zip_code:zip_code, city:city, state:state, country:country,
  //                         email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
  //                         file_npwp:res_npwp, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
  //                         email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2,
  //                         email_2:email_2, username:username, password:password, siup:siup, file_siup:res_siup
  //           },
  //           success: function(data) {
  //           if(data==true){
  //             alert('Create data successfully');
  //             setTimeout(function() {
  //                 $("#submit").button('reset');
  //             }, 500);
  //           }
  //           else
  //           {
  //             alert('Create data failed');
  //             setTimeout(function() {
  //                 $("#submit").button('reset');
  //             }, 500);
  //             }
  //           }
  //       });
  //   }
  // }

//$("#example-form").submit(function(){
  $("#submitxx").click(function()
  {
    //company_name, address_1, address_2, zip_code, city, state, country, email_kantor,
    //telp_kantor, npwp, image_npwp, name_1, phone_1, telp_1, email_1, name_2, phone_2, telp_2,
    //email_2, username, password, siup, image_siup

    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' ||
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' ||
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' ||
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }
    else{
        uploadFileNPWP(image_npwpname, image_siupname);
        check_usertrucker();

        $.ajax({
            type:'POST',
            url: "{{ url('/') }}/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:image_npwpname, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2,
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:image_siupname
            },
            success: function(data) {
            if(data==true){
              alert('Create data successfully');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
            }
            else
            {
              alert('Create data failed');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
              }
            }
        });
    }
  });

</script>
@endsection
