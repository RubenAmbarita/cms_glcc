@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.css')}}">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
@stop

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="{{ url('/') }}/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="{{ url('/') }}/pesan">Pesan</a></li>
        <li class="active">Detail</li>
      </ol>
      <h1 style="font-family:Poppins"> Formulir Penambahan Pesan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Armada Publication</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <form action="{{ 'Order/matchtab1' }}" id="formtab1" method="post">
              {!! csrf_field() !!}
              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Lokasi Gereja</label>
                  <select class="form-control select2" id="lokasi" name="lokasi" required >
                  <option value="NOL">Pilih Lokasi Gereja</option>
                  </select>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Tema Pesan</label>
                  <input type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Judul Pesan</label>
                  <input type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Penanggung Jawab</label>
                  <input type="text" name="perusahaan" value="" class="form-control">
                </div>
              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Divisi</label>
                  <input type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Tanggal Berakhir</label>
                  <input type="text" name="perusahaan" value="" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="">Gambar</label><br>
                  <div class="fileinput-new thumbnail" style="width: 120px; height: 110px;">
                  <img src="" alt="foto" width="100px" height="100px">
                </div>
                </div>


              </div>

              <!-- /.box-header -->
              <div class="box-body pad">

              </div>

            <!-- /.box-header -->
            <div class="box-body pad">
              <label for="">Pesan</label>
              <form>
                <textarea class="textarea" placeholder="Ketik Pesan Anda"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </form>
            </div>

            <button class="btn btn-info" type="submit" name="button"><i class="fa fa-plus-square"></i> Simpan Pesan</button>
          </div>

              <div class="form-group has-feedback {{ $errors->has('add_publish_tab1') ? 'has-error' : '' }}">
                @if ($errors->has('add_publish_tab1'))
                  <span class="help-block">
                    <strong>{{ $errors->first('add_publish_tab1') }}</strong>
                  </span>
                @endif
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('javascript')
<script src="{{url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!--<script src="{{url('adminlte.assets/timepicker/dist/wickedpicker.min.js')}}"></script>-->
<!--<script src="{{url('js/order.js')}}"></script>-->
<!-- CK Editor -->
<script src="{{url('adminlte.assets/bower_components/ckeditor/ckeditor.js')}}"></script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask();
    $('#waktu_loading').numericx();
  });

  $( function()
  {
    $( "#tanggal_declare" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });

  });

</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  $("select[name='id_customer']").change(function(){
      var p_param = $(this).val();
      var waktu_declare = $("#waktu_declare").val();
      var waktu_loading = $("#waktu_loading").val();

      if(waktu_declare == '')
      {
          $("#waktu_declare").focus();
          alert('Jam Keluar Pelabuhan belum terisi!');
          return false;
      }

      if(waktu_loading == '')
      {
          $("#waktu_loading").focus();
          alert('Waktu Unloading Barang belum terisi!');
          return false;
      }

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_warehouse",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#id_gudang option").remove();
            $("#id_gudang option").val();

            var i = 0;
            if(data[i] == undefined)
            {
              //alert('Data gudang tidak ditemukan!');
              $( "#id_gudang" ).append(
                $('<option></option>').val("Choose Your Warehouse").html("Choose Your Warehouse")
              );
              $("#info_estimatefretime").hide("slow");
            }
            else
            {
              $( "#id_gudang" ).html("Choose Your Warehouse");
              $.each(data, function()
              {
                $( "#id_gudang" ).append(
                    $('<option></option>').val(data[i].value+'||'+data[i].coordinate+'||'+data[i].multiplication+'||'+data[i].area).html(data[i].name + ' -- ' + data[i].address)
                );
                i++;
              });
              get_estimatefreetime($("#id_gudang").val());
              $("#info_estimatefretime").show("slow");
            }
          }
      });
  });

  $("select[name='id_gudang']").change(function(){
      var p_param = $(this).val();
      get_estimatefreetime(p_param);
  });

  function get_estimatefreetime(coordinate)
  {
      //var coordinate = $("#id_gudang").val();
      var explode = coordinate.split("||");
      var origin = "-6.107080,106.880518";
      var destination = explode[1];
      var multiplication = explode[2];
      var timestamp = $("#tanggal_declare").val() + ' ' + $("#waktu_declare").val();
      var time_loading = $("#waktu_loading").val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_GoogleAPI",
          datatype: 'JSON',
          data: {origin:origin, destination:destination, multiplication:multiplication, timestamp:timestamp, time_loading:time_loading},
          success: function(data)
          {
            if(data)
            {
                var dataExplode = data.split(",");
                var ExplodeKMVal = dataExplode[0];
                var ExplodeKMText = dataExplode[1];

                var ExplodeTime = dataExplode[2].split(":");
                var ExplodeTimeReturn = 'Estimated finish unloading at the consignee warehouse : ' + ExplodeTime[0] + ' !';
                $("#EstimateFreeTimes").html(ExplodeTimeReturn);

                var ExplodeEFT = dataExplode[3].split(" ");
                $("#tanggal_estimate").val(ExplodeEFT[0]);
                $("#waktu_estimate").val(ExplodeEFT[1]);
                $("#tanggal_estimateValue").val(ExplodeEFT[0]);
                $("#waktu_estimateValue").val(ExplodeEFT[1]);
                //$("#area").val(dataExplode[4]);
            }
          }
      });
  }

  function check_containernumber()
  {
    var tanggal_declare = $("#tanggal_declare").val();
    var no_container = $("#no_container").val();
    if(no_container.length > 20)
    {
        var no_containerSlice = no_container.slice(0, -20);
    }
    else {
        var no_containerSlice = no_container;
    }

    $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_checkcontainernumber",
          datatype: 'JSON',
          data: {p_contnumber:no_containerSlice, p_declaredate:tanggal_declare},
          success: function(data)
          {
            if(data > 0)
            {
              alert('The container has been published, please select another container!');
              $("#Container_InfoPublished").show("slow");
              $("#no_container").focus();
              return false;
            }
            else{
              $("#Container_InfoPublished").hide("slow");
              check_containerBanned(no_containerSlice);
              return false;
            }
          }
      });
  }

  function check_containerBanned(no_container)
  {
    $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/check_containerBanned",
          datatype: 'JSON',
          data: {p_contnumber:no_container},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Status Kontainer di Banned!');
              $("#Container_InfoBanned").show("slow");
              $("#no_container").focus();
              //$("#no_container").focus(function() { $(this).select(); });
              return false;
            }
            else{
              $("#Container_InfoBanned").hide("slow");
              Container_Split()();
              //return false;
            }
          }
      });
  }

  $(function()
  {
    $('#no_container').keyup(function()
    {
        var shipping_line = $('#id_sl').val();
        var vessel_voyage = $('#id_vv').val();
        var p_contnumber = $(this).val();
        if(shipping_line == '')
        {
            alert('Please Choose Shipping Line!');
            return false;
        }
        if(p_contnumber != '')
        {
         //var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ url('/') }}/Published/search_containernumber",
          method:"POST",
          data:{p_sl:shipping_line, p_contnumber:p_contnumber, id_vv:vessel_voyage},
          success:function(data){
            $('#search_container').fadeIn();
            $('#search_container').html(data);
          }
         });
        }
    });

    $("#no_containers").blur(function()
    {
        var no_container = $("#no_container").val();
        if(no_container.length > 20)
        {
            var no_containerSlice = no_container.slice(0, -20);
        }
        else {
            var no_containerSlice = no_container;
        }
        //get_idcontainer(no_containerSlice);
        $("#search_contUI").hide('slow');
        $('#search_container').fadeOut();
        alert('Test Hide');
    });

    $("#no_container").inputmask("AAAA 9999999");
    $("#no_container").blur(function()
    {
        $("#search_container").hide('slow');
    });

    $("#search_container").on('click', 'li', function(){
        $('#no_container').val($(this).text());
        $('#search_container').fadeOut();
    });

    $("#tanggal_declare").change(function()
    {
      reset_kalkulasiJarak();
    });

    $("#waktu_declare").change(function()
    {
      reset_kalkulasiJarak();
    });

    $("#waktu_loading").change(function()
    {
      reset_kalkulasiJarak();
    });

    function reset_kalkulasiJarak()
    {
        $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_importir",
          datatype: 'JSON',
          data: {},
          success: function(data) {
            $("#id_customer option").remove();
            $("#id_customer option").val();
            var i = 0;

            $( "#id_customer" ).append(
              $('<option></option>').val("Choose Your Warehouse").html("Choose Your Consignee")
            );

            $.each(data, function()
            {
              $( "#id_customer" ).append(
                  $('<option></option>').val(data[i].value).html(data[i].name)
              );
              i++;
            });
          }
        });

        $("#id_gudang option").remove();
        $("#id_gudang option").val();
        $( "#id_gudang" ).append(
          $('<option></option>').val("Choose Your Warehouse").html("Choose Your Warehouse")
        );
        $("#info_estimatefretime").hide("slow");
    }


    // Vessel_Voyage

    $("select[name='id_sl']").change(function(){
      var p_param = $(this).val();

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_vesselvoyage",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#id_vv option").remove();
            $("#id_vv option").val();
            var i = 0;
            $.each(data, function()
            {
              $( "#id_vv" ).append(
                  $('<option></option>').val(data[i].id_vv).html(data[i].vessel_name + ' ' + data[i].voyage_number)
              );
              i++;
            });
          }
      });
    });


  });


  function published_process()
  {
    var tanggal_declare = $("#tanggal_declare").val();
    var no_container = $("#no_container").val();
    if(no_container.length > 20)
    {
        var no_containerSlice = no_container.slice(0, -20);
    }
    else {
        var no_containerSlice = no_container;
    }

    $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_checkcontainernumber",
          datatype: 'JSON',
          data: {p_contnumber:no_containerSlice, p_declaredate:tanggal_declare},
          success: function(data)
          {
            if(data > 0)
            {
              alert('The container has been published, please select another container!');
              $("#Container_InfoPublished").show("slow");
              $("#no_container").focus();
              return false;
            }
            else{
              $("#Container_InfoPublished").hide("slow");
              check_containerBanned(no_containerSlice);
              return false;
            }
          }
      });
  }

  function Container_Split()
  {
      var no_container = $("#no_container").val();
      if(no_container.length > 20)
      {
          var no_containerSlice = no_container.slice(0, -20);
      }
      else {
          var no_containerSlice = no_container;
      }

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/get_idcontainer",
          datatype: 'JSON',
          data: {nocontainer:no_containerSlice},
          success: function(data)
          {
            if(data != false)
            {
              data_process(no_containerSlice, data);
            }
            else{
              //alert("Container tidak terdaftar, apakah anda tetap ingin menggunakannya ?");
              data_process(no_containerSlice, 0);
            }
          }
      });
  }

  function data_process(no_containerSlice, id_container)
  {
      var coordinate = $("#id_gudang").val();
      var explode = coordinate.split("||");

      var id_driver = $("#id_driver").val();
      var id_commodity = $("#id_commodity").val();
      var id_truck_fleet = $("#id_truck_fleet").val();
      var id_sl = $("#id_sl").val();
      var id_vv = $("#id_vv").val();
      var area = explode[3];
      var no_container = no_containerSlice;
      var id_container = id_container;
      var id_gudang = explode[0];
      var waktu_declare = $("#tanggal_declare").val() + " " + $("#waktu_declare").val();
      var estimatefreetime = $("#tanggal_estimateValue").val() + " " + $("#waktu_estimateValue").val();
      var ukuran_container = $("#ukuran_container").val();
      var coordinate_tujuan = explode[1];
      var bl_number = $("#bl_number").val();

      //alert('--iddriver--'+id_driver+'--idcommodity--'+id_commodity+'--idtruckfleet--'+id_truck_fleet+'--idsl--'+id_sl+'--area--'+area+'--nocontainer--'+no_container+'--idcontainer--'+id_container+'--idgudang--'+id_gudang+'--waktudeclare--'+waktu_declare+'--estimatefreetime--'+estimatefreetime+'--ukurancontainer--'+ukuran_container+'--coordinatetujuan--'+coordinate_tujuan+'--blnumber--'+bl_number)

      //check_containernumber();
      if(id_driver=='' || id_commodity=='' || id_truck_fleet=='' || id_sl=='' || area=='' || no_container=='' || id_gudang=='' || waktu_declare=='' || estimatefreetime=='' || ukuran_container=='' || coordinate_tujuan=='' || bl_number=='')
      {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
      }

      $.ajax({
          type: 'POST',
          url: "{{ url('/') }}/Published/save",
          datatype: 'JSON',
          data: {id_driver:id_driver, id_commodity:id_commodity, id_truck_fleet:id_truck_fleet, id_sl:id_sl, id_vv:id_vv, area:area, no_container:no_container, id_container:id_container, id_gudang:id_gudang, waktu_declare:waktu_declare, estimatefreetime:estimatefreetime, ukuran_container:ukuran_container, coordinate_tujuan:coordinate_tujuan, bl_number:bl_number},
          success: function(data) {
            if(data==true)
              {
                  swal({
                      title: "Successfully !",
                      text: "Data Successfully Saved" ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Done ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        window.location.href = "{{ url('/') }}/Published/nomatched";
                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
              }
              else{
                  swal({
                      title: "Failed",
                      text: "Data Failed to Save." ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Close ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
              }
          }
      });
  }

</script>
@stop
