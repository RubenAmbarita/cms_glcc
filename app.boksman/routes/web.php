<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    //session()->put('cust_id', 123456);
    //return session()->get('cust_id');
    return view('auth/login');
});

Auth::routes();
Route::get('Registers', 'RegistersController@index');
Route::post('Registers', 'RegistersController@index');
Route::post('Registers/save', 'RegistersController@save');
Route::post('Registers/UploadFile', 'RegistersController@UploadFile');
Route::post('Registers/UploadFileNPWP', 'RegistersController@UploadFileNPWP');
Route::post('Registers/UploadFileSIUP', 'RegistersController@UploadFileSIUP');
Route::post('Registers/check_usertrucker', 'RegistersController@check_usertrucker');
Route::post('Registers/get_lat_longajax', 'RegistersController@get_lat_longajax');
Route::post('Registers/get_npwpsiup', 'RegistersController@get_npwpsiup');
Route::post('Registers/get_regency', 'RegistersController@get_regency');

Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::post('dashboard/save', 'DashboardController@save');
Route::post('dashboard/UploadFile', 'DashboardController@UploadFile');

Route::get('pesan/index', 'PesanController@index');
Route::get('pesan/addmessage', 'PesanController@addmessage');
Route::post('pesan/save', 'PesanController@save');
Route::post('pesan/detail', 'PesanController@detail');
Route::post('pesan/delete_pesan', 'PesanController@delete_pesan');
Route::post('pesan/upload', 'PesanController@upload');

Route::get('glc/index', 'GlcController@index');
Route::post('glc/detail', 'GlcController@detail');
Route::post('glc/daftarpeserta', 'GlcController@daftarpeserta');
Route::get('glc/addglc', 'GlcController@addglc');
Route::post('glc/save', 'GlcController@save');
Route::post('glc/update_status', 'GlcController@update_status');
Route::post('glc/upload', 'GlcController@upload');

Route::get('pelayanan/index', 'PelayananController@index');
Route::post('pelayanan/detail', 'PelayananController@detail');
Route::post('pelayanan/update_status', 'PelayananController@update_status');
Route::post('pelayanan/delete', 'PelayananController@delete');
Route::post('pelayanan/save', 'PelayananController@save_pelayanan');

Route::get('renungan/index', 'RenunganController@index');
Route::get('renungan/add', 'RenunganController@addrenungan');
Route::post('renungan/save', 'RenunganController@save');
Route::post('renungan/detail', 'RenunganController@detail');
Route::post('renungan/delete_renungan', 'RenunganController@delete_renungan');
Route::post('renungan/upload', 'RenunganController@upload');

Route::get('masterpelayanan/index', 'MasterPelayananController@index');
Route::get('masterpelayanan/addpelayanan', 'MasterPelayananController@add');
Route::post('masterpelayanan/save', 'MasterPelayananController@save');
Route::post('masterpelayanan/detail', 'MasterPelayananController@detail');
Route::post('masterpelayanan/upload', 'MasterPelayananController@upload');
Route::post('masterpelayanan/delete_pelayanan', 'MasterPelayananController@delete_pelayanan');

Route::get('masterpujian/index', 'MasterPujianController@index');
Route::get('masterpujian/addpujian', 'MasterPujianController@add');
Route::post('masterpujian/save', 'MasterPujianController@save');
Route::post('masterpujian/detail', 'MasterPujianController@detail');
Route::post('masterpujian/upload', 'MasterPujianController@upload');
Route::post('masterpujian/delete_pujian', 'MasterPujianController@delete_pujian');

Route::get('Masterdata/ListTrans', 'MasterdataController@ListTrans');
Route::get('Masterdata/AddTrans', 'MasterdataController@AddTrans');
Route::post('Masterdata/EditTrans', 'MasterdataController@EditTrans');
Route::post('Masterdata/UploadFileSTNK', 'MasterdataController@UploadFileSTNK');
Route::post('Masterdata/UploadFileKIR', 'MasterdataController@UploadFileKIR');
Route::post('Masterdata/UploadFileTRUK', 'MasterdataController@UploadFileTRUK');
Route::post('Masterdata/UploadFileASURANSI', 'MasterdataController@UploadFileASURANSI');
Route::post('Masterdata/uploadFileKirChasis', 'MasterdataController@uploadFileKirChasis');

Route::post('Masterdata/check_policenumber', 'MasterdataController@check_policenumber');
Route::post('Masterdata/saveTrans', 'MasterdataController@saveTrans');
Route::post('Masterdata/delete_trans', 'MasterdataController@delete_trans');
Route::post('Masterdata/active_trans', 'MasterdataController@active_trans');
Route::post('Masterdata/nonactive_trans', 'MasterdataController@nonactive_trans');
Route::post('Masterdata/get_InsuranceAPI', 'MasterdataController@get_InsuranceAPI');

Route::get('Masterdata/ListDriver', 'MasterdataController@ListDriver');
Route::get('Masterdata/AddDriver', 'MasterdataController@AddDriver');
Route::post('Masterdata/EditDriver', 'MasterdataController@EditDriver');
Route::post('Masterdata/UploadFileSTNK', 'MasterdataController@UploadFileSTNK');
Route::post('Masterdata/UploadFileKIR', 'MasterdataController@UploadFileKIR');
Route::post('Masterdata/UploadFileTRUK', 'MasterdataController@UploadFileTRUK');
Route::post('Masterdata/UploadFileASURANSI', 'MasterdataController@UploadFileASURANSI');
Route::post('Masterdata/check_policenumber', 'MasterdataController@check_policenumber');
Route::post('Masterdata/saveDriver', 'MasterdataController@saveDriver');
Route::post('Masterdata/delete_driver', 'MasterdataController@delete_driver');
Route::post('Masterdata/check_ktpsimtelpuser', 'MasterdataController@check_ktpsimtelpuser');
Route::post('Masterdata/UploadFileKTP', 'MasterdataController@UploadFileKTP');
Route::post('Masterdata/UploadFileSIM', 'MasterdataController@UploadFileSIM');
Route::post('Masterdata/UploadFileSKCK', 'MasterdataController@UploadFileSKCK');
Route::post('Masterdata/UploadFileFoto', 'MasterdataController@UploadFileFoto');

Route::get('LokasiGereja', 'LokasiGerejaController@index');
Route::get('LokasiGereja/create', 'LokasiGerejaController@create');
Route::post('LokasiGereja/save', 'LokasiGerejaController@save');
Route::post('LokasiGereja/edit', 'LokasiGerejaController@edit');
Route::post('LokasiGereja/updates', 'LokasiGerejaController@updates');
Route::post('LokasiGereja/deleteds', 'LokasiGerejaController@deleteds');
Route::post('LokasiGereja/get_city', 'LokasiGerejaController@get_city');
Route::post('LokasiGereja/get_district', 'LokasiGerejaController@get_district');
Route::post('LokasiGereja/get_subdistrict', 'LokasiGerejaController@get_subdistrict');

Route::get('TentangKami', 'TentangKamiController@index');
Route::get('TentangKami/create', 'TentangKamiController@create');

Route::get('Keanggotaan', 'KeanggotaanController@index');
Route::post('Keanggotaan/update_status', 'KeanggotaanController@update_status');
Route::post('Keanggotaan/detail', 'KeanggotaanController@detail');
Route::post('Keanggotaan/delete', 'KeanggotaanController@delete');
Route::post('Keanggotaan/save', 'KeanggotaanController@save');
Route::post('Keanggotaan/get_city', 'KeanggotaanController@get_city');
Route::post('Keanggotaan/get_district', 'KeanggotaanController@get_district');
Route::post('Keanggotaan/get_subdistrict', 'KeanggotaanController@get_subdistrict');

Route::get('Warta', 'WartaController@index');

Route::get('Video', 'VideoController@index');

Route::get('Jadwal', 'JadwalController@index');
Route::get('Jadwal/detail', 'JadwalController@detail');
Route::get('Jadwal/create', 'JadwalController@create');

Route::get('{all}', function($uri_1 = null, $uri_2 = null, $uri_3 = null, $uri_4 = null, $uri_5 = null)
{
    $uri = null;
    for($i = 1; $i <= 5; $i++)
    {
        if(${'uri_'.$i})
        {
            $uri .= ${'uri_'.$i}.'/';
        }
    }

    $uri = rtrim($uri, '/');

    return $uri;
})->where('all', '.*');
