<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Published/nomatched">Truck Report</a></li>
        <li class="active">Non-matched Data</li>
      </ol>
      <h1 style="font-family:Poppins">Non-matched Data</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Data On the Progress of Your Truck Trips</h3>
            </div> -->
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Publish Date</th>
                    <th style="text-align:center;">B/L #</th>
                    <th style="text-align:center;">Consignee</th>
                    <th style="text-align:center;">Shipping Line</th>
                    <th style="text-align:center;">Container #</th>
                    <th style="text-align:center;">Volume</th>
                    <th style="text-align:center;">Comodity</th>
                    <th style="text-align:center;">Plate #</th>
                    <th style="text-align:center;">Driver</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $nomatched_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($row->Publish_Date); ?></td>
                        <td><?php echo e($row->BL_Number); ?></td>
                        <td><?php echo e($row->Importir); ?></td>
                        <td><?php echo e($row->Shipping_Line); ?></td>
                        <td><?php echo e($row->Container_Number); ?></td>
                        <td><?php echo e($row->Container_Size); ?></td>
                        <td><?php echo e($row->Commodity); ?></td>
                        <td><?php echo e($row->Police_Number); ?></td>
                        <td><?php echo e($row->Driver); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('viewnomatched'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="Publish_Number" name="Publish_Number" value="<?php echo e($row->Publish_Number); ?>">
                              <button class="btn btn-info" type="submit"><i class="fa fa-eye"></i> View</button>
                          </form>
                        </td>
                        <td style="text-align:center;">
                          <button class="btn btn-danger" onclick="showCancelMessage(<?php echo e($row->Publish_Number); ?>)" ><i class="fa fa-trash"></i> Delete</button>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Publish Date</th>
                      <th style="text-align:center;">B/L #</th>
                      <th style="text-align:center;">Consignee</th>
                      <th style="text-align:center;">Shipping Line</th>
                      <th style="text-align:center;">Container #</th>
                      <th style="text-align:center;">Volume</th>
                      <th style="text-align:center;">Comodity</th>
                      <th style="text-align:center;">Plate #</th>
                      <th style="text-align:center;">Driver</th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_publish)
  {
      swal({
          title: "Are You Sure?",
          text: "Data Will be Deleted Permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes ",
          cancelButtonText: "No ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == 0 || data == 1)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data Successfully Deleted" ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Done ",
                          cancelButtonText: "No ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data Failed Saved" ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Close ",
                            cancelButtonText: "No ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>