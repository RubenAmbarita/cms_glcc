<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name='csrf-token' content="<?php echo e(csrf_token()); ?>" />
    <title>
      <?php echo $__env->yieldContent('title_prefix', config('adminlte.title_prefix', '')); ?>
      <?php echo $__env->yieldContent('title', config('adminlte.title', 'CMS Trucker || boksman.com')); ?>
      <?php echo $__env->yieldContent('title_postfix', config('adminlte.title_postfix', '')); ?>
    </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo e(asset('images/favicon.png')); ?>" type="image/x-icon">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/font-awesome/css/font-awesome.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/Ionicons/css/ionicons.min.css')); ?>">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">

  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/select2/dist/css/select2.min.css')); ?>">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/dist/css/AdminLTE.min.css')); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/dist/css/skins/_all-skins.min.css')); ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/morris.js/morris.css')); ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/jvectormap/jquery-jvectormap.css')); ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.css')); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/sweetalert/sweetalert.css')); ?>">
 
  <?php echo $__env->yieldContent('css'); ?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo e(url('dashboard')); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>BOK</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <!--<img src="<?php echo e(url('images/bagibagi.png')); ?>" class="user-image" alt="User Image">-->
        <b>TRUCKER</b>
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
    </nav>
    <div style="display:none;" class="pull-right">
      <a id="a_logout" href="<?php echo e(route('logout')); ?>" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        Logout
      </a>
      <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo e(csrf_field()); ?>

      </form>
    </div>
  </header>

  <?php echo $__env->yieldContent('content'); ?>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 4.0.0
    </div>
    <strong>Copyright &copy; 2018 <a target="_blank" href="https://www.boksman.com/">boksman.com</a>.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery/dist/jquery.min.js')); ?>"></script>

<!-- jQuery UI 1.11.4 
AIzaSyCCwWSYaI2ZzPe_H9gmH6ENirEC07tmIEE-->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-ui/jquery-ui.min.js')); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- DataTables -->
<script src="<?php echo e(url('adminlte.assets/bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>

<!-- Select2 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo e(url('adminlte.assets/bower_components/raphael/raphael.min.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/morris.js/morris.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(url('adminlte.assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-knob/dist/jquery.knob.min.js')); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/moment/min/moment.min.js')); ?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(url('adminlte.assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(url('adminlte.assets/bower_components/fastclick/lib/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(url('adminlte.assets/dist/js/adminlte.min.js')); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes)
<script src="<?php echo e(url('adminlte.assets/dist/js/pages/dashboard.js')); ?>"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- InputMask -->
<script src="<?php echo e(url('adminlte.assets/plugins/input-mask/jquery.inputmask.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/plugins/input-mask/jquery.inputmask.date.extensions.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/plugins/input-mask/jquery.inputmask.extensions.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/sweetalert/sweetalert.min.js')); ?>"></script>

<!--<script src="<?php echo e(url('adminlte.assets/dist/js/demo.js')); ?>"></script>-->

<?php echo $__env->yieldContent('javascript'); ?>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable(
    {
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });
</script>

</body>
</html>
