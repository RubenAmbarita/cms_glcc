<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.css')); ?>">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Published/AcceptShipperOrder">Shipper Order</a></li>
        <li class="active">Accept Order</li>
      </ol>
      <h1 style="font-family:Poppins"> Accept Order </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Accept Order</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <form action="<?php echo e('Published/saveAcceptShipperOrder'); ?>" id="formtab1" method="post">
              <?php echo csrf_field(); ?>

              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Company Name</label>
                  <input type="text" name="perusahaan" value="<?php echo e($custname); ?>" class="form-control" disabled>
                  <input type="hidden" id="id_order" name="id_order" value="<?php echo e($order->id_order); ?>" class="form-control">
                  <input type="hidden" id="id_depo" name="id_depo" value="<?php echo e($order->id_depo); ?>" class="form-control">
                  <input type="hidden" id="id_gudang" name="id_gudang" value="<?php echo e($order->id_gudang); ?>" class="form-control">
                  <input type="hidden" id="area" name="area" value="<?php echo e($order->area); ?>" class="form-control">
                  <input type="hidden" id="do_number" name="do_number" value="<?php echo e($order->number_do); ?>" class="form-control">
                  <input type="hidden" id="gk_order" name="gk_order" value="<?php echo e($order->gk_order); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="id_sl">Shipping Lines</label>
                  <input id="id_sl_view" type="text" name="id_sl_view" value="<?php echo e($order->sl_name); ?>" class="form-control" disabled>
                  <input id="id_sl" type="hidden" name="id_sl" value="<?php echo e($order->id_sl); ?>" class="form-control" readonly=readonly>
                </div>

                <div class="form-group has-feedback">
                  <label for="ukuran_container">Container Size</label>
                  <input type="text" id="ukuran_container_view" name="ukuran_container_view" value="<?php echo e($order->ukuran_container_name); ?>" class="form-control" disabled>
                  <input type="hidden" id="ukuran_container" name="ukuran_container" value="<?php echo e($order->ukuran_container); ?>" class="form-control" led>
                </div>

                <div class="form-group has-feedback">
                  <label for="id_commodity">Commodity</label>
                  <input type="text" id="id_commodity_view" name="id_commodity_view" value="<?php echo e($order->commodity_name); ?>" class="form-control" disabled>
                  <input type="hidden" id="id_commodity" name="id_commodity" value="<?php echo e($order->id_commodity); ?>" class="form-control" led>
                </div>

                <div class="form-group has-feedback">
                  <label for="id_driver">Stuffing Date</label>
                  <input id="stuffing_date_view" type="text" name="stuffing_date_view" value="<?php echo e($order->pickup_date); ?>" class="form-control" disabled>
                  <input id="stuffing_date" type="hidden" name="stuffing_date" value="<?php echo e($order->pickup_date); ?>" class="form-control" readonly=readonly>
                </div>
              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">

                <div class="form-group has-feedback">
                  <label for="id_truck_fleet">Choose Truck <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="id_truck_fleet" name="id_truck_fleet" style="width: 100%;" required >
                    <option value="">Choose Your Truck</option>
                    <?php $__currentLoopData = $listtruck; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($row->value); ?>" <?php echo e((old("id_truck_fleet") == $row->value ? "selected":"")); ?>><?php echo e($row->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>

                <div class="form-group has-feedback">
                  <label for="id_driver">Choose Driver <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="id_driver" name="id_driver" style="width: 100%;" required >
                    <option value="">Choose Your Driver</option>
                    <?php $__currentLoopData = $listdriver; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($row->value); ?>" <?php echo e((old("id_driver") == $row->value ? "selected":"")); ?>><?php echo e($row->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
                <br/><br/><br/><br/><br/><br/><br/><br/>
                <div style="display:none;"class="form-group has-feedback">
                  <label for="no_container">Nomor Kontainer <b style="color:red;">*</b></label>
                  <input id="no_container" type="text" name="no_container" value="" class="form-control " aria-invalid="false" onfocus="this.select();" onmouseup="return false;" >
                  <div id="search_container"></div>
                  <div>
                    <p id="Container_InfoPublished" style="color:#ff0016; font-weight:normal; display:none;">
                      Container has been publish, please choose another container!
                    </p>
                    <p id="Container_InfoBanned" style="color:#ff0016; font-weight:normal; display:none;">
                      Container has been Banned!
                    </p>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer" style="text-align:right;">
                <a href="" style="display:none;"> <button type="button" class="btn btn-danger">List Publication</button></a>
                <button type="button" class="btn btn-primary" id="tab1-submit" onclick="assign_process()" ><i class="fa fa-external-link"></i> Assign</button> &nbsp;
              </div>
              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!--<script src="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.js')); ?>"></script>-->
<!--<script src="<?php echo e(url('js/order.js')); ?>"></script>-->

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask();
    $('#waktu_loading').numericx();
  });

  $( function()
  {
    $( "#tanggal_declare" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });

  });

</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  $("select[name='id_customer']").change(function(){
      var p_param = $(this).val();
      var waktu_declare = $("#waktu_declare").val();
      var waktu_loading = $("#waktu_loading").val();

      if(waktu_declare == '')
      {
          $("#waktu_declare").focus();
          alert('Jam Keluar Pelabuhan belum terisi!');
          return false;
      }

      if(waktu_loading == '')
      {
          $("#waktu_loading").focus();
          alert('Waktu Unloading Barang belum terisi!');
          return false;
      }

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/get_warehouse",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
            $("#id_gudang option").remove();
            $("#id_gudang option").val();

            var i = 0;
            if(data[i] == undefined)
            {
              //alert('Data gudang tidak ditemukan!');
              $( "#id_gudang" ).append(
                $('<option></option>').val("Choose Your Warehouse").html("Choose Your Warehouse")
              );
              $("#info_estimatefretime").hide("slow");
            }
            else
            {
              $( "#id_gudang" ).html("Choose Your Warehouse");
              $.each(data, function()
              {
                $( "#id_gudang" ).append(
                    $('<option></option>').val(data[i].value+'||'+data[i].coordinate+'||'+data[i].multiplication+'||'+data[i].area).html(data[i].name + ' -- ' + data[i].address)
                );
                i++;
              });
              get_estimatefreetime($("#id_gudang").val());
              $("#info_estimatefretime").show("slow");
            }
          }
      });
  });

  $("select[name='id_gudang']").change(function(){
      var p_param = $(this).val();
      get_estimatefreetime(p_param);
  });

  function get_estimatefreetime(coordinate)
  {
      //var coordinate = $("#id_gudang").val();
      var explode = coordinate.split("||");
      var origin = "-6.107080,106.880518";
      var destination = explode[1];
      var multiplication = explode[2];
      var timestamp = $("#tanggal_declare").val() + ' ' + $("#waktu_declare").val();
      var time_loading = $("#waktu_loading").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/get_GoogleAPI",
          datatype: 'JSON',
          data: {origin:origin, destination:destination, multiplication:multiplication, timestamp:timestamp, time_loading:time_loading},
          success: function(data)
          {
            if(data)
            {
                var dataExplode = data.split(",");
                var ExplodeKMVal = dataExplode[0];
                var ExplodeKMText = dataExplode[1];

                var ExplodeTime = dataExplode[2].split(":");
                var ExplodeTimeReturn = 'Estimasi perjalan menuju dan selesai Unloading di Gudang Importir ' + ExplodeTime[0] + ' Jam ' + ExplodeTime[1] + ' Menit !';
                $("#EstimateFreeTimes").html(ExplodeTimeReturn);

                var ExplodeEFT = dataExplode[3].split(" ");
                $("#tanggal_estimate").val(ExplodeEFT[0]);
                $("#waktu_estimate").val(ExplodeEFT[1]);
                $("#tanggal_estimateValue").val(ExplodeEFT[0]);
                $("#waktu_estimateValue").val(ExplodeEFT[1]);
                //$("#area").val(dataExplode[4]);
            }
          }
      });
  }

  function check_containernumber()
  {
    var tanggal_declare = $("#tanggal_declare").val();
    var no_container = $("#no_container").val();
    if(no_container.length > 20)
    {
        var no_containerSlice = no_container.slice(0, -20);
    }
    else {
        var no_containerSlice = no_container;
    }

    $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/get_checkcontainernumber",
          datatype: 'JSON',
          data: {p_contnumber:no_containerSlice, p_declaredate:tanggal_declare},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Kontainer sudah di Publikasi, Silahkan Pilih Kontainer Lainnya!');
              $("#Container_InfoPublished").show("slow");
              $("#no_container").focus();
              return false;
            }
            else{
              $("#Container_InfoPublished").hide("slow");
              check_containerBanned(no_containerSlice);
              return false;
            }
          }
      });
  }

  function check_containerBanned(no_container)
  {
    $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/check_containerBanned",
          datatype: 'JSON',
          data: {p_contnumber:no_container},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Container has been banned!');
              $("#Container_InfoBanned").show("slow");
              $("#no_container").focus();
              //$("#no_container").focus(function() { $(this).select(); });
              return false;
            }
            else{
              $("#Container_InfoBanned").hide("slow");
              Container_Split();
              //return false;
            }
          }
      });
  }

  $(function()
  {
    $('#no_container').keyup(function()
    {
        var shipping_line = $('#id_sl').val();
        var p_contnumber = $(this).val();
        if(shipping_line == '')
        {
            alert('Please fill shipping line name!');
            return false;
        }
        if(p_contnumber != '')
        {
         //var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"<?php echo e(url('/')); ?>/Published/search_containernumber",
          method:"POST",
          data:{p_sl:shipping_line, p_contnumber:p_contnumber},
          success:function(data){
            $('#search_container').fadeIn();
            $('#search_container').html(data);
          }
         });
        }
    });

    $("#no_containers").blur(function()
    {
        var no_container = $("#no_container").val();
        if(no_container.length > 20)
        {
            var no_containerSlice = no_container.slice(0, -20);
        }
        else {
            var no_containerSlice = no_container;
        }
        //get_idcontainer(no_containerSlice);
        $("#search_contUI").hide('slow');
        $('#search_container').fadeOut();
        alert('Test Hide');
    });

    $("#no_container").blur(function()
    {
        $("#search_container").hide('slow');
    });

    $(document).on('click', 'li', function(){
        $('#no_container').val($(this).text());
        $('#search_container').fadeOut();
    });

    $("#tanggal_declare").change(function()
    {
        $("#id_gudang option").remove();
        $("#id_gudang option").val();
        $( "#id_gudang" ).append(
          $('<option></option>').val("Choose Your Warehouse").html("Choose Your Warehouse")
        );
        $("#info_estimatefretime").hide("slow");
    });

    $("#waktu_declare").change(function()
    {
        $("#id_gudang option").remove();
        $("#id_gudang option").val();
        $( "#id_gudang" ).append(
          $('<option></option>').val("Choose Your Warehouse").html("Choose Your Warehouse")
        );
        $("#info_estimatefretime").hide("slow");
    });

    $("#waktu_loading").change(function()
    {
        $("#id_gudang option").remove();
        $("#id_gudang option").val();
        $( "#id_gudang" ).append(
          $('<option></option>').val("Choose Your Warehouse").html("Choose Your Warehouse")
        );
        $("#info_estimatefretime").hide("slow");
    });
  });


  function assign_process()
  {
    var tanggal_declare = $("#tanggal_declare").val();
    var no_container = $("#no_container").val();
    if(no_container.length > 20)
    {
        var no_containerSlice = no_container.slice(0, -20);
    }
    else {
        var no_containerSlice = no_container;
    }

    $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/get_checkcontainernumber",
          datatype: 'JSON',
          data: {p_contnumber:no_containerSlice, p_declaredate:tanggal_declare},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Kontainer sudah di Publikasi, Silahkan Pilih Kontainer Lainnya!');
              $("#Container_InfoPublished").show("slow");
              $("#no_container").focus();
              return false;
            }
            else{
              $("#Container_InfoPublished").hide("slow");
              check_containerBanned(no_containerSlice);
              return false;
            }
          }
      });
  }

  function Container_Split()
  {
      var no_container = $("#no_container").val();
      if(no_container.length > 20)
      {
          var no_containerSlice = no_container.slice(0, -20);
      }
      else {
          var no_containerSlice = no_container;
      }

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/get_idcontainer",
          datatype: 'JSON',
          data: {nocontainer:no_containerSlice},
          success: function(data)
          {
            if(data != false)
            {
              data_process(no_containerSlice, data);
            }
            else{
              //alert("Container tidak terdaftar, apakah anda tetap ingin menggunakannya ?");
              data_process(no_containerSlice, 0);
            }
          }
      });
  }

  function data_process(no_containerSlice, id_container)
  {
      var coordinate = $("#id_gudang").val();
      var explode = coordinate.split("||");

      var id_order = $("#id_order").val();
      var do_number = $("#do_number").val();
      var gk_order = $("#gk_order").val();
      var id_depo = $("#id_depo").val();
      var id_driver = $("#id_driver").val();
      var id_commodity = $("#id_commodity").val();
      var id_truck_fleet = $("#id_truck_fleet").val();
      var id_sl = $("#id_sl").val();
      var area = $("#area").val();
      var no_container = no_containerSlice;
      var id_container = id_container;
      var id_gudang = $("#id_gudang").val();;
      var waktu_declare = 'FreshOrder';
      var estimatefreetime = 'FreshOrder';
      var ukuran_container = $("#ukuran_container").val();
      var coordinate_tujuan = 'FreshOrder';
      var bl_number = 'FreshOrder';

      //alert('--iddriver--'+id_driver+'--idcommodity--'+id_commodity+'--idtruckfleet--'+id_truck_fleet+'--idsl--'+id_sl+'--area--'+area+'--nocontainer--'+no_container+'--idcontainer--'+id_container+'--idgudang--'+id_gudang+'--waktudeclare--'+waktu_declare+'--estimatefreetime--'+estimatefreetime+'--ukurancontainer--'+ukuran_container+'--coordinatetujuan--'+coordinate_tujuan+'--blnumber--'+bl_number)

      //check_containernumber();
      if(id_driver=='' || id_commodity=='' || id_truck_fleet=='' || id_sl=='' || area=='' || id_gudang=='' || waktu_declare=='' || estimatefreetime=='' || ukuran_container=='' || coordinate_tujuan=='' || bl_number=='')
      {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
      }

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Published/saveAcceptShipperOrder",
          datatype: 'JSON',
          data: {id_driver:id_driver, id_commodity:id_commodity, id_truck_fleet:id_truck_fleet, id_sl:id_sl, area:area, no_container:no_container, id_container:id_container, id_gudang:id_gudang, waktu_declare:waktu_declare, estimatefreetime:estimatefreetime, ukuran_container:ukuran_container, coordinate_tujuan:coordinate_tujuan, bl_number:bl_number, id_order:id_order, do_number:do_number, gk_order:gk_order, id_depo:id_depo},
          success: function(data) {
            if(data==true)
              {
                  swal({
                      title: "Successfully !",
                      text: "Data successfully saved" ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Done ",
                      cancelButtonText: "Tidak ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        window.location.href = "<?php echo e(url('/')); ?>/Published/matched";
                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
              }
              else{
                  swal({
                      title: "Failed",
                      text: "Data Unsuccesfully saved" ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Close ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
              }
          }
      });
  }

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>