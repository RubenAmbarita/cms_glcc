<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Daftar Driver
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Daftar Driver</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped" style="width:100%;">
                  <thead>
                  <tr>
                    <th colspan="8" style="text-align:right;">
                      <a href="<?php echo e(url('/')); ?>/Masterdata/AddDriver/" class="btn btn-success fa fa-plus-square"> Tambah Driver</a>
                    </th>
                  </tr>

                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Nama Pengemudi</th>
                    <th>Nomor SIM</th>
                    <th>Nomor Telp</th>
                    <th>Email</th>
                    <th style="text-align:center;">Hapus</th>
                    <th style="text-align:center; display:none;">Ubah</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php $__currentLoopData = $driver_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($LoopVar=0+$LoopVar+1); ?></td>
                        <td><?php echo e($row->Register_Date); ?></td>
                        <td><?php echo e($row->name); ?></td>
                        <td><?php echo e($row->no_sim); ?></td>
                        <td><?php echo e($row->no_telp); ?></td>
                        <td><?php echo e($row->email); ?></td>
                        <td style="text-align:center;">
                          <?php if($row->publish_used > 0): ?>
                            <b>Published : <?php echo e($row->publish_used); ?></b>
                          <?php else: ?>
                            <button class="btn btn-danger fa fa-trash" onclick="showCancelMessage(<?php echo e($row->id_driver); ?>)" ></button>
                          <?php endif; ?>
                        </td>
                        <td style="text-align:center; display:none;">
                          <form action="<?php echo e('EditDriver'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="id_driver" name="id_driver" value="<?php echo e($row->id_driver); ?>">
                              <button class="btn btn-info fa fa-eye" type="submit"></button>
                          </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_driver)
  {
      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Masterdata/delete_driver",
                  datatype: 'JSON',
                  data: {id_driver:id_driver},
                  success: function(data)
                  {
                    if(data==0 || data==1)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Masterdata/ListDriver/";
                            
                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";
                                
                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });
            
          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>