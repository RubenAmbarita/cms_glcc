<?php $__env->startSection('content'); ?>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div style="font-family:Poppins;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/dashboard')); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/UsersManagement')); ?>">Users</a></li>
        <li class="active">New</li>
      </ol>
      <h1 style="font-family:Poppins;" >
        Users
        <small>New</small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(url(config('adminlte.register_url', 'UsersManagement/save'))); ?>" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="usr_fullname">Nama Lokasi Gereja <b style="color:red;">*</b></label>
                    <input type="text" id="usr_fullname" name="usr_fullname" class="form-control" value="" placeholder="<?php echo e(trans('auth.full_name')); ?>">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_fullname">Alamat <b style="color:red;">*</b></label>
                    <textarea type="text" id="usr_fullname" name="usr_fullname" class="form-control" value="" placeholder="<?php echo e(trans('auth.full_name')); ?>"></textarea>
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_email">Email <b style="color:red;">*</b></label>
                    <input type="email" id="usr_email" name="usr_email" class="form-control" value=""
                           placeholder="<?php echo e(trans('auth.email')); ?>">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_privileges">Role <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="usr_privileges" name="usr_privileges" style="width: 100%;" >
                    <option value="">--Choose One--</option>
                    <?php $__currentLoopData = $privileges; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($row->code); ?>"><?php echo e($row->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="usr_username">Username <b style="color:red;">*</b></label>
                    <input type="text" id="usr_username" name="usr_username" class="form-control" value="" placeholder="<?php echo e(trans('auth.username')); ?>">
                </div>

                <div class="form-group has-feedback">
                  <label for="usr_password">Password <b style="color:red;">*</b></label>
                    <input type="password" id="usr_password" name="usr_password" class="form-control" placeholder="<?php echo e(trans('auth.password')); ?>">
                </div>
                <div class="form-group has-feedback">
                  <label for="usr_password_conf">Password Verification <b style="color:red;">*</b></label>
                    <input type="password" id="usr_password_conf" name="usr_password_conf" class="form-control" placeholder="<?php echo e(trans('auth.retype_password')); ?>">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer" align="right">
                <button type="button" id="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                <!-- <button type="button" class="btn btn-danger" onclick="cancel_process()"><i class="fa fa-times"></i> Cancel</button> -->
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });


  function cancel_process()
  {
     window.history.back();
  }

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#submit").click(function(){
     // DECLARE OBJECT
     //usr_fullname, usr_email, usr_privileges, usr_username, usr_password, usr_password_conf
     var usr_fullname = $("#usr_fullname").val();
     var usr_email = $("#usr_email").val();
     var usr_privileges = $("#usr_privileges").val();
     var usr_username = $("#usr_username").val();
     var usr_password = $("#usr_password").val();
     var usr_password_conf = $("#usr_password_conf").val();

     if(usr_fullname=='' || usr_email=='' || usr_privileges=='' || usr_username=='' || usr_password=='' || usr_password_conf==''){
        sweetAlert("Oops...","Please fill in all the required fields marked with asterisks (*.", "error");
        // alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
     }
      $.ajax({
          type:'POST',
          url: "<?php echo e(url('/')); ?>/UsersManagement/save",
          datatype: 'JSON',
          data: {usr_fullname:usr_fullname, usr_email:usr_email, usr_privileges:usr_privileges, usr_username:usr_username, usr_password:usr_password, usr_password_conf:usr_password_conf},
          success: function(data) {
              if(data.response){
                sweetAlert("Done!","Create data successfully.", "success");
                // alert('Create data successfully');
                window.location = "<?php echo e(url('/')); ?>/UsersManagement";
              }else{
                sweetAlert("Oops...","Create data failed.", "error");
                // alert('Create data failed');
              }
          }
      });
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>