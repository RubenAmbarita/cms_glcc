<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Driver
        <small>Entry</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tambah Driver</a></li>
        <li class="active">Entry</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="company_name">Company Name</label>
                  <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo e($custname); ?>" placeholder="Nama Perusahaan" disabled>
                  <input type="hidden" class="form-control" id="id_driver" name="id_driver" value="" placeholder="Nama Perusahaan" readonly>
                </div>
                <div class="form-group">
                  <label for="nama_driver">Nama Driver <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="nama_driver" name="nama_driver" placeholder="Nama Driver" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_ktp">Nomor KTP <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="nomor_ktp" name="nomor_ktp" placeholder="Nomor KTP" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_sim">Nomor SIM <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="nomor_sim" name="nomor_sim" placeholder="Nomor SIM" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nomor_telp">Nomor Telepon <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="nomor_telp" name="nomor_telp" placeholder="Nomor Telepon" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="email">Email <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="Email" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="nama_pengguna">Nama Pengguna <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="nama_pengguna" name="nama_pengguna" placeholder="Nama Pengguna" onfocus="this.select();" autocomplete="off" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="kata_sandi">Kata Sandi <b style="color:red;">*</b></label>
                  <input type="password" class="form-control" id="kata_sandi" name="kata_sandi" placeholder="Kata Sandi" onfocus="this.select();" autocomplete="off" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="ulangkata_sandi">Ulang Kata Sandi <b style="color:red;">*</b></label>
                  <input type="password" class="form-control" id="ulangkata_sandi" name="ulangkata_sandi" placeholder="Ulang Kata Sandi" onfocus="this.select();" onmouseup="return false;" required>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea style="height:100px;" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required></textarea>
                </div>                
                <table border="0">
                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload KTP <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_ktp" name="image_ktp"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload SIM <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_sim" name="image_sim"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload SKCK <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_skck" name="image_skck"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload FOTO <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_foto" name="image_foto"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>

                <div class="form-group" style="text-align:right;"><br /><br />
                  <button type="button" class="btn btn-danger" onclick="cancel_process()">Kembali</button>
                  <button type="button" id="submit" class="btn btn-primary">Simpan</button>
                </div>                
              </div>
              <!-- /.box-body -->
              <div class="box-footer" style="text-align:left;">
                &nbsp;
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
#geocomplete { width: 500px}
.map_canvas { 
  width: 600px; 
  height: 400px; 
  margin: 10px 20px 10px 0;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask("9999");
  });

  function cancel_process()
  {
    window.location = "<?php echo e(url('/')); ?>/Masterdata/ListDriver/";
  }
  
  $("#submit").click(function()
  {
      // nama_driver, nomor_ktp, nomor_sim, nomor_telp, email, nama_pengguna, kata_sandi, ulangkata_sandi, image_ktp, image_sim, image_skck, image_foto
      var nama_driver = $("#nama_driver").val();
      var nomor_ktp = $("#nomor_ktp").val();
      var nomor_sim = $("#nomor_sim").val();
      var nomor_telp = $("#nomor_telp").val();
      var email = $("#email").val();
      var nama_pengguna = $("#nama_pengguna").val();
      var kata_sandi = $("#kata_sandi").val();
      var ulangkata_sandi = $("#ulangkata_sandi").val();
      var image_ktp = $("#image_ktp").val();
      var image_sim = $("#image_sim").val();
      var image_skck = $("#image_skck").val();
      var image_foto = $("#image_foto").val();

      if(nama_driver=='' || nomor_ktp=='' || nomor_sim=='' || nomor_telp=='' || email=='' || 
         nama_pengguna=='' || kata_sandi=='' || ulangkata_sandi=='' || image_ktp=='' || 
         image_sim=='' || image_skck=='' || image_foto=='')
      {
        if(nama_driver=='')
        {alert('Kolom Nama Driver tidak boleh kosong!'); $("#nama_driver").focus(); return false;}

        if(nomor_ktp=='')
        {alert('Kolom Nomor KTP tidak boleh kosong!'); $("#nomor_ktp").focus(); return false;}

        if(nomor_sim=='')
        {alert('Kolom Nomor SIM tidak boleh kosong!'); $("#nomor_sim").focus(); return false;}

        if(nomor_telp=='')
        {alert('Kolom Nomor Telp tidak boleh kosong!'); $("#nomor_telp").focus(); return false;}

        if(email=='')
        {alert('Kolom Email tidak boleh kosong!'); $("#email").focus(); return false;}

        if(nama_pengguna=='')
        {alert('Kolom Nama Pengguna tidak boleh kosong!'); $("#nama_pengguna").focus(); return false;}

        if(kata_sandi=='')
        {alert('Kolom Kata Sandi tidak boleh kosong!'); $("#kata_sandi").focus(); return false;}

        if(ulangkata_sandi=='')
        {alert('Kolom Ulang Kata Sandi tidak boleh kosong!'); $("#ulangkata_sandi").focus(); return false;}

        if(image_ktp=='')
        {alert('Kolom Image KTP tidak boleh kosong!'); $("#image_ktp").focus(); return false;}

        if(image_sim=='')
        {alert('Kolom Image SIM tidak boleh kosong!'); $("#image_sim").focus(); return false;}

        if(image_skck=='')
        {alert('Kolom Image SKCK tidak boleh kosong!'); $("#image_skck").focus(); return false;}

        if(image_foto=='')
        {alert('Kolom Image Foto tidak boleh kosong!'); $("#image_foto").focus(); return false;}
      }

      var file_data = $('#image_ktp').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Masterdata/check_ktpsimtelpuser",
          datatype: 'JSON',
          data: {nomor_ktp:nomor_ktp, nomor_sim:nomor_sim, nomor_telp:nomor_telp, nama_pengguna:nama_pengguna},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Data Driver sudah ada!');
              $("#nomor_ktp").focus();
              return false;
            }
            else{
              $.ajax({
                  url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileKTP",
                  dataType: 'text', // what to expect back from the PHP script
                  cache: false,
                  contentType: false,
                  processData: false,
                  data:form_data,
                  type: 'post',
                  success: function (response) {
                      if(response==false)
                      {
                          alert('Upload KTP gagal dilakukan!');
                          return false;
                      }
                      else {
                          uploadFileSIM(response);
                      }
                  },
                  error: function (response) {
                      alert('Upload KTP gagal dilakukan!');
                      return false;
                  }
              }); /* AJAX END */
            }
          }
      });
  });

  function uploadFileSIM(response_KTP)
  {
    var file_data = $('#image_sim').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileSIM",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload SIM gagal dilakukan!');
                return false;
            }
            else {
                uploadFileSKCK(response_KTP, response);
            }
        },
        error: function (response) {
            alert('Upload SIM gagal dilakukan!');
            return false;
        }
    });
  }

  function uploadFileSKCK(response_KTP, response_SIM)
  {
    var file_data = $('#image_skck').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileSKCK",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload SKCK gagal dilakukan!');
                return false;
            }
            else {
                uploadFileFoto(response_KTP, response_SIM, response);
            }
        },
        error: function (response) {
            alert('Upload SKCK gagal dilakukan!');
            return false;
        }
    });
  }

  function uploadFileFoto(response_KTP, response_SIM, response_SKCK)
  {
    var file_data = $('#image_foto').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileFoto",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Foto gagal dilakukan!');
                return false;
            }
            else {
                Save_Data(response_KTP, response_SIM, response_SKCK, response);
                //alert(response_STNK + ' : ' + response_KIR + ' : ' + response_TRUK + ' : ' + response);
            }
        },
        error: function (response) {
            alert('Upload Foto gagal dilakukan!');
            return false;
        }
    });
  }

  function Save_Data(response_KTP, response_SIM, response_SKCK, response_Foto)
  {
      var image_ktp = response_KTP.replace(/"/g, '');
      var image_sim = response_SIM.replace(/"/g, '');
      var image_skck = response_SKCK.replace(/"/g, '');
      var image_foto = response_Foto.replace(/"/g, '');
      var nama_driver = $("#nama_driver").val();
      var nomor_ktp = $("#nomor_ktp").val();
      var nomor_sim = $("#nomor_sim").val();
      var nomor_telp = $("#nomor_telp").val();
      var email = $("#email").val();
      var nama_pengguna = $("#nama_pengguna").val();
      var kata_sandi = $("#kata_sandi").val();
      var ulangkata_sandi = $("#ulangkata_sandi").val();
      var id_driver = $("#id_driver").val();
      
      $.ajax({
        type:'POST',
        url: "<?php echo e(url('/')); ?>/Masterdata/saveDriver",
        datatype: 'JSON',
        data: {nama_driver:nama_driver, nomor_ktp:nomor_ktp, nomor_sim:nomor_sim, nomor_telp:nomor_telp,
               email:email, nama_pengguna:nama_pengguna, kata_sandi:kata_sandi, ulangkata_sandi:ulangkata_sandi,
               image_ktp:image_ktp, image_sim:image_sim, image_skck:image_skck, image_foto:image_foto, id_driver:id_driver},
        success: function(data) {
          if(data==true){
            swal({
                      title: "",
                      text: "Data berhasil disimpan." ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Selesai ",
                      cancelButtonText: "Tidak ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                          window.location.href = "<?php echo e(url('/')); ?>/Masterdata/ListDriver";
                        
                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
            });
          }
          else
          {
            swal({
                      title: "",
                      text: "Data gagal disimpan." ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Tutup ",
                      cancelButtonText: "Tidak ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";
                        
                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
          }
        }
      });

  }








  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Username trucker tidak tersedia atau sudah digunakan,  silakan cari username lain!');
                $("#username").focus();
                return false;
              }
          }
      });
  }


  function save_datatrucker(res_npwp, res_siup)
  {
    var res_npwp = res_npwp.replace(/"/g, '');
    var res_siup = res_siup.replace(/"/g, '');
    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();    
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' || 
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' || 
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' || 
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }
    else{
        check_usertrucker();

        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:res_npwp, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2, 
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:res_siup
            },
            success: function(data) {
            if(data==true){
              alert('Create data successfully');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
            }
            else
            {
              alert('Create data failed');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
              }
            }
        });
    }
  }

//$("#example-form").submit(function(){
  $("#submitxx").click(function()
  {
    //company_name, address_1, address_2, zip_code, city, state, country, email_kantor, 
    //telp_kantor, npwp, image_npwp, name_1, phone_1, telp_1, email_1, name_2, phone_2, telp_2, 
    //email_2, username, password, siup, image_siup

    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();    
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' || 
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' || 
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' || 
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }
    else{
        uploadFileNPWP(image_npwpname, image_siupname);
        check_usertrucker();

        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:image_npwpname, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2, 
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:image_siupname
            },
            success: function(data) {
            if(data==true){
              alert('Create data successfully');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
            }
            else
            {
              alert('Create data failed');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
              }
            }
        });
    }
  });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>