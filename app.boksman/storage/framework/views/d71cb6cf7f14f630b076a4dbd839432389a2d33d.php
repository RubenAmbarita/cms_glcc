<?php $__env->startSection('css'); ?>
	<link href="https://hpneo.github.io/gmaps/prettify/prettify.css" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Published/nomatched">Non-matched Data</a></li>
        <li class="active">Detail</li>
      </ol>
			<h1 style="font-family:Poppins">Non-matched Data</h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Data On the Progress of Your Truck Trips</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <form action="<?php echo e('Order/matchtab1'); ?>" id="formtab1" method="post">
              <?php echo csrf_field(); ?>

              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback" style="padding:0px; margin-left:10px; margin-top:10px; border:0px solid #ccc!important; border-radius:5px; width:100%; height:194px;">
                    <table style="width:100%; height:100px; border="0px">
                        <tr>
                            <td><label style="width:120px;">Delivery Status</label></td>
                            <td style="text-align:center;"><label style="width:20px;"> : </label></td>
                            <td style="width:100%;"><b><?php echo e($row->Shipp_Status); ?></b></td>
                        </tr>

                        <tr>
                            <td><label>Plate #</label></td>
                            <td style="text-align:center;"><label> : </label></td>
                            <td><?php echo e($row->Police_Number); ?></td>
                        </tr>

                        <tr>
                            <?php if($row->Order_Type=='ormatch'): ?>
                            <td><label>Est. Finish Unloading at Consignee</label></td>
                            <?php else: ?>
                            <td><label>Assigned Date</label></td>
                            <?php endif; ?>
                            <td style="text-align:center;"><label> : </label></td>
                            <td><?php echo e($row->estimate_free_time); ?></td>
                        </tr>

                        <tr>
                            <td><label>Shipping Line</label></td>
                            <td style="text-align:center;"><label> : </label></td>
                            <td><?php echo e($row->Shipping_Line); ?></td>
                        </tr>

                        <tr>
                            <td><label>&nbsp;</label></td>
                            <td style="text-align:center;"><label>&nbsp;</label></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="vertical-align:top;"><label>Depo</label></td>
                            <td style="text-align:center; vertical-align:top;"><label> : </label></td>
                            <td>
                            <b><?php echo e($row->Depo_Name); ?></b><br />
                            <?php echo e($row->Depo_Address); ?>

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">
                    <table>
                        <tr>
                            <td colspan="2" style="width:68%;">&nbsp;</td>
                            <td>
                              <br /><br />
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">
                    <table style="padding:0px; margin-top:10px; border:1px solid #ccc!important; border-radius:5px; width:100%; height:160px;">
                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="2" style="padding:10px; width:100%; text-align:left;"><b>&nbsp;<?php echo e($row->Importir); ?></b></td>
                          <td style="padding:10px; width:100%; text-align:right;"><b>CONSIGNEE</b>&nbsp;</td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td><label style="width:120px;">Volume</label></td>
                                    <td style="text-align:center;"><label style="width:20px;"> : </label></td>
                                    <td style="width:100%;"><b><?php echo e($row->Container_Size); ?></b></td>
                                </tr>

                                <tr>
                                    <td><label>Container Number</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Container_Number); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Driver</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Driver); ?></td>
                                </tr>

                                <tr>
                                    <td><label>B/L #</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->BL_Number); ?></td>
                                </tr>
                            </table>
                          </td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td colspan="3"><label style="width:120px;">From :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Importir_Origin); ?>

                                    </td>
                                </tr>

                                <tr><td colspan="3">&nbsp;</tr>

                                <tr>
                                    <td colspan="3"><label style="width:120px;">To :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Importir_Destination); ?>

                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                    </table>

                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                    <table style="padding:0px; margin-top:10px; border:1px solid #ccc!important; border-radius:5px; width:100%; height:200px;">
                        <tr>
                            <td colspan="3"><div id="gmap_markers" style=" height: 100%; " class="gmap"></div></td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">

                            <td colspan="2" style="width:68%;">&nbsp;</td>
                            <tr>
                              <a href="#tab_2" data-toggle="tab" onclick="Refresh_Maps()" >  <button type="button" class="btn btn-success waves-effect"><i class="fa fa-refresh"></i> Refresh</button>

                            </tr>

                </div>

                <div class="form-group has-feedback">
                    <table style="padding:0px; margin-top:10px; border:1px solid #ccc!important; border-radius:5px; width:100%; height:160px; color:#000000">
                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="2" style="padding:10px; width:100%; text-align:left;"><b>&nbsp;<?php echo e($row->Exportir); ?></b></td>
                          <td style="padding:10px; width:100%; text-align:right;"><b>SHIPPER</b>&nbsp;</td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td><label style="width:120px;">Comodity</label></td>
                                    <td style="text-align:center;"><label style="width:20px;"> : </label></td>
                                    <td style="width:100%;"><?php echo e($row->Commodity); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Packaging</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Packaging); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Vessel Voyage</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Vessel_Voyage); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Price</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td>Rp. <?php echo e($row->Basic_Price); ?></td>
                                </tr>
                            </table>
                          </td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td colspan="3"><label style="width:120px;">From :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Exportir_Origin); ?>

                                    </td>
                                </tr>

                                <tr><td colspan="3">&nbsp;</tr>

                                <tr>
                                    <td colspan="3"><label style="width:120px;">To :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Exportir_Destination); ?>

                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                    </table>

                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer" style="text-align:right;">
              </div>
              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDccWhyi1-FYNQOkZYa4Fgzc1VWgrxhyE4&sensor=true"></script>
<script type="text/javascript" src="https://hpneo.github.io/gmaps/gmaps.js"></script>
<script type="text/javascript" src="https://hpneo.github.io/gmaps/prettify/prettify.js"></script>

<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function Refresh_Maps()
  {
    //var Coordinate = "<?php echo e($row->Driver_Coordinate); ?>";
    var Coordinate = "-6.107080,106.880518";
    CoorExplode = Coordinate.split(",");

    prettyPrint();
    var markers = new GMaps({
      div: '#gmap_markers',
      lat: CoorExplode[0],
      lng: CoorExplode[1],
      mapTypeControlOptions: {
        //mapTypeIds : ["hybrid", "roadmap", "satellite", "terrain", "osm"]
        mapTypeIds : [""]
      }
    });

    markers.addMarker({
      lat: CoorExplode[0],
      lng: CoorExplode[1],
      title: "<?php echo e($row->Driver); ?>",
      icon: "<?php echo e(url('images/trucker.png')); ?>"
    });
  }
  $(function(){
    Refresh_Maps();
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     //window.history.back();
     window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_publish)
  {
      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>