<link rel="icon" href="<?php echo e(asset('images/favicon.png')); ?>" type="image/x-icon">

<!DOCTYPE html>
<html>
<head>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo $__env->yieldContent('title_prefix', config('adminlte.title_prefix', '')); ?>
        <?php echo $__env->yieldContent('title', config('adminlte.title', 'GCCC - Control Tower')); ?>
        <?php echo $__env->yieldContent('title_postfix', config('adminlte.title_postfix', '')); ?>
    </title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte.assets/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte.assets/bower_components/font-awesome/css/font-awesome.min.css')); ?>">
  <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte.assets/bower_components/Ionicons/css/ionicons.min.css')); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte.assets/dist/css/AdminLTE.min.css')); ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo e(asset('adminlte.assets/plugins/iCheck/square/blue.css')); ?>">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/dist/css/AdminLTE.min.css')); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/dist/css/skins/_all-skins.min.css')); ?>">
  <style>
    .bg {
       background-image: url("<?php echo e(url('images/gccc.png')); ?>");
        height: 100%;
        width: 100%;
        margin: 0;
        padding : 0;
        background-size: cover;
        background-attachment:fixed;
        font-family: 'Poppins','Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    }

</style>

</head>

<body class="hold-transition skin-yellow fixed login-page bg">
  <?php echo $__env->yieldContent('content'); ?>
</body>
</html>
