<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Masterdata/ListTrans">Trucks</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Trucks</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">List Armada</h3>
            </div> -->
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                    <th colspan="11" style="text-align:right;">
                      <a href="<?php echo e(url('/')); ?>/Masterdata/AddTrans/" class="btn btn-success"><i class="fa fa-plus-square"> </i> New Truck</a>
                    </th>
                  </tr>

                  <tr>
                    <th style="text-align:center;">No.</th>
                    <th style="text-align:center;">Truck</th>
                    <th style="text-align:center;">Plate #</th>
                    <th style="text-align:center;">Production Year</th>
                    <th style="text-align:center;">Truck Type</th>
                    <th style="text-align:center;">Information</th>
                    <th style="text-align:center;">Published</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>

                    <!-- <th style="text-align:center;">Status</th> -->
                    <!-- <th style="text-align:center;">Delete</th> -->
                  </tr>
                  </thead>
                  <tbody>

                  <?php $__currentLoopData = $trans_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($LoopVar=0+$LoopVar+1); ?></td>
                        <td><?php echo e($row->brand_type); ?></td>
                        <td><?php echo e($row->no_polisi); ?></td>
                        <td><?php echo e($row->tahun_pembuatan); ?></td>
                        <td><?php echo e($row->type_truck); ?></td>
                        <td><?php echo e($row->keterangan); ?></td>
                        <td><?php echo e($row->publish_used); ?></td>

                        <td style="text-align:center;">
                          <?php if($row->deleted_at == null): ?>
                            <button class="btn btn-danger" onclick="showNonActiveMessage(<?php echo e($row->id_truck_fleet); ?>)"><i class="fa fa-minus-square-o"></i> Deactive</button>
                          <?php else: ?>
                            <button class="btn btn-success" onclick="showActiveMessage(<?php echo e($row->id_truck_fleet); ?>)" ><i class="fa fa-check-square-o"></i> Active</button>
                          <?php endif; ?>
                        </td>

                        <td style="text-align:center;">
                          <form action="<?php echo e('EditTrans'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="id_truck_fleet" name="id_truck_fleet" value="<?php echo e($row->id_truck_fleet); ?>">
                              <button class="btn btn-info" type="submit"><i class="fa fa-edit"> </i> Edit</button>
                          </form>
                        </td>
                        <!--
                        <td style="text-align:center;">
                          <?php if($row->publish_used > 0): ?>
                            <b>Published : <?php echo e($row->publish_used); ?></b>
                          <?php else: ?>
                            <button class="btn btn-danger fa fa-trash" onclick="showCancelMessage(<?php echo e($row->id_truck_fleet); ?>)" ></button>
                          <?php endif; ?>
                        </td>
                        -->
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th style="text-align:center;">No.</th>
                      <th style="text-align:center;">Truck</th>
                      <th style="text-align:center;">Plate #</th>
                      <th style="text-align:center;">Production Year</th>
                      <th style="text-align:center;">Truck Type</th>
                      <th style="text-align:center;">Information</th>
                      <th style="text-align:center;">Published</th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showActiveMessage(id_truck_fleet)
  {
      swal({
          title: "Are You Sure?",
          text: "Data Will be Reactive !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes ",
          cancelButtonText: "No ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Masterdata/active_trans",
                  datatype: 'JSON',
                  data: {id_truck_fleet:id_truck_fleet},
                  success: function(data)
                  {
                    if(data==0 || data==1)
                    {
                        swal({
                          title: "",
                          text: "Data Successfully Deleted" ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Done ",
                          cancelButtonText: "No ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Masterdata/ListTrans/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "",
                            text: "Data Failed to Saved" ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Close ",
                            cancelButtonText: "No ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

  function showNonActiveMessage(id_truck_fleet)
  {
      swal({
          title: "Are You Sure?",
          text: "Data Will be NonActive !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes ",
          cancelButtonText: "No ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Masterdata/nonactive_trans",
                  datatype: 'JSON',
                  data: {id_truck_fleet:id_truck_fleet},
                  success: function(data)
                  {
                    if(data==0 || data==1)
                    {
                        swal({
                          title: "",
                          text: "Data Successfully Deleted" ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Done ",
                          cancelButtonText: "No ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Masterdata/ListTrans/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "",
                            text: "Data Successfully Saved" ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Close ",
                            cancelButtonText: "No ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>