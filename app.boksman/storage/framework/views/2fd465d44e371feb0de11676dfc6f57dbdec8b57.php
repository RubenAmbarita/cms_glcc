<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ul class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ul>
    <!-- </section>
    <section class="content-header"> -->
      <h1 style="font-family:Poppins">
        Dashboard
      </h1>
    </section>

    <!-- <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section> -->
    <!-- Main content -->

    <section class="content">
		<!-- Chat box -->
    <!-- <div class="box">
      <div class="box-header">
        <h3 class="box-title"></h3>
      </div> -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo e($TotalDashboard->Total_Armada); ?></h3>
              <p>Truck(s)</p>
            </div>
            <div class="icon">
              <i class="fa fa-truck"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Masterdata/ListTrans" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo e($TotalDashboard->Total_Driver); ?><sup style="font-size: 20px"></sup></h3>
              <p>Driver(s)</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Masterdata/ListDriver" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo e($TotalDashboard->Total_Publish); ?></h3>
              <p>Truck(s)</p>
            </div>
            <div class="icon">
              <i class="fa fa-truck"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Published/nomatched" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
             <div class="inner">
				  <h3><?php echo e($TotalDashboard->Total_Matched); ?></h3>
				  <p>Matched Order(s)</p>
            </div>
            <div class="icon">
               <i class="fa fa-truck"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Published/matched" class="small-box-footer">More Info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- </div> -->
    </div>
		<!-- /.Chat box -->
    <!-- </section> -->
    <!-- /.content -->

  <!-- </div> -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>