<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 style="font-family:Poppins">
        Credit Note
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="/Published/CreditNote">Credit Note</a></li>
        <li class="active">Print</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">&nbsp;</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <form action="<?php echo e('Published/CreditNote'); ?>" id="formtab1" method="post">
              <?php echo csrf_field(); ?>

              <div class="box-body" onload="window.print();">
    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
          <img src="<?php echo e(asset('images/bagiinv.png')); ?>" style="width:150px;">
            <small class="pull-right">Tanggal Cetak : <?php echo e($Tanggal_Cetak); ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-6 invoice-col">

        <address>
            <strong>PT. Boksman Asia Anugrah Teknologi</strong><br>
            Jl. Duren Tiga Raya, No 19 Kav 4. Lantai II, RT.8/RW.5, Pancoran, South Jakarta City, Jakarta 12760<br>
            Phone: +62 21 900 9999<br>
            Email: info@boksman.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-6 invoice-col pull-right" >
		   <address class="pull-right">
		    To<br>
            <strong><?php echo e($data->Nama_Trucker); ?></strong><br>
            <?php echo e($data->Alamat_Trucker); ?><br>
            Phone : <?php echo e($data->TelpKantor_Trucker); ?><br>
            Email : <?php echo e($data->EmailKantor_Trucker); ?>

          </address>

        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th colspan="2">Route</th>
              <th>Qty</th>
              <th>Ammount</th>
            </tr>
            </thead>
            <tbody>
            <tr>
				<td style="width:250px;"><strong><?php echo e($data->Nama_Daerah); ?> - Tanjung Priok&nbsp;&nbsp;</strong></td>
				<td><strong> test</strong></td>
				<td>1</td>

				<td><?php echo e($data->TruckValue); ?></td>
            </tr>
			<tr>
				<td style="width:200px;">CN Number</td>
				<td><?php echo e($data->CN_Number); ?></td>
				<td></td>
				<td></td>
            </tr>
			<tr>
				<td style="width:170px;">Police Number</td>
				<td ><?php echo e($data->Police_Number); ?></td>
				<td></td>
				<td></td>
            </tr>
            <tr>
				<td style="width:200px;">Execution Date</td>
				<td ><?php echo e($data->createddate); ?></td>
				<td></td>
				<td></td>
            </tr>
			<tr>
				<td style="width:200px;">Driver Name</td>
				<td><?php echo e($data->Driver_Name); ?></td>
				<td></td>
				<td></td>
            </tr>
			<tr>
				<td style="width:200px;">Container Number</td>
				<td><?php echo e($data->Container_Number); ?></td>
				<td></td>
				<td></td>
            </tr>
			<tr>
				<td style="width:200px;">Ukuran Container</td>
				<td><?php echo e($data->Ukuran_Container); ?></td>
				<td></td>
				<td></td>
      </tr>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-6">
          <p class="lead">Amount</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td><?php echo e($data->SubTotal); ?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?php echo e($data->Total); ?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12" style="text-align:right;">
          <!-- <button type="button" class="btn btn-danger fa fa-backward" onclick="cancel_process()">Kembali</button> -->
          <button type="button" class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
        </div>
      </div>
    </section>
</div>
              <div class="box-footer" style="text-align:right;">
              </div>
              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });


  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function cancel_process()
  {
     //window.history.back();
     window.location = "<?php echo e(url('/')); ?>/Published/CreditNote/";
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>