<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">GLC</a></li>
        <li class="active">List</li>
      </ol>
      <h1>
      Grace Life Cell (GLC)
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div> -->
            <!-- /.box-header -->
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                  <a href="#unpaid" aria-controls="unpaid" role="tab" data-toggle="tab">
                    Daftar GLC
                  </a>
                </li>
                <li role="presentation">
                  <a href="#paid" aria-controls="paid" role="tab" data-toggle="tab">
                    Daftar Terlaksana
                  </a>
                </li>
              </ul><br />

              <!-- Tab panes -->
              <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="unpaid">
              <div class="box-body">
                <table id="tblunpaid" class="table table-bordered table-striped dataTable display nowrap" style="width:100%;">
                <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Nama GLC</th>
                    <th style="text-align:center;">Tanggal & Waktu GLC</th>
                    <th style="text-align:center;">Pemimpin GLC</th>
                    <th></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $order_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($row->Publish_Date); ?></td>
                        <td><?php echo e($row->DO_Number); ?></td>
                        <td><?php echo e($row->GK_Order); ?></td>
                        <td><?php echo e($row->Stuffing_Date); ?></td>
                        <td><?php echo e($row->GK_Order); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('acceptOrder'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="ID_Order" name="ID_Order" value="<?php echo e($row->ID_Order); ?>">
                              <input type="hidden" id="DO_Number" name="DO_Number" value="<?php echo e($row->DO_Number); ?>">
                              <input type="hidden" id="GK_Order" name="GK_Order" value="<?php echo e($row->GK_Order); ?>">
                              <input type="hidden" id="Depo_ID" name="Depo_ID" value="<?php echo e($row->Depo_ID); ?>">
                              <input type="hidden" id="Stuffing_Date" name="Stuffing_Date" value="<?php echo e($row->Stuffing_Date); ?>">
                              <button class="btn btn-warning fa fa-pencil" type="submit"></button>
                          </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Nama GLC</th>
                    <th style="text-align:center;">Tanggal & Waktu GLC</th>
                    <th style="text-align:center;">Pemimpin GLC</th>
                    <th></th>
                  </tr>
                  </tr>
                  </tfoot>
                  </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- tab panel 1 -->
          
          <div role="tabpanel" class="tab-pane" id="paid">
              <div class="box-body">
                <table id="tblpaid" class="table table-bordered table-striped dataTable display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Nama GLC</th>
                    <th style="text-align:center;">Tanggal & Waktu GLC</th>
                    <th style="text-align:center;">Pemimpin GLC</th>
                    <th></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $order_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($row->Publish_Date); ?></td>
                        <td><?php echo e($row->DO_Number); ?></td>
                        <td><?php echo e($row->GK_Order); ?></td>
                        <td><?php echo e($row->Stuffing_Date); ?></td>
                        <td><?php echo e($row->GK_Order); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('acceptOrder'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="ID_Order" name="ID_Order" value="<?php echo e($row->ID_Order); ?>">
                              <input type="hidden" id="DO_Number" name="DO_Number" value="<?php echo e($row->DO_Number); ?>">
                              <input type="hidden" id="GK_Order" name="GK_Order" value="<?php echo e($row->GK_Order); ?>">
                              <input type="hidden" id="Depo_ID" name="Depo_ID" value="<?php echo e($row->Depo_ID); ?>">
                              <input type="hidden" id="Stuffing_Date" name="Stuffing_Date" value="<?php echo e($row->Stuffing_Date); ?>">
                              <button class="btn btn-warning fa fa-pencil" type="submit"></button>
                          </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Nama GLC</th>
                    <th style="text-align:center;">Tanggal & Waktu GLC</th>
                    <th style="text-align:center;">Pemimpin GLC</th>
                    <th></th>
                  </tr>
                  </tr>
                  </tfoot>
                  </table>
                  </div>
              <!-- /.box-body -->
          </div>
          <!-- TAB HALAMAN KEDUA -->
          
          </div>
          <!-- tab-content -->

        </div>
        <!-- tab panel -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function triger_tab()
  {
    $('[href="#paid"]').tab('show');
  }

  $('.nav-tabs a:last').click(function(){
          $(this).tab('show');
          $('#tblpaid').DataTable().draw();
  });

  $(document).ready(function() {
      $('#tblunpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });


      $('#tblpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });
  } );

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_publish)
  {
      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";
                            
                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";
                                
                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });
            
          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>