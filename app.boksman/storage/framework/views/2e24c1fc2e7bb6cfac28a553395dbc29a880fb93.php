<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/masterpelayanan/index">Master Pelayanan </a></li>
        <li class="active">Add</li>
      </ol>
      <h1 style="font-family:Poppins"> Formulir Penambahan Pelayanan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Add Driver</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="nama_driver">Nama Pelayanan<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="nama_pelayanan" name="nama_driver" placeholder="Nama Pelayanan" onfocus="this.select();" onmouseup="return false;" required>
                  </div>
                  <div class="form-group">
                    <label for="lokasi_gereja">Lokasi Gereja<b style="color:red;">*</b></label>
                    <select class="form-control select2" id="id_gereja" name="lokasi_gereja" style="width: 100%;" required >
                      <?php $__currentLoopData = $lokasi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($row->id_gereja); ?>" <?php echo e((old("lokasi_gereja") == $row->id_gereja ? "selected":"")); ?>><?php echo e($row->nama_gereja); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                  </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="nomor_ktp">Divisi Penanggung Jawab<b style="color:red;"> *</b></label>
                  <input type="text" class="form-control" id="divisi_penanggung_jawab" name="divisi_penanggung_jawab" placeholder="Divisi Penanggung Jawab" onfocus="this.select();" onmouseup="return false;" required>
                </div>

                <table border="0">
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Gambar</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Pilih Foto </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Ubah </span>
                              <input type="file" id="nama_gambar" name="nama_gambar"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Hapus </a>
                            </div>
                          </div>
                        </div>
                    </td>
                </table>

                <!-- <div class="form-group">
      						<b>Upload Dokumen</b><br/>
      						<input id="nama_dokumen" type="file" name="file">
      					</div> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">

                </div>

              <!-- /.box-header -->
              <div class="box-body pad">
                <label for="">Deskripsi<b style="color:red;"> *</b></label>
                <form>
                  <textarea id="deskripsi" class="textarea" placeholder="Ketik ..."
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </form>
              </div>

                  <div class="box-body">
                     <button style="float:right" type="button" id="submit" class="btn btn-info"><i class="fa fa-check"></i> Simpan</button>
                   </div>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
#geocomplete { width: 500px}
.map_canvas {
  width: 600px;
  height: 400px;
  margin: 10px 20px 10px 0;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>
<!-- CK Editor -->
<script src="<?php echo e(url('adminlte.assets/bower_components/ckeditor/ckeditor.js')); ?>"></script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function UploadGambar()
  {
    var file_data = $('#nama_gambar').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/masterpelayanan/upload",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        async: false,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Gambar Gagal!');
                return false;
            }
            else {
              res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload KTP Failed!');
        //     return false;
        // }
    }); /* AJAX END */
    return res;
  }

  $("#submit").click(function(){
    var id_gereja = $("#id_gereja").val();
    var id_master_pelayanan = $("#id_master_pelayanan").val();
    var nama_pelayanan = $("#nama_pelayanan").val();
    var divisi_penanggung_jawab = $("#divisi_penanggung_jawab").val();
    var nama_dokumen = $("#nama_dokumen").val();
    var deskripsi = $("#deskripsi").val();
    var nama_gambar = $("#nama_gambar").val();

    if(id_gereja=='' || nama_pelayanan=='' || divisi_penanggung_jawab=='' || deskripsi=='')
    {
      alert('Wajib disi yang Bertanda Bintang (*).');
      return false;
    }

    if($('#nama_gambar').val()){
      nama_gambar = UploadGambar();
    }else{
      nama_gambar = '';
    }

    $.ajax({
        type: 'POST',
        url: "<?php echo e(url('/')); ?>/masterpelayanan/save",
        datatype: 'JSON',
        data: {id_gereja:id_gereja,id_master_pelayanan:id_master_pelayanan, nama_pelayanan:nama_pelayanan, divisi_penanggung_jawab:divisi_penanggung_jawab, nama_dokumen:nama_dokumen, deskripsi:deskripsi, nama_gambar:nama_gambar},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Sukses !",
                    text: "Data Berhasil disimpan." ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "<?php echo e(url('/')); ?>/masterpelayanan/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Gagal",
                    text: "Data Gagal disimpan." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Tutup ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>