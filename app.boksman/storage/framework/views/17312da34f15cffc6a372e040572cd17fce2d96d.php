<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Form Pendaftaran Akun Truk
        <small>Entry</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Register</a></li>
        <li><a href="#">Account</a></li>
        <li class="active">Create</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Mohon di Isi Dengan Benar Formulir Register Ini.</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="company">Company Name <b style="color:red;">*</b></label>
                  <input type="text" name="company_name" class="form-control" id="company_name" placeholder="Company Name" required>
                </div>
                <div class="form-group">
                  <label for="address">Address 1 <b style="color:red;">*</b></label>
                  <textarea class="form-control" id="address_1" name="address_1" placeholder="Address" required></textarea>
                </div>
                <div class="form-group">
                  <label for="address">Address 2 <b style="color:red;">*</b></label>
                  <textarea class="form-control" id="address_2" name="address_2" placeholder="Address" required></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode Zip <b style="color:red;">*</b></label>
                  <input type="text" name="zip_code" class="form-control" id="zip_code" placeholder="Kode Zip" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Kota <b style="color:red;">*</b></label>
                  <input type="text" name="city" class="form-control" id="city" placeholder="Kota" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Provinsi <b style="color:red;">*</b></label>
                  <input type="text" name="state" class="form-control" id="state" placeholder="Provinsi" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Negara <b style="color:red;">*</b></label>
                  <input type="text" id="country" name="country" class="form-control" id="exampleInputEmail1" placeholder="Negara" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email Kantor <b style="color:red;">*</b></label>
                  <input type="email" name="email_kantor" class="form-control" id="email_kantor" placeholder="Email Kantor" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Telp. Kantor <b style="color:red;">*</b></label>
                  <input type="number" name="telp_kantor" class="form-control" id="telp_kantor" placeholder="Telp. Kantor" required>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputEmail1">No NPWP <b style="color:red;">*</b></label>
                  <input type="text" name="npwp" class="form-control" id="npwp" placeholder="NPWP" required>
                </div>
                <div class="form-group">               
                  <label for="exampleInputEmail1">Foto NPWP <b style="color:red;">*</b></label><br />
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                      <div>
                        <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                        <span class="fileinput-new" > Select Foto </span>
                        <span class="fileinput-exists"> Change </span>
                        <input type="file" id="image_npwp" name="image_npwp"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                      </div>
                    </div>
                  </div>  
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Name PIC 1 <b style="color:red;">*</b></label>
                  <input type="text" name="name_1" class="form-control" id="name_1" placeholder="PIC 1" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">No. Handphone PIC 1 <b style="color:red;">*</b></label>
                  <input type="number" name="phone_1" class="form-control" id="phone_1" placeholder="No. Phone 1" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">No. Telp PIC 1 <b style="color:red;">*</b></label>
                  <input type="number" name="telp_1" class="form-control" id="telp_1" placeholder="No. Telp" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email PIC 1 <b style="color:red;">*</b></label>
                  <input type="email" name="email_1" class="form-control" id="email_1" placeholder="Email PIC 1" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Name PIC 2 <b style="color:red;">*</b></label>
                  <input type="text" name="name_2" class="form-control" id="name_2" placeholder="PIC 2" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">No. Handphone PIC 2 <b style="color:red;">*</b></label>
                  <input type="number" name="phone_2" class="form-control" id="phone_2" placeholder="No. Phone 1" required>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">No. Telp PIC 2 <b style="color:red;">*</b></label>
                  <input type="number" name="telp_2" class="form-control" id="telp_2" placeholder="No. Telp" required>
                </div>
               
                <div class="form-group">
                  <label for="exampleInputEmail1">Email PIC 2 <b style="color:red;">*</b></label>
                  <input type="email" name="email_2" class="form-control" id="email_2" placeholder="Email PIC 2" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Username <b style="color:red;">*</b></label>
                  <input type="text" name="username" class="form-control" id="username" placeholder="Username" required>
                  <span id="pesan"></span>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Password <b style="color:red;">*</b></label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">No SIUP <b style="color:red;">*</b></label>
                  <input type="text" name="siup" class="form-control" id="siup" placeholder="SIUP" required>
                </div>        
                <div class="form-group">               
                  <label for="exampleInputEmail1">Foto Siup <b style="color:red;">*</b></label><br />
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                      <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                      </div>
                      <div>
                        <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                        <span class="fileinput-new" > Select Foto </span>
                        <span class="fileinput-exists"> Change </span>
                        <input type="file" id="image_siup" name="image_siup"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer" style="text-align:right;">
                <button type="button" class="btn btn-danger" onclick="cancel_process()">Kembali</button>
                <button type="button" id="submit" class="btn btn-primary">Daftar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
#geocomplete { width: 500px}
.map_canvas { 
  width: 600px; 
  height: 400px; 
  margin: 10px 20px 10px 0;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC0Fa_0maHwtWlLKKzZc6lH2szmSrsuHDg"></script>
<script src="<?php echo e(url('js/jquery.geocomplete.min.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function cancel_process()
  {
     $("#a_logout").click();
     //window.history.back();
  }
  
  function uploadFileNPWP()
  {
    var file_data = $('#image_npwp').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        //url: 'upload.php', // point to server-side PHP script 
        url: "<?php echo e(url('/')); ?>/Registers/UploadFileNPWP",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
          alert(response);
          return false;
            if(response==false)
            {
                alert('Upload gambar gagal dilakukan!');
                return false;
            }
            else {
                var res = response.replace(/"/g, '');
                $("#image_npwpname").val(res);
                uploadFileSIUP();
                //$('#msg').html(response); // display success response from the PHP script
                //$("#a_logout").click();
            }
        },
        error: function (response) {
            alert('Upload gambar gagal dilakukan!');
            return false;
        }
    });
  }

  function uploadFileSIUP(response_NPWP)
  {
    var file_data = $('#image_siup').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        //url: 'upload.php', // point to server-side PHP script 
        url: "<?php echo e(url('/')); ?>/Registers/UploadFileSIUP",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload gambar gagal dilakukan!');
                return false;
            }
            else {
              var p_param = $("#username").val();

              $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
                  datatype: 'JSON',
                  data: {p_param:p_param},
                  success: function(data) {
                      if(data > 0)
                      {
                        alert('Username trucker tidak tersedia atau sudah digunakan,  silakan cari username lain!');
                        $("#username").focus();
                        return false;
                      }
                      else {
                        save_datatrucker(response_NPWP, response);
                      }
                  }
              });
            }
        },
        error: function (response) {
            alert('Upload gambar gagal dilakukan!');
            return false;
        }
    });
  }

  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Username trucker tidak tersedia atau sudah digunakan,  silakan cari username lain!');
                $("#username").focus();
                return false;
              }
          }
      });
  }

  $("#submit").click(function()
  {
      var file_data = $('#image_npwp').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);
      var npwp = $("#npwp").val();
      var siup = $("#siup").val();

      if(npwp == '')
      {alert('NWPW tidak boleh kosong!'); $("#npwp").focus(); return false;}

      if(siup == '')
      {alert('SIUP tidak boleh kosong!'); $("#siup").focus(); return false;}

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/get_npwpsiup",
          datatype: 'JSON',
          data: {npwp:npwp, siup:siup},
          success: function(data)
          {
            if(data > 0)
            {
              alert('No. NPWP atau Nomor SIUP sudah terinput didalam database!');
              return false;
            }
            else{
              $.ajax({
                  //url: 'upload.php', // point to server-side PHP script 
                  url: "<?php echo e(url('/')); ?>/Registers/UploadFileNPWP",
                  dataType: 'text', // what to expect back from the PHP script
                  cache: false,
                  contentType: false,
                  processData: false,
                  data:form_data,
                  type: 'post',
                  success: function (response) {
                      if(response==false)
                      {
                          alert('Upload gambar gagal dilakukan!');
                          return false;
                      }
                      else {
                          //var res = response.replace(/"/g, '');
                          //$("#image_npwpname").val(res);
                          uploadFileSIUP(response);
                          //$('#msg').html(response); // display success response from the PHP script
                          //$("#a_logout").click();
                      }
                  },
                  error: function (response) {
                      alert('Upload gambar gagal dilakukan!');
                      return false;
                  }
              }); /* END Registers/UploadFileNPWP */
            }
          }
      });

  });

  function save_datatrucker(res_npwp, res_siup)
  {
    var res_npwp = res_npwp.replace(/"/g, '');
    var res_siup = res_siup.replace(/"/g, '');
    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();    
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' || 
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' || 
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' || 
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
      if(address_1=='')
      {alert('Kolom Address 1 tidak boleh kosong!'); $("#address_1").focus(); return false;}
        
      if(address_2=='')
      {alert('Kolom Address 2 tidak boleh kosong!'); $("#address_2").focus(); return false;}

      if(zip_code=='')
      {alert('Kolom Zip Code tidak boleh kosong!'); $("#zip_code").focus(); return false;}

      if(city=='')
      {alert('Kolom City tidak boleh kosong!'); $("#city").focus(); return false;}

      if(state=='')
      {alert('Kolom State tidak boleh kosong!'); $("#state").focus(); return false;}

      if(country=='')
      {alert('Kolom Country tidak boleh kosong!'); $("#cuontry").focus(); return false;}

      if(email_kantor=='')
      {alert('Kolom Email Kantor tidak boleh kosong!'); $("#email_kantor").focus(); return false;}

      if(telp_kantor=='')
      {alert('Kolom Telp Kantor tidak boleh kosong!'); $("#telp_kantor").focus(); return false;}

      if(npwp=='')
      {alert('Kolom NPWP tidak boleh kosong!'); $("#npwp").focus(); return false;}

      if(name_1=='')
      {alert('Kolom Name 1 tidak boleh kosong!'); $("#name_1").focus(); return false;}

      if(phone_1=='')
      {alert('Kolom Phone 1 tidak boleh kosong!'); $("#phone_1").focus(); return false;}

      if(telp_1=='')
      {alert('Kolom Telp 1 tidak boleh kosong!'); $("#telp_1").focus(); return false;}

      if(email_1=='')
      {alert('Kolom Email 1 tidak boleh kosong!'); $("#email_1").focus(); return false;}

      if(name_2=='')
      {alert('Kolom Name 2 tidak boleh kosong!'); $("#name_2").focus(); return false;}

      if(phone_2=='')
      {alert('Kolom Phone 2 tidak boleh kosong!'); $("#phone_2").focus(); return false;}

      if(telp_2=='')
      {alert('Kolom Telp 2 tidak boleh kosong!'); $("#telp_2").focus(); return false;}

      if(email_2=='')
      {alert('Kolom Email 2 tidak boleh kosong!'); $("#email_2").focus(); return false;}

      if(username=='')
      {alert('Kolom Username tidak boleh kosong!'); $("#username").focus(); return false;}

      if(password=='')
      {alert('Kolom Password tidak boleh kosong!'); $("#password").focus(); return false;}

      if(siup=='')
      {alert('Kolom SIUP tidak boleh kosong!'); $("#siup").focus(); return false;}
    }
    else{
        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:res_npwp, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2, 
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:res_siup
            },
            success: function(data) {
            if(data==true){
              swal({
                title: "Successfully !",
                text: "Data berhasil disimpan." ,
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Selesai ",
                cancelButtonText: "Tidak ",
                closeOnConfirm: true,
                closeOnCancel: false
              }, function (isConfirm) {
                if (isConfirm) {
                  //window.location.href = "<?php echo e(url('/')); ?>";        
                  $("#a_logout").click();
                } else {
                  //swal("Dibatalkan", " ", "error");
                }
              });
            }
            else
            {
              swal({
                title: "Failed",
                text: "Data gagal disimpan." ,
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Tutup ",
                cancelButtonText: "Tidak ",
                closeOnConfirm: true,
                closeOnCancel: false
              }, function (isConfirm) {
                if (isConfirm) {
                  // window.location.href = "tampilan-data";
                } else {
                  //swal("Dibatalkan", " ", "error");
                }
              });
            }
          }
        });
    }
  }

//$("#example-form").submit(function(){
  $("#submitxx").click(function()
  {
    //company_name, address_1, address_2, zip_code, city, state, country, email_kantor, 
    //telp_kantor, npwp, image_npwp, name_1, phone_1, telp_1, email_1, name_2, phone_2, telp_2, 
    //email_2, username, password, siup, image_siup

    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();    
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' || 
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' || 
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' || 
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }
    else{
        uploadFileNPWP(image_npwpname, image_siupname);
        check_usertrucker();

        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:image_npwpname, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2, 
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:image_siupname
            },
            success: function(data) {
            if(data==true){
              alert('Create data successfully');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
            }
            else
            {
              alert('Create data failed');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
              }
            }
        });
    }
  });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.appreg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>