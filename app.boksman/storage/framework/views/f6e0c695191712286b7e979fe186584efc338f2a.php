<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Masterdata/ListDriver">Drivers</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Drivers </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <div class="box">
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                    <th colspan="8" style="text-align:right;">
                      <a href="<?php echo e(url('/')); ?>/Masterdata/AddDriver/" class="btn btn-success"><i class="fa fa-plus-square"> </i> New Driver</a>
                    </th>
                  </tr>

                  <tr>
                    <th style="text-align:center;">No.</th>
                    <th style="text-align:center;">SIM Expired Date</th>
                    <th style="text-align:center;">Driver Name</th>
                    <th style="text-align:center;">SIM #</th>
                    <th style="text-align:center;">Telephone #</th>
                    <th style="text-align:center;">Email</th>
                    <th style="text-align:center; ">Published</th>
                    <th style="text-align:center; "></th>
                  </tr>
                  </thead>
                  <tbody>

                  <?php $__currentLoopData = $driver_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($LoopVar=0+$LoopVar+1); ?></td>
                        <td><?php echo e($row->expired_sim_date); ?></td>
                        <td><?php echo e($row->name); ?></td>
                        <td><?php echo e($row->no_sim); ?></td>
                        <td><?php echo e($row->no_telp); ?></td>
                        <td><?php echo e($row->email); ?></td>
                        <td style="text-align:center;">
                          <?php if($row->publish_used > 0): ?>
                            <?php echo e($row->publish_used); ?>

                          <?php else: ?>
                            <button class="btn btn-danger" onclick="showCancelMessage(<?php echo e($row->id_driver); ?>)" ><i class="fa fa-trash"></i> Delete</button>
                          <?php endif; ?>
                        </td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('EditDriver'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="id_driver" name="id_driver" value="<?php echo e($row->id_driver); ?>">
                              <button class="btn btn-info" type="submit"><i class="fa fa-edit"> </i> Edit</button>
                          </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th style="text-align:center;">No.</th>
                      <th style="text-align:center;">SIM Expired Date</th>
                      <th style="text-align:center;">Driver Name</th>
                      <th style="text-align:center;">SIM #</th>
                      <th style="text-align:center;">Telephone #</th>
                      <th style="text-align:center;">Email</th>
                      <th style="text-align:center;">Published</th>
                      <th style="text-align:center;"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_driver)
  {
      swal({
          title: "Are You Sure?",
          text: "Data Will be Permanently Deleted !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes ",
          cancelButtonText: "No ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Masterdata/delete_driver",
                  datatype: 'JSON',
                  data: {id_driver:id_driver},
                  success: function(data)
                  {
                    if(data==0 || data==1)
                    {
                        swal({
                          title: "",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Masterdata/ListDriver/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>