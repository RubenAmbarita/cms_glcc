<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Data perkembangan perjalanan Truck anda
        <small>Match</small>
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="#">Lokasi Gereja</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Lokasi Gereja</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Data On the Progress of Your Truck Trips</h3>
            </div> -->
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <th colspan="7" style="text-align:right;">
                    <a href="<?php echo e(action('UsersManagementController@create')); ?>" class="btn btn-success"> <i class="fa fa-plus-square"></i> Tambah Lokasi Gereja</a>
                  </th>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Nama Lokasi</th>
                    <th style="text-align:center;">Provinsi</th>
                    <th style="text-align:center;">Kota/Kabupaten</th>
                    <th style="text-align:center;">PIC</th>
                    <th style="text-align:center;"># Handphone</th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $matched_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($row->Publish_Date); ?></td>
                        <td><?php echo e($row->BL_Number); ?></td>
                        <td><?php echo e($row->gk_order); ?></td>
                        <td><?php echo e($row->Importir); ?></td>
                        <td><?php echo e($row->Exportir); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('viewmatched'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="Publish_Number" name="Publish_Number" value="<?php echo e($row->Publish_Number); ?>">
                              <button class="btn btn-info" type="submit"><i class="fa fa-eye"></i> View</button>
                          </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <th style="text-align:center;">Nama Lokasi</th>
                    <th style="text-align:center;">Provinsi</th>
                    <th style="text-align:center;">Kota/Kabupaten</th>
                    <th style="text-align:center;">PIC</th>
                    <th style="text-align:center;"># Handphone</th>
                    <th style="text-align:center;"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_publish)
  {
      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>