<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name='csrf-token' content="<?php echo e(csrf_token()); ?>" />
    <title>
      <?php echo $__env->yieldContent('title_prefix', config('adminlte.title_prefix', '')); ?>
      <?php echo $__env->yieldContent('title', config('adminlte.title', 'CMS Trucker || boksman.com')); ?>
      <?php echo $__env->yieldContent('title_postfix', config('adminlte.title_postfix', '')); ?>
    </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo e(asset('images/favicon.png')); ?>" type="image/x-icon">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/font-awesome/css/font-awesome.min.css')); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/Ionicons/css/ionicons.min.css')); ?>">

  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">

  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/select2/dist/css/select2.min.css')); ?>">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/dist/css/AdminLTE.min.css')); ?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/dist/css/skins/_all-skins.min.css')); ?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/morris.js/morris.css')); ?>">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/jvectormap/jquery-jvectormap.css')); ?>">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')); ?>">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/plugins/timepicker/bootstrap-timepicker.min.css')); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.css')); ?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>">

  <link rel="stylesheet" href="<?php echo e(url('adminlte.assets/sweetalert/sweetalert.css')); ?>">
  
  <link rel="stylesheet" href="<?php echo e(url('css/bagibagi.css')); ?>">

  <?php echo $__env->yieldContent('css'); ?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo e(url('dashboard')); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Boks</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
        <!--<img src="<?php echo e(url('images/bagibagi.png')); ?>" class="user-image" alt="User Image">-->
        <b>BoksMan</b>.Com
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="hidden-xs" style="font-size:20px;"><?php echo e(session()->get('custname')); ?></span>
          </a>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span id="master_count" class="label label-warning"><?php echo e($notifcount); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><span id="detail_count"><b>You have <?php echo e($notifcount); ?> notifications</b></span></li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                <?php $__currentLoopData = $notification; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li>
                    <a href="<?php echo e(url('Order/history/')); ?>/<?php echo e($row->id_notifikasi); ?>">
                      <i></i><b><?php echo e($row->No_GK); ?></b><br /><i class='fa fa-edit text-aqua'></i> <?php echo e($row->message); ?>

                    </a><!-- class="fa fa-users text-aqua"-->
                  </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </li>
              <li class="footer"><a href="#">&nbsp;</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo e(url('images/user-icon.png')); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo e(Auth::user()->name); ?></span>
              &nbsp;<i class="fa fa-fw fa-power-off"></i>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo e(url('images/user-icon.png')); ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo e(Auth::user()->name); ?>

                  <small>&nbsp;</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  &nbsp;
                  <!--<a href="#" class="btn btn-default btn-flat"></a>-->
                </div>
                <div class="pull-right">
                  <a href="<?php echo e(route('logout')); ?>" class="btn btn-default btn-flat"
                      onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        Logout
                  </a>
                  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                      <?php echo e(csrf_field()); ?>

                  </form>
                </div>

              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo e(url('images/user-icon.png')); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo e(Auth::user()->name); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> <b>Online</b></a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li  class="<?php if($menu_active=='dashboard'): ?> active <?php else: ?> '' <?php endif; ?>">
          <a href="<?php echo e(url('dashboard')); ?>">
            <i class="fa fa-home"></i> <span><b>Dashboard</b></span>
            <span class="pull-right-container">
              
            </span>
          </a>
        </li>
 
        <li class="treeview <?php if($menu_active=='ListTrans'): ?> active <?php elseif($menu_active=='ListDriver'): ?> active <?php else: ?> '' <?php endif; ?>">
          <a href="#">
            <i class="fa fa-list"></i>
            <span><b>List Pengemudi/Truk</b></span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($menu_active=='ListTrans'): ?> active <?php else: ?> '' <?php endif; ?>"><a href="<?php echo e(url('Masterdata/ListTrans')); ?>"><i class="fa fa-circle-o"></i> <b>Data Armada Truk</b></a></li>
            <li class="<?php if($menu_active=='ListDriver'): ?> active <?php else: ?> '' <?php endif; ?>"><a  href="<?php echo e(url('Masterdata/ListDriver')); ?>"><i class="fa fa-circle-o"></i> <b>Data Pengemudi Truk</b></a></li> 
          </ul>
        </li>

        <li  class="<?php if($menu_active=='Published'): ?> active <?php else: ?> '' <?php endif; ?>">
          <a href="<?php echo e(url('Published')); ?>">
            <i class="fa fa-truck"></i> <span><b>Publikasi Armada</b></span>
            <span class="pull-right-container">              
            </span>
          </a>
        </li>

        <li  class="<?php if($menu_active=='Assignment'): ?> active <?php else: ?> '' <?php endif; ?>">
          <a href="<?php echo e(url('Published/assignOrder')); ?>">
            <i class="fa fa-truck"></i> <span><b>Penugasan (Order)</b></span>
            <span class="pull-right-container">              
            </span>
          </a>
        </li>

        <li class="treeview <?php if($menu_active=='matched'): ?> active <?php elseif($menu_active=='viewmatched'): ?> active <?php elseif($menu_active=='nomatched'): ?> active <?php elseif($menu_active=='viewnomatched'): ?> active <?php elseif($menu_active=='history'): ?> active <?php elseif($menu_active=='viewhistory'): ?> active <?php else: ?> '' <?php endif; ?>">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span><b>Laporan Truk</b> </span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php if($menu_active=='matched'): ?> active <?php elseif($menu_active=='viewmatched'): ?> active <?php else: ?> '' <?php endif; ?>"><a href="<?php echo e(url('Published/matched')); ?>"><i class="fa fa-circle-o"></i> <b>Data Sesuai</b></a></li>
            <li class="<?php if($menu_active=='nomatched'): ?> active <?php elseif($menu_active=='viewnomatched'): ?> active <?php else: ?> '' <?php endif; ?>"><a href="<?php echo e(url('Published/nomatched')); ?>"><i class="fa fa-circle-o"></i><b>Data Belum Sesuai</b></a></li>
            <li class="<?php if($menu_active=='history'): ?> active <?php elseif($menu_active=='viewhistory'): ?> active <?php else: ?> '' <?php endif; ?>"><a href="<?php echo e(url('Published/history')); ?>"><i class="fa fa-circle-o"></i> <b>Riwayat</b></a></li> 
          </ul>
        </li>

        <li class="header">LABELS</li>
        <?php if(session()->get('status_internal')==1): ?>
        <li class="<?php if($menu_active=='OrderCancel'): ?> active <?php else: ?> '' <?php endif; ?>"><a href="<?php echo e(url('Published/OrderCancel')); ?>"><i class="fa fa-book"></i> <span><b>Order Cancel</b></span></a></li> 
        <?php endif; ?>
        <li class="<?php if($menu_active=='CreditNote'): ?> active <?php else: ?> '' <?php endif; ?>"><a href="<?php echo e(url('Published/CreditNote')); ?>"><i class="fa fa-book"></i> <span><b>Credit Note</b></span></a></li> 
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  
  <?php echo $__env->yieldContent('content'); ?>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 5.0.0
    </div>
    <strong>Copyright &copy; 2019 <a target="_blank" href="http://www.boksman.com/">BoksMan</a>.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

        <button id="id_loading" type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default" onclick="MyProgressBar()" style="display:none;">
                Launch Default Modal
        </button>
        <button id="id_success" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-success" style="display:none;">
                Launch Info Modal
        </button>
        <button id="id_failed" type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger" style="display:none;">
                Launch Danger Modal
        </button>

        <div class="modal modal-success fade" id="modal-success">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Informasi Proses Berhasil</h4>
              </div>
              <div class="modal-body">
                <!--<p>One fine body&hellip;</p>-->
                <p>Data berhasil disimpan!</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Mohon tunggu, proses penyimpanan data sedang berjalan !</h4>
              </div>
              <div class="modal-body" style="text-align:center;">
                <div id="myProgress">
                  <div id="myBar"></div>
                </div>
                <div id="myPercent" style="text-align:right;">0%</div>
                <div style="display:none;" class="loader"></div>                
              </div>

              <div class="modal-footer">
                <button id="id_close" style="display:none;" type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal modal-danger fade" id="modal-danger">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Informasi Proses Gagal</h4>
              </div>
              <div class="modal-body">
                <p>Data Gagal Disimpan!</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline]" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-ui/jquery-ui.min.js')); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!--
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
-->

<!-- DataTables -->
<script src="<?php echo e(url('adminlte.assets/bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>

<!-- Select2 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Morris.js charts -->
<script src="<?php echo e(url('adminlte.assets/bower_components/raphael/raphael.min.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/morris.js/morris.min.js')); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')); ?>"></script>
<!-- jvectormap -->
<script src="<?php echo e(url('adminlte.assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')); ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-knob/dist/jquery.knob.min.js')); ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/moment/min/moment.min.js')); ?>"></script>

<!-- bootstrap time picker -->
<script src="<?php echo e(url('adminlte.assets/timepicker/jquery.ui.datepicker.min.js')); ?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!-- bootstrap color picker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')); ?>"></script>
<!-- bootstrap time picker -->
<script src="<?php echo e(url('adminlte.assets/plugins/timepicker/bootstrap-timepicker.min.js')); ?>"></script>
<!-- bootstrap time picker -->
<script src="<?php echo e(url('adminlte.assets/timepicker/jquery.ui.datepicker.min.js')); ?>"></script>



<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo e(url('adminlte.assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<!-- Slimscroll -->
<script src="<?php echo e(url('adminlte.assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- FastClick -->
<script src="<?php echo e(url('adminlte.assets/bower_components/fastclick/lib/fastclick.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo e(url('adminlte.assets/dist/js/adminlte.min.js')); ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes)
<script src="<?php echo e(url('adminlte.assets/dist/js/pages/dashboard.js')); ?>"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- InputMask -->
<script src="<?php echo e(url('adminlte.assets/plugins/input-mask/jquery.inputmask.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/plugins/input-mask/jquery.inputmask.date.extensions.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/plugins/input-mask/jquery.inputmask.extensions.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/sweetalert/sweetalert.min.js')); ?>"></script>

<script src="<?php echo e(url('js/jquery.numeric.pack.js')); ?>"></script>

<!--<script src="<?php echo e(url('adminlte.assets/dist/js/demo.js')); ?>"></script>-->

<?php echo $__env->yieldContent('javascript'); ?>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable(
    {
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });

  function MyProgressBar() {
    var elem = document.getElementById("myBar"); 
    var per = document.getElementById("myPercent"); 
    var width = 1;
    var id = setInterval(frame, 60);
    function frame() {
        if (width >= 100) {
            clearInterval(id);
        } else {
            width++; 
            elem.style.width = width + '%'; 
            per.innerHTML = width * 1 + '%';
        }
    }
 }

</script>

</body>
</html>
