<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="/glc/index">GLC</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Grace Life Cell (GLC)</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                  <a href="#unpaid" aria-controls="unpaid" role="tab" data-toggle="tab">
                    Daftar GLC
                  </a>
                </li>
                <li role="presentation">
                  <a href="#paid" aria-controls="paid" role="tab" data-toggle="tab">
                    Daftar Terlaksana
                  </a>
                </li>
              </ul><br />

              <!-- Tab panes -->
              <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="unpaid">
              <div class="box-body">
                <table id="tblunpaid" class="table table-bordered table-striped dataTable display nowrap" style="width:100%;">
                  <div class="row">
                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('lokasi') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="lokasi" name="lokasi" required >
                      <option value="NOL">Pilih Lokasi Gereja</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('bulan') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('tahun') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                    <div class="col-md-2">
                    <a href="<?php echo e(action('GlcController@addglc')); ?>" style="margin-left: 54px;" id="add" class="btn btn-success" type="submit" name="button"><i class="fa fa-plus-square"></i> Tambah GLC</a>
                    </div>
                </div>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Nama GLC</th>
                    <th style="text-align:center;">Tanggal & Waktu GLC</th>
                    <th style="text-align:center;">Pemimpin GLC</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td ><?php echo e($row->nama_gereja); ?></td>
                        <td ><?php echo e($row->nama_glc); ?></td>
                        <td ><?php echo e($row->tanggal_waktu_glc); ?></td>
                        <td ><?php echo e($row->nama_pemimpin); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('daftarpeserta'); ?>" method="post">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" id="id_glc" name="id_glc" value="<?php echo e($row->id_glc); ?>">
                            <button id="add_peserta" class="btn btn-info" onclick="" type="submit" name="button"><i class="fa fa-users"></i> Peserta</button>
                          </form>
                        </td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('detail'); ?>" method="post">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" id="id_glc" name="id_glc" value="<?php echo e($row->id_glc); ?>">
                            <button class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                          </form>
                        </td>
                        <td style="text-align:center;">
                          <button id="hapus" class="btn btn-warning" onclick="showCancelMessage(<?php echo e($row->id_glc); ?>)" type="button" name="button"><i class="fa fa-list-alt"></i> Terlaksana</button>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Lokasi Gereja</th>
                      <th style="text-align:center;">Nama GLC</th>
                      <th style="text-align:center;">Tanggal & Waktu GLC</th>
                      <th style="text-align:center;">Pemimpin GLC</th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- tab panel 1 -->

            <div role="tabpanel" class="tab-pane" id="paid">
              <div class="box-body">
                <table id="tblpaid" class="table table-bordered table-striped dataTable display nowrap" cellspacing="0" width="100%">
                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('lokasi') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="lokasi" name="lokasi" required >
                      <option value="NOL">Pilih Lokasi Gereja</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('bulan') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('tahun') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Nama GLC</th>
                    <th style="text-align:center;">Tanggal & Waktu GLC</th>
                    <th style="text-align:center;">Pemimpin GLC</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $list_terlaksana; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td ><?php echo e($row->nama_gereja); ?></td>
                      <td ><?php echo e($row->nama_glc); ?></td>
                      <td ><?php echo e($row->tanggal_waktu_glc); ?></td>
                      <td ><?php echo e($row->nama_pemimpin); ?></td>
                      <td style="text-align:center;">

                        <form action="<?php echo e('daftarpeserta'); ?>" method="post">
                          <?php echo e(csrf_field()); ?>

                          <input type="hidden" id="id_glc" name="id_glc" value="<?php echo e($row->id_glc); ?>">
                          <button id="add_peserta" class="btn btn-info" onclick="" type="submit" name="button"><i class="fa fa-users"></i> Peserta</button>
                        </form>
                      </td>
                      <td style="text-align:center;">
                        <form action="<?php echo e('detail'); ?>" method="post">
                          <?php echo e(csrf_field()); ?>

                          <input type="hidden" id="id_glc" name="id_glc" value="<?php echo e($row->id_glc); ?>">
                          <button id="detail" class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                        </form>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Lokasi Gereja</th>
                      <th style="text-align:center;">Nama GLC</th>
                      <th style="text-align:center;">Tanggal & Waktu GLC</th>
                      <th style="text-align:center;">Pemimpin GLC</th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- TAB HALAMAN KEDUA -->

          </div>
          <!-- tab-content -->

        </div>
        <!-- tab panel -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function triger_tab()
  {
    $('[href="#paid"]').tab('show');
  }

  $('.nav-tabs a:last').click(function(){
          $(this).tab('show');
          $('#tblpaid').DataTable().draw();
  });

  $(document).ready(function() {
      $('#tblunpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });


      $('#tblpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });
  } );

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  function showCancelMessage(id_glc)
  {
      swal({
          title: "Pelayanan Telah Terlaksana?",
          type: "warning",
          confirmButtonColor: "#ffa600",
          confirmButtonText: "Terlaksana ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/glc/update_status",
                  datatype: 'JSON',
                  data: {id_glc:id_glc},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Sukses !",
                          text: "Data berhasil diupdate." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/glc/index";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Gagal",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>