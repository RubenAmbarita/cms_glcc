<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Masterdata/ListTrans"> Trucks </a></li>
        <li class="active">Edit</li>
      </ol>
      <h1 style="font-family:Poppins"> Edit Truck </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Add Armada</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="company_name">Company Name</label>
                  <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo e($custname); ?>" placeholder="Nama Perusahaan" disabled>
                  <input type="hidden" class="form-control" id="id_truck_fleet" name="id_truck_fleet" value="<?php echo e($trans->id_truck_fleet); ?>" placeholder="Nama Perusahaan" readonly>
                </div>
                <div class="form-group">
                  <label for="Police_Number">Plate # <b style="color:red;">*</b></label>
                  <input type="text" oninput="calculate(this)" onkeypress="return isNumber(event)" class="form-control" id="Police_Number" value="<?php echo e($trans->no_polisi); ?>"name="Police_Number" placeholder="Plate #" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="brand_type">Truck Brand <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="brand_type" name="brand_type" style="width: 100%;" required >
                    <?php $__currentLoopData = $listBrand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($row->mst_codedetail); ?>" <?php echo e((old("brand_type") == $row->mst_codedetail ? "selected":"")); ?>><?php echo e($row->mst_codedesc); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="Created_Year">Production Year <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" data-inputmask="'alias': 'yyyy'" data-mask id="Created_Year" name="Created_Year" value="<?php echo e($trans->tahun_pembuatan); ?>" placeholder="Year" required>
                </div>
                <div class="form-group">
                  <label for="Trasport_Type">Truck Type <b style="color:red;">*</b></label>
                  <select id="Trasport_Type" name="Trasport_Type" class="form-control"   >
                    <option value="TRAILER">TRAILER</option>
                  </select>
                </div>
                <div class="row">
                  <div class="col-md-6">
                <div class="form-group">
                  <label for="tanggal_declare">KIR Head Expired <b style="color:red;">*</b></label>
                  <div class="input-group">
                    <input id="tanggal_declare" type="text" autocomplete="off" name="tanggal_declare" value="<?php echo e($trans->kir_head_expired); ?>" placeholder="Tanggal (dd/mm/yyyy)" class="form-control" >
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="tanggal_declare2">KIR Chasis Expired <b style="color:red;">*</b></label>
                  <div class="input-group">
                    <input id="tanggal_declare2" type="text" autocomplete="off" name="tanggal_declare2" value="<?php echo e($trans->kir_chasis_expired); ?>" placeholder="Tanggal (dd/mm/yyyy)" class="form-control" >
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                  </div>
                </div>
              </div>
              </div>
                <div class="form-group" style="display:none;">
                  <table id="table_insurance">
                    <tr>
                      <td>
                        <label for="insurance_value">Harga Armada <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_value_view" name="insurance_value_view" style="width: 100%;" onkeyup="Insurance_Value()" value="0">
                        <input type="hidden" id="insurance_value" name="insurance_value" style="width: 100%;" value="0">
                      </td>
                      <td style="width:20px;">&nbsp;</td>
                      <td>
                        <label for="insurance_type">Asuransi <b style="color:red;">*</b></label>
                          <select class="form-control select2" id="insurance_type" name="insurance_type" style="width: 100%;" required >
                            <?php $__currentLoopData = $listInsurance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($row->mst_codedetail); ?>" <?php echo e((old("insurance_type") == $row->mst_codedetail ? "selected":"")); ?>><?php echo e($row->mst_codedesc); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                      </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr id="tr_insurance_1" style="display:none">
                      <td>
                        <label for="insurance_policy">Nomor Polis <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_policy" name="insurance_policy" style="width: 100%;" value="" readonly>
                      </td>
                      <td style="width:20px;">&nbsp;</td>
                      <td>
                        <label for="insurance_premi">Premi <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_premi_view" name="insurance_premi_view" style="width: 100%;" value="0" readonly>
                        <input type="hidden" id="insurance_premi" name="insurance_premi" style="width: 100%;" value="0" readonly>
                      </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr id="tr_insurance_2" style="display:none">
                      <td>
                        <label for="insurance_admin">Admin <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_admin_view" name="insurance_admin_view" style="width: 100%;" value="0" readonly>
                        <input type="hidden" id="insurance_admin" name="insurance_admin" style="width: 100%;" value="0" readonly>
                      </td>
                      <td style="width:20px;">
                        &nbsp;
                      </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>
                <div class="form-group">
                  <label for="description">Information</label>
                  <input class="form-control" id="description" value="<?php echo e($trans->keterangan); ?>" name="description" placeholder="Information" required></textarea>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <table border="0">
                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">STNK ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="../images/trucker/npwp/NPWP_1582014457_thump.png" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_stnk" name="image_stnk"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:0px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Image Truck ( Max 1MB )</label>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_truk" name="image_truk"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>

                  </tr>

                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">KIR Head ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_kir" name="image_kir"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">KIR Chasis ( Max 1MB )</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_kir_chasis" name="image_kir_chasis"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Polis Insurance ( Max 1MB )</label>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Photo </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                              <input type="file" id="image_asuransi" name="image_asuransi"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                            </div>
                          </div>
                        </div>
                    </td>
                  </tr>
                </table>
                <div class="form-group">
                  <label>
                    <input type="checkbox" id="cek" value="1" name="check[]" onchange="checkbox();" class="flat-red">
                    By this I, as representative of this company, declare that all information given here is true.
                  </label>
                </div>
                <table>
                  <tr>
                  <td>
                  </td>
                <td style="width:350px;"></td>
                 <td>
                  <div class="form-group" style="text-align:right">
                    <!-- <button type="button" class="btn btn-success" onclick="cancel_process()"><i class="fa fa-backward"></i> Back </button> -->
                    <button type="button" id="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                  </div>
                </td>
                </tr>
                </table>
              </div>
              <!-- /.box-body -->
              <div class="box-footer" style="text-align:left;">
                &nbsp;
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
#geocomplete { width: 500px}
.map_canvas {
  width: 600px;
  height: 400px;
  margin: 10px 20px 10px 0;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>


<script>

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.charCode) ? event.charCode : ((evt.which) ? evt.which : evt.keyCode);
  if (charCode == 32) {
    return false;
  }
  return true;
}

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask("9999");
  });

  $(function()
    {
      $("#tanggal_declare" ).datepicker({
          minDate: 0,
          dateFormat: "d-m-Y"
      });
  });

  $(function()
    {
      $("#tanggal_declare2" ).datepicker({
          minDate: 0,
          dateFormat: "d-m-Y"
      });
  });

  function cancel_process()
  {
    window.location = "<?php echo e(url('/')); ?>/Masterdata/ListTrans/";
  }

  function number_format(user_input){
    var filtered_number = user_input.replace(/[^0-9]/gi, '');
    var length = filtered_number.length;
    var breakpoint = 1;
    var formated_number = '';

    for(i = 1; i <= length; i++){
        if(breakpoint > 3){
            breakpoint = 1;
            formated_number = ',' + formated_number;
        }
        var next_letter = i + 1;
        formated_number = filtered_number.substring(length - i, length - (i - 1)) + formated_number;

        breakpoint++;
    }

    return formated_number;
  }

  $("#insurance_value_view").focusout(function(){
      var insval = $("#insurance_value_view").val();
      $("#insurance_value_view").val(number_format(insval));

//          $("#tr_insurance_1").hide('slow');
 //         $("#tr_insurance_2").hide('slow');

          $("#insurance_policy").val('');
          $("#insurance_premi_view").val(0);
          $("#insurance_admin_view").val(0);
          $("#insurance_premi").val(0);
          $("#insurance_admin").val(0);
  });

  function Insurance_Value()
  {
      var p_param = $("#or_insurancevalue_view").val();

      $("#or_insurancevalue").val(p_param);
  }

  $("select[name='insurance_type']").change(function(){
      var p_param = $(this).val();
      //var orinsurance_text = $("#or_insurance option:selected").html();
      //$('#ordertype_text').val(ordertype_text);

      if(p_param=='ins000')
      {
          $("#tr_insurance_1").hide('slow');
          $("#tr_insurance_2").hide('slow');
          //$("#or_insurancevalue_view").val(0);
          //$("#or_insurancevalue").val(0);

          $("#insurance_policy").val('');
          $("#insurance_premi_view").val(0);
          $("#insurance_admin_view").val(0);
          $("#insurance_premi").val(0);
          $("#insurance_admin").val(0);
      }
      else{
          $("#tr_insurance_1").show('slow');
          $("#tr_insurance_2").show('slow');

          var brand_type = $("#brand_type").val();
          var Created_Year = $("#Created_Year").val();
          var Trasport_Type = $("#Trasport_Type").val();
          var insurancevalue = $("#insurancevalue").val();

          $.ajax({
            type: 'POST',
            url: "<?php echo e(url('/')); ?>/Masterdata/get_InsuranceAPI",
            datatype: 'JSON',
            data: {p_param:p_param, brand_type:brand_type, Created_Year:Created_Year, Trasport_Type:Trasport_Type, insurancevalue:insurancevalue},
            success: function(data) {
                $("#insurance_policy").val(data.Policy);
                $("#insurance_premi_view").val(number_format(data.Premi));
                $("#insurance_admin_view").val(number_format(data.Admin));
                $("#insurance_premi").val(data.Premi);
                $("#_insurance_admin").val(data.Admin);
            }
          });
      }
  });

  $("#submit").click(function()
  {
      var image_stnk = $('#image_stnk').val();
      var brand_type = $('#brand_type').val();
      var Police_Number = $('#Police_Number').val();
      var Created_Year = $('#Created_Year').val();
      var Trasport_Type = $('#Trasport_Type').val();
      var tanggal_declare = $('#tanggal_declare').val();
      var tanggal_declare2 = $('#tanggal_declare2').val();

      var chks = document.getElementsByName('check[]');
      var hasChecked = false;

      for (var i = 0; i < chks.length; i++)
      {
      if (chks[i].checked)
      {
      hasChecked = true;
      break;
      }else {
      alert("Please select term & condition.");
      return false;
        }
      }

      if(Police_Number=='' || Created_Year=='' || Trasport_Type=='' || brand_type == 'brd000' || tanggal_declare=='' || tanggal_declare2=='')
      {
        if($('#brand_type'=='brd000'))
        {alert('Column Truck Brand Cant Empty!'); $("#brand_type").focus(); return false;}

        if(Police_Number=='')
        {alert('Column Police Number Cant Empty!'); $("#Police_Number").focus(); return false;}

        if(Created_Year=='')
        {alert('Column Production Year Cant Empty!'); $("#Created_Year").focus(); return false;}

        if(Trasport_Type=='')
        {alert('Column Truck Type Cant Empty!'); $("#Trasport_Type").focus(); return false;}

        if(tanggal_declare==''){
          alert('Column KIR Head Expired Cant Empty!');$("tanggal_declare").focus();return false;
        }

        if(tanggal_declare2==''){
          alert('Column KIR Chasis Expired Cant Empty!');$("tanggal_declare2").focus();return false;
        }
      }else{
                if($('#image_asuransi').val()){
                  response_image_asuransi = uploadFileASURANSI();
                }else{
                  response_image_asuransi = '';
                }

                if($('#image_kir').val()){
                  response_images_kir = uploadFileKIR();
                }else{
                  response_images_kir = '';
                }

                if($('#image_truk').val()){
                  response_images_truk = uploadFileTRUK();
                }else{
                  response_images_truk = '';
                }

                if($('#image_stnk').val()){
                  response_images_stnk = uploadFileSTNK();
                }else{
                  response_images_stnk = '';
                }

                if($('#image_kir_chasis').val()){
                  response_image_kir_chasis = uploadFileKirChasis();
                }else{
                  response_image_kir_chasis = '';
                }
                Save_Data(response_image_asuransi,response_images_kir,response_images_truk,response_images_stnk,response_image_kir_chasis);
      }
  });

  function uploadFileKirChasis(){
    var file_data = $('#image_kir_chasis').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/uploadFileKirChasis",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Image Kir Chasis Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload STNK Failed!');
        //     return false;
        // }
    }); /* AJAX END */
  }

  function uploadFileSTNK(){
    var file_data = $('#image_stnk').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileSTNK",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload STNK Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload STNK Failed!');
        //     return false;
        // }
    }); /* AJAX END */
  }
  function uploadFileKIR()
  {
    var file_data = $('#image_kir').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileKIR",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload KIR Head Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload KIR Failed!');
        //     return false;
        // }
    });
  }

  function uploadFileTRUK()
  {
    var file_data = $('#image_truk').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileTRUK",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Truck Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload Truck Failed!');
        //     return false;
        // }
    });
  }

  function uploadFileASURANSI()
  {
    var file_data = $('#image_asuransi').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileASURANSI",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Insurance Failed!');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload Insurance Failed!');
        //     return false;
        // }
    });
  }

  function Save_Data(response_ASURANSI,response_KIR,response_TRUK,response_STNK,response_KIRChasis)
  {
    var gtime = new Date();
    var cek = $('#cek').val();

    var images_kir = $('#image_kir').val();
    var file_kir_replace = images_kir.replace(/C:|fakepath./g,'');
    var file_kir = file_kir_replace.replace(/\\/g,'');
    var ext_kir = file_kir.split('.').pop();
    var image_kirname = "KIRHEAD_" + gtime.getTime() + "_thump." + ext_kir;
    if(images_kir == ''){
      var image_kirname = "";
    }else{
        var image_kirname = "KIRHEAD_" + gtime.getTime() + "_thump." + ext_kir;
    }

    var image_asuransi = $('#image_asuransi').val();
    var file_asuransi_replace = image_asuransi.replace(/C:|fakepath./g,'');
    var file_asuransi = file_asuransi_replace.replace(/\\/g,'');
    var ext_asuransi = file_asuransi.split('.').pop();
    var image_asuransiname = "ASURANSI_" + gtime.getTime() + "_thump." + ext_asuransi;
    if(image_asuransi == ''){
      var image_asuransiname = "";
    }else{
        var image_asuransiname = "ASURANSI_" + gtime.getTime() + "_thump." + ext_asuransi;
    }

    var image_stnk = $('#image_stnk').val();
    var file_stnk_replace = image_stnk.replace(/C:|fakepath./g,'');
    var file_stnk = file_stnk_replace.replace(/\\/g,'');
    var ext_stnk = file_stnk.split('.').pop();
    var image_stnkname = "STNK_" + gtime.getTime() + "_thump." + ext_stnk;
    if(image_stnk == ''){
      var image_stnkname = "";
    }else{
        var image_stnkname = "STNK_" + gtime.getTime() + "_thump." + ext_stnk;
    }

    var image_kir_chasis = $('#image_kir_chasis').val();
    var file_chasis_replace = image_kir_chasis.replace(/C:|fakepath./g,'');
    var file_chasis = file_chasis_replace.replace(/\\/g,'');
    var ext_chasis = file_chasis.split('.').pop();
    var image_chasisname = "CHASIS_" + gtime.getTime() + "_thump." + ext_chasis;
    if(image_kir_chasis == ''){
      var image_chasisname = "";
    }else{
        var image_chasisname = "CHASIS_" + gtime.getTime() + "_thump." + ext_chasis;
    }

    var image_truk = $('#image_truk').val();
    var file_truk_replace = image_truk.replace(/C:|fakepath./g,'');
    var file_truk = file_truk_replace.replace(/\\/g,'');
    var ext_truk = file_truk.split('.').pop();
    var image_trukname = "TRUCK_" + gtime.getTime() + "_thump." + ext_truk;
    if(image_truk == ''){
      var image_trukname = "";
    }else{
        var image_trukname = "TRUCK_" + gtime.getTime() + "_thump." + ext_truk;
    }

    var brand_type = $('#brand_type').val();
    var Police_Number = $('#Police_Number').val();
    var Created_Year = $('#Created_Year').val();
    var Trasport_Type = $('#Trasport_Type').val();
    var tanggal_declare = $('#tanggal_declare').val();
    var tanggal_declare2 = $('#tanggal_declare2').val();
    var description = $("#description").val();
    var id_truck_fleet = $("#id_truck_fleet").val();

      $.ajax({
        type:'POST',
        url: "<?php echo e(url('/')); ?>/Masterdata/saveTrans",
        datatype: 'JSON',
        data: {stnk:image_stnkname, kir:image_kirname, cek:cek, truk:image_trukname, asuransi:image_asuransiname, kirChasis:image_chasisname, Police_Number:Police_Number, brand_type:brand_type,
               Created_Year:Created_Year, Trasport_Type:Trasport_Type, description:description, tanggal_declare, tanggal_declare2, id_truck_fleet},
        success: function(data) {
          if(data==true){
            swal({
                      title: "",
                      text: "Data Saved Successfully" ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Done ",
                      cancelButtonText: "No ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                          window.location.href = "<?php echo e(url('/')); ?>/Masterdata/ListTrans";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
            });
          }
          else
          {
            swal({
                      title: "",
                      text: "Data Saved" ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Close ",
                      cancelButtonText: "NO ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";

                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
          }
        }
      });

  }

  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Username Already to Used,  Please Find Another Username!');
                $("#username").focus();
                return false;
              }
          }
      });
  }

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>