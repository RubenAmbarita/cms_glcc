<?php $__env->startSection('content'); ?>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div style="font-family:Poppins;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/dashboard')); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/LokasiGereja')); ?>">Lokasi Gereja</a></li>
        <li class="active">Edit</li>
      </ol>
      <h1 style="font-family:Poppins;" >
        Lokasi Gereja
        <small>Edit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo e(url(config('adminlte.register_url', 'LokasiGereja/updates'))); ?>" method="post">
                <?php echo csrf_field(); ?>

                <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="nama_gereja">Nama Gereja <b style="color:red;">*</b></label>
                    <input type="text" id="nama_gereja" name="nama_gereja" class="form-control" value="" placeholder="Nama Gereja">
                </div>
                <div class="form-group has-feedback">
                  <label for="alamat">Alamat <b style="color:red;">*</b></label>
                    <textarea type="text" id="alamat" name="alamat" class="form-control" value="" placeholder="eg. Jalan Rambutan No. 11"></textarea>
                </div>
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group has-feedback <?php echo e($errors->has('provinsi') ? 'has-error' : ''); ?>">
                          <label for="provinsi">Provinsi<b style="color:red;"> *</b></label>
                          <!-- default negara Indonesia -->
                          <input type="hidden" id="negara" name="negara" class="form-control" value="Indonesia" placeholder="Negara">
                          <select class="form-control select2" id="provinsi" name="provinsi" style="width: 100%;" required >
                          <option value="">Pilih Provinsi</option>
                          <?php $__currentLoopData = $provinsi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($row->pro_id); ?>" <?php echo e((old("provinsi") == $row->pro_id ? "selected":"")); ?>><?php echo e($row->pro_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php if($errors->has('provinsi')): ?>
                                  <span class="help-block">
                                      <strong><?php echo e($errors->first('provinsi')); ?></strong>
                                  </span>
                            <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback <?php echo e($errors->has('kokab') ? 'has-error' : ''); ?>">
                          <label for="kokab">Kota/Kabupaten<b style="color:red;"> *</b></label>
                          <select class="form-control select2" id="kokab" name="kokab" style="width: 100%;" required >
                          <option value="">Pilih Kota/Kabupaten</option>
                          </select>
                          <?php if($errors->has('kokab')): ?>
                                <span class="help-block">
                                    <strong><?php echo e($errors->first('kokab')); ?></strong>
                                </span>
                          <?php endif; ?>
                  </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                <label for="kecamatan">Kecamatan <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="kecamatan" name="kecamatan" style="width: 100%;" required>
                  <option value="">Pilih Kecamatan</option>
                  </select>
                  <?php if($errors->has('kecamatan')): ?>
                              <span class="help-block">
                                  <strong><?php echo e($errors->first('kecamatan')); ?></strong>
                              </span>
                  <?php endif; ?>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="kelurahan">Kelurahan <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="kelurahan" name="kelurahan" style="width: 100%;" required>
                  <option value="">Pilih Kelurahan</option>
                  </select>
                  <?php if($errors->has('kelurahan')): ?>
                              <span class="help-block">
                                  <strong><?php echo e($errors->first('kelurahan')); ?></strong>
                              </span>
                  <?php endif; ?>
                </div>
                </div>
                </div>
                <div class="form-group has-feedback">
                  <label for="kode_pos">Kode Pos</label>
                    <Input type="text" id="kode_pos" name="kode_pos" class="form-control" value="" placeholder="eg. 12760">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="PIC">Penanggung Jawab <b style="color:red;">*</b></label>
                    <input type="text" id="PIC" name="PIC" class="form-control" value="" placeholder="Nama Penanggung Jawab">
                </div>

                <div class="row">
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="no_telp"># Handphone <b style="color:red;">*</b></label>
                    <input type="tel" id="no_telp" name="no_telp" class="form-control" placeholder="eg. 0812123456789">
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group has-feedback">
                  <label for="no_telp_2"># Telephone <b style="color:red;">*</b></label>
                    <input type="tel" id="no_telp_2" name="no_telp_2" class="form-control" placeholder="eg. 0212123456789">
                    <br/>
                </div>
                </div>
                </div>
                <div class="form-group has-feedback">
                  <label for="foto_gereja">Upload Foto Gereja</label>
                  <input type="file" id="foto_gereja"  name="foto_gereja" value="">
                </div>
              </div>
              <!-- /.box-body -->

            </form>
          </div>
          <!-- /.box -->
        </div>
              <div class="box-footer" align="right">
                <button type="button" id="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
                <!-- <button type="button" class="btn btn-danger" onclick="cancel_process()"><i class="fa fa-times"></i> Cancel</button> -->
              </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });


  function cancel_process()
  {
     window.history.back();
  }

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#submit").click(function(){
     // DECLARE OBJECT
     //usr_fullname, usr_email, usr_privileges, usr_username, usr_password, usr_password_conf
     var nama_gereja = $("#nama_gereja").val();
     var alamat = $("#alamat").val();
     var kelurahan = $("#kelurahan").val();
     var kecamatan = $("#kecamatan").val();
     var kokab = $("#kokab").val();
     var provinsi = $("#provinsi").val();
     var negara = $("#negara").val();
     var kode_pos = $("#kode_pos").val();
     var PIC = $("#PIC").val();
     var no_telp = $("#no_telp").val();
     var no_telp_2 = $("#no_telp_2").val();
     var str_foto_gereja = $("#foto_gereja").val();
     var foto_gereja_replace = str_foto_gereja.replace(/C:|fakepath./g,'');
     var foto_gereja = foto_gereja_replace.replace(/\\/g,'');

     if(nama_gereja=='' || alamat=='' || kelurahan=='' || kecamatan=='' || kokab=='' || provinsi=='' || PIC=='' || no_telp=='' || no_telp_2==''){
        sweetAlert("Oops...","Please fill in all the required fields marked with asterisks (*.", "error");
        // alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
     }
      $.ajax({
          type:'POST',
          url: "<?php echo e(url('/')); ?>/LokasiGereja/updates",
          datatype: 'JSON',
          data: {nama_gereja:nama_gereja, alamat:alamat, kelurahan:kelurahan, kecamatan:kecamatan, kokab:kokab, provinsi:provinsi, PIC:PIC, no_telp:no_telp, no_telp_2:no_telp_2},
          success: function(data) {
              if(data.response){
                // alert('Create data successfully');
                sweetAlert("Done!","Create data successfully.", "success");
                window.location = "<?php echo e(url('/')); ?>/LokasiGereja";
              }else{
                // alert('Create data failed');
                sweetAlert("Oops...","Create data failed.", "error");
              }
          }
      });
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>