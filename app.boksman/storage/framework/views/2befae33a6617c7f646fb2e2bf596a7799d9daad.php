<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Credit Note
        <small>List</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Credit Note</a></li>
        <li class="active">List</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped" style="width:100%;">
                  <thead>
                  <tr>
                  <tr>
                    <th>CN Number</th>
                    <th>Tanggal Pemesanan</th>
                    <th>Biaya</th>
                    <th>Status Pembayaran</th>
                    <th>Rincian</th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $creditnote_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($row->CN_Number); ?></td>
                        <td><?php echo e($row->Tanggal_Pemesanan); ?></td>
                        <td><?php echo e($row->Biaya); ?></td>
                        <td><?php echo e($row->Status_Pembayaran); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('viewCreditNote'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" id="CN_Number" name="CN_Number" value="<?php echo e($row->CN_Number); ?>">
                              <input type="hidden" id="Nama_Trucker" name="Nama_Trucker" value="<?php echo e($row->Nama_Trucker); ?>">
                              <input type="hidden" id="Alamat_Trucker" name="Alamat_Trucker" value="<?php echo e($row->Alamat_Trucker); ?>">
                              <input type="hidden" id="TelpKantor_Trucker" name="TelpKantor_Trucker" value="<?php echo e($row->TelpKantor_Trucker); ?>">
                              <input type="hidden" id="EmailKantor_Trucker" name="EmailKantor_Trucker" value="<?php echo e($row->EmailKantor_Trucker); ?>">
                              <input type="hidden" id="Nama_Daerah" name="Nama_Daerah" value="<?php echo e($row->Nama_Daerah); ?>">
                              <input type="hidden" id="TruckValue" name="TruckValue" value="<?php echo e($row->Biaya); ?>">
                              <input type="hidden" id="Police_Number" name="Police_Number" value="<?php echo e($row->Police_Number); ?>">
                              <input type="hidden" id="createddate" name="createddate" value="<?php echo e($row->createddate); ?>">
                              <input type="hidden" id="Driver_Name" name="Driver_Name" value="<?php echo e($row->Driver_Name); ?>">
                              <input type="hidden" id="Container_Number" name="Container_Number" value="<?php echo e($row->Container_Number); ?>">
                              <input type="hidden" id="Ukuran_Container" name="Ukuran_Container" value="<?php echo e($row->Ukuran_Container); ?>">
                              <input type="hidden" id="SubTotal" name="SubTotal" value="<?php echo e($row->Biaya); ?>">
                              <input type="hidden" id="Total" name="Total" value="<?php echo e($row->Biaya); ?>">
                              <button class="btn btn-info fa fa-eye" type="submit"></button>
                          </form>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function showCancelMessage(id_publish)
  {
      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";
                            
                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";
                                
                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });
            
          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>