<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo e($TotalDashboard->Total_Armada); ?></h3>
              <p>Total Armada</p>
            </div>
            <div class="icon">
              <i class="fa fa-truck"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Masterdata/ListTrans" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo e($TotalDashboard->Total_Driver); ?><sup style="font-size: 20px"></sup></h3>
              <p>Total Driver</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Masterdata/ListDriver" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo e($TotalDashboard->Total_Publish); ?></h3>
              <p>Total Publish Truck</p>
            </div>
            <div class="icon">
              <i class="fa fa-truck"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Published/nomatched" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
             <div class="inner">
				  <h3><?php echo e($TotalDashboard->Total_Matched); ?></h3>
				  <p>Total Matched</p>
            </div>
            <div class="icon">
               <i class="fa fa-truck"></i>
            </div>
            <a href="<?php echo e(url('/')); ?>/Published/matched" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

		<!-- Chat box -->
    <div class="box box-success">
      <div class="box-header">
        <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
        </div>
      </div>
      <div class="box-body chat" id="chat-box">
      </div>           
    </div>
		<!-- /.Chat box -->
    </section>
    <!-- /.content -->

  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>