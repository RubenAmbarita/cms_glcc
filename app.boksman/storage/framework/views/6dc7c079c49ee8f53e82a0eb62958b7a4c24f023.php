<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <!-- <h1>
        Form Pendaftaran Akun Truk
        <small>Entry</small>
      </h1> -->
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>"><i class="fa fa-dashboard"></i> Login</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Registers">Register</a></li>
        <li class="active">Create Account</li>
      </ol>
      <h1 style="font-family:Poppins">Register Form </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header ">
              <h3 class="box-title">Register Form</h3>
            </div> -->

            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                    <div class="form-group">
                      <label for="company">Type <b style="color:red;"> *</b></label><br>
                      <div class="col-md-6">
                        <input type="radio" id="trucker" name="type" value="trucker">
                        <label for="trucker">Trucker</label>
                      </div>
                      <div class="col-md-6">
                        <input type="radio" id="emkl" name="type" value="EMKL">
                        <label for="emkl">EMKL</label>
                      </div>
                    </div><br>
                    <div class="form-group">
                      <label for="company">Company Name <b style="color:red;"> *</b></label>
                      <input type="text" name="company_name" class="form-control" id="company_name" placeholder="Company Name" required>
                    </div>

                    <div class="form-group has-feedback <?php echo e($errors->has('province') ? 'has-error' : ''); ?>">
                        <label for="province">Province<b style="color:red;"> *</b></label>
                        <select class="form-control select2" id="state" name="state" required >
                        <option value="">Choose Your Province</option>
                          <?php $__currentLoopData = $province; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($row->pro_id); ?>|<?php echo e($row->pro_name); ?>" <?php echo e((old("province") == $row->pro_id ? "selected":"")); ?>><?php echo e($row->pro_name); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <?php if($errors->has('state')): ?>
                              <span class="help-block">
                                  <strong><?php echo e($errors->first('state')); ?></strong>
                              </span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group has-feedback <?php echo e($errors->has('city') ? 'has-error' : ''); ?>">
                        <label for="city">City<b style="color:red;"> *</b></label>
                        <select class="form-control select2" id="city" name="city" style="width: 100%;" required >
                        <option value="">Choose Your City</option>
                        </select>
                        <?php if($errors->has('city')): ?>
                              <span class="help-block">
                                  <strong><?php echo e($errors->first('city')); ?></strong>
                              </span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                      <label for="address">Address<b style="color:red;"> *</b></label>
                      <input class="form-control" id="address_1" name="address_1" placeholder="Company Address" required maxlength="50"></input>
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Postal Code</label>
                      <input type="text" name="zip_code" class="form-control" id="zip_code" placeholder="Postal Code" >
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Contact Name <b style="color:red;">*</b></label>
                      <input type="text" name="name_1" class="form-control" id="name_1" placeholder="Contact Name" required>
                    </div>

                    <div class="form-group has-feedback">
                        <label for="usr_companymobilephone">Phone # <b style="color:red;"> *</b></label>
                        <table style = "width:100%;">
                          <tr>
                              <td>
                                  <input type="number" name="phone_1" class="form-control" id="phone_1" placeholder="Mobile Phone #" required>
                              </td>
                              <td style = "width:10px;"></td>
                              <td>
                                  <input type="number" name="telp_1" class="form-control" id="telp_1" placeholder="Business Phone #" required>
                              </td>
                          </tr>
                        </table>
                      </div>


                    <div class="form-group">
                      <label for="exampleInputEmail1">Business Email<b style="color:red;"> *</b></label>
                      <input type="email" name="email_kantor" class="form-control" id="email_kantor" placeholder="Business Email" required>
                    </div>
                </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">

                <div class="form-group">
                  <label for="exampleInputEmail1">Username <b style="color:red;">*</b></label>
                  <input type="text" name="username" class="form-control" id="username" placeholder="Username" required>
                  <span id="pesan"></span>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Password <b style="color:red;">*</b></label>
                  <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                  <!-- <input type="hidden" name="siup" class="form-control" id="siup" value="" placeholder="SIUP" required> -->
                  <input type="hidden" name="email_2" class="form-control" id="email_2" value="" placeholder="Email PIC 2" required>
                  <input type="hidden" name="telp_2" class="form-control" id="telp_2" value="" placeholder="No. Telp" required>
                  <input type="hidden" name="phone_2" class="form-control" id="phone_2" value="" placeholder="No. Phone 1" required>
                  <input type="hidden" name="name_2" class="form-control" id="name_2" value="" placeholder="PIC 2" required>
                  <input type="hidden" name="email_1" class="form-control" id="email_1" value="" placeholder="Email" required>
                  <input type="hidden" name="telp_kantor" class="form-control" id="telp_kantor" value="" placeholder="Work Telp. " required>
                  <!-- <input type="hidden" name="npwp" class="form-control" id="npwp" value="" placeholder="No. NPWP" required> -->
                  <input type="hidden" class="form-control" id="address_2" name="address_2" value="" placeholder="Address" required></input>
                  <input type="hidden" id="country" name="country" class="form-control" id="exampleInputEmail1" value="Indonesia" placeholder="Country" required>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">NPWP #</label>
                  <input type="text" name="npwp" class="form-control" id="npwp" placeholder="NPWP #" >
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">SIUP #</label>
                  <input type="text" name="siup" class="form-control" id="siup" placeholder="SIUP #" >
                </div>

                <table>
                <td>
                <div class="form-group">
                  <label for="exampleInputEmail1">SIUP Photo</label><br />
                  <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                      <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                      <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                      </div>
                      <div>
                        <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                        <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Foto </span>
                        <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                        <input type="file" id="image_siup" name="image_siup"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                      </div>
                    </div>
                  </div>
                </td>

                <td style="width:50px;"></td>

                <td>
                  <div class="form-group">
                    <label for="exampleInputEmail1">NPWP Photo </label><br />
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                      <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                          <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                          </div>
                        <div>
                          <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                          <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Select Foto </span>
                          <span class="fileinput-exists"><i class="fa fa-exchange"></i> Change </span>
                          <input type="file" id="image_npwp" name="image_npwp"> </span>
                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Remove </a>
                        </div>
                      </div>
                    </div>
                  </td>
                  </table>
                </div>

              <!-- /.box-body -->

              <div class="box-footer" style="text-align:right;">
                <button type="button" id="submit" class="btn btn-primary"><i class="fa fa-registered"></i> Register</button>
              </div>
              </form>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      <!-- </div> -->
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
#geocomplete { width: 500px}
.map_canvas {
  width: 600px;
  height: 400px;
  margin: 10px 20px 10px 0;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC0Fa_0maHwtWlLKKzZc6lH2szmSrsuHDg"></script>
<script src="<?php echo e(url('js/jquery.geocomplete.min.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
  });

  function cancel_process()
  {
     $("#a_logout").click();
     //window.history.back();
  }

  $("select[name='state']").change(function(){
      var p_param = $(this).val();
      var explode = p_param.split("|");
      var value_one = explode[0];
      var value_two = explode[1];
      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/get_regency",
          datatype: 'JSON',
          data: {p_param:value_one},
          success: function(data) {
            $("#city option").remove();
            $("#city option").val();

            $( "#city" ).append(
                $('<option></option>').val("").html("Choose Your City")
            );

            var i = 0;
            $.each(data, function()
            {
              $( "#city" ).append(
                  $('<option></option>').val(data[i].reg_name).html(data[i].reg_name)
              );
              i++;
            });
          }
      });
    });

  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Trucker username already exist,  please find another username!');
                $("#username").focus();
                return false;
              }
          }
      });
  }

  $("#submit").click(function()
  {
    if(document.getElementById("trucker").checked == true){
        var type = $("#trucker").val();
    }else if(document.getElementById("emkl").checked == true){
        var type = $("#emkl").val();
    }else {
      alert("Please select type !");
      $("#trucker").focus();
      return false;
    }
      var file_data_npwp = $('#image_npwp').prop('files')[0];
      var file_data_siup = $('#image_siup').prop('files')[0];

      var form_data = new FormData();
      form_data.append('file', file_data_npwp);
      var npwp = $("#npwp").val();
      var siup = $("#siup").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/get_npwpsiup",
          datatype: 'JSON',
          data: {npwp:npwp, siup:siup},
          success: function(data)
          {
            if(data > 0)
            {
              alert('No. NPWP or No. SIUP already exist in database!');
              return false;
            }
            else{
                if ($('#image_npwp').val()) {
                  response_npwp = upload_file_npwp();

                }else{
                  response_npwp = '';
                }

                if ($('#image_siup').val()) {
                  response_siup = upload_file_siup();
                }else{
                  response_siup = '';
                }

                save_data(response_npwp,response_siup);
            }
          }
      });

  });

  function save_data(response_npwp, response_siup){
    var p_param = $("#username").val();
    var response_siup = response_siup;
    var response_npwp = response_npwp;
    $.ajax({
        type: 'POST',
        url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
        datatype: 'JSON',
        data: {p_param:p_param},
        success: function(data) {
            if(data > 0)
            {
              alert('Trucker username already exist, please find another username!');
              $("#username").focus();
              return false;
            }
            else {
              save_datatrucker(response_npwp, response_siup);
            }
        }
    });
  }

  function save_datatrucker(res_npwp, res_siup)
  {
    var res_siup = res_siup;
    var res_npwp = res_npwp;
    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();

    var state = $("#state").val();
    var explode = state.split("|");
    var value_one = explode[0];
    var value_two = explode[1];

    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');
    var ext_npwp = file_npwp.split('.').pop();

    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;
    if(str_npwp == ''){
      var image_npwpname = '';
    }else{
        var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;
    }


    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();

    if(str_siup == ''){
      var image_siupname = '';
    }else{
        var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;
    }

    if(document.getElementById("trucker").checked == true){
        var type = $("#trucker").val();
    }else if(document.getElementById("emkl").checked == true){
        var type = $("#emkl").val();
    }

    if(company_name=='' || address_1=='' || city=='' ||
       state=='' || country=='' || email_kantor==''||
       name_1=='' || phone_1=='' || telp_1=='' || username=='' || password=='' )
    {
      if(company_name=='')
      {alert('Company name column cant empty!'); $("#company_name").focus(); return false;}

      if(address_1=='')
      {alert('Company address column cant empty!'); $("#address_1").focus(); return false;}

      if(city=='')
      {alert('City column cant empty!'); $("#city").focus(); return false;}

      if(state=='')
      {alert('State column cant empty!'); $("#state").focus(); return false;}

      if(country=='')
      {alert('Country column cant empty!'); $("#cuontry").focus(); return false;}

      if(email_kantor=='')
      {alert('Business email column cant empty!'); $("#email_kantor").focus(); return false;}

      if(name_1=='')
      {alert('Contact name column cant empty!'); $("#name_1").focus(); return false;}

      if(phone_1=='')
      {alert('Mobile phone column cant empty!'); $("#phone_1").focus(); return false;}

      if(telp_1=='')
      {alert('Business phone column cant empty!'); $("#telp_1").focus(); return false;}

      if(username=='')
      {alert('Username column cant empty!'); $("#username").focus(); return false;}

      if(password=='')
      {alert('Password column cant empty!'); $("#password").focus(); return false;}
    }
    else{
        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:'',
                          zip_code:zip_code, city:city, state:value_two, country:country,
                          email_kantor:email_kantor, telp_kantor:'', npwp:npwp,
                          file_npwp:image_npwpname, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:'', name_2:'', phone_2:'', telp_2:'', type:type,
                          email_2:'', username:username, password:password, siup:siup, file_siup:image_siupname,
            },
            success: function(data) {
            if(data==true){
              swal({
                title: "Successfully !",
                text: "Data successfully saved" ,
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Done ",
                cancelButtonText: "No ",
                closeOnConfirm: true,
                closeOnCancel: false
              }, function (isConfirm) {
                if (isConfirm) {
                  //window.location.href = "<?php echo e(url('/')); ?>";
                  $("#a_logout").click();
                } else {
                  //swal("Dibatalkan", " ", "error");
                }
              });
            }
            else
            {
              swal({
                title: "Failed",
                text: "Failed to save data" ,
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Tutup ",
                cancelButtonText: "Tidak ",
                closeOnConfirm: true,
                closeOnCancel: false
              }, function (isConfirm) {
                if (isConfirm) {
                  // window.location.href = "tampilan-data";
                } else {
                  //swal("Dibatalkan", " ", "error");
                }
              });
            }
          }
        });
    }
  }

  function upload_file_siup()
  {
    var file_data = $('#image_siup').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

     $.ajax({
        //url: 'upload.php', // point to server-side PHP script
        url: "<?php echo e(url('/')); ?>/Registers/UploadFileSIUP",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload SIUP failed !');
                return false;

            }
            else {
              var res = JSON.parse(response);
              // $("#image_siup").val(res);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload SIUP error !');
        //     return false;
        // }
    });
  }

  function upload_file_npwp(){
    var file_data_npwp = $('#image_npwp').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data_npwp);

    $.ajax({
        /// url: 'upload.php', /// point to server-side PHP script
        url: "<?php echo e(url('/')); ?>/Registers/UploadFileNPWP",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload NPWP failed !');
                return false;
            }
            else {
              var res = JSON.parse(response);
              return res;
          }
        },
        // error: function (response) {
        //     alert('Upload NPWP error !');
        //     return false;
        // }
    });
  }

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.appreg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>