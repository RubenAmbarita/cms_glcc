<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Published/AcceptShipperOrder">Jadwal Kegiatan</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Jadwal Kegiatan</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div> -->
            <!-- /.box-header -->
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                  <a href="#unpaid" aria-controls="unpaid" role="tab" data-toggle="tab">
                    Daftar Tunggu
                  </a>
                </li>
                <li role="presentation">
                  <a href="#paid" aria-controls="paid" role="tab" data-toggle="tab">
                    Daftar Terlaksana
                  </a>
                </li>
              </ul><br />

              <!-- Tab panes -->
              <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="unpaid">
              <div class="box-body">
                <table id="tblunpaid" class="table table-bordered table-striped dataTable display nowrap" style="width:100%;">
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Tema Kegiatan</th>
                    <th style="text-align:center;">Tanggal Mulai</th>
                    <th style="text-align:center;">Tanggal Berakhir</th>
                    <th style="text-align:center;">PIC</th>
                    <!-- <th style="text-align:center;">Cost</th> -->
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $order_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                          <b>No. DO : </b> <?php echo e($row->number_do); ?><br />
                          <b>No. : </b><?php echo e($row->gk_order); ?><br />
                          <b>Stuffing Date : </b><?php echo e($row->pickup_date); ?><br />
                          <b>Container : </b><?php echo e($row->ukuran_container_name); ?><br />
                          <b>Commodity : </b><?php echo e($row->commodity_name); ?>

                        </td>
                        <td>
                          <?php echo e($row->customer_name); ?><br />
                          <b>Alamat Penjemputan :</b><br />
                          <?php echo e($row->pickup_address); ?>

                        </td>
                        <td>
                          <?php echo e($row->depo_name); ?><br />
                          <b>Alamat Depo :</b><br />
                          <?php echo e($row->address_depo); ?>

                        </td>
                        <td>
                          <?php echo e($row->sl_name); ?><br />
                          <b>Vessel Voyage : </b><?php echo e($row->vessel_name); ?> - <?php echo e($row->voyage_number); ?><br />
                          <b>Open Stack : </b><?php echo e($row->open_time); ?>, <b>Closing Time : </b><?php echo e($row->closing_time); ?>, <b>ETD : </b><?php echo e($row->ETD); ?><br />
                        </td>
                        <td>
                          <b>Deductible : </b><?php echo e($row->Deductible_view); ?><br />
                          <b>Premi : </b><?php echo e($row->Premi_view); ?><br />
                          <b>Admin : </b><?php echo e($row->Admin_view); ?><br />
                        </td>
                        <!-- <td><?php echo e($row->harga_view); ?></td> -->
                        <td style="text-align:center;">
                        <form action="<?php echo e('AcceptShipperOrderProcess'); ?>" method="post">
                          <?php echo csrf_field(); ?>

                            <input type="hidden" id="id_order" name="id_order" value="<?php echo e($row->id_order); ?>">
                            <input type="hidden" id="gk_order" name="gk_order" value="<?php echo e($row->gk_order); ?>">
                            <input type="hidden" id="id_customer" name="id_customer" value="<?php echo e($row->id_customer); ?>">
                            <input type="hidden" id="stuffing_date" name="stuffing_date" value="<?php echo e($row->pickup_date); ?>">
                            <input type="hidden" id="id_sl" name="id_sl" value="<?php echo e($row->id_sl); ?>">
                            <input type="hidden" id="id_vv" name="id_vv" value="<?php echo e($row->id_vv); ?>">
                            <input type="hidden" id="open_time" name="open_time" value="<?php echo e($row->open_time); ?>">
                            <input type="hidden" id="closing_time" name="closing_time" value="<?php echo e($row->closing_time); ?>">
                            <input type="hidden" id="ETD" name="ETD" value="<?php echo e($row->ETD); ?>">
                            <input type="hidden" id="ukuran_container" name="ukuran_container" value="<?php echo e($row->ukuran_container); ?>">
                            <input type="hidden" id="id_commodity" name="id_commodity" value="<?php echo e($row->id_commodity); ?>">
                            <input type="hidden" id="area" name="area" value="<?php echo e($row->area); ?>">
                            <input type="hidden" id="pickup_coordinate" name="pickup_coordinate" value="<?php echo e($row->pickup_coordinate); ?>">
                            <input type="hidden" id="Deductible" name="Deductible" value="<?php echo e($row->Deductible); ?>">
                            <input type="hidden" id="Premi" name="Premi" value="<?php echo e($row->Premi); ?>">
                            <input type="hidden" id="Admin" name="Admin" value="<?php echo e($row->Admin); ?>">
                            <input type="hidden" id="Harga" name="Harga" value="<?php echo e($row->harga); ?>">
                            <input type="hidden" id="id_depo" name="id_depo" value="<?php echo e($row->id_depo); ?>">
                            <input type="hidden" id="coordinate_depo" name="coordinate_depo" value="<?php echo e($row->coordinate_depo); ?>">
                            <button class="btn btn-info" type="submit"><i class="fa fa-file-text-o"></i> Detail</button>
                        </form>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Tema Kegiatan</th>
                    <th style="text-align:center;">Tanggal Mulai</th>
                    <th style="text-align:center;">Tanggal Berakhir</th>
                    <th style="text-align:center;">PIC</th>
                      <!-- <th style="text-align:center;">Cost</th> -->
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- tab panel 1 -->

          <div role="tabpanel" class="tab-pane" id="paid">
              <div class="box-body">
                <table id="tblpaid" class="table table-bordered table-striped dataTable display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                  <tr>
                  <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Tema Kegiatan</th>
                    <th style="text-align:center;">Tanggal Mulai</th>
                    <th style="text-align:center;">Tanggal Berakhir</th>
                    <th style="text-align:center;">PIC</th>
                    <!-- <th style="text-align:center;">Cost</th> -->
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $order_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td>
                          <b>No. DO : </b> <?php echo e($row->number_do); ?><br />
                          <b>No. : </b><?php echo e($row->gk_order); ?><br />
                          <b>Stuffing Date : </b><?php echo e($row->pickup_date); ?><br />
                          <b>Container : </b><?php echo e($row->ukuran_container_name); ?><br />
                          <b>Commodity : </b><?php echo e($row->commodity_name); ?>

                        </td>
                        <td>
                          <?php echo e($row->customer_name); ?><br />
                          <b>Alamat Penjemputan :</b><br />
                          <?php echo e($row->pickup_address); ?>

                        </td>
                        <td>
                          <?php echo e($row->depo_name); ?><br />
                          <b>Alamat Depo :</b><br />
                          <?php echo e($row->address_depo); ?>

                        </td>
                        <td>
                          <?php echo e($row->sl_name); ?><br />
                          <b>Vessel Voyage : </b><?php echo e($row->vessel_name); ?> - <?php echo e($row->voyage_number); ?><br />
                          <b>Open Stack : </b><?php echo e($row->open_time); ?>, <b>Closing Time : </b><?php echo e($row->closing_time); ?>, <b>ETD : </b><?php echo e($row->ETD); ?><br />
                        </td>
                        <td>
                          <b>Deductible : </b><?php echo e($row->Deductible_view); ?><br />
                          <b>Premi : </b><?php echo e($row->Premi_view); ?><br />
                          <b>Admin : </b><?php echo e($row->Admin_view); ?><br />
                        </td>
                        <!-- <td><?php echo e($row->harga_view); ?></td> -->
                        <td style="text-align:center;">
                        <form action="<?php echo e('AcceptShipperOrderProcess'); ?>" method="post">
                          <?php echo csrf_field(); ?>

                            <input type="hidden" id="id_order" name="id_order" value="<?php echo e($row->id_order); ?>">
                            <input type="hidden" id="gk_order" name="gk_order" value="<?php echo e($row->gk_order); ?>">
                            <input type="hidden" id="id_customer" name="id_customer" value="<?php echo e($row->id_customer); ?>">
                            <input type="hidden" id="stuffing_date" name="stuffing_date" value="<?php echo e($row->pickup_date); ?>">
                            <input type="hidden" id="id_sl" name="id_sl" value="<?php echo e($row->id_sl); ?>">
                            <input type="hidden" id="id_vv" name="id_vv" value="<?php echo e($row->id_vv); ?>">
                            <input type="hidden" id="open_time" name="open_time" value="<?php echo e($row->open_time); ?>">
                            <input type="hidden" id="closing_time" name="closing_time" value="<?php echo e($row->closing_time); ?>">
                            <input type="hidden" id="ETD" name="ETD" value="<?php echo e($row->ETD); ?>">
                            <input type="hidden" id="ukuran_container" name="ukuran_container" value="<?php echo e($row->ukuran_container); ?>">
                            <input type="hidden" id="id_commodity" name="id_commodity" value="<?php echo e($row->id_commodity); ?>">
                            <input type="hidden" id="area" name="area" value="<?php echo e($row->area); ?>">
                            <input type="hidden" id="pickup_coordinate" name="pickup_coordinate" value="<?php echo e($row->pickup_coordinate); ?>">
                            <input type="hidden" id="Deductible" name="Deductible" value="<?php echo e($row->Deductible); ?>">
                            <input type="hidden" id="Premi" name="Premi" value="<?php echo e($row->Premi); ?>">
                            <input type="hidden" id="Admin" name="Admin" value="<?php echo e($row->Admin); ?>">
                            <input type="hidden" id="Harga" name="Harga" value="<?php echo e($row->harga); ?>">
                            <input type="hidden" id="id_depo" name="id_depo" value="<?php echo e($row->id_depo); ?>">
                            <input type="hidden" id="coordinate_depo" name="coordinate_depo" value="<?php echo e($row->coordinate_depo); ?>">
                            <button class="btn btn-info" type="submit"><i class="fa fa-file-text-o"></i> Detail</button>
                        </form>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <th style="text-align:center;">Lokasi Gereja</th>
                    <th style="text-align:center;">Tema Kegiatan</th>
                    <th style="text-align:center;">Tanggal Mulai</th>
                    <th style="text-align:center;">Tanggal Berakhir</th>
                    <th style="text-align:center;">PIC</th>
                      <!-- <th style="text-align:center;">Cost</th> -->
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- TAB HALAMAN KEDUA -->
          
          </div>
          <!-- tab-content -->

        </div>
        <!-- tab panel -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function triger_tab()
  {
    $('[href="#paid"]').tab('show');
  }

  $('.nav-tabs a:last').click(function(){
          $(this).tab('show');
          $('#tblpaid').DataTable().draw();
  });

  $(document).ready(function() {
      $('#tblunpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });


      $('#tblpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });
  } );

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  function Accept_Order()
  {
    var gk_order = $("#gk_order").val();
    var id_customer = $("#id_customer").val();

      swal({
          title: "Terima Order ini?",
          text: "Klik tombol Ya untuk melanjutkan proses!" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/Accept_Order",
                  datatype: 'JSON',
                  data: {gk_order:gk_order, id_customer:id_customer},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil diterima." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/AcceptShipperOrder/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal diterima." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>