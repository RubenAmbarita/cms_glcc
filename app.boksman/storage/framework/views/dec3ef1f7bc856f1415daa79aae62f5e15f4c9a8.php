<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.css')); ?>">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/renungan/index">Renungan</a></li>
        <li class="active">Detail</li>
      </ol>
      <h1 style="font-family:Poppins"> Detail Renungan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Armada Publication</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <form action="<?php echo e('Order/matchtab1'); ?>" id="formtab1" method="post">
              <?php echo csrf_field(); ?>

              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Tema Renungan<b style="color:red;"> *</b></label>
                  <input type="hidden" id="id_renungan" value="<?php echo e($detail->id_renungan); ?>" name="">
                  <input type="text" id="tema_renungan" value="<?php echo e($detail->tema_renungan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Judul Renungan<b style="color:red;"> *</b></label>
                  <input type="text" id="judul_renungan" value="<?php echo e($detail->judul_renungan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Bacaan Alkitab I<b style="color:red;"> *</b></label>
                  <input type="text" id="alkitab_1" name="perusahaan" value="<?php echo e($detail->bacaan_alkitab1); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Bacaan Alkitab II</label>
                  <input type="text" id="alkitab_2" name="perusahaan" value="<?php echo e($detail->bacaan_alkitab2); ?>" class="form-control">
                </div>
              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Tanggal Renungan<b style="color:red;"> *</b></label>
                  <input type="text" id="tanggal_renungan" name="perusahaan" value="<?php echo e($detail->tanggal_renungan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Penulis Renungan<b style="color:red;"> *</b></label>
                  <input type="text" id="penulis_renungan" name="perusahaan" value="<?php echo e($detail->penulis_renungan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Editor</label>
                  <input type="text" id="editor" name="perusahaan" value="<?php echo e($detail->editor_renungan); ?>" class="form-control">
                </div>

                <div class="form-group">
      						<b>Upload Gambar</b><br/>
      						<input type="file" id="gambar" name="file" value="<?php echo e($detail->gambar_name); ?>">
      					</div>


              </div>

              <!-- /.box-header -->
              <div class="box-body pad">

              </div>

            <!-- /.box-header -->
            <div class="box-body pad">
              <label for="">Renungan <b style="color:red;"> *</b></label>
              <form>
                <textarea id="renungan" class="textarea" placeholder="Ketik ..." value="<?php echo e($detail->renungan); ?>"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </form>
            </div>
            <div class="box-body pad">
              <button style="float:right" class="btn btn-info" id="submit" type="submit" name="button"><i class="fa fa-check"></i> Simpan Perubahan</button>
            </div>
          </div>

              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!--<script src="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.js')); ?>"></script>-->
<!--<script src="<?php echo e(url('js/order.js')); ?>"></script>-->
<!-- CK Editor -->
<script src="<?php echo e(url('adminlte.assets/bower_components/ckeditor/ckeditor.js')); ?>"></script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})

$( function()
{
  $( "#tanggal_renungan" ).datepicker({
      minDate: 0,
      dateFormat: "d-m-Y"
  });

});


  function showCancelMessage()
  {
      swal({
          title: "Anda Yakin?",
          text: "Data Akan Dihapus !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/delete",
                  datatype: 'JSON',
                  data: {id_publish:id_publish},
                  success: function(data)
                  {
                    if(data == 0 || data == 1)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data Successfully Deleted" ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Done ",
                          cancelButtonText: "No ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/nomatched/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data Failed Saved" ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Close ",
                            cancelButtonText: "No ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }
</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  $("#submit").click(function(){
    var tema_renungan = $("#tema_renungan").val();
    var judul_renungan = $("#judul_renungan").val();
    var alkitab_1 = $("#alkitab_1").val();
    var alkitab_2 = $("#alkitab_2").val();
    var tanggal_renungan = $("#tanggal_renungan").val();
    var penulis_renungan = $("#penulis_renungan").val();
    var editor = $("#editor").val();
    var gambar = $("#gambar").val();
    var renungan = $("#renungan").val();
    var id_renungan = $("#id_renungan").val();

    if(tema_renungan=='' || judul_renungan=='' || alkitab_1=='' || tanggal_renungan=='' || penulis_renungan=='' || renungan=='')
    {
      alert('Wajib disi yang Bertanda Bintang (*).');
      return false;
    }

    $.ajax({
        type: 'POST',
        url: "<?php echo e(url('/')); ?>/renungan/save",
        datatype: 'JSON',
        data: {id_renungan:id_renungan,tema_renungan:tema_renungan, judul_renungan:judul_renungan, alkitab_1:alkitab_1, alkitab_2:alkitab_2, tanggal_renungan:tanggal_renungan, penulis_renungan:penulis_renungan, editor:editor, gambar:gambar, renungan:renungan},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Successfully !",
                    text: "Data Successfully Saved" ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Done ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "<?php echo e(url('/')); ?>/renungan/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Failed",
                    text: "Data Failed to Save." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Close ",
                    cancelButtonText: "No ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>