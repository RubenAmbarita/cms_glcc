<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.css')); ?>">

<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/pelayanan/index">Pelayanan</a></li>
        <li class="active">Detail</li>
      </ol>
      <h1 style="font-family:Poppins"> Detail Permintaan Pelayanan </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Armada Publication</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
              <?php echo csrf_field(); ?>

              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Lokasi Gereja</label>
                  <input type="hidden" id="id_permintaan_pelayanan" value="<?php echo e($detail->id_permintaan_pelayanan); ?>">
                  <input type="hidden" id="id_gereja" value="<?php echo e($detail->id_gereja); ?>">
                  <input type="text" id="lokasi_gereja" name="perusahaan" value="<?php echo e($detail->nama_gereja); ?>" class="form-control" disabled>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Jenis Pelayanan</label>
                  <input type="text" id="jenis_pelayanan" name="perusahaan" value="<?php echo e($detail->jenis_pelayanan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Nama Jemaat</label>
                  <input type="text" id="nama_jemaat" name="perusahaan" value="<?php echo e($detail->nama_jemaat); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Nama Pelayan</label>
                  <input type="text" id="nama_pelayan" name="perusahaan" value="<?php echo e($detail->nama_pelayan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan"># Handphone</label>
                  <input type="text" id="no_handphone" name="perusahaan" value="<?php echo e($detail->no_handphone); ?>" class="form-control" disabled>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <textarea style="height:100px;" value="<?php echo e($detail->alamat); ?>" class="form-control" id="alamat" name="alamat" placeholder="Alamat" disabled></textarea>
                </div>

              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Provinsi</label>
                  <input type="hidden" id="id_provinsi" value="<?php echo e($detail->id_provinsi); ?>">
                  <input type="text" id="nama_provinsi" name="perusahaan" value="<?php echo e($detail->nama_provinsi); ?>" class="form-control" disabled>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Kota</label>
                  <input type="hidden" id="id_kota" value="<?php echo e($detail->id_kota); ?>">
                  <input type="text" name="perusahaan" id="nama_kota" value="<?php echo e($detail->nama_kota); ?>" class="form-control" disabled>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Kecamatan</label>
                  <input type="hidden" id="id_kecamatan" value="<?php echo e($detail->id_kecamatan); ?>">
                  <input type="text" id="nama_kecamatan" name="perusahaan" value="<?php echo e($detail->nama_kecamatan); ?>" class="form-control" disabled>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Kelurahan</label>
                  <input type="hidden" id="id_kelurahan" value="<?php echo e($detail->id_kelurahan); ?>">
                  <input type="text" id="nama_kelurahan" name="perusahaan" value="<?php echo e($detail->nama_kelurahan); ?>" class="form-control" disabled>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Kode Pos</label>
                  <input type="text" id="kode_pos" name="perusahaan" value="<?php echo e($detail->kode_pos); ?>" class="form-control" disabled>
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Tanggal Permintaan</label>
                  <input type="text" id="tanggal_permintaan" autocomplete="off" name="perusahaan" value="<?php echo e($detail->tanggal_permintaan); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Tanggal Realisasi Pelayanan</label>
                  <input type="text" id="tanggal_realisasi" name="perusahaan" autocomplete="off" value="<?php echo e($detail->tanggal_realisasi); ?>" class="form-control">
                </div>
              </div>

              <!-- /.box-header -->
              <div class="box-body pad">
                <button style="float:right" class="btn btn-info" id="submit" name="button"><i class="fa fa-check"></i> Simpan Perubahan</button>
              </div>
          </div>

              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!--<script src="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.js')); ?>"></script>-->
<!--<script src="<?php echo e(url('js/order.js')); ?>"></script>-->
<!-- CK Editor -->
<script src="<?php echo e(url('adminlte.assets/bower_components/ckeditor/ckeditor.js')); ?>"></script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask();
    $('#waktu_loading').numericx();
  });

  $( function()
  {
    $( "#tanggal_realisasi" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });

    $( "#tanggal_permintaan" ).datepicker({
        minDate: 0,
        dateFormat: "d-m-Y"
    });
  });

</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  $("#submit").click(function(){
    var id_permintaan_pelayanan = $("#id_permintaan_pelayanan").val();
    var id_gereja = $("#id_gereja").val();
    var jenis_pelayanan = $("#jenis_pelayanan").val();
    var nama_jemaat = $("#nama_jemaat").val();
    var nama_pelayan = $("#nama_pelayan").val();
    var no_handphone = $("#no_handphone").val();
    var alamat = $("#alamat").val();
    var id_provinsi = $("#id_provinsi").val();
    var id_kota = $("#id_kota").val();
    var id_kecamatan = $("#id_kecamatan").val();
    var id_kelurahan = $("#id_kelurahan").val();
    var kode_pos = $("#kode_pos").val();
    var tanggal_permintaan = $("#tanggal_permintaan").val();
    var tanggal_realisasi = $("#tanggal_realisasi").val();

    $.ajax({
        type: 'POST',
        url: "<?php echo e(url('/')); ?>/pelayanan/save",
        datatype: 'JSON',
        data: {id_permintaan_pelayanan:id_permintaan_pelayanan,id_gereja:id_gereja, jenis_pelayanan:jenis_pelayanan, nama_jemaat:nama_jemaat, nama_pelayan:nama_pelayan, no_handphone:no_handphone, alamat:alamat, id_provinsi:id_provinsi, id_kota:id_kota, id_kecamatan:id_kecamatan, id_kelurahan:id_kelurahan, kode_pos:kode_pos, tanggal_permintaan:tanggal_permintaan, tanggal_realisasi:tanggal_realisasi},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Sukses !",
                    text: "Data Berhasil disimpan." ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "<?php echo e(url('/')); ?>/pelayanan/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Gagal",
                    text: "Data Gagal disimpan." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Tutup ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>