<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="/pelayanan/index">Pelayanan</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Permintaan Pelayanan</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                  <a href="#unpaid" aria-controls="unpaid" role="tab" data-toggle="tab">
                    Daftar Tunggu
                  </a>
                </li>
                <li role="presentation">
                  <a href="#paid" aria-controls="paid" role="tab" data-toggle="tab">
                    Daftar Terlaksana
                  </a>
                </li>
              </ul><br />

              <!-- Tab panes -->
              <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="unpaid">
              <div class="box-body">
                <table id="tblunpaid" class="table table-bordered table-striped dataTable display nowrap" style="width:100%;">
                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('lokasi') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="lokasi" name="lokasi" required >
                      <option value="NOL">Pilih Lokasi Gereja</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('bulan') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('tahun') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Jenis Pelayanan</th>
                    <th style="text-align:center;">Jemaat</th>
                    <th style="text-align:center;">Pelayanan</th>
                    <th style="text-align:center;">Tanggal Permintaan</th>
                    <th style="text-align:center;">Tanggal Realisasi</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $list_tunggu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                        <td><?php echo e($row->jenis_pelayanan); ?></td>
                        <td><?php echo e($row->nama_jemaat); ?></td>
                        <td><?php echo e($row->nama_pelayan); ?></td>
                        <td><?php echo e($row->tanggal_permintaan); ?></td>
                        <td><?php echo e($row->tanggal_realisasi); ?></td>
                        <td style="text-align:center;">
                          <form action="<?php echo e('detail'); ?>" method="post">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="id_provinsi" value="<?php echo e($row->id_provinsi); ?>">
                            <input type="hidden" name="id_kota" value="<?php echo e($row->id_kota); ?>">
                            <input type="hidden" name="id_kecamatan" value="<?php echo e($row->id_kecamatan); ?>">
                            <input type="hidden" name="id_kelurahan" value="<?php echo e($row->id_kelurahan); ?>">
                            <input type="hidden" name="id_permintaan_pelayanan" value="<?php echo e($row->id_permintaan_pelayanan); ?>">
                            <input type="hidden" name="id_gereja" value="<?php echo e($row->id_gereja); ?>">
                            <button id="detail" class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                          </form>
                        </td>
                        <td>
                          <button id="hapus" class="btn btn-warning" onclick="showCancelMessage(<?php echo e($row->id_permintaan_pelayanan); ?>)" type="button" name="button"><i class="fa fa-list-alt"></i> Terlaksana</button>
                        </td>
                        <td>
                          <button id="hapus" class="btn btn-danger" onclick="showDeleteMessage(<?php echo e($row->id_permintaan_pelayanan); ?>)" type="button" name="button"><i class="fa fa-close"></i> Delete</button>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Jenis Pelayanan</th>
                      <th style="text-align:center;">Jemaat</th>
                      <th style="text-align:center;">Pelayanan</th>
                      <th style="text-align:center;">Tanggal Permintaan</th>
                      <th style="text-align:center;">Tanggal Realisasi</th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- tab panel 1 -->

            <div role="tabpanel" class="tab-pane" id="paid">
              <div class="box-body">
                <table id="tblpaid" class="table table-bordered table-striped dataTable display nowrap" cellspacing="0" width="100%">
                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('lokasi') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="lokasi" name="lokasi" required >
                      <option value="NOL">Pilih Lokasi Gereja</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('bulan') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="bulan" name="bulan" required >
                      <option value="NOL">Bulan</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                  <div class="form-group has-feedback <?php echo e($errors->has('tahun') ? 'has-error' : ''); ?>">
                      <!-- <label for="province"></label> -->
                      <select class="form-control select2" id="tahun" name="tahun" required >
                      <option value="NOL">Tahun</option>
                      </select>
                    </div>
                  </div>
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Jenis Pelayanan</th>
                    <th style="text-align:center;">Jemaat</th>
                    <th style="text-align:center;">Pelayanan</th>
                    <th style="text-align:center;">Tanggal Permintaan</th>
                    <th style="text-align:center;">Tanggal Realisasi</th>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $list_terlaksana; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($row->jenis_pelayanan); ?></td>
                      <td><?php echo e($row->nama_jemaat); ?></td>
                      <td><?php echo e($row->nama_pelayan); ?></td>
                      <td><?php echo e($row->tanggal_permintaan); ?></td>
                      <td><?php echo e($row->tanggal_realisasi); ?></td>
                      <td style="text-align:center;">
                        <form action="<?php echo e('detail'); ?>" method="post">
                          <?php echo e(csrf_field()); ?>

                          <input type="hidden" name="id_provinsi" value="<?php echo e($row->id_provinsi); ?>">
                          <input type="hidden" name="id_kota" value="<?php echo e($row->id_kota); ?>">
                          <input type="hidden" name="id_kecamatan" value="<?php echo e($row->id_kecamatan); ?>">
                          <input type="hidden" name="id_kelurahan" value="<?php echo e($row->id_kelurahan); ?>">
                          <input type="hidden" name="id_permintaan_pelayanan" value="<?php echo e($row->id_permintaan_pelayanan); ?>">
                          <input type="hidden" name="id_gereja" value="<?php echo e($row->id_gereja); ?>">
                          <button id="detail" class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                        </form>
                      </td>
                      <td>
                        <button id="hapus" class="btn btn-danger" onclick="showDeleteMessage(<?php echo e($row->id_permintaan_pelayanan); ?>)" type="button" name="button"><i class="fa fa-close"></i> Delete</button>
                      </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Jenis Pelayanan</th>
                      <th style="text-align:center;">Jemaat</th>
                      <th style="text-align:center;">Pelayanan</th>
                      <th style="text-align:center;">Tanggal Permintaan</th>
                      <th style="text-align:center;">Tanggal Realisasi</th>
                      <th style="text-align:center;"></th>
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- TAB HALAMAN KEDUA -->

          </div>
          <!-- tab-content -->

        </div>
        <!-- tab panel -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function triger_tab()
  {
    $('[href="#paid"]').tab('show');
  }

  $('.nav-tabs a:last').click(function(){
          $(this).tab('show');
          $('#tblpaid').DataTable().draw();
  });

  $(document).ready(function() {
      $('#tblunpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });


      $('#tblpaid').DataTable({
              scrollX : true,
              scrollCollapse : true
      });
  } );

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  function showCancelMessage(id_permintaan_pelayanan)
  {
      swal({
          title: "Pelayanan Telah Terlaksana?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Terlaksana ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/pelayanan/update_status",
                  datatype: 'JSON',
                  data: {id_permintaan_pelayanan:id_permintaan_pelayanan},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Sukses !",
                          text: "Data berhasil diupdate." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/pelayanan/index";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal disimpan." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

  function showDeleteMessage(id_permintaan_pelayanan)
  {
      swal({
          title: "Anda Yakin?",
          text: "Data Akan Dihapus!" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/pelayanan/delete",
                  datatype: 'JSON',
                  data: {id_permintaan_pelayanan:id_permintaan_pelayanan},
                  success: function(data)
                  {
                    if(data == 0 || data == 1)
                    {
                        swal({
                          title: "Sukses !",
                          text: "Data Berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Ya ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/pelayanan/index";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Gagal",
                            text: "Data Gagal dihapus." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>