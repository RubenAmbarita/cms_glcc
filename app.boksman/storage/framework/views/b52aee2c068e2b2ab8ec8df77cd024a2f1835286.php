<?php $__env->startSection('content'); ?>
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div style="font-family:Poppins;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/dashboard')); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/UsersManagement">Lokasi Gereja</a></li>
        <li class="active">List</li>

      </ol>
      <h1 style="font-family:Poppins;">
      Lokasi Gereja
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <th colspan="7" style="text-align:right;">
                    <a href="<?php echo e(action('UsersManagementController@create')); ?>" class="btn btn-success"> <i class="fa fa-plus-square"></i> Tambah Lokasi Gereja</a>
                  </th>
                <tr>
                  <th style="text-align:center;">Nama Lokasi</th>
                  <th style="text-align:center;">Provinsi</th>
                  <th style="text-align:center;">Kota/Kabupaten</th>
                  <th style="text-align:center;">PIC</th>
                  <th style="text-align:center;"># Handphone</th>
                  <th style="text-align:center; width:80px;"></th>
                </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                      <td><?php echo e($row->name); ?></td>
                      <td><?php echo e($row->email); ?></td>
                      <td><?php echo e($row->username); ?></td>
                      <td><?php echo e($row->privilegesname); ?></td>
                      <td><?php echo e($row->privilegesname); ?></td>
                      <td style="text-align:center;">
                        <table>
                          <tr>
                            <td>
                              <form action="<?php echo e('UsersManagement/edit'); ?>" method="post">
                                 <?php echo csrf_field(); ?>

                                <input type="hidden" id="username" name="username" value="<?php echo e($row->username); ?>">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-edit"></i> Edit</button>
                              </form>
                            </td>
                            <td style="width:10px;">&nbsp;</td>
                            <td>
                                <?php if($row->Count_Trx > 0): ?>
                                    &nbsp;
                                <?php else: ?>
                                    <button id="btn_delete" class="btn btn-danger" type="button" onclick="delete_data('<?php echo e($row->username); ?>')"><i class="fa fa-trash-o"></i> Delete</button>
                                <?php endif; ?>
                            </td>
                          </tr>
                        </table>
                      </td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <tfoot>
                <tr>
                <th style="text-align:center;">Nama Lokasi</th>
                  <th style="text-align:center;">Provinsi</th>
                  <th style="text-align:center;">Kota/Kabupaten</th>
                  <th style="text-align:center;">PIC</th>
                  <th style="text-align:center;"># Handphone</th>
                  <th style="text-align:center; width:80px;"></th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function delete_data($username)
  {
    // swal({
    //       title: "Are You Sure?",
    //       text: "Data Will be NonActive !" ,
    //       type: "warning",
    //       showCancelButton: true,
    //       confirmButtonColor: "#DD6B55",
    //       confirmButtonText: "Yes ",
    //       cancelButtonText: "No ",
    //       closeOnConfirm: true,
    //       closeOnCancel: true
    //   }, function (isConfirm) {
    //       if (isConfirm) {
    //         $.ajax({
    //               type: 'POST',
    //               url: "<?php echo e(url('/')); ?>/Masterdata/nonactive_trans",
    //               datatype: 'JSON',
    //               data: {username:$username},
    //               success: function(data)
    //               {
    //                 if(data==0 || data==1)
    //                 {
    //                     swal({
    //                       title: "",
    //                       text: "Data Successfully Deleted" ,
    //                       type: "success",
    //                       showCancelButton: false,
    //                       confirmButtonColor: "#DD6B55",
    //                       confirmButtonText: "Done ",
    //                       cancelButtonText: "No ",
    //                       closeOnConfirm: true,
    //                       closeOnCancel: false
    //                     }, function (isConfirm) {
    //                       if (isConfirm) {
    //                           window.location = "<?php echo e(url('/')); ?>/Masterdata/ListTrans/";
    //
    //                       } else {
    //                           return false;
    //                       }
    //                     });
    //
    //                 }
    //                 else{
    //                   swal({
    //                         title: "",
    //                         text: "Data Successfully Saved" ,
    //                         type: "error",
    //                         showCancelButton: false,
    //                         confirmButtonColor: "#DD6B55",
    //                         confirmButtonText: "Close ",
    //                         cancelButtonText: "No ",
    //                         closeOnConfirm: true,
    //                         closeOnCancel: false
    //                       }, function (isConfirm) {
    //                           if (isConfirm) {
    //                             // window.location.href = "tampilan-data";
    //
    //                           } else {
    //                               return false;
    //                           }
    //                       });
    //                 }
    //               }
    //           });
    //
    //       } else {
    //           return false;
    //       }
    //   });
      var deleteconfirm = confirm("Delete Data ?");
      if (deleteconfirm == true) {
          $.ajax({
              type: 'POST',
              url: "<?php echo e(url('/')); ?>/UsersManagement/deleteds",
              datatype: 'JSON',
              data: {username:$username},
              success: function(data) {
                if(data.respon){
                  // alert('Data failed deleted');
                  sweetAlert("Oops...","Deleted data failed.", "error");
                  return false;
                }else{
                  // alert('Data Successfully deleted');
                  sweetAlert("Done!","Deleted data successfully.", "success");
                  window.location.reload();
                }
              }
          });
      } else {

          return false;
      }

  }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>