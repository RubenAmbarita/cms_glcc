<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">
<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

ul.scroll-menu {
    position:relative;
    display:inherit!important;
    overflow-x:auto;
    -webkit-overflow-scrolling:touch;
    -moz-overflow-scrolling:touch;
    -ms-overflow-scrolling:touch;
    -o-overflow-scrolling:touch;
    overflow-scrolling:touch;
    top:0!important;
    left:0!important;
    width:100%;
    height:auto;
    max-height:500px;
    margin:0;
    border-left:none;
    border-right:none;
    -webkit-border-radius:0!important;
    -moz-border-radius:0!important;
    -ms-border-radius:0!important;
    -o-border-radius:0!important;
    border-radius:0!important;
    -webkit-box-shadow:none;
    -moz-box-shadow:none;
    -ms-box-shadow:none;
    -o-box-shadow:none;
    box-shadow:none
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb" style="font-family:Poppins">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/masterpujian/index">Master Pujian</a></li>
        <li class="active">Detail</li>
      </ol>
      <h1 style="font-family:Poppins"> Detail</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div id="loading">
        <img id="loading-image" src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Loading..." />
      </div>

      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Armada Publication</h3>
            </div> -->
            <!-- /.box-header -->

            <!-- form start -->
            <form action="<?php echo e('Order/matchtab1'); ?>" id="formtab1" method="post">
              <?php echo csrf_field(); ?>

              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan"># Pujian<b style="color:red;"> *</b></label>
                  <input type="hidden" id="id_pujian" value="<?php echo e($detail->id_kidung_pujian); ?>">
                  <input type="text" id="pujian" name="perusahaan" value="<?php echo e($detail->pujian); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Judul<b style="color:red;"> *</b></label>
                  <input type="text" id="judul" name="perusahaan" value="<?php echo e($detail->judul); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Penulis<b style="color:red;"> *</b></label>
                  <input type="text" id="penulis" name="perusahaan" value="<?php echo e($detail->penulis); ?>" class="form-control">
                </div>
              </div>
              <!-- /.box-body -->

              <!-- HALAMAN PERTAMA SEBELAH KANAN -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                  <label for="perusahaan">Editor<b style="color:red;"> *</b></label>
                  <input type="text" id="editor" name="perusahaan" value="<?php echo e($detail->editor); ?>" class="form-control">
                </div>

                <div class="form-group has-feedback">
                  <label for="perusahaan">Tautan Video</label>
                  <input type="text" id="tautan_video" name="perusahaan" value="<?php echo e($detail->tautan_video); ?>" class="form-control">
                </div>

                <table border="0">
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Gambar</label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="<?php echo e('../images/GCCC/pujian/'.$detail->gambar_name); ?>" alt="" />
                          </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" ><i class="fa fa-file-image-o"></i> Pilih Foto </span>
                              <span class="fileinput-exists"><i class="fa fa-exchange"></i> Ubah </span>
                              <input type="file" id="gambar_name" name="gambar_name"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#dd4b39; color:white;"><i class="fa fa-remove"></i> Hapus </a>
                            </div>
                          </div>
                        </div>
                    </td>
                </table>
              </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <label for="">Lirik Pujian<b style="color:red;"> *</b></label>
              <form>
                <textarea id="lirik_pujian" class="textarea" placeholder="Ketik ..." value=""
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo e($detail->lirik_pujian); ?></textarea>
              </form>
            </div>
            <div class="box-body pad">
              <button style="float:right" class="btn btn-info" id="submit" name="button"><i class="fa fa-check"></i> Simpan Perubahan</button>
            </div>
          </div>

              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<!-- datepicker -->
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!--<script src="<?php echo e(url('adminlte.assets/timepicker/dist/wickedpicker.min.js')); ?>"></script>-->
<!--<script src="<?php echo e(url('js/order.js')); ?>"></script>-->
<!-- CK Editor -->
<script src="<?php echo e(url('adminlte.assets/bower_components/ckeditor/ckeditor.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>

<script>
$(function () {
  // Replace the <textarea id="editor1"> with a CKEditor
  // instance, using default configuration.
  //bootstrap WYSIHTML5 - text editor
  $('.textarea').wysihtml5()
})
</script>

<script type="text/javascript">
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $("#loading").hide();

  function UploadGambar()
  {
    var file_data = $('#gambar_name').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/masterpujian/upload",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        async: false,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Gambar Gagal!');
                return false;
            }
            else {
              res = JSON.parse(response);
              return res;
            }
        },
        // error: function (response) {
        //     alert('Upload KTP Failed!');
        //     return false;
        // }
    }); /* AJAX END */
    return res;
  }

  $("#submit").click(function(){
    var pujian = $("#pujian").val();
    var judul_renungan = $("#judul_renungan").val();
    var judul = $("#judul").val();
    var penulis = $("#penulis").val();
    var editor = $("#editor").val();
    var tautan_video = $("#tautan_video").val();
    var gambar_name = $("#gambar_name").val();
    var lirik_pujian = $("#lirik_pujian").val();
    var id_pujian = $("#id_pujian").val();

    if(pujian=='' || judul_renungan=='' || penulis=='' || editor=='' || lirik_pujian=='')
    {
      alert('Wajib disi yang Bertanda Bintang (*).');
      return false;
    }

    if($('#gambar_name').val()){
      gambar_name = UploadGambar();
    }else{
      gambar_name = '';
    }
    
    $.ajax({
        type: 'POST',
        url: "<?php echo e(url('/')); ?>/masterpujian/save",
        datatype: 'JSON',
        data: {id_pujian:id_pujian,pujian:pujian, judul:judul, penulis:penulis, editor:editor, tautan_video:tautan_video, gambar_name:gambar_name, lirik_pujian:lirik_pujian},
        success: function(data) {
          if(data==true)
            {
                swal({
                    title: "Sukses !",
                    text: "Data Berhasil disimpan." ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = "<?php echo e(url('/')); ?>/masterpujian/index";
                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
            else{
                swal({
                    title: "Gagal",
                    text: "Data Gagal disimpan." ,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Tutup ",
                    cancelButtonText: "Tidak ",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                      // window.location.href = "tampilan-data";

                    } else {
                        //swal("Dibatalkan", " ", "error");
                    }
                });
            }
        }
    });
  });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>