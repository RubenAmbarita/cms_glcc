<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="/Published/CreditNote">Keanggotaan</a></li>
        <li class="active">List</li>
      </ol>
      <h1 style="font-family:Poppins">Keanggotaan</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- /.box-header -->
            <div class="nav-tabs-custom">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs">
                <li role="presentation" class="active">
                  <a href="#menunggu" aria-controls="menunggu" role="tab" data-toggle="tab">
                    Daftar Tunggu
                  </a>
                </li>
                <li role="presentation">
                  <a href="#aktif" aria-controls="aktif" role="tab" data-toggle="tab">
                    Daftar Jemaat
                  </a>
                </li>
              </ul><br />

              <!-- Tab panes -->
              <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="menunggu">
              <div class="box-body">
                <table id="tblmenunggu" class="table table-bordered table-striped dataTable display nowrap" style="width:100%;">
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;">Nama</th>
                    <th style="text-align:center;">Alamat</th>
                    <th style="text-align:center;">No. Handphone</th>
                    <th style="text-align:center;">Pekerjaan</th>
                    <!-- <th style="text-align:center;">Shipper</th> -->
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $anggota; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($row->status==NULL || $row->status==0): ?>
                    <tr>
                        <td><?php echo e($row->name); ?></td>
                        <td><?php echo e($row->adress); ?></td>
                        <td><?php echo e($row->no_telp); ?></td>
                        <td><?php echo e($row->pekerjaan); ?></td>
                        <td style="text-align:center;" width="20px">
                          <form action="<?php echo e('detail'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" name="id" value="<?php echo e($row->id); ?>">
                              <button id="detail" class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                          </form>
                        </td>
                        <td style="text-align:center;" width="20px"> 
                          <button id="hapus" class="btn btn-info" onclick="showVerificationMessage(<?php echo e($row->id); ?>)" type="button" name="button"><i class="fa fa-check"></i> Verify</button>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Nama</th>
                      <th style="text-align:center;">Alamat</th>
                      <th style="text-align:center;">No. Handphone</th>
                      <th style="text-align:center;">Pekerjaan</th>
                      <!-- <th style="text-align:center;">Shipper</th> -->
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- tab panel 1 -->

            <div role="tabpanel" class="tab-pane" id="aktif">
              <div class="box-body">
                <table id="tblaktif" class="table table-bordered table-striped dataTable display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                  <tr>
                  <th style="text-align:center;">Nama</th>
                    <th style="text-align:center;">Alamat</th>
                    <th style="text-align:center;">No. Handphone</th>
                    <th style="text-align:center;">Pekerjaan</th>
                    <!-- <th style="text-align:center;">Shipper</th> -->
                    <th style="text-align:center;"></th>
                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $anggota; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($row->status!=NULL && $row->status==1): ?>
                    <tr>
                        <td><?php echo e($row->name); ?></td>
                        <td><?php echo e($row->adress); ?></td>
                        <td><?php echo e($row->no_telp); ?></td>
                        <td><?php echo e($row->pekerjaan); ?></td>
                        <td style="text-align:center;" width="20px">
                          <form action="<?php echo e('detail'); ?>" method="post">
                            <?php echo csrf_field(); ?>

                              <input type="hidden" name="id" value="<?php echo e($row->id); ?>">
                              <button id="detail" class="btn btn-success" type="submit" name="button"><i class="fa fa-file-text-o"></i> Detail</button>
                          </form>
                        </td>
                        <td style="text-align:center;" width="20px">
                        <button class="btn btn-danger" onclick="showDeleteMessage(<?php echo e($row->id); ?>)" type="button" name="button"><i class="fa fa-close"></i> Hapus</button>
                        </td>
                    </tr>
                    <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    <tr>
                      <th style="text-align:center;">Nama</th>
                      <th style="text-align:center;">Alamat</th>
                      <th style="text-align:center;">No. Handphone</th>
                      <th style="text-align:center;">Pekerjaan</th>
                      <!-- <th style="text-align:center;">Shipper</th> -->
                      <th style="text-align:center;"></th>
                    </tr>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- TAB HALAMAN KEDUA -->

          </div>
          <!-- tab-content -->

        </div>
        <!-- tab panel -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function triger_tab()
  {
    $('[href="#aktif"]').tab('show');
  }

  $('.nav-tabs a:last').click(function(){
          $(this).tab('show');
          $('#tblaktif').DataTable().draw();
  });

  $(document).ready(function() {
      $('#tblmenunggu').DataTable({
              scrollX : true,
              scrollCollapse : true
      });


      $('#tblaktif').DataTable({
              scrollX : true,
              scrollCollapse : true
      });
  } );

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  function showVerificationMessage(id)
  {
      swal({
          title: "Verifikasi Data Anggota?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Verifikasi",
          closeOnConfirm: true,
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Keanggotaan/update_status",
                  datatype: 'JSON',
                  data: {id:id},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Sukses!",
                          text: "Data berhasil diverifikasi." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "OK ",
                          closeOnConfirm: true,
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Keanggotaan";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Oops!",
                            text: "Data gagal diverifikasi." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            closeOnConfirm: true,
                          }, function (isConfirm) {
                              if (isConfirm) {

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

  function showDeleteMessage(id)
  {
      swal({
          title: "Anda Yakin?",
          text: "Data Akan Dihapus!" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Keanggotaan/delete",
                  datatype: 'JSON',
                  data: {id:id},
                  success: function(data)
                  {
                    if(data == 0 || data == 1)
                    {
                        swal({
                          title: "Sukses !",
                          text: "Data Berhasil dihapus." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Ya ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Keanggotaan";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Gagal",
                            text: "Data Gagal dihapus." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>