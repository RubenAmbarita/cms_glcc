<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="<?php echo e(url('css/bootstrap-fileinput.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Armada
        <small>Entry</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tambah Armada</a></li>
        <li class="active">Entry</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" action="" method="post">
                <?php echo csrf_field(); ?>

              <div class="box-body col-md-6">
                <div class="form-group">
                  <label for="company_name">Company Name</label>
                  <input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo e($custname); ?>" placeholder="Nama Perusahaan" disabled>
                  <input type="hidden" class="form-control" id="id_truck_fleet" name="id_truck_fleet" value="" placeholder="Nama Perusahaan" readonly>
                </div>
                <div class="form-group">
                  <label for="Police_Number">Nomor Polisi <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" id="Police_Number" name="Police_Number" placeholder="Nomor Polisi" onfocus="this.select();" onmouseup="return false;" required>
                </div>
                <div class="form-group">
                  <label for="brand_type">Merek Armada <b style="color:red;">*</b></label>
                  <select class="form-control select2" id="brand_type" name="brand_type" style="width: 100%;" required >
                    <?php $__currentLoopData = $listBrand; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($row->mst_codedetail); ?>" <?php echo e((old("brand_type") == $row->mst_codedetail ? "selected":"")); ?>><?php echo e($row->mst_codedesc); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="Created_Year">Tahun Pembuatan <b style="color:red;">*</b></label>
                  <input type="text" class="form-control" data-inputmask="'alias': 'yyyy'" data-mask id="Created_Year" name="Created_Year" value="" placeholder="Tahun" required>
                </div>
                <div class="form-group">
                  <label for="Trasport_Type">Tipe Armada <b style="color:red;">*</b></label>
                  <select id="Trasport_Type" name="Trasport_Type" class="form-control"   >
                    <option value="TRAILER">TRAILER</option>
                  </select> 
                </div>
                <div class="form-group">
                  <table id="table_insurance">
                    <tr>
                      <td>
                        <label for="insurance_value">Harga Armada <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_value_view" name="insurance_value_view" style="width: 100%;" onkeyup="Insurance_Value()" value="0">
                        <input type="hidden" id="insurance_value" name="insurance_value" style="width: 100%;" value="0">
                      </td>
                      <td style="width:20px;">&nbsp;</td>
                      <td>
                        <label for="insurance_type">Asuransi <b style="color:red;">*</b></label>
                          <select class="form-control select2" id="insurance_type" name="insurance_type" style="width: 100%;" required >
                            <?php $__currentLoopData = $listInsurance; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($row->mst_codedetail); ?>" <?php echo e((old("insurance_type") == $row->mst_codedetail ? "selected":"")); ?>><?php echo e($row->mst_codedesc); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                      </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr id="tr_insurance_1" style="display:none">
                      <td>
                        <label for="insurance_policy">Nomor Polis <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_policy" name="insurance_policy" style="width: 100%;" value="" readonly>
                      </td>
                      <td style="width:20px;">&nbsp;</td>
                      <td>
                        <label for="insurance_premi">Premi <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_premi_view" name="insurance_premi_view" style="width: 100%;" value="0" readonly>
                        <input type="hidden" id="insurance_premi" name="insurance_premi" style="width: 100%;" value="0" readonly>
                      </td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr id="tr_insurance_2" style="display:none">
                      <td>
                        <label for="insurance_admin">Admin <b style="color:red;">*</b></label>
                        <input type="text" id="insurance_admin_view" name="insurance_admin_view" style="width: 100%;" value="0" readonly>
                        <input type="hidden" id="insurance_admin" name="insurance_admin" style="width: 100%;" value="0" readonly>
                      </td>
                      <td style="width:20px;">
                        &nbsp;
                      </td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>
                <div class="form-group">
                  <label for="description">Keterangan</label>
                  <textarea class="form-control" id="description" name="description" placeholder="Keterangan" required></textarea>
                </div>                
                <div class="form-group">
                  <button type="button" class="btn btn-danger" onclick="cancel_process()">Kembali</button>
                  <button type="button" id="submit" class="btn btn-primary">Simpan</button>
                </div>                
              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <table border="0">
                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload STNK <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_stnk" name="image_stnk"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload KIR <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_kir" name="image_kir"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Image TRUK <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_truk" name="image_truk"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="width:50px;"></td>
                    <td>
                      <div class="form-group">
                        <label for="company_name">Upload Polis ASURANSI <b style="color:red;">*</b></label><br />
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                              <span class="btn default btn-file" style="background:#3C8DBC; color:white;">
                              <span class="fileinput-new" > Select Foto </span>
                              <span class="fileinput-exists"> Change </span>
                              <input type="file" id="image_asuransi" name="image_asuransi"> </span>
                              <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput" style="background:#3C8DBC; color:white;">Remove </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- /.box-body -->
              <div class="box-footer" style="text-align:left;">
                &nbsp;
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->


      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<style>
#geocomplete { width: 500px}
.map_canvas { 
  width: 600px; 
  height: 400px; 
  margin: 10px 20px 10px 0;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-daterangepicker/daterangepicker.js')); ?>"></script>
<script src="<?php echo e(url('adminlte.assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(url('js/bootstrap-fileinput.js')); ?>"></script>

<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    $('[data-mask]').inputmask("9999");
  });

  function cancel_process()
  {
    window.location = "<?php echo e(url('/')); ?>/Masterdata/ListTrans/";
  }
  
  function number_format(user_input){
    var filtered_number = user_input.replace(/[^0-9]/gi, '');
    var length = filtered_number.length;
    var breakpoint = 1;
    var formated_number = '';

    for(i = 1; i <= length; i++){
        if(breakpoint > 3){
            breakpoint = 1;
            formated_number = ',' + formated_number;
        }
        var next_letter = i + 1;
        formated_number = filtered_number.substring(length - i, length - (i - 1)) + formated_number; 

        breakpoint++;
    }

    return formated_number;
  }

  $("#insurance_value_view").focusout(function(){
      var insval = $("#insurance_value_view").val();
      $("#insurance_value_view").val(number_format(insval));

//          $("#tr_insurance_1").hide('slow');
 //         $("#tr_insurance_2").hide('slow');

          $("#insurance_policy").val('');
          $("#insurance_premi_view").val(0);
          $("#insurance_admin_view").val(0);
          $("#insurance_premi").val(0);
          $("#insurance_admin").val(0);
  });

  function Insurance_Value()
  {
      var p_param = $("#or_insurancevalue_view").val();
      
      $("#or_insurancevalue").val(p_param);
  }

  $("select[name='insurance_type']").change(function(){
      var p_param = $(this).val();
      //var orinsurance_text = $("#or_insurance option:selected").html();
      //$('#ordertype_text').val(ordertype_text);

      if(p_param=='ins000')
      {
          $("#tr_insurance_1").hide('slow');
          $("#tr_insurance_2").hide('slow');
          //$("#or_insurancevalue_view").val(0);
          //$("#or_insurancevalue").val(0);

          $("#insurance_policy").val('');
          $("#insurance_premi_view").val(0);
          $("#insurance_admin_view").val(0);
          $("#insurance_premi").val(0);
          $("#insurance_admin").val(0);
      }
      else{
          $("#tr_insurance_1").show('slow');
          $("#tr_insurance_2").show('slow');

          var brand_type = $("#brand_type").val();
          var Created_Year = $("#Created_Year").val();
          var Trasport_Type = $("#Trasport_Type").val();
          var insurancevalue = $("#insurancevalue").val();

          $.ajax({
            type: 'POST',
            url: "<?php echo e(url('/')); ?>/Masterdata/get_InsuranceAPI",
            datatype: 'JSON',
            data: {p_param:p_param, brand_type:brand_type, Created_Year:Created_Year, Trasport_Type:Trasport_Type, insurancevalue:insurancevalue},
            success: function(data) {
                $("#insurance_policy").val(data.Policy);
                $("#insurance_premi_view").val(number_format(data.Premi));
                $("#insurance_admin_view").val(number_format(data.Admin));
                $("#insurance_premi").val(data.Premi);
                $("#_insurance_admin").val(data.Admin);
            }
          });
      }
  });

  $("#submit").click(function()
  {
      var stnk = $("#images_stnk").val();
      var kir = $("#images_kir").val();
      var truk = $("#images_truk").val();
      var asuransi = $("#images_asuransi").val();
      var Police_Number = $("#Police_Number").val();
      var Created_Year = $("#Created_Year").val();
      var Trasport_Type = $("#Trasport_Type").val();
      var description = $("#description").val();

      if(Police_Number=='' || Created_Year=='' || Trasport_Type=='' || stnk=='' 
         || kir=='' || truk=='' || asuransi=='')
      {
        if(Police_Number=='')
        {alert('Kolom Nomor Polisi tidak boleh kosong!'); $("#Police_Number").focus(); return false;}

        if(Created_Year=='')
        {alert('Kolom Tahun Pembuatan tidak boleh kosong!'); $("#asuransi").focus(); return false;}
        if(Trasport_Type=='')
        {alert('Kolom Tipe Armada tidak boleh kosong!'); $("#Created_Year").focus(); return false;}
        if(stnk=='')
        {alert('Kolom Upload STNK tidak boleh kosong!'); $("#stnk").focus(); return false;}
        if(kir=='')
        {alert('Kolom Upload Kir tidak boleh kosong!'); $("#kir").focus(); return false;}
        if(truk=='')
        {alert('Kolom Upload Truk tidak boleh kosong!'); $("#truk").focus(); return false;}
        if(asuransi=='')
        {alert('Kolom Upload Asuransi tidak boleh kosong!'); $("#asuransi").focus(); return false;}
      }

      var file_data = $('#image_stnk').prop('files')[0];
      var form_data = new FormData();
      form_data.append('file', file_data);

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Masterdata/check_policenumber",
          datatype: 'JSON',
          data: {Police_Number:Police_Number},
          success: function(data)
          {
            if(data > 0)
            {
              alert('Nomor Polisi sudah ada!');
              $("#Police_Number").focus();
              return false;
            }
            else{
              $.ajax({
                  url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileSTNK",
                  dataType: 'text', // what to expect back from the PHP script
                  cache: false,
                  contentType: false,
                  processData: false,
                  data:form_data,
                  type: 'post',
                  success: function (response) {
                      if(response==false)
                      {
                          alert('Upload STNK gagal dilakukan!');
                          return false;
                      }
                      else {
                          uploadFileKIR(response);
                      }
                  },
                  error: function (response) {
                      alert('Upload STNK gagal dilakukan!');
                      return false;
                  }
              }); /* AJAX END */
            }
          }
      });
  });

  function uploadFileKIR(response_STNK)
  {
    var file_data = $('#image_kir').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileKIR",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload KIR gagal dilakukan!');
                return false;
            }
            else {
                uploadFileTRUK(response_STNK, response);
            }
        },
        error: function (response) {
            alert('Upload KIR gagal dilakukan!');
            return false;
        }
    });
  }

  function uploadFileTRUK(response_STNK, response_KIR)
  {
    var file_data = $('#image_truk').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileTRUK",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Truk gagal dilakukan!');
                return false;
            }
            else {
                uploadFileASURANSI(response_STNK, response_KIR, response);
            }
        },
        error: function (response) {
            alert('Upload Truk gagal dilakukan!');
            return false;
        }
    });
  }

  function uploadFileASURANSI(response_STNK, response_KIR, response_TRUK)
  {
    var file_data = $('#image_asuransi').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);

    $.ajax({
        url: "<?php echo e(url('/')); ?>/Masterdata/UploadFileASURANSI",
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data:form_data,
        type: 'post',
        success: function (response) {
            if(response==false)
            {
                alert('Upload Asuransi gagal dilakukan!');
                return false;
            }
            else {
                Save_Data(response_STNK, response_KIR, response_TRUK, response);
                //alert(response_STNK + ' : ' + response_KIR + ' : ' + response_TRUK + ' : ' + response);
            }
        },
        error: function (response) {
            alert('Upload Asuransi gagal dilakukan!');
            return false;
        }
    });
  }

  function Save_Data(response_STNK, response_KIR, response_TRUK, response_ASURANSI)
  {
      var stnk = response_STNK.replace(/"/g, '');
      var kir = response_KIR.replace(/"/g, '');
      var truk = response_TRUK.replace(/"/g, '');
      var asuransi = response_ASURANSI.replace(/"/g, '');
      var Police_Number = $("#Police_Number").val();
      var Created_Year = $("#Created_Year").val();
      var Trasport_Type = $("#Trasport_Type").val();
      var description = $("#description").val();

      $.ajax({
        type:'POST',
        url: "<?php echo e(url('/')); ?>/Masterdata/saveTrans",
        datatype: 'JSON',
        data: {stnk:stnk, kir:kir, truk:truk, asuransi:asuransi, Police_Number:Police_Number,
               Created_Year:Created_Year, Trasport_Type:Trasport_Type, description:description},
        success: function(data) {
          if(data==true){
            swal({
                      title: "",
                      text: "Data berhasil disimpan." ,
                      type: "success",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Selesai ",
                      cancelButtonText: "Tidak ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                          window.location.href = "<?php echo e(url('/')); ?>/Masterdata/ListTrans";
                        
                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
            });
          }
          else
          {
            swal({
                      title: "",
                      text: "Data gagal disimpan." ,
                      type: "error",
                      showCancelButton: false,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Tutup ",
                      cancelButtonText: "Tidak ",
                      closeOnConfirm: true,
                      closeOnCancel: false
                  }, function (isConfirm) {
                      if (isConfirm) {
                        // window.location.href = "tampilan-data";
                        
                      } else {
                          //swal("Dibatalkan", " ", "error");
                      }
                  });
          }
        }
      });

  }








  function check_usertrucker()
  {
      var p_param = $("#username").val();

      $.ajax({
          type: 'POST',
          url: "<?php echo e(url('/')); ?>/Registers/check_usertrucker",
          datatype: 'JSON',
          data: {p_param:p_param},
          success: function(data) {
              if(data!='')
              {
                alert('Username trucker tidak tersedia atau sudah digunakan,  silakan cari username lain!');
                $("#username").focus();
                return false;
              }
          }
      });
  }


  function save_datatrucker(res_npwp, res_siup)
  {
    var res_npwp = res_npwp.replace(/"/g, '');
    var res_siup = res_siup.replace(/"/g, '');
    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();    
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' || 
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' || 
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' || 
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }
    else{
        check_usertrucker();

        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:res_npwp, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2, 
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:res_siup
            },
            success: function(data) {
            if(data==true){
              alert('Create data successfully');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
            }
            else
            {
              alert('Create data failed');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
              }
            }
        });
    }
  }

//$("#example-form").submit(function(){
  $("#submitxx").click(function()
  {
    //company_name, address_1, address_2, zip_code, city, state, country, email_kantor, 
    //telp_kantor, npwp, image_npwp, name_1, phone_1, telp_1, email_1, name_2, phone_2, telp_2, 
    //email_2, username, password, siup, image_siup

    var gtime = new Date();
    var company_name = $("#company_name").val();
    var address_1 = $("#address_1").val();
    var address_2 = $("#address_2").val();
    var zip_code = $("#zip_code").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var country = $("#country").val();
    var email_kantor = $("#email_kantor").val();
    var telp_kantor = $("#telp_kantor").val();
    var npwp = $("#npwp").val();    
    var str_npwp = $("#image_npwp").val();
    var file_npwp_replace = str_npwp.replace(/C:|fakepath./g,'');
    var file_npwp = file_npwp_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_npwp = file_npwp.split('.').pop();
    var image_npwpname = "NPWP_" + gtime.getTime() + "_thump." + ext_npwp;

    var name_1 = $("#name_1").val();
    var phone_1 = $("#phone_1").val();
    var telp_1 = $("#telp_1").val();
    var email_1 = $("#email_1").val();
    var name_2 = $("#name_2").val();
    var phone_2 = $("#phone_2").val();
    var telp_2 = $("#telp_2").val();
    var email_2 = $("#email_2").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var siup = $("#siup").val();
    var str_siup = $("#image_siup").val();
    var file_siup_replace = str_siup.replace(/C:|fakepath./g,'');
    var file_siup = file_siup_replace.replace(/\\/g,'');

    //console.log(file_npwp.split('.').pop());
    var ext_siup = file_siup.split('.').pop();
    var image_siupname = "SIUP_" + gtime.getTime() + "_thump." + ext_siup;

    if(company_name=='' || address_1=='' || address_2=='' || zip_code=='' || city=='' || 
       state=='' || country=='' || email_kantor=='' || telp_kantor=='' || npwp=='' || 
       name_1=='' || phone_1=='' || telp_1=='' || email_1=='' || name_2=='' || 
       phone_2=='' || telp_2=='' || email_2=='' || username=='' || password=='' || siup=='')
    {
        alert('Please fill in all the required fields marked with asterisks (*).');
        return false;
    }
    else{
        uploadFileNPWP(image_npwpname, image_siupname);
        check_usertrucker();

        $.ajax({
            type:'POST',
            url: "<?php echo e(url('/')); ?>/Registers/save",
            datatype: 'JSON',
            data: {
                          company_name:company_name, address_1:address_1, address_2:address_2,
                          zip_code:zip_code, city:city, state:state, country:country,
                          email_kantor:email_kantor, telp_kantor:telp_kantor, npwp:npwp,
                          file_npwp:image_npwpname, name_1:name_1, phone_1:phone_1, telp_1:telp_1,
                          email_1:email_1, name_2:name_2, phone_2:phone_2, telp_2:telp_2, 
                          email_2:email_2, username:username, password:password, siup:siup, file_siup:image_siupname
            },
            success: function(data) {
            if(data==true){
              alert('Create data successfully');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
            }
            else
            {
              alert('Create data failed');
              setTimeout(function() {
                  $("#submit").button('reset');
              }, 500);
              }
            }
        });
    }
  });

</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>