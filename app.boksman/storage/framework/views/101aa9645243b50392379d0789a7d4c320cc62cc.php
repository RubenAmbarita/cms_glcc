<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="<?php echo e(url('/')); ?>/dashboard"><i class="fa fa-home"></i> Home</a></li>
        <li><a href="<?php echo e(url('/')); ?>/Published/AcceptShipperOrder">Shipper Order</a></li>
        <li class="active">Shipper Order</li>
      </ol>
      <h1 style="font-family:Poppins"> Shipper Order </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title"> Shipper Order List</h3>
            </div> -->
            <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
                  <thead>
                  <tr>
                  <tr>
                    <th style="text-align:center;"></th>
                    <th style="text-align:center;">Order </th>
                    <th style="text-align:center;">Shipper</th>
                    <th style="text-align:center;">Depo</th>
                    <th style="text-align:center;">Shipping Line</th>
                    <th style="text-align:center;">Insurance</th>
                    <th style="text-align:center;">Cost</th>

                  </tr>
                  </tr>
                  </thead>
                  <tbody>
                  <?php $__currentLoopData = $order_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td style="text-align:center;">
                        <form action="<?php echo e('AcceptShipperOrderProcess'); ?>" method="post">
                          <?php echo csrf_field(); ?>

                            <input type="hidden" id="id_order" name="id_order" value="<?php echo e($row->id_order); ?>">
                            <input type="hidden" id="gk_order" name="gk_order" value="<?php echo e($row->gk_order); ?>">
                            <input type="hidden" id="id_customer" name="id_customer" value="<?php echo e($row->id_customer); ?>">
                            <input type="hidden" id="stuffing_date" name="stuffing_date" value="<?php echo e($row->pickup_date); ?>">
                            <input type="hidden" id="id_sl" name="id_sl" value="<?php echo e($row->id_sl); ?>">
                            <input type="hidden" id="id_vv" name="id_vv" value="<?php echo e($row->id_vv); ?>">
                            <input type="hidden" id="open_time" name="open_time" value="<?php echo e($row->open_time); ?>">
                            <input type="hidden" id="closing_time" name="closing_time" value="<?php echo e($row->closing_time); ?>">
                            <input type="hidden" id="ETD" name="ETD" value="<?php echo e($row->ETD); ?>">
                            <input type="hidden" id="ukuran_container" name="ukuran_container" value="<?php echo e($row->ukuran_container); ?>">
                            <input type="hidden" id="id_commodity" name="id_commodity" value="<?php echo e($row->id_commodity); ?>">
                            <input type="hidden" id="area" name="area" value="<?php echo e($row->area); ?>">
                            <input type="hidden" id="pickup_coordinate" name="pickup_coordinate" value="<?php echo e($row->pickup_coordinate); ?>">
                            <input type="hidden" id="Deductible" name="Deductible" value="<?php echo e($row->Deductible); ?>">
                            <input type="hidden" id="Premi" name="Premi" value="<?php echo e($row->Premi); ?>">
                            <input type="hidden" id="Admin" name="Admin" value="<?php echo e($row->Admin); ?>">
                            <input type="hidden" id="Harga" name="Harga" value="<?php echo e($row->harga); ?>">
                            <input type="hidden" id="id_depo" name="id_depo" value="<?php echo e($row->id_depo); ?>">
                            <input type="hidden" id="coordinate_depo" name="coordinate_depo" value="<?php echo e($row->coordinate_depo); ?>">
                            <button class="btn btn-info" type="submit"><i class="fa fa-external-link"></i> Assign</button>
                            <!--
                            <?php if($row->id_trucker): ?>
                              <button class="btn btn-info" type="submit">Assign</button>
                            <?php else: ?>
                              <button class="btn btn-warning" type="button" onclick="Accept_Order()">Accept</button>
                            <?php endif; ?>-->
                        </form>
                      </td>
                        <td>
                          <b>No. DO : </b> <?php echo e($row->number_do); ?><br />
                          <b>No. : </b><?php echo e($row->gk_order); ?><br />
                          <b>Stuffing Date : </b><?php echo e($row->pickup_date); ?><br />
                          <b>Container : </b><?php echo e($row->ukuran_container_name); ?><br />
                          <b>Commodity : </b><?php echo e($row->commodity_name); ?>

                        </td>
                        <td>
                          <?php echo e($row->customer_name); ?><br />
                          <b>Alamat Penjemputan :</b><br />
                          <?php echo e($row->pickup_address); ?>

                        </td>
                        <td>
                          <?php echo e($row->depo_name); ?><br />
                          <b>Alamat Depo :</b><br />
                          <?php echo e($row->address_depo); ?>

                        </td>
                        <td>
                          <?php echo e($row->sl_name); ?><br />
                          <b>Vessel Voyage : </b><?php echo e($row->vessel_name); ?> - <?php echo e($row->voyage_number); ?><br />
                          <b>Open Stack : </b><?php echo e($row->open_time); ?>, <b>Closing Time : </b><?php echo e($row->closing_time); ?>, <b>ETD : </b><?php echo e($row->ETD); ?><br />
                        </td>
                        <td>
                          <b>Deductible : </b><?php echo e($row->Deductible_view); ?><br />
                          <b>Premi : </b><?php echo e($row->Premi_view); ?><br />
                          <b>Admin : </b><?php echo e($row->Admin_view); ?><br />
                        </td>
                        <td><?php echo e($row->harga_view); ?></td>

                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th style="text-align:center;">Order </th>
                      <th style="text-align:center;">Shipper</th>
                      <th style="text-align:center;">Depo</th>
                      <th style="text-align:center;">Shipping Line</th>
                      <th style="text-align:center;">Insurance</th>
                      <th style="text-align:center;">Cost</th>
                      <th style="text-align:center;"></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     window.history.back();
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true,
                    ordering : false
                });
  });

  function Accept_Order()
  {
    var gk_order = $("#gk_order").val();
    var id_customer = $("#id_customer").val();

      swal({
          title: "Terima Order ini?",
          text: "Klik tombol Ya untuk melanjutkan proses!" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/Accept_Order",
                  datatype: 'JSON',
                  data: {gk_order:gk_order, id_customer:id_customer},
                  success: function(data)
                  {
                    if(data == true)
                    {
                        swal({
                          title: "Successfully !",
                          text: "Data berhasil diterima." ,
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Selesai ",
                          cancelButtonText: "Tidak ",
                          closeOnConfirm: true,
                          closeOnCancel: false
                        }, function (isConfirm) {
                          if (isConfirm) {
                              window.location = "<?php echo e(url('/')); ?>/Published/AcceptShipperOrder/";

                          } else {
                              return false;
                          }
                        });

                    }
                    else{
                      swal({
                            title: "Failed",
                            text: "Data gagal diterima." ,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Tutup ",
                            cancelButtonText: "Tidak ",
                            closeOnConfirm: true,
                            closeOnCancel: false
                          }, function (isConfirm) {
                              if (isConfirm) {
                                // window.location.href = "tampilan-data";

                              } else {
                                  return false;
                              }
                          });
                    }
                  }
              });

          } else {
              return false;
          }
      });
  }

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>