<?php $__env->startSection('css'); ?>
	<link href="https://hpneo.github.io/gmaps/prettify/prettify.css" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        History Detail
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/Published/history">History</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">&nbsp;</h3>
            </div>
            <!-- /.box-header -->

            <!-- form start -->
            <form action="<?php echo e('Order/matchtab1'); ?>" id="formtab1" method="post">
              <?php echo csrf_field(); ?>

              <!-- HALAMAN PERTAMA SEBELAH KIRI -->
              <div class="box-body col-md-6">
                <div class="form-group has-feedback" style="padding:0px; margin-left:10px; margin-top:10px; border:0px solid #ccc!important; border-radius:5px; width:100%; height:194px;">
                    <table style="width:100%; height:100px; border="0px">
                        <tr>
                            <td><label style="width:120px;">Delivery Status</label></td>
                            <td style="text-align:center;"><label style="width:20px;"> : </label></td>
                            <td style="width:100%;"><b><?php echo e($row->Shipp_Status); ?></b></td>
                        </tr>

                        <tr>
                            <td><label>Plate #</label></td>
                            <td style="text-align:center;"><label> : </label></td>
                            <td><?php echo e($row->Police_Number); ?></td>
                        </tr>

                        <tr>
                            <?php if($row->Order_Type=='ormatch'): ?>
                            <td><label>Est. Finish Unloading at Consignee</label></td>
                            <?php else: ?>
                            <td><label>Assigned Date</label></td>
                            <?php endif; ?>
                            <td style="text-align:center;"><label> : </label></td>
                            <td><?php echo e($row->estimate_free_time); ?></td>
                        </tr>

                        <tr>
                            <td><label>Shipping Line</label></td>
                            <td style="text-align:center;"><label> : </label></td>
                            <td><?php echo e($row->Shipping_Line); ?></td>
                        </tr>

                        <tr>
                            <td><label>&nbsp;</label></td>
                            <td style="text-align:center;"><label>&nbsp;</label></td>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="vertical-align:top;"><label>Depo</label></td>
                            <td style="text-align:center; vertical-align:top;"><label> : </label></td>
                            <td>
                            <b><?php echo e($row->Depo_Name); ?></b><br />
                            <?php echo e($row->Depo_Address); ?>

                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">
                    <table>
                        <tr>
                            <td colspan="2" style="width:68%;">&nbsp;</td>
                            <td>
                              <br /><br />
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">
                    <table style="padding:0px; margin-top:10px; border:1px solid #ccc!important; border-radius:5px; width:100%; height:160px;">
                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="2" style="padding:10px; width:100%; text-align:left;"><b>&nbsp;<?php echo e($row->Importir); ?></b></td>
                          <td style="padding:10px; width:100%; text-align:right;"><b>CONSIGNEE</b>&nbsp;</td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td><label style="width:120px;">Volume</label></td>
                                    <td style="text-align:center;"><label style="width:20px;"> : </label></td>
                                    <td style="width:100%;"><b><?php echo e($row->Container_Size); ?></b></td>
                                </tr>

                                <tr>
                                    <td><label>Container #</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Container_Size); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Driver</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Driver); ?></td>
                                </tr>

                                <tr>
                                    <td><label>B/L #</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->BL_Number); ?></td>
                                </tr>
                            </table>
                          </td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td colspan="3"><label style="width:120px;">From :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Importir_Origin); ?>

                                    </td>
                                </tr>

                                <tr><td colspan="3">&nbsp;</tr>

                                <tr>
                                    <td colspan="3"><label style="width:120px;">To :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Importir_Destination); ?>

                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                    </table>

                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-body col-md-6">
                <div class="form-group has-feedback">
                    <table style="padding:0px; margin-top:10px; border:1px solid #ccc!important; border-radius:5px; width:100%; height:200px;">
                        <tr>
                            <td colspan="3"><div id="gmap_markers" style=" height: 100%; " class="gmap"></div></td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">
                    <table>
                        <tr>
                            <td colspan="2" style="width:68%;">&nbsp;</td>
                            <td>
                              <a href="#tab_2"  data-toggle="tab"  onclick="Refresh_Maps()" >  <button type="button" class="btn btn-success waves-effect"><i class="fa fa-refresh"></i> Refresh</button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" id="Cancel_Process" style="display:none; text-align:left;">
                                <table style="padding:0px; margin-top:10px; border:1px solid #FF0000!important; border-radius:5px; width:100%; height:100px; box-shadow: 2px 2px 2px 2px #888888;"">
                                    <tr style="padding:10px; background-color: #FF0000; color: #FFFFFF; border-bottom:1px solid #FF0000;">
                                        <td colspan="3" style="padding:10px; width:100%; text-align:left;"><b>CANCEL YOUR PUBLISH</b>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px; text-align:left;">
                                            <label for="email_address_2">Reason</label>
                                        </td>
                                        <td colspan="2" style="padding:10px; width:100%; text-align:left;">
                                            <select class="form-control" name="reason" id="reason">
                                                <option value="0">Choose Your Reason</option>
                                                <option value="1">TRUCK STORING</option>
                                                <option value="2">CONTAINER MAJOR DEMAGE</option>
                                                <option value="3">OTHERS</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:10px; text-align:left;">
                                            <label for="email_address_2">&nbsp;</label>
                                        </td>
                                        <td colspan="2" style="padding:10px; width:100%; text-align:left;">
                                            <div id="idreason_other" style="display:none;" class="form-group has-feedback">
                                                <textarea id="reason_other" name="reason_other" placeholder="Others Reason *" class="form-control"></textarea>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="email_address_2">&nbsp;</label>
                                        </td>
                                        <td colspan="2" style="padding:10px; width:100%; text-align:right;">
                                            <input type="hidden" id="Closing_Time" name="Closing_Time" value="<?php echo e($row->Closing_Time); ?>">
                                            <input type="hidden" id="Publish_Number" name="Publish_Number" value="<?php echo e($row->Publish_Number); ?>">
                                            <input type="hidden" id="GK_Order" name="GK_Order" value="<?php echo e($row->GK_Order); ?>">
                                            <input type="hidden" id="id_Exportir" name="id_Exportir" value="<?php echo e($row->Exportir_ID); ?>">
                                            <input type="hidden" id="sysdate" name="sysdate" value="<?php echo e($row->sysdate); ?>">
                                            <a href="#tab_1"  onclick="Confirm_Cancel(<?php echo e($row->Publish_Number); ?>)"  data-toggle="tab"> <button type="button" class="btn btn-info">Proses</button></a>
                                            <a href="#tab_1"  onclick="cancel_processHide()"  data-toggle="tab"> <button type="button" class="btn btn-danger">Cancel</button></a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="form-group has-feedback">
                    <table style="padding:0px; margin-top:10px; border:1px solid #ccc!important; border-radius:5px; width:100%; height:160px;">
                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="2" style="padding:10px; width:100%; text-align:left;"><b>&nbsp;<?php echo e($row->Exportir); ?></b></td>
                          <td style="padding:10px; width:100%; text-align:right;"><b>SHIPPER</b>&nbsp;</td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td><label style="width:120px;">Commodity</label></td>
                                    <td style="text-align:center;"><label style="width:20px;"> : </label></td>
                                    <td style="width:100%;"><?php echo e($row->Commodity); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Packaging</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Packaging); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Vessel Voyage</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td><?php echo e($row->Vessel_Voyage); ?></td>
                                </tr>

                                <tr>
                                    <td><label>Price</label></td>
                                    <td style="text-align:center;"><label> : </label></td>
                                    <td>Rp. <?php echo e($row->Basic_Price); ?></td>
                                </tr>
                            </table>
                          </td>
                        </tr>

                        <tr style="padding:10px; border-bottom:1px solid #ccc;">
                          <td  colspan="3" style="padding:10px; width:100%; text-align:left;">
                            <table style="width:100%;" border="0px">
                                <tr>
                                    <td colspan="3"><label style="width:120px;">From :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Exportir_Origin); ?>

                                    </td>
                                </tr>

                                <tr><td colspan="3">&nbsp;</tr>

                                <tr>
                                    <td colspan="3"><label style="width:120px;">To :</label></td>
                                </tr>

                                <tr>
                                    <td colspan="3">
                                      <?php echo e($row->Exportir_Destination); ?>

                                    </td>
                                </tr>
                            </table>
                          </td>
                        </tr>
                    </table>

                </div>


              </div>
              <!-- /.box-body -->

              <div class="box-footer" style="text-align:right;">
              </div>
              <div class="form-group has-feedback <?php echo e($errors->has('add_publish_tab1') ? 'has-error' : ''); ?>">
                <?php if($errors->has('add_publish_tab1')): ?>
                  <span class="help-block">
                    <strong><?php echo e($errors->first('add_publish_tab1')); ?></strong>
                  </span>
                <?php endif; ?>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDccWhyi1-FYNQOkZYa4Fgzc1VWgrxhyE4&sensor=true"></script>
<script type="text/javascript" src="https://hpneo.github.io/gmaps/gmaps.js"></script>
<script type="text/javascript" src="https://hpneo.github.io/gmaps/prettify/prettify.js"></script>

<script>
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  function Refresh_Maps()
  {
    var Coordinate = "<?php echo e($row->Driver_Coordinate); ?>";
    if(Coordinate=='')
    {
        Coordinate = "-6.107080,106.880518";
    }

    var CoorExplode = Coordinate.split(",");

    prettyPrint();
    var markers = new GMaps({
      div: '#gmap_markers',
      lat: CoorExplode[0],
      lng: CoorExplode[1],
      mapTypeControlOptions: {
        //mapTypeIds : ["hybrid", "roadmap", "satellite", "terrain", "osm"]
        mapTypeIds : [""]
    }
    });

    markers.addMarker({
      lat: CoorExplode[0],
      lng: CoorExplode[1],
      title: "<?php echo e($row->Driver); ?>",
      icon: "<?php echo e(url('images/trucker.png')); ?>"
    });
  }
  $(function(){
    Refresh_Maps();
  });

  // CANCEL PROCESS REDIRECT LINK WAREHOSE HOME
  function done_process()
  {
     //window.history.back();
     window.location = "<?php echo e(url('/')); ?>/Published/matched/";
  }

  $(function () {
    var table = $('#example1').dataTable({
                    scrollX : true,
                    scrollCollapse : true
                });
  });

  function cancel_process()
  {
      $("#Cancel_Process").show('slow');
  }

  function cancel_processHide()
  {
      $("#Cancel_Process").hide('slow');
  }

  function Confirm_Cancel(id_publish)
  {
      var sysdate = $("#sysdate").val();
      var closing_time = $("#Closing_Time").val();
      var Publish_Number = $("#Publish_Number").val();
      var GK_Order = $("#GK_Order").val();
      var id_Exportir = $("#id_Exportir").val();
      var reason = $("#reason_other").val();

      if(reason=='' || reason=='Choose Your Reason')
      {
          alert("Alasan pembatalan belum terisi !");
          return false;
      }


      swal({
          title: "Apakah Anda Yakin?",
          text: "Data akan dihapus secara permanent !" ,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya ",
          cancelButtonText: "Tidak ",
          closeOnConfirm: true,
          closeOnCancel: true
      }, function (isConfirm) {
          if (isConfirm) {
            $.ajax({
                  type: 'POST',
                  url: "<?php echo e(url('/')); ?>/Published/get_TimeDiff",
                  datatype: 'JSON',
                  data: {DateNow:sysdate, closing_time:closing_time},
                  success: function(data)
                  {
                    if(data > 24)
                    {
                        var canceled = 2;
                        var internal_state = 0;
                    }
                    else if(data > 0 && data < 24)
                    {
                        var canceled = 0;
                        var internal_state = 2;
                    }
                    else{
                        alert('Data tidak dapat dibatalkan !');
                        $("#Cancel_Process").hide('slow');
                        return false;
                    }
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo e(url('/')); ?>/Published/deleteMatched",
                            datatype: 'JSON',
                            data: {GK_Order:GK_Order, Publish_Number:Publish_Number, canceled:canceled, id_Exportir:id_Exportir, reason:reason},
                            success: function(data)
                            {
                                if(data == true)
                                {
                                    swal({
                                    title: "Successfully !",
                                    text: "Data berhasil dihapus." ,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Selesai ",
                                    cancelButtonText: "Tidak ",
                                    closeOnConfirm: true,
                                    closeOnCancel: false
                                    }, function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = "<?php echo e(url('/')); ?>/Published/matched/";

                                    } else {
                                        return false;
                                    }
                                    });
                                }
                                else{
                                swal({
                                        title: "Failed",
                                        text: "Data gagal disimpan." ,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Tutup ",
                                        cancelButtonText: "Tidak ",
                                        closeOnConfirm: true,
                                        closeOnCancel: false
                                    }, function (isConfirm) {
                                        if (isConfirm) {
                                            // window.location.href = "tampilan-data";

                                        } else {
                                            return false;
                                        }
                                    });
                                }
                            }
                        });
                  }
              });

          } else {
              return false;
          }
      });
  }

  $("select[name='reason']").change(function(){
    var reason = $("#reason").val();
    var reason_text = $( "#reason option:selected" ).text();

    if(reason == 3){
        $("#idreason_other").show("slow");
        $("#reason_other").val('');
    }
    else{
        $("#idreason_other").hide("slow");
        $("#reason_other").val(reason_text);
    }
  });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>