/**
*   Pengarang : eddiesw
*   Dibuat Tanggal : 12 Des 2011
*   Tujuan : Untuk menampilkan tooltip (petunjuk) pada saat mengetik nominal rupiah
*   Cara pemakaian :
*       -   cek apakah id tooltip_currency sudah ada? (dibuat di common.tpl)
*       -   daftarkan semua id yang menggunakan tooltip
            var ids = ["id1","id2"]; ganti id1 dan id2 dengan id2 yang ada
        -   contoh (ketiga baris harus ada):
            var ids = ["cn_goods_value"];
            var x = new currency_tooltip(ids);
            x.run();
*/
function currency_tooltip(ids)
{
    var this_ids = new Array();
    this_ids = ids;

    if(typeof $('#tooltip_currency') == 'undefined')
    {
        alert('Id div tooltip_currency Tidak Ada');
        return false;
    }

    this.run = function()
    {
        //loop sebanyak id yg ingin ada tooltipnya
        $.each(this_ids, function(key, value)
        {
            $("#"+value).addClass('currency');
        });

        if($(".currency").length > 0)
            this.proses();
        else
            alert('Id belum dicatat');
    }

    this.proses = function()
    {
        //on keyup
        $(".currency").keyup(function()
        {
            //get the position of the input element
            var pos   = $(this).offset();
            var width = $(this).width()/2;

            //show the tooltip above the element
            $('#tooltip_currency').css(
            {
                "left": (pos.left + width) + "px",
                "top":(pos.top-35) + "px"
            }).show();

            //Tampilkan nilai di tooltip
            var curr_value = formatCurrency( $(this).val() );
            $('#currencyText').text(curr_value);
        });

        //onblur
        $(".currency").blur(function()
        {
            $('#tooltip_currency').hide();
            $('#currencyText').text('');
        });
    }
}