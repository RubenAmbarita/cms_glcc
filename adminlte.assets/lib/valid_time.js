function chkTime(timeStr) {
	var timRegX = /^(\d{1,2}):(\d{2})?$/;
	var timArr = timeStr.match(timRegX);
	if (timArr == null) {
		alert("Format jam salah.");
		return false;
	}
	hour = timArr[1];
	minute = timArr[2];

	if (hour < 0  || hour > 23) {
		alert("Format jam yang anda input salah, range inputan antara 0 sampai 23.");
		return false;
	}

	if (minute<0 || minute > 59) {
		alert ("Format menit yang anda input salah, range inputan antara 0 sampai 59.");
		return false;
	}

	return true;
}