<?php

include_once 'config.php';


//Connect to MySql Database
$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE);
if (!$con) {
    die('Could not Connect Database : ' . mysqli_error());
}
 

session_start();
ob_start();
error_reporting(E_ALL ^ E_NOTICE);
 
?>