<?php

	function jangkawaktu($tgl1, $tgl2){
			$tgl1 = strtotime($tgl1);
			$tgl2 = strtotime($tgl2);
			$diff_secs = abs($tgl1 - $tgl2);
			$base_year = min(date("Y", $tgl1), date("Y", $tgl2));
			$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
			return array( "years" => date("Y", $diff) - $base_year, "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, "months" => date("n", $diff) - 1, "days_total" 		=> floor($diff_secs / (3600 * 24)), "days" => date("j", $diff) - 1, "hours_total" => floor($diff_secs / 3600), "hours" => date("G", $diff), "minutes_total" => floor($diff_secs / 60),		 "minutes" => (int) date("i", $diff), "seconds_total" => $diff_secs, "seconds" => (int) date("s", $diff) );
		}

function get_time_line($recent){

  $now=date("Y-m-d H:i:s");
    $a = jangkawaktu($now, $recent);
    $hours=$a['hours_total']; 
    $days=$a['days_total']; 
  if ($days==0) {


       if($hours>0 ){$recent_name=$hours." jam yang lalu";}else{$recent_name="Recently Active";}
  }else if ($days<=7) {
    $recent_name=$days." Hari yang lalu";
  }else {
    if ($recent!="0000-00-00 00:00:00") {
      # code...
    $recent_name=date("d-M-Y",strtotime($row['recent_active']));
    }
  }
  return $recent_name;
}
		?>