<?php 

include 'database_connection.php';

         $DBDATAARRAY = array(); 

    function deleteimg ($dir,$name){
        if(unlink($dir.$name)){
            return true;
        }else{
            return false;
        }
        
    }        
    
    function upload ($dir,$name,$title){
        
        $file_type_foto     = array('jpg','jpeg','png','gif');
        $folder_foto        = $dir;
        $max_size_foto      = 7000000; // 1MB 700000 -> 700kb
        $file_size_foto     = $_FILES[$name]['size'];
        $file_name_foto2    = $_FILES[$name]['name']; 
        $explode_foto       = explode('.',$file_name_foto2);
        $extensi_foto       = $explode_foto[count($explode_foto)-1];
          
            if (move_uploaded_file($_FILES[$name]['tmp_name'],$folder_foto.$title)) {
               return $title;
            }else{
                return false;
            }
             
    }    
         
    function datafield ($field,$value){
            global $DBDATAARRAY;
        
            $set = array(  'field' => $field, 'value' => $value  ); 
            array_push($DBDATAARRAY, $set);
             
    }

    function clearfield (){
           global $DBDATAARRAY;  // break references 
           $DBDATAARRAY=array();
    }   


    function insert(  $table) {
		global $con;
        global $DBDATAARRAY;
        $li = sizeof($DBDATAARRAY);
        $saperator = "";
        $str_field = "";
        $str_data = "";
        $result = "0";
        for ($i = 0; $i < $li; $i++) {
            $str_field .=$saperator . $DBDATAARRAY[$i]["field"];
            $str_data .=$saperator . "'" . $DBDATAARRAY[$i]["value"] . "'";
            $saperator = ",";
        }
        $str = "INSERT INTO $table($str_field)VALUES($str_data)";
        try {
            $result = mysqli_query($con,$str);
        } catch (Exception $e) {
            
        }
        //return $str;
     return mysqli_affected_rows();
    }

    function update( $condition, $table) {
		global $con;
         global $DBDATAARRAY;
        $li = sizeof($DBDATAARRAY);
        $saperator = "";
        $str_field = "SET ";
        $str_data = "";
        $result = "0";
        if ($condition != "")
            $condition = "WHERE " . $condition;
        for ($i = 0; $i < $li; $i++) {
            $str_field .="$saperator" . $DBDATAARRAY[$i]['field'] . "='" . $DBDATAARRAY[$i]['value'] . "'";
            $saperator = ",";
        }
        $str = "UPDATE $table $str_field $condition";
        try {
            $result = mysqli_query($con,$str);
        } catch (Exception $e) {
            
        }
       // return  $str;
        return mysqli_affected_rows();
    }

    function delete( $condition, $table) {
		global $con;
         global $DBDATAARRAY;
        $li = sizeof($DBDATAARRAY);
        $saperator = "";
        $str_field = "SET ";
        $str_data = "";
        $result = "0";
        if ($condition != "")
            $condition = "WHERE " . $condition;
        
        $str = "DELETE FROM $table  $condition";
        try {
            $result = mysqli_query($con,$str);
        } catch (Exception $e) {
            
        }

        // return $str;
        return mysqli_affected_rows();
    }
    
    function select($DBDATAARRAY, $condition, $table) {
		global $con;
        if ($condition != "")
            $condition = "WHERE " . $condition;
       $str = "SELECT $DBDATAARRAY FROM $table $condition";
        try {
            $result = mysqli_query($con,$str);
            $rows = array();
            while ($row = mysqli_fetch_array($result)) {
                $rows[] = $row;
            }
        } catch (Exception $e) {
            return $e;
        }
        return $rows;
    }
    
    function countselect($DBDATAARRAY, $condition, $table) {
		global $con;
        if ($condition != "")
            $condition = "WHERE " . $condition;
       $str = "SELECT $DBDATAARRAY FROM $table $condition";
        try {
            $result = mysqli_query($con,$str); 
            $numrows=mysqli_num_rows($result);
        } catch (Exception $e) {
            return $e;
        }
        return $numrows;
    }

    function query_exec($query)
   {
       return mysqli_query($con,$query);
   }
   function query_exec_no($fetch_data)
   {
        $total_record = mysqli_num_rows($fetch_data);
        return $total_record ;
   }
    

?>