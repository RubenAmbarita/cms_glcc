<?php
	/*
	=================================================================================================================================
	Created By Secret107; GwPunya Corporation; Author By Rahim Pulungan, Arfiansyah Pulungan
	Copyright � GwPunya, 2011 All Right Reserved
	About Author : The Real Green Hacker
	Description  : Creating Cookies with method Encrypt
	License      : Free
	Type         : PHP 4 Source Code
	=================================================================================================================================
	*/

	// Cookies function	
	function setMyCookies($name="", $value=""){
		if (isset($name)){
			if (isset($value)){
				$name=mychangeChar1(myEncrypt($name));
				$value=myEncrypt($value);
			}	
			setcookie($name, $value, time() + 86400, "/"); // Cookie aktif dalam kurun waktu 24 jam
		}
	}
	
	function setMyibeCookies($name="", $value=""){
		if (isset($name)){
			if (isset($value)){
				$name=mychangeChar1(myEncrypt($name));
				$value=myEncrypt($value);
			}	
			setcookie($name, $value, time() + 3600, "/"); // Cookie aktif dalam kurun waktu 1 jam
		}
	}
	
	function setMyibeBPCookies($name="", $value=""){
		if (isset($name)){
			if (isset($value)){
				$name=mychangeChar1(myEncrypt($name));
				$value=myEncrypt($value);
			}	
			setcookie($name, $value, time() + 14400, "/"); // Cookie aktif dalam kurun waktu 4 jam (Khusus proses BACKUP & RESTORE)
		}
	}		
	
	function setCookies($name="", $value=""){
		if (isset($name)){
			if (isset($value)){
				$name=mychangeChar1(myEncrypt($name));
				$value=myEncrypt($value);
			}
		
			setcookie($name, $value, time() + 86400, "/"); // Cookie aktif dalam kurun waktu 24 jam
		}
	}
	
	function getMyCookies($name=""){
		if (isset($name)){
			$name=mychangeChar1(myEncrypt($name));
			$value=myDecrypt($_COOKIE[$name]);
		}
		
		return $value;
	}
	
	function clearMyCookies($name=""){
		if (isset($name)){
			$name=mychangeChar1(myEncrypt($name));
			
			setcookie($name, "", time() + 86400, "/"); // Cookie aktif dalam kurun waktu 24 jam
		}
	}
	
	function clearMyibeCookies($name=""){
		if (isset($name)){
			$name=mychangeChar1(myEncrypt($name));
			
			setcookie($name, "", time() + 3600, "/"); // Cookie aktif dalam kurun waktu 1 jam
		}
	}
	
	function clearMyibeBPCookies($name=""){
		if (isset($name)){
			$name=mychangeChar1(myEncrypt($name));
			
			setcookie($name, "", time() + 14400, "/"); // Cookie aktif dalam kurun waktu 4 jam (khusus proses BACKUP & RESTORE)
		}
	}	
	// Akhir Cookies function
	
	// Membuat fungsi untuk mengacak nilai
	function myEncrypt($sData, $sKey='hastings'){
		$sResult = '';
		for($i=0;$i<strlen($sData);$i++){
			$sChar    = substr($sData, $i, 1);
			$sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
			$sChar    = chr(ord($sChar) + ord($sKeyChar));
			$sResult .= $sChar;
		}
		return my_encode_base64($sResult);
	}
	
	function myDecrypt($sData, $sKey='hastings'){
		$sResult = '';
		$sData   = my_decode_base64($sData);
		for($i=0;$i<strlen($sData);$i++){
			$sChar    = substr($sData, $i, 1);
			$sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1);
			$sChar    = chr(ord($sChar) - ord($sKeyChar));
			$sResult .= $sChar;
		}
		return $sResult;
	}
	
	function my_encode_base64($sData){
		$sBase64 = base64_encode($sData);
		return strtr($sBase64, '+/', '-_');
	}
	
	function my_decode_base64($sData){
		$sBase64 = strtr($sData, '-_', '+/');
		return base64_decode($sBase64);
	}
	// Akhir fungsi
	
	// Mengganti karakter huruf yang merusak perintah pemrograman
	function mychangeChar1($value){
		if(!empty($value)){
			$value=str_replace("&", "__and", $value);
			$value=str_replace("'", "__ptk", $value);
			$value=str_replace("$", "__dlr", $value);
			$value=str_replace("@", "__at", $value);
			$value=str_replace("(", "__bk1", $value);
			$value=str_replace(")", "__bk2", $value);
			$value=str_replace("[", "__kt1", $value);
			$value=str_replace("]", "__kt2", $value);
			$value=str_replace("{", "__kr1", $value);
			$value=str_replace("}", "__kr2", $value);
			$value=str_replace("|", "__lrs", $value);
			$value=str_replace("\\", "__gr1", $value);
			$value=str_replace("/", "__gr2", $value);
			$value=str_replace("?", "__tny", $value);
			$value=str_replace("!", "__sru", $value);
			$value=str_replace("#", "__pgr", $value);
			$value=str_replace("%", "__prc", $value);
			$value=str_replace("*", "__kl", $value);
			$value=str_replace("=", "__sm", $value);
			$value=str_replace(":", "__ttk2", $value);
			$value=str_replace(";", "__ttkk", $value);
			$value=str_replace(";", "__pls", $value);
		}
		
		return $value;
	}
	
	function mychangeChar2($value){
		if(!empty($value)){
			$value=str_replace("__and", "&", $value);
			$value=str_replace("__ptk", "'", $value);
			$value=str_replace("__dlr", "$", $value);
			$value=str_replace("__at", "@", $value);
			$value=str_replace("__bk1", "(", $value);
			$value=str_replace("__bk2", ")", $value);
			$value=str_replace("__kt1", "[", $value);
			$value=str_replace("__kt2", "]", $value);
			$value=str_replace("__kr1", "{", $value);
			$value=str_replace("__kr2", "}", $value);
			$value=str_replace("__lrs", "|", $value);
			$value=str_replace("__gr1", "\\", $value);
			$value=str_replace("__gr2", "/", $value);
			$value=str_replace("__tny", "?", $value);
			$value=str_replace("__sru", "!", $value);
			$value=str_replace("__pgr", "#", $value);
			$value=str_replace("__prc", "%", $value);
			$value=str_replace("__kl", "*", $value);
			$value=str_replace("__sm", "=", $value);
			$value=str_replace("__ttk2", ":", $value);
			$value=str_replace("__ttkk", ";", $value);
			$value=str_replace("__pls", ";", $value);
		}
		
		return $value;
	}
	
	
?>